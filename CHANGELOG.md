<a name="1.1.1"></a>
## [1.1.1](https://github.com/mindslab-ai/brain-ta/compare/v1.1.0...v1.1.1)(2017-12-22)

### Bug Fixes

* **build**: libmaum 빌드시 MAUM_ROOT 지정, 및 libmaum 빌드시 오류 수정
* **proto**: hmd, cl-train 에서 pb2 대신에 pb2_grpc 사용하도록 변경

### Features

None

### Enhancements

None

### Breaking Changes

None


<a name="1.1.0"></a>
# [1.1.0](https://github.com/mindslab-ai/brain-ta/compare/v1.0.0...v1.1.0)(2017-12-08)

### Features

* **build.sh**: grpc 1.7.0
* **proto**: ``` wordembedding.proto ``` 수정,
* **proto**: grpc c++ cmake rule 변경에 따른 ``` CMakeLists.txt ``` 변경
* **nlp-korean-2**: M2U에 Intent Finder 구현에 따른 pos_tagged_str() 구현
* **nlp-english-2**: M2U에 Intent Finder 구현에 따른 pos_tagged_str() 구현

### Features

None

### Enhancements

None

### Breaking Changes

* **proto**: namespace를 `minds`에서 `maum`으로 변경
* **classifier-trainer**: 실행엔진과 학습엔진의 분리에 따라서 [brain-ta-train](https://github.com/mindslab-ai/brain-ta-train)으로 이동

<a name="1.0"></a>
# [1.0.0](https://github.com/mindslab-ai/brain-ta/compare/master...canary)(2017-10-30)
  <!---
  MAJOR.MINOR 패치는 # 즉, H1으로 표시
  MAJOR>MINOR.PATCH는 ## 즉, H2로 표시한다.
--->

### Bug Fixes

* **import_modules**: nlp-kor3 BUILD에 필요한 library 중 일부 파일이 제한 용량(25MB)를 초과하여 업로드 못함

### Features

* **build.sh**: build에 필요한 tool 설치 및 설정
* **build.sh**: gcc 4.8 및 gcc 5.3 버전을 동시에 빌드할 수 있는 구조
* **config**: resource path 및 port 관리, recipe 및 supervisord 설정
* **proto**: grpc 통신을 위한 proto
* **import_modules**: build에 필요한 resource는 #[AWS S3](https://aws.amazon.com/ko/s3/?nc2=h_l3_sc)에서 다운로드 받음
* **import_modules**: build에 필요한 header 및 library는  #[import-binaries](https://github.com/mindslab-ai/import-binaries)에서 다운로드 받음
* **nlp-korean-1**: ETRI에서 제공한 첫번째 한국어 nlp 분석기
* **nlp-korean-2**: 강원대 이창기 교수가 제공한 한국어 nlp 분석기
* **nlp-korean-3**: ETRI에서 제공한 두번째 한국어 nlp 분석기(Wise NLU)
* **nlp-english-2**: 강원대 이창기 교수가 제공한 영어 nlp 분석기
* **classifier-trainer**: Deep Neural Network(dnn) 학습을 위한 코드
* **classifier**: 강원대 이창기 교수가 제공한 Deep Neural Network(dnn) 엔진의 실행 코드
* **word_embedding**: 단어 단위의 유사도를 측정할 수 있는 Word Embedding 관련 코드
* **hmd**: rule 기반의 Heirachical Multiple Dictionary(hmd) 학습 및 실행 코드
* **samples**: 각 엔진에 대해 테스트를 할 수 있는 테스트 코드 내장


### Enhancements

None - Initial release


### Breaking Changes

None
