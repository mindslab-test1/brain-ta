syntax = "proto3";

import "maum/common/lang.proto";
import "maum/brain/nlp/nlp.proto";
import "google/protobuf/empty.proto";
import "google/protobuf/timestamp.proto";

package maum.brain.hmd;

// 분류기를 조회하기 위한 질의
message HmdRule {
  string rule = 1;
  repeated string categories = 2;

  // <rule>      <categories>
  // (a|b)(c|d)  [cate1. sub1]
  // (a|x)(c|y)  [cate1. sub2]
  //
}

message HmdModel {
  maum.common.LangCode lang = 1;
  string model = 2;
  repeated HmdRule rules = 3;
}

// 분류기 목록
message HmdModelList {
  repeated HmdModel models = 1;
}


enum HmdOperator {
  HMD_OP_NONE = 0;  // 기호연산 없을 경우
  HMD_OP_NEXT = 43; //'+' n번째 어절 뒤에 단어 위치 할 경우
  HMD_OP_PREV = 45; //'-' n번째 어절 앞에 단어 위치 할 경우
  HMD_OP_POST = 64; //'@' 앞단어를 기준으로 그뒤에 해당 단어가 올 경우
  HMD_OP_NOT = 33; //'!' 단어가 포함되지 않는 경우
  HMD_OP_LEMMA = 94; //'^' 영어version. 기준 문장 선택
  HMD_OP_COMMENT = 35; //'#' 주석
  HMD_OP_LIKE = 37; //'%' 단어가 substring으로 포함된 경우
}

// 하나의 HMD Matrix Item 내부의 구성 요소
message HmdMatrixElement {
  // 필수
  string elem = 1;
  // +, -, @, ^.
  HmdOperator op = 2;
  // +, -의 경우, 1-9까지,  ^의 경우, 0~2까지 허용
  int32 param = 3;
  bool use_like = 4; // %연산 포함(!%, @%, ...)
  bool use_not = 5; // !연산 포함(!%, !@, ...)
}

// 하나의 HmdMatrix ITEM
message HmdMatrixItem {
  // Rule의 catetories를 "_"로 묶어서 하나의 문자열로 생성
  // cate1_cate2_cate3
  // 규칙을 matrix화 한 하나의 ITEM
  // (a|b)(x|y)
  // a$x, a$y 으로 matrix 화된다.
  repeated HmdMatrixElement elems = 1;
  // [cate1, sub1] 카테고리 배열을 cate1_cate2 잇는다.
  // cate1_sub1
  // 해당 룰에대하여 여러 카테고리가 존재하는 경우.
  // ex) [cate1, sub1], [cate2, sub2]
  // cate1_sub1, cate2_sub2 저장
  repeated string categories = 2;

  // HMD 매트릭스화 문자열
  // a$x
  string origin_rule = 3;
}


// 매트릭스 구조 전체
message HmdMatrix {
  // 언어
  maum.common.LangCode lang = 1;
  // 모델
  string model = 2;

  // 미리 태트릭스화된 데이터 집합
  // ex::
  // (a|b)(x|y)
  // 총 4개의 매트릭스 결과물
  repeated HmdMatrixItem matrix_items = 10;
  // 매트릭스화 과정에서 도출된 단어 목록
  // not은 제외된 결과
  // [a, b, x, y],
  repeated string words = 20;

  // 추가적인 정보
  // 주석
  // 매트릭스를 만든 버전 (c++, python)
  string description = 100;
  // 만든 날짜 및 시간
  google.protobuf.Timestamp timestamp = 101;
}


message ModelKey {
  maum.common.LangCode lang = 1;
  string model = 2;
}

// 분류를 위한 입력 정보
message HmdInputDocument {
  string model = 1;
  maum.common.LangCode lang = 2;
  maum.brain.nlp.Document document = 3;
}

message HmdInputText {
  string model = 1;
  maum.common.LangCode lang = 2;
  string text = 3;
}

// 분류 아이템
message HmdClassified {
  int32 sent_seq = 1;
  string category = 2;
  string pattern = 3;
  string search_key = 4;
  string sentence = 5;
}

/// HMD 분류 결과
message HmdResult {
  repeated HmdClassified cls = 1;
}

message HmdResultDocument {
  maum.brain.nlp.Document document = 1;
  repeated HmdClassified cls = 2;
}

service HmdClassifier {
  rpc SetModel (HmdModel) returns (google.protobuf.Empty);
  rpc SetMatrix (HmdModel) returns (google.protobuf.Empty);
  rpc GetModel (ModelKey) returns (HmdModel);
  rpc GetModels (google.protobuf.Empty) returns (HmdModelList);
  rpc GetClass (HmdInputDocument) returns (HmdResult);
  rpc GetClassMultiple (stream HmdInputDocument) returns (stream HmdResultDocument);
  rpc GetClassByText (HmdInputText) returns (HmdResultDocument);
  rpc GetClassMultipleByText (stream HmdInputText) returns (stream HmdResultDocument);
}
