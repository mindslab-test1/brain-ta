# maum.ai BRAIN TA
- brain ta servers on grpc
- This repo is not packaged standalone.
- The results are melt into `MAUM Platform` and `REVO`.

## Build environment
- gcc 4.8.x
- g++ 4.8.x
- gcc 5.x
- gcc 5.x
- python 2.7
- boto3

```bash
sudo apt-get install gcc-4.8 g++-4.8 gcc-5 g++-5
sudo apt-get install python-pip python-dev
sudo pip install --upgrade pip
```

### install boto3
- `boto3` is required to copy TA resources from aws S3.

```bash
sudo pip install boto3
```

## Library dependencies
### internal libraries
- protobuf
- grpc
- spdlog
- maum-json
- maum-common
- maum-pb

SEE libmaum [README.md](https://github.com/mindslab-ai/libmaum/blob/canary/README.md)

### External libraries
#### 
#### C or C++
- libarchive
- cuda
- ETRI NLP libaries used by import_modules
```bash
sudo apt-get install libarchive13 libarchive-dev
sudo apt-get install libcurl4-openssl-dev
```

#### CUDA and NVIDIA
- https://developer.nvidia.com/cuda-downloads
- Select valid version for your OS

```bash
sudo dpkg -i cuda-repo-ubuntu1604_8.0.44-1_amd64.deb
sudo apt-get update
sudo apt-get install cuda
```
#### Python
```bash
sudo pip install requests 
sudo pip install virtualenv 
sudo pip install numpy
sudo pip install gensim==2.2.0
sudo pip install theano
sudo pip install grpcio
sudo pip install grpcio-tools #optional
```

## How to build
- git : ~/git
- build : ~/build

### Clone

```sh
mkdir ~/git
cd ~/git
git clone git@github.com:mindslab-ai/brain-ta
```

### Build for development
- install to `$HOME/maum`
```sh
export MAUM_ROOT=${HOME}/maum
cd ~/git/brain-ta
mkdir build-debug
cd build-debug
CC=/usr/bin/gcc-5
CXX=/usr/bin/g++-5
cmake \
  -DCMAKE_C_COMPILER=${CC} \
  -DCMAKE_CXX_COMPILER=${CXX} \
  -DCMAKE_INSTALL_PREFIX=${MAUM_ROOT} \
  -DCMAKE_BUILD_TYPE=Debug \
  ..
make
make install
```

### Build for deployment (do not use)
- install to `$HOME/maum`
```sh
export MAUM_ROOT=${HOME}/maum
cd ~/git/brain-ta
mkdir build-release
cd build-release
CC=/usr/bin/gcc-5
CXX=/usr/bin/g++-5
cmake \
  -DCMAKE_C_COMPILER=${CC} \
  -DCMAKE_CXX_COMPILER=${CXX} \
  -DCMAKE_INSTALL_PREFIX=${MAUM_ROOT} \
  -DCMAKE_BUILD_TYPE=Release \
  ..
make
make install
```

### How to run
#### Prepare TA resources
##### runtime directories
```sh
export MAUM_ROOT=$HOME/maum
mkdir -p ${MAUM_ROOT}/trained
mkdir -p ${MAUM_ROOT}/trained/hmd
mkdir -p ${MAUM_ROOT}/trained/classifier
mkdir -p ${MAUM_ROOT}/workspace
mkdir -p ${MAUM_ROOT}/workspace/hmd
mkdir -p ${MAUM_ROOT}/workspace/classifier
```

### Run
```sh
# edit /etc/ld.so.conf.d/my.conf
/home/YOURNAME/maum/lib
# edit .bash_profile or .bashrc
cd ${MAUM_ROOT}/bin
./nlp-kor1d

# log directory
ls ../logs
# run directory
ls ../run
```

### How to test

```sh
cd ${MAUM_ROOT}/samples
./test_hmd.py
./test_nlp.py
./test_cl.py
./test_cltrain.py
```
