#!/usr/bin/env bash

maum_root=${MAUM_ROOT}
if [ -z "${maum_root}" ]; then
  echo 'MAUM_ROOT is not defined!'
  exit 1
fi

git submodule update --init

test -d ${maum_root} || mkdir -p ${maum_root}
test -d ${maum_root} && mkdir -p ${maum_root}/samples/test/hmd

export LD_LIBRARY_PATH=${maum_root}/lib:$LD_LIBRARY_PATH

repo_root=$(pwd)
[ -n $(which nproc) ] && {
  NPROC=$(nproc)
} || {
  NPROC=$(cat /proc/cpuinfo | grep cores | wc -l)
}
echo "[brain-ta]" repo_root: ${repo_root}, NPROC: ${NPROC}, MAUM_ROOT: ${MAUM_ROOT}

OS=
if [ -f /etc/lsb-release ]; then
  OS=ubuntu
elif [ -f /etc/centos-release ]; then
  OS=centos
elif [ -f /etc/redhat-release ]; then
  OS=centos
else
  echo "Illegal OS, use ubuntu or centos"
  exit 1
fi

git submodule update --init

# not docker build && not va-extracted exists && not nlp-installed exist
function get_requirements() {
  if [ "${OS}" = "ubuntu" ]; then
    sudo apt-get install libarchive13 libarchive-dev \
      make cmake g++ g++-4.8 automake libtool
    sudo apt-get install python-pip python-dev build-essential
    sudo apt-get install libbz2-dev
  else
    sudo yum install -y epel-release
    sudo yum install -y gcc gcc-c++ libarchive-devel.x86_64 \
      python-devel.x86_64 glibc.x86_64 autoconf automake \
      libtool make bzip2-devel
    curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
    sudo python get-pip.py
    rm get-pip.py
  fi
  sudo pip install --upgrade pip
  sudo pip install --upgrade virtualenv
  sudo pip install boto3 grpcio==1.9.1 requests numpy theano gensim==2.2.0
  sudo pip install openpyxl
}

GLOB_BUILD_DIR=${HOME}/.maum-build
test -d ${GLOB_BUILD_DIR} || mkdir -p ${GLOB_BUILD_DIR}
sha1=$(git log -n 1 --pretty=format:%H ./build.sh)
echo "Last commit for build.sh: ${sha1}"

# not docker build && update commit
if [ -z "${DOCKER_MAUM_BUILD}" ] && [ ! -f ${GLOB_BUILD_DIR}/${sha1}.done ]; then
  get_requirements
  if [ "$?" = "0" ]; then
    touch ${GLOB_BUILD_DIR}/${sha1}.done
  fi
else
  echo " get_requirements had been done! "
fi

__CC=${CC}
__CXX=${CXX}
if [ "${OS}" = "centos" ]; then
  if [ -z ${__CC} ]; then
    echo CC not defined, use /usr/bin/gcc
    __CC=/usr/bin/gcc
  fi
  if [ -z ${__CXX} ]; then
    echo CXX not defined, use /usr/bin/g++
    __CXX=/usr/bin/g++
  fi
  CMAKE=/usr/bin/cmake3
else
  if [ -z ${__CC} ]; then
    echo CC not defined, use /usr/bin/gcc-4.8
    __CC=/usr/bin/gcc-4.8
  fi
  if [ -z ${__CXX} ]; then
    echo CXX not defined, use /usr/bin/g++-4.8
    __CXX=/usr/bin/g++-4.8
  fi
  CMAKE=/usr/bin/cmake
fi

GCC_VER=$(${__CC} -dumpversion)

build_base="build-debug" && [[ "${MAUM_BUILD_DEPLOY}" == "true" ]] && build_base="build-deploy-debug"
build_dir=${PWD}/${build_base}-${GCC_VER}

LIBMAUM_SHA1=$(cd ${PWD}/submodule/rocksdb > /dev/null && git log -n1 --pretty=%H -- .)
CACHE_PREFIX=${HOME}/.maum-build/cache/brain-ta-${LIBMAUM_SHA1}-${GCC_VER}
echo "[brain-ta] cache prefix is $CACHE_PREFIX"

# 다른 프로젝트의 build.sh tar .... 의 명령을 실행할 때 build.sh clean-deploy를 한 적이 있을 경우
# CMakeCache.txt가 예전 MAUM_ROOT(deploy-XXXXX)를 바라보는 문제가 있으므로 빌드 디렉토리를 새로 만든다
function build_ta() {
  if [ "$MAUM_BUILD_DEPLOY" = true ]; then
    test -d ${build_dir} && rm -rf ${build_dir}
  fi

  test -d ${build_dir} || mkdir -p ${build_dir}
  cd ${build_dir}

  ${CMAKE} \
    -DCMAKE_PREFIX_PATH=${maum_root} \
    -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_C_COMPILER=${__CC} \
    -DCMAKE_CXX_COMPILER=${__CXX} \
    -DCMAKE_INSTALL_PREFIX=${maum_root} ..

  if [ "$1" = "proto" ]; then
    (cd proto && make install -j${NPROC})
    (cd pysrc/proto && make install -j${NPROC})
  else
    if [ "${MAUM_BUILD_DEPLOY}" = "true" ]; then
      make install -j${NPROC}
    else
      make install -j${NPROC}
    fi
  fi
}

function build_rocksdb() {
  # rocksdb build

  if [ -f ${CACHE_PREFIX}//lib/librocksdb.a ] &&
    [ -f ${repo_root}/submodule/rocksdb/bin-unix-release/librocksdb.a ]; then
    echo "rocksdb is already built."
  else
    echo "rocksdb is not ready"
    echo "Build rocksdb..."

    cd ${repo_root}/submodule/rocksdb

    if [ "${OS}" = "centos" ]; then
      git clean -fdx
      git clone https://github.com/gflags/gflags.git
      cd gflags
      git checkout v2.0
      ./configure && make && sudo make install
      cd ..
      sudo yum install -y snappy snappy-devel zlib zlib-devel bzip2 bzip2-devel lz4-devel libasan
      wget https://github.com/facebook/zstd/archive/v1.1.3.tar.gz
      mv v1.1.3.tar.gz zstd-1.1.3.tar.gz
      tar zxvf zstd-1.1.3.tar.gz
      cd zstd-1.1.3
      make && sudo make install
      cd ..
      rm zstd-1.1.3.tar.gz
    elif [ "${OS}" = "ubuntu" ]; then
      sudo apt-get install libgflags-dev libsnappy-dev zlib1g-dev libbz2-dev liblz4-dev libzstd-dev
      sudo apt-get install
    fi

    mkdir bin-unix-release
    cd bin-unix-release
    ${CMAKE} .. -G "CodeBlocks - Unix Makefiles" -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS_RELEASE="-Ofast -DNDEBUG -w -fopenmp -fPIC"
    make -j${NPROC} rocksdb
    test -d ${CACHE_PREFIX}/lib || mkdir -p ${CACHE_PREFIX}/lib
    cp librocksdb.a ${CACHE_PREFIX}/lib
  fi
}

function build_libmaum() {
  {
    git clone git@github.com:mindslab-ai/libmaum.git temp-libmaum &&
    cd temp-libmaum &&
    MAUM_ROOT=${maum_root} ./build.sh &&
    rm -rf temp-libmaum
  } || {
    echo "We tried to build libmaum but failed. Please check the log." && exit 1
  }
}

function check_libmaum() {
  if [ -f ${maum_root}/lib/libmaum-pb.so ]; then
    echo "libmaum has built"
    return 0
  else
    echo "libmaum has not built, now build libmaum first."
    return 1
  fi
}

if ! check_libmaum; then
  build_libmaum
fi

build_rocksdb
build_ta
