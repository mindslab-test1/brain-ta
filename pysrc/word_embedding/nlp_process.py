#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os

reload(sys)
sys.setdefaultencoding('utf-8')

import grpc
import pprint
from google.protobuf import empty_pb2
from google.protobuf import json_format

from maum.brain.nlp import nlp_pb2
from maum.brain.nlp import nlp_pb2_grpc
from maum.brain.we import wordembedding_pb2
from maum.common import lang_pb2
from common.config import Config

class NlpClient:
    stub = None

    def __init__(self):
        self.conf = Config()
        self.remote = "localhost:" + self.conf.get('brain-ta.nlp.1.kor.port')
        self.channel = grpc.insecure_channel(self.remote)
        self.stub = nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(self.channel)

    def analyze(self, text):
        in_text = wordembedding_pb2.WordEmbeddingInputText()
        in_text.text = text

        message_result = self.stub.Analyze(in_text)

        sentence_result = []
        morp_result = []
        ner_result = []
        for i in range(len(message_result.sentences)):
            sentence = message_result.sentences[i].text
            sentence_result.append(sentence)
            morp_analysis = message_result.sentences[i].morps
            morp = ""
            for j in range(len(morp_analysis)):
                morp = morp + " " + morp_analysis[j].lemma + "/" + morp_analysis[j].type
            morp = morp.encode('utf-8').strip()
            morp_result.append(morp)

            ner_analysis = message_result.sentences[i].res
            ner = ""
            for j in range(len(ner_analysis)):
                ner = ner_analysis[j].text + "/" + ner[j].type
                if not ner:
                    continue
                else:
                    ner = ner.encode('utf-8').strip()
                    ner_result.append(ner)

        return sentence_result, morp_result, ner_result

    def word_analyze(self, text):
        in_text = wordembedding_pb2.WordEmbeddingInputText()
        in_text.text = text

        message_result = self.stub.Analyze(in_text)

        morp_result = []
        for i in range(len(message_result.sentences)):
            morp_analysis = message_result.sentences[i].morps
            morp = ""
            for j in range(len(morp_analysis)):
                morp = morp_analysis[j].lemma + "/" + morp_analysis[j].type
                morp_result.append(morp)
        
        if not morp_result:
            return morp
        else:
            morp = morp_result[0]
            return morp

    def word_analyze_list(self, text):
        in_text = wordembedding_pb2.WordEmbeddingInputText()
        in_text.text = text

        message_result = self.stub.Analyze(in_text)

        morp_result = []
        for i in range(len(message_result.sentences)):
            morp_analysis = message_result.sentences[i].morps
            morp = ""
            for j in range(len(morp_analysis)):
                morp = morp_analysis[j].lemma + "/" + morp_analysis[j].type
                morp_result.append(morp)

        return morp_result

    def noun_word_find(self, word_result):

        noun_result = []

        # nc : 자립명사, nr : 고유명사
        for result in word_result:

            words = result.split('/')
            if "nc" in words[1] or "nr" in words[1]:
                noun_result.append(result)

        return noun_result

