#! /usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import grpc
import argparse
import time

from gensim.models.keyedvectors import KeyedVectors

from concurrent import futures
from google.protobuf import empty_pb2

from common.config import Config
from maum.brain.we import wordembedding_pb2
from maum.brain.we import wordembedding_pb2_grpc

from word_embedding.nlp_process import NlpClient
from word_embedding.nlp_word_embedding import WordEmbeddingAnalysis

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class WordEmbeddingServiceServicer(wordembedding_pb2_grpc.WordEmbeddingServiceServicer):
    def TestGetWordEmbedding(self, text):
        word_embedding_document = wordembedding_pb2.WordEmbeddingDocument()
        text = text.replace('-', ' - ')
        text = text.replace('+', ' + ')
        tokens = text.split()
        word_result = []
        similarity_result = []
        # word_embedding_analysis_level에 따라 결과값을 다르게 출력
        word_embedding_analysis_level = wordembedding_pb2.WordEmbeddingAnalysisLevel.DESCRIPTOR
        """
        Word Embedding을 처리하는데 있어서 단어는 어절(띄어쓰기)단위로 입력받는 가정
        example: 서울 날씨
        '서울'이라는 단어와 '날씨'라는 단어에 대해서 분석
        """
        # 결과값이 없는 경우에는 word_embedding_analysis_level.values_by_name['WORD_EMBEDDING_UNAVAILABLE'] 라고 명시

        # print Single Word Embedding
        # analysis_level 0 : WORD_EMBEDDING_SINGLE_WORD
        # example : 서울
        # 단어 하나를 입력값으로 받아 유사한 단어와 유사도를 출력
        if len(tokens) == 1:
            analyzed_word = nlp_analyze.word_analyze(tokens[0])
            if len(analyzed_word) == 1:
                word_result = word_embedding.WordEmbeddingSingleWordAnalyze(analyzed_word)
            # 만약 '인공지능'이라는 단어가 왔을 경우에는 '인공'이라는 단어의 결과값을 보여준다
            else:
                word_result = word_embedding.WordEmbeddingSingleWordAnalyze(analyzed_word)
            if not word_result:
                word_embedding_document.analysis_level = word_embedding_analysis_level.values_by_name['WORD_EMBEDDING_UNAVAILABLE'].number
            else:
                word_embedding_document.wes.extend(word_result)
                word_embedding_document.analysis_level = word_embedding_analysis_level.values_by_name['WORD_EMBEDDING_SINGLE_WORD'].number

        # print similarity between two word
        # analysis_level 1 : WORD_EMBEDDING_TWO_WORD
        # example: (오늘, 날씨), (서울 판교)
        # 두 개의 단어를 입력 단어 단어 간의 유사도를 출력
        elif len(tokens) == 2 or "," in text:
            # 위의 example과는 달리 '인공지능분야는 마인즈랩' 로 입력받는다는 가정으로 진행
            if len(text) > 10:
                word_list = nlp_analyze.word_analyze_list(text)
                noun_result = nlp_analyze.noun_word_find(word_list)
                if not noun_result:
                    word_embedding_document.analysis_level = word_embedding_analysis_level.values_by_name['WORD_EMBEDDING_UNAVAILABLE'].number
                    return word_embedding_document
                else:
                    if len(noun_result) == 1:
                        analyzed_word = nlp_analyze.word_analyze(noun_result[0])
                        word_result = word_embedding.WordEmbeddingSingleWordAnalyze(analyzed_word)
                    else:
                        word_result = word_embedding.WordEmbeddingSentenceWordsAnalyze(noun_result)
                    if not word_result :
                        word_embedding_document.analysis_level = word_embedding_analysis_level.values_by_name['WORD_EMBEDDING_UNAVAILABLE'].number
                    else:
                        word_embedding_document.wes.extend(word_result)
            else:
                word_list = []
                for token in tokens:
                    if token == ",":
                        continue
                    analyzed_word = nlp_analyze.word_analyze(token)
                    word_list.append(analyzed_word)

                word_result = word_embedding.WordEmbeddingCompareBetweenWords(word_list)
                if not word_result :
                    word_embedding_document.analysis_level = word_embedding_analysis_level.values_by_name['WORD_EMBEDDING_UNAVAILABLE'].number
                else:
                    word_embedding_document.wes = word_result
            word_embedding_document.analysis_level = word_embedding_analysis_level.values_by_name['WORD_EMBEDDING_TWO_WORDS'].number

        # print word & similarity between multiple words using -, + operator
        # analysis_level 2 : WORD_EMBEDDING_MULTIPLE_WORD
        # example : 한국 - 서울, 서울 + 부산, 한국 - 서울 + 프랑스 등
        # word embedding 연산
        # 결과값으로는 유사한 단어와 유사도를 출력
        elif "-" in text or "+" in text or len(tokens) == 3:
            positive_word = [] ; negative_word = [] ; changed_word = []
            for i in range(0, len(tokens)):
                if i == 0:
                    if tokens[i] == "-" or tokens[i] == "+":
                        continue
                    else:
                        posi_word = nlp_analyze.word_analyze(tokens[i])
                        words = posi_word
                        changed_word.append(words)
                        positive_word.append(words)
                else:
                    if tokens[i] == "-" or tokens[i] == "+":
                        changed_word.append(tokens[i])
                        continue
                    elif tokens[i - 1] == "+":
                        posi_word = nlp_analyze.word_analyze(tokens[i])
                        words = posi_word
                        changed_word.append(words)
                        positive_word.append(words)
                    elif tokens[i - 1] == "-":
                        nega_word = nlp_analyze.word_analyze(tokens[i])
                        words = nega_word
                        changed_word.append(words)
                        negative_word.append(words)
                    else:
                        posi_word = nlp_analyze.word_analyze(tokens[i])
                        words = posi_word
                        changed_word.append(words)
                        positive_word.append(words)
            if (len(positive_word) + len(negative_word)) == 2:
                word_result = word_embedding.WordEmbeddingTwoWordsAnalyze(positive_word, negative_word)
            else:
                word_result = word_embedding.WordEmbeddingThreeWordsAnalyze(positive_word, negative_word)
            if not word_result :
                word_embedding_document.analysis_level = word_embedding_analysis_level.values_by_name['WORD_EMBEDDING_UNAVAILABLE'].number
            else:
                word_embedding_document.wes.extend(word_result)
                word_changed = ""
                for change in changed_word:
                    word_changed = word_changed + " " + change
                word_changed = word_changed.strip()
                word_embedding_document.analysis_level = word_embedding_analysis_level.values_by_name['WORD_EMBEDDING_MULTIPLE_WORDS'].number

        # print similarity between multiple words in the sentence
        # analysis_level 3 : WORD_EMBEDDING_SENTENCE_WORD
        # example: "오늘 서울 날씨 어때?"
        # 문장 내 명사를 하나의 단어로 가정하여 추출
        # 여러 개의 단어 간의 유사도를 순차적으로 출력

        elif len(tokens) > 3:
            word_list = nlp_analyze.word_analyze_list(text)
            noun_result = nlp_analyze.noun_word_find(word_list)
            if not noun_result:
                word_embedding_document.analysis_level = word_embedding_analysis_level.values_by_name['WORD_EMBEDDING_UNAVAILABLE'].number
                return word_embedding_document
            else:
                if len(noun_result) == 1:
                    analyzed_word = nlp_analyze.word_analyze(noun_result[0])
                    word_result = word_embedding.WordEmbeddingSingleWordAnalyze(analyzed_word)
                else:
                    word_result = word_embedding.WordEmbeddingSentenceWordsAnalyze(noun_result)
                if not word_result :
                    word_embedding_document.analysis_level = word_embedding_analysis_level.values_by_name['WORD_EMBEDDING_UNAVAILABLE'].number
                else:
                    word_embedding_document.wes.extend(word_result)
                    word_embedding_document.analysis_level = word_embedding_analysis_level.values_by_name['WORD_EMBEDDING_SENTENCE_WORDS'].number
        return word_embedding_document

    def GetWordEmbedding(self, request, context):
        text = request.text
        wed = self.TestGetWordEmbedding(text)
        return wed

def server():
    conf = Config()
    conf.init('brain-ta.conf')
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    wordembedding_pb2_grpc.add_WordEmbeddingServiceServicer_to_server(WordEmbeddingServiceServicer(),
                                                                      server)
    remote = '[::]:' + conf.get('brain-ta.nlp.word_embedding')
    server.add_insecure_port(remote)
    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

def testserver():
    svc = WordEmbeddingServiceServicer()

    while True:
        text = raw_input('Input : ')
        if text == "quit" or text == "QUIT" or text == "q" or text == "Q":
            print("Exit the Word Embedding")
            break
        wes = svc.TestGetWordEmbedding(text)
        print wes

if __name__ == '__main__':
    nlp_analyze = NlpClient()
    word_embedding = WordEmbeddingAnalysis()
    server()
    # testserver()
