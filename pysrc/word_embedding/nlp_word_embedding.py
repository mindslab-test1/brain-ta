#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import random
import platform
from gensim.models.keyedvectors import KeyedVectors
from common.config import Config

from maum.brain.we import wordembedding_pb2
from maum.brain.we import wordembedding_pb2_grpc

class WordEmbeddingAnalysis:
    conf = Config()
    conf.init('brain-ta.conf')
    print("Word Embedding Loading...")
    word_embedding_model_path = os.path.join(conf.get('brain-ta.resource.word_embedding'), 'minds_word_embedding_v1.txt')
    word_embedding = KeyedVectors.load_word2vec_format(word_embedding_model_path)
    print("Complete Loaded Word Embedding...")
    # single word

    def weRes(self, result):
        wer = wordembedding_pb2.WordEmbeddingResult()
        words = result[0].split('/')
        wer.word = str(words[0])
        wer.similarity = float(result[1])
        wer.morp = str(words[1])
        return wer

    def weRes2(self,s,f,m):
        wer = wordembedding_pb2.WordEmbeddingResult()
        wer.word = str(s)
        wer.similarity = float(f)
        wer.morp = str(m)
        return wer

    def WordEmbeddingSingleWordAnalyze(self, text):
        try:
            wes = []
            result_list = self.word_embedding.similar_by_word(text)
            for result in result_list:
                wes.append(self.weRes(result))
        except:
            wes = []
        return wes

    # operate three words
    def WordEmbeddingThreeWordsAnalyze(self, positive_word, negative_word):
        try:
            wes = []
            if len(positive_word) == 3:
                result_list = self.word_embedding.most_similar(
                    positive=[positive_word[0], positive_word[1], positive_word[2]])
                for result in result_list:
                    wes.append(self.weRes(result));
            elif len(positive_word) == 2:
                result_list = self.word_embedding.most_similar(positive=[positive_word[0], positive_word[1]],
                                                               negative=[negative_word[0]])
                for result in result_list:
                    wes.append(self.weRes(result));
            elif len(positive_word) == 2:
                result_list = self.word_embedding.most_similar(positive=[positive_word[0], positive_word[1]],
                                                               negative=[negative_word[0]])
                for result in result_list:
                    wes.append(self.weRes(result));
            elif len(positive_word) == 1:
                result_list = self.word_embedding.most_similar(positive=[positive_word[0]],
                                                               negative=[negative_word[0], negative_word[1]])
                for result in result_list:
                    wes.append(self.weRes(result));
            elif len(positive_word) == 0:
                result_list = self.word_embedding.most_similar(
                    negative=[negative_word[0], negative_word[1], negative_word[2]])
                for result in result_list:
                    wes.append(self.weRes(result));
        except:
            wes = []
        return wes

    # operate two word
    def WordEmbeddingTwoWordsAnalyze(self, positive_word, negative_word):
        try:
            wes = []
            if len(positive_word) == 2:
                result_list = self.word_embedding.most_similar(positive=[positive_word[0], positive_word[1]])
                for result in result_list:
                    wes.append(self.weRes(result));
            elif len(positive_word) == 1:
                result_list = self.word_embedding.most_similar(positive=[positive_word[0]], negative=[negative_word[0]])
                for result in result_list:
                    wes.append(self.weRes(result));
            elif len(positive_word) == 0:
                result_list = self.word_embedding.most_similar(negative=[negative_word[0], negative_word[1]])
                for result in result_list:
                    wes.append(self.weRes(result));
        except:
            wes = []
        return wes
        # all word in sentence

    def WordEmbeddingSentenceWordsAnalyze(self, noun_result):
        try:
            wes = []
            word_result = []
            similarity_result = []
            if len(noun_result) == 2:
                similarity = self.word_embedding.similarity(noun_result[0], noun_result[1])
                similarity_result.append(float(similarity))
                word = noun_result[0] + " " + noun_result[1]
                word_result.append(str(word))
            elif len(noun_result) >= 3:
                for i in range(0, len(noun_result)):
                    for j in range(1, len(noun_result)):
                        if noun_result[i] == noun_result[j]:
                            continue
                        word = noun_result[i] + " " + noun_result[j]
                        if word in word_result:
                            continue
                        reversed_word = noun_result[j] + " " + noun_result[i]
                        if reversed_word in word_result:
                            continue
                        word_result.append(str(word))
                        similarity = self.word_embedding.similarity(noun_result[i], noun_result[j])
                        similarity_result.append(float(similarity))
            for i in range(0,len(word_result)):
                wes.append(self.weRes(word_result[i], similarity_result[i], "Empty"))
        except:
            wes = []
        return wes

    # compare word embedding between words
    def WordEmbeddingCompareBetweenWords(self, word_list):
        try:
            word_result = ""
            for word in word_list:
                word_result = word_result + " " + word
            word_result = word_result.strip()
            similarity_result = self.word_embedding.similarity(word_list[0], word_list[1])
        except:
            word_result = ""
            similarity_result = 0.0
        wes = []
        wes.append(self.weRes(word_result, similarity_result, "Empty"))
        return wes

