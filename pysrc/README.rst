hmd server
==========

Hmd server is written with python.

Build
=====

From PyPI
~~~~~~~~~

::
  pip install grpcio
  pip install grpcio-tools


Prepare protobuf or grpc files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::
  python -m grpc.tools.protoc  --python_out=. --grpc_python_out=. \
    -I /usr/local/include -I ~/git/brain-ta/proto \
    ~/git/brain-ta/proto/maum/brain/hmd/hmd.proto
  python -m grpc.tools.protoc  --python_out=. --grpc_python_out=. \
    -I /usr/local/include -I ~/git/brain-ta/proto \
    ~/git/brain-ta/proto/maum/brain/nlp/nlp.proto
  python -m grpc.tools.protoc  --python_out=. --grpc_python_out=. \
    -I /usr/local/include -I ~/git/brain-ta/proto \
    /usr/local/include/maum/common/lang.proto
  python -m grpc.tools.protoc  --python_out=. --grpc_python_out=. \
    -I /usr/local/include -I ~/git/brain-ta/proto \
    ~/git/brain-ta/proto/maum/brain/we/wordembedding.proto


Install
=======

Use cmake.

It is located in maum-va package.
