# -*- coding:utf-8 -*-

import maum.brain.hmd.hmd_pb2 as hmd_pb2
from base_hmd_classifier import BaseHmdClassifier


class KoreanHmdClassifier(BaseHmdClassifier):
    def __init__(self):
        BaseHmdClassifier.__init__(self)

    def search_key(self, veckey, oline, vec_line, vec_space):
        pos = 0
        not_line = vec_line
        len_line = len(vec_line)
        len_vs = len(vec_space)
        for i in range(len(veckey)):
            if len(veckey[i]) == 0:
                continue
            b_pos = True
            b_sub = False
            b_neg = False
            b_pls = False
            b_ne = False # not used
            k_str = '' # not used
            key_pos = 0
            end_pos = len_line
            # Searching Special Command
            w_loc = -1
            while True:
                w_loc += 1
                if w_loc == len(veckey[i]):
                    return not_line, False
                elif veckey[i][w_loc] == '!':
                    if b_pos == False:
                        b_neg = True
                    else:
                        b_pos = False
                elif veckey[i][w_loc] == '@':
                    key_pos = pos
                elif veckey[i][w_loc] == '+':
                    b_pls = True
                    break
                elif veckey[i][w_loc] == '-':
                    w_loc += 1
                    try:
                        int_wsize = int(veckey[i][w_loc])
                        s_i = 0
                        while True:
                            if s_i == len_vs or vec_space[s_i] > pos:
                                s_i -= 1
                                break
                            s_i += 1
                        end_pos = vec_space[s_i] + 1
                        if s_i - int_wsize <= 0:
                            key_pos = 0
                        else:
                            key_pos = vec_space[s_i - int_wsize]
                    except ValueError:
                        return not_line, False
                elif veckey[i][w_loc] == '%':
                    b_sub = True
                elif veckey[i][w_loc] == '#':
                    b_ne = True
                else:
                    k_str = veckey[i][w_loc:]
                    break
            if b_pls == True:
                continue
            # Searching Lemma
            t_pos = 0
            if b_pos == True:
                if b_sub == True:
                    pos = vec_line[key_pos:end_pos].find(k_str)
                # elif b_ne == True:
                #  pos = find_ne(vec_ne, k_str)
                else:
                    pos = vec_line[key_pos:end_pos].find(' ' + k_str + ' ')
                if pos != -1:
                    pos = pos + key_pos
            else:
                t_pos = key_pos
                if b_neg == True:
                    pos = oline.find(k_str)
                elif b_sub == True:
                    pos = not_line[key_pos:end_pos].find(k_str)
                # elif b_ne == True:
                #  pos = find_ne(vec_ne, k_str)
                else:
                    pos = not_line[key_pos:end_pos].find(' ' + k_str + ' ')
            # Checking Result
            if b_pos:
                if pos > -1:
                    if b_sub == True:
                        vec_line = \
                            vec_line[:key_pos] +\
                            vec_line[key_pos:end_pos].replace(
                                    k_str, '_' * len(k_str)) +\
                            vec_line[end_pos:]
                    # elif b_ne == True:
                    #  ne_begin = vec_ne[pos][0]
                    #  ne_end = vec_ne[pos][1]
                    #  line = line[:ne_begin] + '_' * ( ne_end - ne_begin )\
                    #         + line[ne_end:]
                    #  pos = ne_begin + key_pos
                    else:
                        vec_line = vec_line[:key_pos] +\
                                   vec_line[key_pos:end_pos].replace(
                                           ' ' + k_str + ' ',
                                           ' ' + '_' * len(k_str) + ' ') +\
                                   vec_line[end_pos:]
                else:
                    return not_line, False
            else:
                if pos > -1:
                    return not_line, False
                else:
                    pos = t_pos
        return vec_line, True

    # '+' 경우 문자열 슬라이싱 문제로 인하여 따로 구현함
    def plus_key(self, veckey, line):
        """
            :param veckey: 입력된 단어 vector
            :param line: classification 하기 위한 key
            :return boolean
        """
        find = False
        for i in range(len(veckey)):
            if len(veckey[i]) == 0:
                continue
            plus_size = 0
            prev_str = ''
            if veckey[i][0] == '+':
                plus_size = int(veckey[i][1])
                if plus_size < 1 or plus_size > 9:
                    return False
                word = veckey[i][2:]
                if i >= 1:
                    prev_str = veckey[i-1]
                else:
                    prev_str = ' '
            if plus_size != 0:
                token = line.split()
                keyword = prev_str + word
                for j in range(len(token)):
                    if int(j + plus_size) >= len(token):
                        last_index = len(token) - 1
                        detecting_word = token[j] + token[last_index]
                    else:
                        detecting_word = token[j] + token[j + plus_size]
                    
                    if keyword.strip() == detecting_word.strip():
                        find = True
                if find == True:
                    find = False
                else:
                    return False
        return True

    def get_class(self, model, doc):
        hmd_dic = self.dic[model]
        result = []
        res_id = 0 # TODO, 문서 ID

        for sent in doc.sentences:
            oline = ' ' + sent.text.strip() + ' '
            line = ' '
            vec_space = [0]
            space_i = 0
            for morp in sent.morps:
                add_str = ''
                if morp.type == 'pv' or morp.type =='pa':
                    add_str = '다'
                if add_str != '':
                    line += morp.lemma + add_str.decode('utf-8') + ' '
                    space_i += len(morp.lemma) + 2
                
                line += morp.lemma + ' '
                space_i += len(morp.lemma) + 1

                vec_space.append(space_i)

            for dic_key in hmd_dic.keys():
                veckey = dic_key.split('$')
                tmp_line, b_print = self.search_key(veckey, oline, line,
                                                    vec_space)
                if b_print:
                    plus_print = self.plus_key(veckey, line)
                    if plus_print:
                        for item in hmd_dic[dic_key]:
                            cl = hmd_pb2.HmdClassified()
                            # TODO, 문서번호가 나간다. 현재는 비어 있다.
                            cl.sent_seq = sent.seq
                            cl.category = item[0]
                            cl.pattern = dic_key
                            cl.sentence = sent.text.strip()
                            result.append(cl)
        return result
