# -*- coding:utf-8 -*-

import copy
import re

import maum.brain.hmd.hmd_pb2 as hmd_pb2
from base_hmd_classifier import BaseHmdClassifier


class EnglishHmdClassifier(BaseHmdClassifier):

    def __init__(self):
        BaseHmdClassifier.__init__(self)

    def combine_vector_word(self, result, output, input, level):
        """
        영문 버전의 combine_vector_word 함수는 기본 버전에서 2줄이 추가된다.
        :param result:
        :param output:
        :param input:
        :param level:
        :return:
        """
        if level == len(input):
            result.append(output)
        elif level == 0:
            for i in range(len(input[level])):
                tmp = output + input[level][i]
                self.combine_vector_word(result, tmp, input, level + 1)
        else:
            for i in range(len(input[level])):
                if output[-1] == '@':
                    tmp = output[:-1] + '$@' + input[level][i]
                elif output[-1] == '%':
                    tmp = output[:-1] + '$%' + input[level][i]
                elif output[-2] == '+' and (output[-1] >= '0' and output[-1] <= '9'):
                    tmp = output[:-2] + '$+' + output[-1] + input[level][i]
                elif output[-2] == '-' and (output[-1] >= '0' and output[-1] <= '9'):
                    tmp = output[:-2] + '$-' + output[-1] + input[level][i]
                elif output[-2] == '^' and (output[-1] >= '0' and output[-1] <= '2'):
                    tmp = output[:-2] + '$^' + output[-1] + input[level][i]
                elif output[-1] == '#':
                    tmp = output[:-1] + '$#' + input[level][i]
                else:
                    tmp = output + '$' + input[level][i]
                self.combine_vector_word(result, tmp, input, level + 1)
        return result

    def get_space_vector(self, mstr):
        p = re.compile(' ')
        q = p.finditer(mstr)
        vec_s = []
        s = -1
        for qp in q:
            if s != -1:
                vec_s.append([s, qp.end() - 1])
            s = qp.start()
        return vec_s

    def find_word_id(self, pos, word_size, vec_space, vec_id):
        wid = word_size
        for vec_i in range(word_size):
            if vec_space[vec_id][vec_i][0] <= pos < vec_space[vec_id][vec_i][1]:
                wid = vec_i
                break
        return wid

    def search_key(self, veckey, oline, vec_line, vec_space):
        pos = 0
        # shallow copy
        not_vec_line = copy.copy(vec_line)
        not_line = vec_line[0]
        len_line = [len(vec_line[0]), len(vec_line[1]), len(vec_line[2])]
        word_size = len(vec_space[0])

        wid = 0
        search_wid = 0
        next_wid = 0
        vec_out = ''
        for i in range(len(veckey)):
            if len(veckey[i]) == 0:
                continue
            b_pos = True
            b_sub = False
            b_ne = False
            b_neg = False
            b_minus = False
            k_str = ''
            back_keypos = 0
            vec_id = 0
            keypos = 0
            endpos = len_line[vec_id]
            wid = next_wid
            wloc = -1
            while True:
                wloc += 1
                if wloc == len(veckey[i]):
                    return not_vec_line, False, vec_out
                # 기호가 '!'인 경우
                if veckey[i][wloc] == '!':
                    if b_pos == False:
                        b_neg = True
                    else:
                        b_pos = False
                # 기호가 '@'인 경우
                elif veckey[i][wloc] == '@':
                    if wid >= word_size:
                        keypos = len_line[vec_id]
                    else:
                        keypos = vec_space[vec_id][wid][0]
                # 기호가 '+'인 경우
                elif veckey[i][wloc] == '+':
                    wloc += 1
                    try:
                        int_wsize = int(veckey[i][wloc])
                        if wid >= word_size:
                            keypos = len_line[vec_id]
                        else:
                            keypos = vec_space[vec_id][wid][0]
                            if wid + int_wsize < word_size:
                                endpos = vec_space[vec_id][wid + int_wsize][0] + 1
                    except ValueError:
                        print('ERR: + next number Check Dictionary ' + veckey[i])
                        return not_vec_line, False, vec_out
                # 기호가 '-'인 경우
                elif veckey[i][wloc] == '-':
                    wloc += 1
                    try:
                        int_wsize = int(veckey[i][wloc])
                        if wid - 1 <= 0:
                            endpos = 0
                        elif wid >= word_size:
                            endpos = len_line[vec_id]
                        else:
                            endpos = vec_space[vec_id][wid - 1][0] + 1
                        if wid - int_wsize - 1 <= 0:
                            keypos = 0
                        else:
                            keypos = vec_space[vec_id][wid - int_wsize - 1][0]
                    except ValueError:
                        print('ERR: - next number Check Dictionary ' + veckey[i])
                        return not_vec_line, False, vec_out
                # 기호가 '^'인 경우
                elif veckey[i][wloc] == '^':
                    wloc += 1
                    try:
                        int_vec_id = int(veckey[i][wloc])
                        if int_vec_id >= 0 and int_vec_id <= 2:
                            vec_id = int_vec_id
                            endpos = len_line[vec_id]
                        else:
                            print('ERR: ^ next number(0-2) Check Dictionary ' + veckey[i])
                    except ValueError:
                        print('ERR: ^ next number(0-2) Check Dictionary ' + veckey[i])
                        return not_vec_line, False, vec_out
                # 기호가 '%'인 경우
                elif veckey[i][wloc] == '%':
                    b_sub = True
                # 기호가 '#'인 경우
                elif veckey[i][wloc] == '#':
                    b_ne = True
                else:
                    k_str = veckey[i][wloc:]
                    break
            t_pos = 0

            # '!' 부호를 만나지 않은 경우
            # '!'일 경우에는 b_pos는 false
            if b_pos == True:
                vec_out += k_str.strip() + ' '
                if b_sub == True:
                    pos = vec_line[vec_id][keypos:endpos].find(k_str)
                # elif b_ne == True:
                #  pos = self.fimnd_ne(vec_ne, k_str)
                else:
                    pos = vec_line[vec_id][keypos:endpos].find(
                            ' ' + k_str + ' ')
                if pos != -1:
                    search_wid = self.find_word_id(pos + keypos, word_size,
                                                   vec_space, vec_id)
                    pos = pos + keypos + len(k_str)
                    if b_sub == False:
                        pos -= 1
                    next_wid = self.find_word_id(pos, word_size,
                                                 vec_space, vec_id) + 1
            # '!' 부호를 만난 경우
            else:
                t_pos = keypos
                if b_neg == True:
                    pos = oline.find(k_str)
                elif b_sub == True:
                    pos = not_vec_line[vec_id][keypos:endpos].find(k_str)
                # elif b_ne == True:
                #  pos = find_ne( vec_ne, k_str )
                else:
                    pos = not_vec_line[vec_id][keypos:endpos].find(
                            ' ' + k_str + ' ')
            # '!' 부호를 만나지 않은 경우
            if b_pos:
                if pos > -1:
                    for vl_i in range(len(vec_line)):
                        s_start = vec_space[vl_i][search_wid][0]
                        if next_wid >= word_size:
                            s_end = vec_space[vl_i][word_size - 1][1]
                        else:
                            s_end = vec_space[vl_i][next_wid][0]
                        vec_line[vl_i] = vec_line[vl_i][:s_start + 1] +\
                                         '_' * (s_end - s_start - 1) +\
                                         vec_line[vl_i][s_end:]
                else:
                    return not_vec_line, False, vec_out
            # '!' 부호를 만난 경우
            else:
                if pos > -1:
                    return not_vec_line, False, vec_out
                else:
                    pos = t_pos
        return vec_line, True, vec_out.strip()

    # 입력된 text에 대해서 분석 및 분석결과를 보내기 위한 함수
    def get_class(self, model, doc):
        """
            :param model: 분석하기 위한 model 정보
            :param doc: 언어분석 결과가 담긴 message object
            :return result: 분석결과가 담긴 message object
        """
        res_id = 0
        hmd_dic = self.dic[model]
        result = []
        for sent in doc.sentences:
            oline = ' ' + sent.text.strip() + ' '
            vec_tmp = ['', '', '']
            for morp in sent.morps:
                vec_tmp[0] += morp.lemma + ' '
                vec_tmp[2] += morp.lemma + '/' + morp.type + ' '
            for word in sent.words:
                vec_tmp[1] += word.text + ' '

            vec_space = []
            for i in range(len(vec_tmp)):
                vec_tmp[i] = ' ' + vec_tmp[i].strip() + ' '
                vec_space.append(self.get_space_vector(vec_tmp[i]))

            tmp = vec_tmp
            for dic_key in hmd_dic.keys():
                veckey = dic_key.split('$')
                tmp, b_print, str_search_key = self.search_key(veckey, oline,
                                                               tmp, vec_space)
                if b_print:
                    for item in hmd_dic[dic_key]:
                        cl = hmd_pb2.HmdClassified()
                        cl.sent_seq = sent.seq
                        cl.category = item[0]
                        cl.pattern = dic_key
                        cl.search_key = str_search_key
                        cl.sentence = sent.text.strip()
                        result.append(cl)
        return result
