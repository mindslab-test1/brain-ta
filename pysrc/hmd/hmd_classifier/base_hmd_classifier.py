# -*- coding:utf-8 -*-


class BaseHmdClassifier:
    """
    Hmd 기본 구현

    내부적으로 모델 전체를 가지고 있다.
    모델은 grpc에 정의된 HmdModel 구조체로 정의된다.
    """
    dic = {}

    def __init__(self):
        self.dic = {}
        self.models = {}

    def split_input(self, input):
        line = []
        set = 0
        tmp = ''
        for i in range(len(input)):
            if input[i] == '(':
                set = 1
            elif input[i] == ')' and len(tmp) != 0:
                line.append(tmp)
                tmp = ''
                set = 0
            elif set == 1:
                tmp += input[i]
        return line

    # 하나의 rule이 여러가지 조합으로 이루어진 경우 분할하여 조합하기 위해
    # example: (선생)(+1님|@먹다)
    # (선생)(+1님), (선생)(@먹다)
    def combine_vector_word(self, result, output, input, level):
        """
            :param result: 분석 결과를 담기 위한 message object
            :param output: 
            :param input:
            :param level: 
            :return result: 분석 결과 message object
        """
        if level == len(input):
            result.append(output)
        elif level == 0:
            for i in range(len(input[level])):
                tmp = output + input[level][i]
                self.combine_vector_word(result, tmp, input, level + 1)
        else:
            # 각각 기호에 따라 분석하는 알고리즘이 달라지기 때문에
            # 기호에 따라 분석을 위한 input 형태를 맞춰줌
            for i in range(len(input[level])):
                if output[-1] == '@':
                    tmp = output[:-1] + '$@' + input[level][i]
                elif output[-1] == '%':
                    tmp = output[:-1] + '$%' + input[level][i]
                elif output[-2] == '+' and (output[-1] >= '0' and output[-1] <= '9'):
                    tmp = output[:-2] + '$+' + output[-1] + input[level][i]
                elif output[-2] == '-' and (output[-1] >= '0' and output[-1] <= '9'):
                    tmp = output[:-2] + '$-' + output[-1] + input[level][i]
                elif output[-1] == '#':
                    tmp = output[:-1] + '$#' + input[level][i]
                else:
                    tmp = output + '$' + input[level][i]
                self.combine_vector_word(result, tmp, input, level + 1)
        return result

    def set_model(self, hmd_model):
        result = []
        # parse hmd rules
        for hmd_rule in hmd_model.rules:
            strarr = []
            rule_line = self.split_input(hmd_rule.rule)
            for i in range(len(rule_line)):
                tmps = rule_line[i].split('|')
                strarr.append(tmps)
            tmpresult = []
            out = ''
            for cate in hmd_rule.categories:
                out += cate + '\t'
            result += self.combine_vector_word(tmpresult, out, strarr, 0)
        # hmd matrix generation
        hmd = {}
        for res in result:
            res = res.split('\t')
            cate = ''
            for i in range(len(res[:-1])):
                if i != 0:
                    cate += '_'
                cate += res[i]
            if res[-1] not in hmd:
                hmd[res[-1]] = [[cate]]
            else:
                hmd[res[-1]].append([cate])
        self.dic[hmd_model.model] = hmd
        self.models[hmd_model.model] = hmd_model

    def has_model(self, name):
        """
        모델이 존재하는지 찾는다.

        :param name: 찾고자 하는 모델 이름
        :return: 발견된 모델의 원본을 그대로 반환한다.
        """
        return name in self.models.keys()

    def get_model(self, name):
        """
        모델을 반환한다.
        :param name:
        :return:
        """
        return self.models[name]

    def get_models(self):
        """
        현재의 모든 모델들을 배열로 반환한다.
        :return: 모델의 배열
        """
        return self.models.values()
