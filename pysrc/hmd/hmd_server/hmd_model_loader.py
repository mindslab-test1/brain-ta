# -*- coding:utf-8 -*-

import os

import maum.brain.hmd.hmd_pb2 as hmd_pb2


class HmdModelLoader:
    model_dir = ''

    def __init__(self, model_dir):
        self.model_dir = model_dir

    def load_all(self):
        result = []
        for root, dirs, files in os.walk(self.model_dir):
            if root != self.model_dir:
                continue

            for f in files:
                file_ext = os.path.splitext(f)[1]
                if file_ext != '.hmdmodel':
                    continue
                try:
                    in_file = open(os.path.join(self.model_dir, f), 'rb')
                    hm = hmd_pb2.HmdModel()
                    hm.ParseFromString(in_file.read())
                    print 'loading hmd model message from file', f
                    in_file.close()
                    result.append(hm)
                except IOError:
                    print f, 'Could not open or parse.'
        print 'total stored hmd model count : ', len(result)
        return result

    def save(self, model):
        model_file = model.model + '__' + model.lang.__str__() + '.hmdmodel'
        if not os.path.exists(self.model_dir):
            os.makedirs(self.model_dir)
        f_name = os.path.join(self.model_dir, model_file)
        out_file = open(f_name, 'wb')
        out_file.write(model.SerializeToString())
        out_file.close()
