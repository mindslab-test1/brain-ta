import time

import grpc
from concurrent import futures

import maum.brain.hmd.hmd_pb2 as hmd_pb2
import maum.brain.hmd.hmd_pb2_grpc as hmd_pb2_grpc
from common.config import Config
from hmd_server import HmdClassifier

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

def serve():
    conf = Config()
    conf.init('brain-ta.conf')
    data = [
        ('grpc.max_connection_idle_ms', int(conf.get("brain-ta.hmd.front.timeout"))),
        ('grpc.max_connection_age_ms', int(conf.get("brain-ta.hmd.front.timeout"))),
	('grpc.max_send_message_length', 64 * 1024 * 1024),
	('grpc.max_receive_message_length', 64 * 1024 * 1024)
    ]

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10), options=data)
    hmd_pb2_grpc.add_HmdClassifierServicer_to_server(
            HmdClassifier(), server)

    port = conf.get('brain-ta.hmd.front.port')
    server.add_insecure_port('[::]:' + port)

    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
