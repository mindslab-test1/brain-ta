# -*- coding:utf-8 -*-

import grpc
from google.protobuf import empty_pb2 as _empty

import maum.common.lang_pb2 as lang_pb2
import maum.brain.nlp.nlp_pb2 as nlp_pb2
import maum.brain.nlp.nlp_pb2_grpc as nlp_pb2_grpc
import maum.brain.hmd.hmd_pb2 as hmd_pb2
import maum.brain.hmd.hmd_pb2_grpc as hmd_pb2_grpc
from common.config import Config
from hmd_classifier.eng_hmd_classifier import EnglishHmdClassifier
from hmd_classifier.kor_hmd_classifier import KoreanHmdClassifier
from hmd_model_loader import HmdModelLoader


class HmdClassifier(hmd_pb2_grpc.HmdClassifierServicer):
    conf = Config()

    """
    Provide methods that implement HmdClassifier
    """
    def __init__(self):
        self.cl = dict()
        model_dir = self.conf.get('brain-ta.hmd.model.dir')
        self.loader = HmdModelLoader(model_dir)
        self.cl[lang_pb2.kor] = KoreanHmdClassifier()
        self.cl[lang_pb2.eng] = EnglishHmdClassifier()
        remote_k = self.conf.get('brain-ta.hmd.nlp.kor.remote')
        remote_e = self.conf.get('brain-ta.hmd.nlp.eng.remote')
        self.chan_k = grpc.insecure_channel(remote_k)
        self.chan_e = grpc.insecure_channel(remote_e)
        self.nlp = dict()
        self.nlp[lang_pb2.kor] =\
            nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(self.chan_k)
        self.nlp[lang_pb2.eng] =\
            nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(self.chan_e)
        # 모든 모델들 미리 로딩해 놓는다.
        models = self.loader.load_all()
        for m in models:
            if m.lang in self.cl:
                cl = self.cl[m.lang]
                cl.set_model(m)

    def get_classifer(self, key):
        if key.lang in self.cl:
            classifier = self.cl[key.lang]
            if classifier.has_model(key.model):
                return classifier
        else:
            return None

    def get_nlp_stub(self, key):
        if key.lang == lang_pb2.kor:
            return nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(self.chan_k)
        elif key.lang == lang_pb2.eng:
            return nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(self.chan_e)
        else:
            return None


    # Model 정보를 지정하기 위해
    def SetModel(self, model, context):
        """
            :param model: 모델 정보
            :return Empty()
        """
        if model.lang in self.cl:
            cl = self.cl[model.lang]
            cl.set_model(model)
            self.loader.save(model)
            return _empty.Empty()
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('Language not found')
            raise grpc.RpcError("Method not found")

    # key에 따른 Model 정보를 가져오기 위해
    def GetModel(self, key, context):
        """
            :param key: 모델 정보를 가져오기 위한
            :return model 정보
        """
        classifier = self.get_classifer(key)
        if classifier:
            return classifier.get_model(key.model)

        context.set_code(grpc.StatusCode.NOT_FOUND)
        context.set_details('Model not found')
        raise grpc.RpcError("Model not found")

    # Model List 정보를 가져오기 위해
    def GetModels(self, empty, context):
        """
            :param empty: 빈 구조체
            :return model list
        """
        list = hmd_pb2.HmdModelList()
        for cl in self.cl:
            classifier = self.cl[cl]
            list.models.extend(classifier.get_models())
        return list

    # hmd 분석 결과를 가져오기 위해
    def GetClass(self, doc, context):
        """
            :param doc: text, model 등 정보를 가지고 있는 message object
            :return result: 분석결과를 저장하고 있는 message object
        """
        classifier = self.get_classifer(doc)
        if classifier:
            result = hmd_pb2.HmdResult()
            cls = classifier.get_class(doc.model, doc.document)
            result.cls.extend(cls)
            return result

        context.set_code(grpc.StatusCode.NOT_FOUND)
        context.set_details('Model not found')
        raise grpc.RpcError("Model not found")

    def GetClassMultiple(self, doc_iter, context):
        for doc in doc_iter:
            classifier = self.get_classifer(doc)
            if classifier:
                result = hmd_pb2.HmdResult()
                cls = classifier.get_class(doc.model, doc.document)
                result.cls.extend(cls)
                yield result
            else:
                # 실패한 경우에는 빈 결과물만 반환한다.
                yield hmd_pb2.HmdResult()

    # hmd 분석결과를 가져오기 위해
    def GetClassByText(self, text, context):
        """
            :param text: text, lang 등 정보를 가지고 있는 message object
            :return result 분석결과를 저장하고 있는 message object
        """
        classifier = self.get_classifer(text)
        stub = self.get_nlp_stub(text)
        if classifier and stub:
            req = nlp_pb2.InputText()
            req.text = text.text
            req.lang = text.lang
            req.split_sentence = True
            req.level = nlp_pb2.NLP_ANALYSIS_MORPHEME
            req.use_space = False
            req.use_tokenizer = False
            req.keyword_frequency_level = nlp_pb2.KEYWORD_FREQUENCY_NONE
            result = hmd_pb2.HmdResultDocument()

            doc = stub.Analyze(req)

            cls = classifier.get_class(text.model, doc)
            result.document.CopyFrom(doc)
            result.cls.extend(cls)
            return result
        context.set_code(grpc.StatusCode.NOT_FOUND)
        context.set_details('Model not found')
        raise grpc.RpcError("Model not found")


    def GetClassMultipleByText(self, text_iter, context):
        for text in text_iter:
            classifier = self.get_classifer(text)
            stub = self.get_nlp_stub(text)
            if classifier and stub:
                req = nlp_pb2.InputText
                req.text = text.text
                req.lang = text.lang
                req.split_sentence = True
                result = hmd_pb2.HmdResultDocument()
                result.document = stub.Analyize(req)
                result.cls = classifier.get_class(result.document)
                yield result
            else:
                yield hmd_pb2.HmdResultDocument()

