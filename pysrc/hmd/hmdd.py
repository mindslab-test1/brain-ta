#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys

from hmd_server import serve

if __name__ == '__main__':
    serve.serve()
