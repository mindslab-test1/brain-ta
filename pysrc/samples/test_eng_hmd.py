#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys

import grpc
from google.protobuf import json_format

from maum.brain.hmd import hmd_pb2
from maum.common import lang_pb2
from common.config import Config


class HmdClient:
    conf = Config()
    stub = None

    def __init__(self):
        remote = 'localhost:' + conf.get('brain-ta.hmd.front.port')
        print remote
        channel = grpc.insecure_channel(remote)
        self.stub = hmd_pb2.HmdClassifierStub(channel)

    def set_model(self):
        model = hmd_pb2.HmdModel()
        model.lang = lang_pb2.kor
        model.model = 'news'

        rules = []
        rule1 = hmd_pb2.HmdRule()
        # 한국어와 동일
        # 형태소 분석기 이용
        rule1.rule = '(boy)'
        rule1.categories.extend(['level1', 'level2'])
        rules.append(rule1)
        rule2 = hmd_pb2.HmdRule()
        rule2.rule = '(girl)'
        rule2.categories.extend(['level1', 'level3'])
        rules.append(rule2)
        model.rules.extend(rules)
        self.stub.SetModel(model)

        model_key = hmd_pb2.ModelKey()
        model_key.lang = lang_pb2.kor
        model_key.model = 'news'
        ret_model = self.stub.GetModel(model_key)
        print json_format.MessageToJson(ret_model)

    def get_class_bytext(self, text):
        in_doc = hmd_pb2.HmdInputText()
        in_doc.text = text
        in_doc.model = 'news'
        in_doc.lang = lang_pb2.kor

        ret = self.stub.GetClassByText(in_doc)
        json_ret = json_format.MessageToJson(ret, True)
        print json_ret

        #
        # self.stub.AnalyzeMultiple()
        # SEE grpc.io python examples


def test_hmd():
    hmd_client = HmdClient()
    hmd_client.set_model()
    hmd_client.get_class_bytext('I am a boy. You are a girl.')


if __name__ == '__main__':
    conf = Config()
    conf.init('brain-ta.conf')
    test_hmd()
