#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys
import time

reload(sys)
sys.setdefaultencoding('utf-8')

import grpc
from google.protobuf import empty_pb2
from google.protobuf import json_format

from maum.brain.nlp import nlp_pb2
from maum.brain.nlp import nlp_pb2_grpc
from maum.common import lang_pb2
from common.config import Config

import argparse, textwrap

import json
from util import JsonPrinter


class NlpClient():
    conf = Config()
    stub = None
    remote = None
    json_printer = JsonPrinter()

    def __init__(self, args):
        """
        create grp stub object
        :param args: input params
        """
        self.conf.init('brain-ta.conf')

        if args.engine == "nlp1" and args.lang == 'kor':
            self.remote = 'localhost:' + self.conf.get(
                'brain-ta.nlp.1.kor.port')
            channel = grpc.insecure_channel(self.remote)
            self.stub = nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(
                channel)
        elif args.engine == "nlp1" and args.lang == 'eng':
            print "Not exist Engine"
        elif args.engine == "nlp2" and args.lang == 'kor':
            self.remote = 'localhost:' + self.conf.get(
                'brain-ta.nlp.2.kor.port')
            channel = grpc.insecure_channel(self.remote)
            self.stub = nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(
                channel)
        elif args.engine == "nlp2" and args.lang == 'eng':
            self.remote = 'localhost:' + self.conf.get(
                'brain-ta.nlp.2.eng.port')
            channel = grpc.insecure_channel(self.remote)
            self.stub = nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(
                channel)
        elif args.engine == "nlp3" and args.lang == 'kor':
            self.remote = 'localhost:' + self.conf.get(
                'brain-ta.nlp.3.kor.port')
            channel = grpc.insecure_channel(self.remote)
            self.stub = nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(
                channel)
        elif args.engine == "nlp3" and args.lang == 'eng':
            print "Not exist Engine"
        else:
            print "Not exist Engine"

    def get_provider(self):
        ret = self.stub.GetProvider(empty_pb2.Empty())
        json_text = json_format.MessageToJson(ret, True)
        data = json.loads(json_text)
        self.json_printer.pprint(data)

    def get_named_entity(self):
        """
        nlp1 Named Entity
        """
        ret = self.stub.GetNamedEntityTagList(empty_pb2.Empty())
        json_text = json_format.MessageToJson(ret, True)
        data = json.loads(json_text)
        self.json_printer.pprint(data)

    def analyze(self, args):
        """
        analyze sentence
        :param args: exec params
        """
        in_text = nlp_pb2.InputText()
        in_text.text = args.text
        in_text.lang = lang_pb2.kor
        in_text.split_sentence = True
        in_text.use_tokenizer = False
        in_text.use_space = bool(args.space)
        in_text.level = int(args.level)
        in_text.keyword_frequency_level = int(1)

        ret = self.stub.Analyze(in_text)

        # JSON text로 만들어낸다.
        json_text = json_format.MessageToJson(ret, True, True)

        data = json.loads(json_text)
        self.json_printer.pprint(data)

        for i in range(len(ret.sentences)):
            text = ret.sentences[i].text
            print "[ORI]"
            print text
            analysis = ret.sentences[i].morps
            morp = ""
            for j in range(len(analysis)):
                morp = morp + " " + analysis[j].lemma + "/" + analysis[j].type
            morp = morp.encode('utf-8').strip()
            addstr = 'morp -> ' + morp
            print addstr
            ner = ret.sentences[i].nes
            for j in range(len(ner)):
                ne = ner[j].text + "/" + ner[j].type
                ne = ne.encode('utf-8').strip()
                addNE = 'NE -> ' + ne
                print addNE

        # call GetPosTaggedList
        self.get_pos_tagged_list(ret, args.engine, args.lang)

        # call GetNerTaggedList
        self.get_ner_tagged_list(ret, args.engine, args.lang)

        # call GetPosNerTaggedList
        self.get_pos_ner_tagged_list(ret, args.engine, args.lang)

    def get_pos_tagged_list(self, document, engine, lang):
        """
        nlp2,3 kor pos tagged list
        TODO nlp1kor, nlp2eng
        :param document: nlp document
        :param engine: engine name
        :param lang: language
        """
        if engine == 'nlp3' or (engine == 'nlp2' and lang == 'kor'):
            tagged_list = self.stub.GetPosTaggedList(document)
            print '[POS Tagged List]'
            for tagged in tagged_list.result:
                print tagged
        else:
            print engine, lang, "is not supported pos tagged list."

    def get_ner_tagged_list(self, document, engine, lang):
        """
        nlp1,2,3 kor ner tagged list
        TODO nlp1 kor, nlp2eng 구현
        :param document: nlp document
        :param engine: engine name
        :param lang: language
        """
        if engine == 'nlp3' or (engine == 'nlp2' and lang == 'kor'):
            tagged_list = self.stub.GetNerTaggedList(document)
            print '[NER Tagged List]'
            for tagged in tagged_list.result:
                print tagged
        else:
            print engine, lang, "is not supported ner tagged list."

    def get_pos_ner_tagged_list(self, document, engine, lang):
        """
        nlp3 pos + ner string return
        :param document: nlp document
        :param engine: engine name
        :param lang: language
        """
        if engine == 'nlp3' and lang == 'kor':
            tagged_list = self.stub.GetPosNerTaggedList(document)
            print '[POS NER Tagged List]'
            for tagged in tagged_list.result:
                print tagged

    def update_ner_dict(self, args):
        """
        update ner dictionary
        """
        if args.engine == 'nlp3':
            dic = {}
            lines = []

            with open(args.file, "r") as f:
                lines = f.readlines()

            for line in lines:
                key, value = line.split("=1=")
                if key not in dic.keys():
                    dic[key] = []
                dic[key].append(value)

            nerDict = nlp_pb2.NerDict()

            """
                [
                ...
                {
                category: "TM_CELL_TISSUE",
                values: "(세포/NNG@\d+) 소/XPN@\d+ 기관/NNG@\d+"
                values: "(세포/NNG@\d+) 소/XPN@\d+ 기관/NNG@\d+"
                },
                ...
                ]
                """
            for key in dic.keys():
                temp = nlp_pb2.NerDict().Dictionary()
                temp.category = key
                for value in dic[key]:
                    temp.values.extend([value])
                nerDict.text.extend([temp])

            nerDict.callback_url = ''

            self.stub.UpdateNerDict(nerDict)
            print 'NER update done'
        else:
            print args.engine, args.lang, "is not supported."

    def multi_run(self, args):
        """
        analyze text by file
        :param args: input params
        """
        lines = []
        with open(args.file, "r") as f:
            lines = f.readlines()

        for line in lines:
            start_time = time.time()
            args.text = str(line)
            self.analyze(args)
            print "[TIME]"
            print(time.time() - start_time)
            print "----------------------------"

    def run(self, args):
        """
        analyze text by command line
        :param args: input params
        """
        start_time = time.time()
        # self.analyze(args.text, int(args.level), int(1), args.engine, args.lang)
        self.analyze(args)
        print "[TIME]"
        print(time.time() - start_time)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="NLP CLIENT",
        formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("-e", "--engine",
                        nargs="?",
                        required=True,
                        help="nlp1 / nlp2 / nlp3")

    parser.add_argument("-la", "--lang",
                        nargs="?",
                        required=True,
                        help="kor / eng")

    parser.add_argument("-l", "--level",
                        nargs="?",
                        help=textwrap.dedent('''\
    0 : All
    1 : Word, Morpheme
    2 : Named Entity
    3 : Word sense disambiguation
    4 : Dependency Parser
    5 : Semantic Role Labeling
    6 : Zero Anaphora
    '''))

    parser.add_argument("-s", "--space",
                        help="Optional use space",
                        action="store_true")

    parser.add_argument("-t", "--text",
                        nargs="?",
                        help=textwrap.dedent('''\
    1)set nlp level
    2)analyze text
    '''))

    parser.add_argument("-u", "--update",
                        help="update ner dictionary and add file option",
                        action="store_true")

    parser.add_argument("-f", "--file",
                        nargs="?",
                        help=textwrap.dedent('''\
    Nlp Analysis                   
     1)set nlp level
     2)set file(relative path)
    Nlp Ner Dictionary update
     1)set file(relative path)
    '''))

    args = parser.parse_args()
    nlp_client = NlpClient(args)

    if args.update and args.file:
        nlp_client.update_ner_dict(args)
    elif args.level and args.file:
        nlp_client.multi_run(args)
    elif args.level and args.text:
        nlp_client.run(args)
    else:
        print("Not available. please check help command")
