#!/usr/bin/env python
# -*- coding:utf-8 -*-

import openpyxl
import os
import sys

import grpc
from google.protobuf import json_format

from maum.brain.hmd import hmd_pb2
from maum.brain.hmd import hmd_pb2_grpc
from maum.common import lang_pb2
from common.config import Config


class HmdClient:
    conf = Config()
    stub = None

    def __init__(self):
        # brain-hmdd(python) port
        # remote = 'localhost:' + conf.get('brain-ta.hmd.front.port')
        # brain-full-hmdd(c++) port
        remote = 'localhost:' + conf.get('brain-ta.hmd.cpp.port')

        print remote
        channel = grpc.insecure_channel(remote)
        self.stub = hmd_pb2_grpc.HmdClassifierStub(channel)

    def set_model(self):
        model = hmd_pb2.HmdModel()
        model.lang = lang_pb2.kor
        model.model = 'news'

        rules = []
        # 형태소 분석 결과를 바탕으로 원형 단어를 이용
        # ex) 안녕하세요. -> 안녕하/pa 시/ep 어/ec 요/jx ./s (ETRI 기준)
        # 안녕하, 시어, 요 를 이용 rule을 제작
        rule1 = hmd_pb2.HmdRule()
        rule1.rule = '(안녕|감사)(+1하)'
        rule1.categories.extend(['level1', 'level2'])
        rules.append(rule1)
        rule2 = hmd_pb2.HmdRule()
        rule2.rule = '(자연)(@어)(%처리)'
        rule2.categories.extend(['level1', 'level3'])
        rules.append(rule2)
        model.rules.extend(rules)
        self.stub.SetModel(model)


    def set_matrix_eng(self):
        model = hmd_pb2.HmdModel()
        model.lang = lang_pb2.eng
        model.model = 'menu'

        rules = []
        # 형태소 분석 결과를 바탕으로 원형 단어를 이용
        # ex) 안녕하세요. -> 안녕하/pa 시/ep 어/ec 요/jx ./s (ETRI 기준)
        # 안녕하, 시어, 요 를 이용 rule을 제작
        rule1 = hmd_pb2.HmdRule()
        rule1.rule = '(hi|apple)(%uic)'
        rule1.categories.extend(['drink', '1'])
        rules.append(rule1)
        rule2 = hmd_pb2.HmdRule()
        rule2.rule = '(hi)(+2bread)(-1choco)'
        rule2.categories.extend(['food', '2'])
        rules.append(rule2)

        model.rules.extend(rules)
        self.stub.SetMatrix(model)

    def set_matrix(self):
        model = hmd_pb2.HmdModel()
        model.lang = lang_pb2.kor
        model.model = 'test'

        rules = []
        # 형태소 분석 결과를 바탕으로 원형 단어를 이용
        # ex) 안녕하세요. -> 안녕하/pa 시/ep 어/ec 요/jx ./s (ETRI 기준)
        # 안녕하, 시어, 요 를 이용 rule을 제작
        rule1 = hmd_pb2.HmdRule()
        rule1.rule = '(안녕|감사)(+1하)'
        rule1.categories.extend(['level1', 'level2'])
        rules.append(rule1)

        rule2 = hmd_pb2.HmdRule()
        rule2.rule = '(자연)(@어)(%처리)'
        rule2.categories.extend(['level1', 'level3'])
        rules.append(rule2)

        model.rules.extend(rules)
        self.stub.SetMatrix(model)

        print('set Matrix done!')

    def get_class_bytext(self, text):
        in_doc = hmd_pb2.HmdInputText()
        in_doc.text = text
        in_doc.model = 'test'
        in_doc.lang = lang_pb2.kor
        ret = self.stub.GetClassByText(in_doc)
        json_ret = json_format.MessageToJson(ret, True)
        print json_ret

    def get_class_bytext_eng(self, text):
        in_doc = hmd_pb2.HmdInputText()
        in_doc.text = text
        in_doc.model = 'menu'
        in_doc.lang = lang_pb2.eng
        ret = self.stub.GetClassByText(in_doc)
        json_ret = json_format.MessageToJson(ret, True)
        print json_ret

    ##
    # ${MAUM_ROOT}/samples/test/hmd에 위치한 파일의 데이터를 읽어 SetModel 수행하는 함수.
    # 'Sheet1'의 두번째행부터 읽어, 입력한 데이터 중 가장 마지막 열을 definition으로,
    # 나머지 열은 categroy로 취급한다.
    # @filename 로드 할 "파일명.xlsx"
    #
    def load_hmd(self, filename):
        filepath = os.path.join('test', 'hmd', filename)
        try:
            hmd_doc = openpyxl.load_workbook(filepath)
            sheet = hmd_doc.get_sheet_by_name('Sheet1')
            rules = []
            model = hmd_pb2.HmdModel()
            model.lang = lang_pb2.kor
            model.model = 'sample'
            for x in range(1, sheet.max_row):
                hmd_rule = hmd_pb2.HmdRule()
                rule_list = []
                str = ''
                for y in range(sheet.max_column - 1):
                    tmp = sheet.cell(row=x + 1, column=y + 1).value
                    if (tmp == None):
                        continue
                    rule_list.append(tmp)  # 첫줄은 분류 값.
                if len(rule_list) == 0:  # 카테고리 값 없을 경우
                    continue
                if (sheet.cell(row=x + 1, column=sheet.max_column).value):
                    str = sheet.cell(row=x + 1, column=sheet.max_column).value.encode('utf8')
                else: #  사전 규칙이 없는 경우
                    continue
                hmd_rule.rule = str
                hmd_rule.categories.extend(rule_list)
                rules.append(hmd_rule)
            model.rules.extend(rules)
            print 'load ' + filepath + 'done.'
            self.stub.SetMatrix(model)
            print 'make matrix file done.'
        except IOError:
            print 'cannot found ' + filepath

    ##
    # ${MAUM_ROOT}/samples/test/hmd에 위치한 파일의 데이터를 읽어 한 문장으로 반환하는 함수.
    # @filename 로드 할 "파일명.txt"
    # @return line
    #
    def load_text(self, filename):
        filepath = os.path.join('test', 'hmd', filename)
        line = ''
        try:
            with open(filepath, 'r') as f:
                contents = f.readlines()
            line = "".join(contents)
        except IOError:
            print 'cannot open ' + filepath
        return line


def test_hmd():
    hmd_client = HmdClient()
    # hmd_client.set_matrix()
    hmd_client.set_matrix()
    # hmd_client.set_matrix_eng()
    hmd_client.get_class_bytext("안녕하세요. 자연어처리기를 사용합니다.")
    # hmd_client.get_class_bytext_eng("hi choco bread.")
    # hmd_client.get_class_bytext_eng("hi apple juice.")
    # hmd_client.load_hmd('rules.xlsx')
    # line = hmd_client.load_text('sentences.txt')
    # hmd_client.get_class_bytext(line)


if __name__ == '__main__':
    conf = Config()
    conf.init('brain-ta.conf')
    test_hmd()
