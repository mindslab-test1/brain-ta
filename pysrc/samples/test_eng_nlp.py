#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys

import grpc
from google.protobuf import empty_pb2
from google.protobuf import json_format

from maum.brain.nlp import nlp_pb2
from maum.common import lang_pb2
from common.config import Config


class NlpClient:
    conf = Config()
    stub = None

    def __init__(self):
        remote = 'localhost:' + conf.get('brain-ta.nlp.2.eng.port')
        channel = grpc.insecure_channel(remote)
        self.stub = nlp_pb2.NaturalLanguageProcessingServiceStub(channel)

    def get_provider(self):
        ret = self.stub.GetProvider(empty_pb2.Empty())
        json_ret = json_format.MessageToJson(ret, True)
        print json_ret

    def analyze(self, text,level):
        in_text = nlp_pb2.InputText()
        in_text.text = text
        in_text.lang = lang_pb2.kor
        in_text.split_sentence = True
        in_text.use_tokenizer = False
        in_text.level = level

        ret = self.stub.Analyze(in_text)

        # JSON Object 로 만들어 낸다.
        printer = json_format._Printer(True, True)
        doc = printer._MessageToJsonObject(ret)
        print doc

        # JSON text로 만들어낸다.
        json_text = json_format.MessageToJson(ret, True, True)
        print json_text

        # json_ret's format is changed into json object because json_ret is str type.
        # It is possible to have a number of features in each element.
        # This loop analyzes each sentence.
        for i in range(len(doc['sentences'])):
            text = doc['sentences'][i]['text']
            analysis = doc['sentences'][i]['morps']
            morp = ""

            # This loop finds a pair of lemma and type string.
            for j in range(len(analysis)):
                    morp = morp + " "+ analysis[j]['lemma']+"/"+analysis[j]['type']

            # And, the type string is converted into encode utf-8
            morp = morp.encode('utf-8').strip()

            # Especially, if you want print each element, refer to the command : addStr
            # print("morp -> ",morp) will be displayed as Hexdemical code
            addStr = 'morp -> '+morp
            print(addStr)

            # This loop finds NE and type
            ner = doc['sentences'][i]['nes']
            for j in range(len(ner)):
                    ne = ner[j]['text']+"/"+ner[j]['type']
                    ne = ne.encode('utf-8').strip()
                    addNE = 'NE -> '+ne
                    print(addNE)

        # self.stub.AnalyzeMultiple()
        # SEE grpc.io python examples

def is_number(select):
    try:
        int(select)
        return True, int(select)
    except ValueError:
        return False, select

def test_nlp():
    nlp_client = NlpClient()
    nlp_client.get_provider()
    
    # NLP analysis level 지정 방법
    # 1. parameter로 지정( or 변수로 할당)
    # level = 0
    # nlp_client.analyze('안녕하세요. 자연어 처리 엔진을 원격으로 호출해요.', level)

    # 2. 사용자가 입력
    while True:
        print("Usage : NLP Analysis Engine")
        print("Level 0 : All NLP Analysis with Word, Morpheme, NamedEntity")
        print("Level 1 : Word, Morpheme")
        print("Level 2 : Level 1 + Named Entity")
        print("quit : Exit the System!!")

        select = raw_input("Select NLP Analysis Level : ")

        if select == "quit":
            break

        flag, level = is_number(select)

        if flag == True:
            if level > 2 or level < 0:
                print("Select Error!!")
                print("You must type only Number range 0-2")
                continue
            else:
                nlp_client.analyze('안녕하세요. 판교에 위치한 회사입니다.',level)
        else:
            print("Select Error!!")
            print("You must type only Number!!")
            continue


if __name__ == '__main__':
    conf = Config()
    conf.init('brain-ta.conf')
    test_nlp()
