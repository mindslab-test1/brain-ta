#!/usr/bin/env python
# -*- coding:utf-8 -*-

import openpyxl
import os
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import grpc
from google.protobuf import json_format
from google.protobuf import empty_pb2
from maum.brain.hmd import hmd_pb2
from maum.brain.hmd import hmd_pb2_grpc
from maum.common import lang_pb2
from common.config import Config
import argparse, textwrap

class HmdClient:
    conf = Config()
    stub = None

    def __init__(self):
        # brain-full-hmdd(c++) port
        remote = 'localhost:' + conf.get('brain-ta.hmd.cpp.port')
        print remote
        channel = grpc.insecure_channel(remote)
        self.stub = hmd_pb2_grpc.HmdClassifierStub(channel)

    def hmdmodel_to_matrix_all(self):
        hmdmodels = self.stub.GetModels(empty_pb2.Empty())
        models = hmdmodels.models
        for model in models:
            self.stub.SetMatrix(model)
            print 'make matrix file : ' + model.model
        print 'hmdmodel_to_matrix_all done'

    def hmdmodel_to_matrix(self, args):
        key = hmd_pb2.ModelKey()
        key.model = args.model
        if args.lang == 'kor':
            key.lang = lang_pb2.kor
        elif args.lang == 'eng':
            key.lang = lang_pb2.eng
        else:
            print 'wrong lang. Stop make matrix.'
            return
        hmdmodel = self.stub.GetModel(key)
        isdone = self.stub.SetMatrix(hmdmodel)
        print 'make matrix file done.'

if __name__ == '__main__':
    conf = Config()
    conf.init('brain-ta.conf')
    parser = argparse.ArgumentParser(
        description="HMD CLIENT",
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-a", "--all",
                        help="load all hmd modle(kor, eng)",
                        action="store_true"
                        )

    parser.add_argument("-l", "--lang",
                        nargs="?",
                        help="kor / eng")

    parser.add_argument("-m", "--model",
                        nargs="?",
                        help=textwrap.dedent('''\
    hmd model name.
    ex) news
    '''))

    hmd_client = HmdClient()
    args = parser.parse_args()

    if bool(args.all):
        hmd_client.hmdmodel_to_matrix_all()
    elif args.model and args.lang:
        hmd_client.hmdmodel_to_matrix(args)
    else:
        print("Not available. please check help command")