#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys
import time

reload(sys)
sys.setdefaultencoding('utf-8')

import grpc
from google.protobuf import empty_pb2
from google.protobuf import json_format

from maum.brain.nlp import nlp3_custom_pb2
from maum.brain.nlp import nlp3_custom_pb2_grpc
from maum.brain.nlp import nlp_pb2
from maum.brain.nlp import nlp_pb2_grpc
from maum.common import lang_pb2
from common.config import Config

import argparse, textwrap

import json
from util import JsonPrinter


class NlpClient():
    conf = Config()
    stub = None
    stub_nlp3 = None
    remote = None
    json_printer = JsonPrinter()

    def __init__(self, args):
        """
        create grp stub object
        :param args: input params
        """
        self.conf.init('brain-ta.conf')
        self.remote = 'localhost:' + self.conf.get('brain-ta.nlp.3.kor.port')
        channel = grpc.insecure_channel(self.remote)
        self.stub = nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(channel)
        self.stub_nlp3 = nlp3_custom_pb2_grpc.Nlp3CustomizeServiceStub(channel)

    def get_provider(self):
        ret = self.stub.GetProvider(empty_pb2.Empty())
        json_text = json_format.MessageToJson(ret, True)
        data = json.loads(json_text)
        self.json_printer.pprint(data)

    def get_named_entity(self):
        """
        nlp1 Named Entity
        """
        ret = self.stub.GetNamedEntityTagList(empty_pb2.Empty())
        json_text = json_format.MessageToJson(ret, True)
        data = json.loads(json_text)
        self.json_printer.pprint(data)

    def analyze(self, args):
        """
        analyze sentence
        :param args: exec params
        """
        in_text = nlp_pb2.InputText()
        in_text.text = args.text
        in_text.lang = lang_pb2.kor
        in_text.split_sentence = True
        in_text.use_tokenizer = False
        in_text.use_space = bool(args.space)
        in_text.level = int(args.level)
        in_text.keyword_frequency_level = int(1)

        ret = self.stub.Analyze(in_text)

        # JSON text로 만들어낸다.
        json_text = json_format.MessageToJson(ret, True, True)

        data = json.loads(json_text)
        self.json_printer.pprint(data)

        for i in range(len(ret.sentences)):
            text = ret.sentences[i].text
            print "[ORI]"
            print text
            analysis = ret.sentences[i].morps
            morp = ""
            for j in range(len(analysis)):
                morp = morp + " " + analysis[j].lemma + "/" + analysis[j].type
            morp = morp.encode('utf-8').strip()
            addstr = 'morp -> ' + morp
            print addstr
            ner = ret.sentences[i].nes
            for j in range(len(ner)):
                ne = ner[j].text + "/" + ner[j].type
                ne = ne.encode('utf-8').strip()
                addNE = 'NE -> ' + ne
                print addNE

        # call GetPosTaggedList
        self.get_pos_tagged_list(ret)

        # call GetNerTaggedList
        self.get_ner_tagged_list(ret)

        # call GetPosNerTaggedList
        self.get_pos_ner_tagged_list(ret)


    def analyze_with_space(self, args):
        """
        analyze sentence
        :param args: exec params
        """
        in_text = nlp_pb2.InputText()
        in_text.text = args.text
        in_text.lang = lang_pb2.kor
        in_text.split_sentence = True
        in_text.use_tokenizer = False
        in_text.use_space = bool(args.space)
        in_text.level = int(args.level)
        in_text.keyword_frequency_level = int(1)

        ret = self.stub.AnalyzeWithSpace(in_text)

        # JSON text로 만들어낸다.
        json_text = json_format.MessageToJson(ret, True, True)

        data = json.loads(json_text)
        self.json_printer.pprint(data)

        for i in range(len(ret.sentences)):
            text = ret.sentences[i].text
            print "[ORI]"
            print text
            analysis = ret.sentences[i].morps
            morp = ""
            for j in range(len(analysis)):
                morp = morp + " " + analysis[j].lemma + "/" + analysis[j].type
            morp = morp.encode('utf-8').strip()
            addstr = 'morp -> ' + morp
            print addstr
            ner = ret.sentences[i].nes
            for j in range(len(ner)):
                ne = ner[j].text + "/" + ner[j].type
                ne = ne.encode('utf-8').strip()
                addNE = 'NE -> ' + ne
                print addNE

        # call GetPosTaggedList
        self.get_pos_tagged_list(ret)

        # call GetNerTaggedList
        self.get_ner_tagged_list(ret)

        # call GetPosNerTaggedList
        self.get_pos_ner_tagged_list(ret)

    def get_pos_tagged_list(self, document):
        """
        nlp2,3 kor pos tagged list
        TODO nlp1kor, nlp2eng
        :param document: nlp document
        :param engine: engine name
        :param lang: language
        """
        tagged_list = self.stub.GetPosTaggedList(document)
        print '[POS Tagged List]'
        for tagged in tagged_list.result:
            print tagged

    def get_ner_tagged_list(self, document):
        """
        nlp1,2,3 kor ner tagged list
        TODO nlp1 kor, nlp2eng 구현
        :param document: nlp document
        :param engine: engine name
        :param lang: language
        """
        tagged_list = self.stub.GetNerTaggedList(document)
        print '[NER Tagged List]'
        for tagged in tagged_list.result:
            print tagged

    def get_pos_ner_tagged_list(self, document):
        """
        nlp3 pos + ner string return
        :param document: nlp document
        :param engine: engine name
        :param lang: language
        """
        tagged_list = self.stub.GetPosNerTaggedList(document)
        print '[POS NER Tagged List]'
        for tagged in tagged_list.result:
            print tagged

    def update_morp_dict(self, args):
        """
        update morp dictionary
        """
        morpDictList = nlp3_custom_pb2.MorpDictList()
        morpDict = nlp3_custom_pb2.MorpDictList().MorpDict()

        # test type
        morpDict.type = nlp3_custom_pb2.MorpDictList().MORP_CUSTOM

        lines = []
        with open(args.file, "r") as f:
            lines = f.readlines()

        for line in lines:
            line = line.replace("\n", "").replace("\r", "")
            print(line)
            if len(line) <= 1 or line[0] == ';':
                continue
            morpEntry = nlp3_custom_pb2.MorpDictList().MorpCustomDict()
            value = line.split('\t')

            # make data
            morpEntry.word = value[0]
            morpEntry.pattern = value[1]
            morpEntry.description = value[2].replace("[", "").replace("]", "")
            # morpEntry.file_path = "/data1/maum/resources/nlp3-kor/MLT/morph_dic_custom.txt"
            # morpEntry.append = False
            morpDict.entries.custom_dict.entries.extend([morpEntry])

        # make data
        # morpEntry = nlp3_custom_pb2.MorpDictList().MorpCustomDict()
        # morpEntry.word = "몇명이서해"
        # morpEntry.pattern = "B-MM|B-NNB|B-JKS|I-JKS|B-VV:DIC"
        # morpEntry.description = "몇/MM 명/NNB 이서/JKS 하/VV+어/EF"
        # morpEntry.file_path = "/data1/maum/resources/nlp3-kor/MLT/morph_dic_custom.txt"
        #morpEntry.append = False
        #morpDict.entries.custom_dict.entries.extend([morpEntry])

        morpDictList.morp_dicts.extend([morpDict])

        # send update
        self.stub_nlp3.UpdateMorpDictList(morpDictList)
        print 'MORP update done'

    def update_morp_comp_dict(self, args):
        """
        update morp compound dictionary
        """
        morpDictList = nlp3_custom_pb2.MorpDictList()
        morpDict = nlp3_custom_pb2.MorpDictList().MorpDict()
        #morpEntry = nlp3_custom_pb2.MorpDictList().MorpCompoundNounDict()

        # test type
        morpDict.type = nlp3_custom_pb2.MorpDictList().MORP_COMPOUND_WORD

        lines = []
        with open(args.file, "r") as f:
            lines = f.readlines()

        description = ""

        for line in lines:
            morpEntry = nlp3_custom_pb2.MorpDictList().MorpCompoundNounDict()
            line = line.replace("\n", "").replace("\r","")
            if len(line) <= 1:
                continue

            if ";;" in line:
                description = line.replace(";;","")
                continue

            value = line.split('\t')

            # make data
            morpEntry.src_pos = value[0]
            morpEntry.dest_pos = value[1]
            morpEntry.type = value[2]
            morpEntry.description = description.replace(" ","")
            if description != "":
                description = ""
            morpDict.entries.compound_noun_dict.entries.extend([morpEntry])


        # make data
        # morpEntry.src_pos = "지난주/NNG"
        # morpEntry.dest_pos = "지난/NNG+주/NNG"
        # morpEntry.type = "manual"
        # morpEntry.description = "테스트"
        # morpDict.entries.compound_noun_dict.entries.extend([morpEntry])

        morpDictList.morp_dicts.extend([morpDict])  # send update
        self.stub_nlp3.UpdateMorpDictList(morpDictList)
        print 'MORP compound update done'

    def update_morp_pattern_str_dict(self, args):
        """
        update morp pattern str dictionary
        """
        morpDictList = nlp3_custom_pb2.MorpDictList()
        morpDict = nlp3_custom_pb2.MorpDictList().MorpDict()
        #morpEntry = nlp3_custom_pb2.MorpDictList().MorpPatternDict()

        # test type
        morpDict.type = nlp3_custom_pb2.MorpDictList().MORP_POST_PATTERN_STR

        lines = []
        with open(args.file, "r") as f:
            lines = f.readlines()

        description = ""

        for line in lines:
            morpEntry = nlp3_custom_pb2.MorpDictList().MorpPatternDict()
            line = line.replace("\n", "").replace("\r","")
            if len(line) <= 1:
                continue

            if ";;" in line:
                description = line.replace(";;","")
                continue

            value = line.split('\t')

            # make data
            morpEntry.index = value[0]
            morpEntry.src_pos = value[1]
            morpEntry.dest_pos = value[2]
            morpEntry.pattern = value[3]
            morpEntry.description = description.replace(" ","")
            if description != "":
                description = ""
            morpDict.entries.pattern_dict.entries.extend([morpEntry])

        # make data
        # morpEntry.index = "1"
        # morpEntry.src_pos = "그리고/MAG"
        # morpEntry.dest_pos = "그리/VV+고/EC"
        # morpEntry.pattern = "(prev)그림을$"
        #morpEntry.description = "테스트"
        #morpDict.entries.pattern_dict.entries.extend([morpEntry])

        morpDictList.morp_dicts.extend([morpDict]) # send update
        self.stub_nlp3.UpdateMorpDictList(morpDictList)
        print 'MORP pattern str update done'

    def update_ner_dict(self, args):
        """
        update NER dictionary
        """
        neDictList = nlp3_custom_pb2.NamedEntityDictList()
        neDict = nlp3_custom_pb2.NamedEntityDictList().NamedEntityDict()

        # test type
        neDict.type = nlp3_custom_pb2.NamedEntityDictList().NE_PRE_PATTERN

        lines = []
        with open(args.file, "r") as f:
            lines = f.readlines()

        description = ""

        for line in lines:
            nePatternEntry = nlp3_custom_pb2.NamedEntityDictList().NePatternDict()
            line = line.replace("\n", "").replace("\r","")
            if len(line) <= 1:
                continue

            if ";;" in line:
                description = line.replace(";;","")
                continue

            if line[0] == ";":
                continue

            value = line.split('=1=')

            # make data
            nePatternEntry.ne_tag = value[0]
            nePatternEntry.pattern = value[1]
            nePatternEntry.description = description.replace(" ","")
            if description != "":
                description = ""
            neDict.dict.pattern_dict.entries.extend([nePatternEntry])

        # make data
        # nePatternEntry.ne_tag = "OGG_ECONOMY"
        # nePatternEntry.pattern = "(노벨/(NNP|NNG)@\d+ 문학/NNG@\d+) 수상/NNG@\d+"
        # nePatternEntry.description = "테스트"
        # neDict.dict.pattern_dict.entries.extend([nePatternEntry])

        neDictList.ne_dicts.extend([neDict])

        # send update
        self.stub_nlp3.UpdateNamedEntityDictList(neDictList)
        print 'NER pattern dict update done'

    def update_ner_dict_change(self, args):
        """
        update NER change dictionary
        """
        neDictList = nlp3_custom_pb2.NamedEntityDictList()
        neDict = nlp3_custom_pb2.NamedEntityDictList().NamedEntityDict()

        # test type
        neDict.type = nlp3_custom_pb2.NamedEntityDictList().NE_POST_CHANGE

        lines = []
        with open(args.file, "r") as f:
            lines = f.readlines()

        for line in lines:
            neChangeEntry = nlp3_custom_pb2.NamedEntityDictList().NeChangeDict()
            line = line.replace("\n", "").replace("\r","")
            if len(line) <= 1:
                continue

            value = line.split('->')

            # make data
            neChangeEntry.word = value[0].split("^^")[0]
            neChangeEntry.src_ne_tag = value[0].split("^^")[1].replace("\t","")
            neChangeEntry.dest_ne_tag = value[1].split("^^")[1]
            neDict.dict.change_dict.entries.extend([neChangeEntry])

        # make data
        # neChangeEntry.word = "보아뱀"
        # neChangeEntry.src_ne_tag = "PS_NAME"
        # neChangeEntry.dest_ne_tag = "AM_REPTILIA"
        # neDict.dict.change_dict.entries.extend([neChangeEntry])

        neDictList.ne_dicts.extend([neDict])

        # send update
        self.stub_nlp3.UpdateNamedEntityDictList(neDictList)
        print 'NER change dict update done'

    def update_ner_dict_filter(self, args):
        """
        update NER filter dictionary
        """
        neDictList = nlp3_custom_pb2.NamedEntityDictList()
        neDict = nlp3_custom_pb2.NamedEntityDictList().NamedEntityDict()

        # test type
        neDict.type = nlp3_custom_pb2.NamedEntityDictList().NE_FILTER

        lines = []
        with open(args.file, "r") as f:
            lines = f.readlines()

        description = ""

        for line in lines:
            neFilterEntry = nlp3_custom_pb2.NamedEntityDictList().NeFilterDict()
            line = line.replace("\n", "").replace("\r","")
            if len(line) <= 1:
                continue

            if ";;" in line:
                description = line.replace(";;","")
                continue

            # make data
            neFilterEntry.word = line
            neFilterEntry.description = description.replace(" ","")
            if description != "":
                description = ""
            neDict.dict.filter_dict.entries.extend([neFilterEntry])

        # make data
        # neFilterEntry.word = "보아뱀"
        # neFilterEntry.description = "테스트"
        # neDict.dict.filter_dict.entries.extend([neFilterEntry])

        neDictList.ne_dicts.extend([neDict])

        # send update
        self.stub_nlp3.UpdateNamedEntityDictList(neDictList)
        print 'NER filter update done'

    def update_ner_dict_add_and_etc(self, args):
        """
        update NER add dictionary & etc dictionary
        """
        neDictList = nlp3_custom_pb2.NamedEntityDictList()
        neDict = nlp3_custom_pb2.NamedEntityDictList().NamedEntityDict()

        # test type
        #neDict.type = nlp3_custom_pb2.NamedEntityDictList().NE_ADD_POST_DIC
        neDict.type = nlp3_custom_pb2.NamedEntityDictList().NE_ADD_PRE_DIC

        lines = []
        with open(args.file, "r") as f:
            lines = f.readlines()

        description = ""

        for line in lines:
            neAddEntry = nlp3_custom_pb2.NamedEntityDictList().NeEtcDict()
            line = line.replace("\n", "").replace("\r","")
            if len(line) <= 1:
                continue

            if ";;" in line:
                description = line.replace(";;","")
                continue

            value = line.split('=')

            # make data
            neAddEntry.word = value[0]
            neAddEntry.ne_tag = value[1]
            neAddEntry.description = description.replace(" ","")
            if description != "":
                description = ""
            neDict.dict.etc_dict.entries.extend([neAddEntry])

        # make data
        # neAddEntry.word = "철인3종경기"
        # neAddEntry.ne_tag = "EV_SPORTS"
        # neAddEntry.description = "테스트"
        # neDict.dict.etc_dict.entries.extend([neAddEntry])

        neDictList.ne_dicts.extend([neDict])

        # send update
        self.stub_nlp3.UpdateNamedEntityDictList(neDictList)
        print 'NER add & etc dict update done'

    def get_ner_tag_map(self):
        """
        get NER tag map(list)
        """
        # request tag map
        neTagMap = self.stub_nlp3.GetNamedEntityTagMap(empty_pb2.Empty())
        print 'NER Tag Map'
        for tag_info in neTagMap.tags:
            print tag_info.tag
            print tag_info.code
            print tag_info.name

    def update_ner_tag_map(self):
        """
        update NER tag map(list)
        """
        neTagMap = nlp3_custom_pb2.NamedEntityTagMap()
        neTag = nlp3_custom_pb2.NamedEntityTagMap().NeTag()

        # make data
        neTag.tag = "NE_TEST"
        neTag.code = 8888
        neTag.name = "테스트"
        neTagMap.tags.extend([neTag])

        # request tag map
        neTagMap = self.stub_nlp3.UpdateNamedEntityTagMap(neTagMap)
        print 'NER Update Tag Map'

    def update_ner_config(self):
        """
        update NER config
        """
        neConfig = nlp3_custom_pb2.NamedEntityConfig()

        # make config data
        neConfig.use_pre_dictionary = True
        neConfig.use_pre_add_dictionary = True
        neConfig.use_pre_pattern = True
        neConfig.use_post_dictionary = True
        neConfig.use_post_add_dictionary = True
        neConfig.use_post_pattern = True
        neConfig.use_post_change_dictionary = True
        neConfig.use_filter = True
        neConfig.use_svm_model_all = True

        # request config
        self.stub_nlp3.UpdateNamedEntityConfig(neConfig)
        print 'NER Update Config'

    def find_rocksdb(self):
        """
        search rocksdb
        """
        target_info = nlp3_custom_pb2.FindWordRequest()

        # make data
        target_info.word = args.text

        # request search rocks DB
        ret_info = self.stub_nlp3.FindWord(target_info)
        
        print ret_info.word
        for entry in ret_info.entries:
            print entry.dict_name + " : " + entry.value

        print 'RocksDB Test'

    def get_replace_dict(self):
        """
        get replacement dictionary(list)
        """
        # request tag map
        replaceDictList = self.stub_nlp3.GetReplacementDict(empty_pb2.Empty())
        print 'Replacement dictionary'
        for entry in replaceDictList.entries:
            print entry.src_str + " -> " + entry.dest_str

    def update_replace_dict(self, args):
        """
        update replacement dictionary(list)
        """
        replaceDictList = nlp3_custom_pb2.ReplacementDictList()

        lines = []
        with open(args.file, "r") as f:
            lines = f.readlines()

        replaceDictList.auth_key = "7c3a778e29ac522de62c4e22aa90c9"

        for line in lines:
            entry = nlp3_custom_pb2.ReplacementDictList().ReplacementDict()
            line = line.replace("\n", "").replace("\r","")
            if len(line) <= 1:
                continue

            value = line.split('\t')

            if len(value) < 2:
                continue

            # make data
            entry.src_str = value[0]
            entry.dest_str = value[1]
            if len(value) >= 3:
                entry.used = value[2]

            if len(value) >= 4:
                entry.type = value[3]

            replaceDictList.entries.extend([entry])

        # request update replacement dictionary
        self.stub_nlp3.UpdateReplacementDict(replaceDictList)
        print 'Update replacement dictionary'

    def multi_run(self, args):
        """
        analyze text by file
        :param args: input params
        """
        lines = []
        with open(args.file, "r") as f:
            lines = f.readlines()

        for line in lines:
            start_time = time.time()
            args.text = str(line)
            self.analyze(args)
            print "[TIME]"
            print(time.time() - start_time)
            print "----------------------------"

    def run(self, args):
        """
        analyze text by command line
        :param args: input params
        """
        start_time = time.time()
        # self.analyze(args.text, int(args.level), int(1), args.engine, args.lang)
        self.analyze(args)
        print "[TIME]"
        print(time.time() - start_time)


    def new_run(self, args):
        start_time = time.time()
        self.analyze_with_space(args)
        print "[TIME]"
        print(time.time() - start_time)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="NLP CLIENT",
        formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("-l", "--level",
                        nargs="?",
                        help=textwrap.dedent('''\
    0 : All
    1 : Word, Morpheme
    2 : Named Entity
    3 : Word sense disambiguation
    4 : Dependency Parser
    5 : Semantic Role Labeling
    6 : Zero Anaphora
    '''))

    parser.add_argument("-s", "--space",
                        help="Optional use space",
                        action="store_true")

    parser.add_argument("-r", "--replace",
                        help="Optional use replace dict",
                        action="store_true")

    parser.add_argument("-t", "--text",
                        nargs="?",
                        help=textwrap.dedent('''\
    1)set nlp level
    2)analyze text
    '''))

    parser.add_argument("-f", "--file",
                        nargs="?",
                        help=textwrap.dedent('''\
    Nlp Analysis                   
     1)set nlp level
     2)set file(relative path)
    Nlp Ner Dictionary update
     1)set file(relative path)
    '''))

    parser.add_argument("-u", "--update",
                        nargs="?")

    args = parser.parse_args()
    nlp_client = NlpClient(args)

    if args.update != None and args.file:
        if (int(args.update) == 1):
            nlp_client.update_morp_dict(args)
        elif int(args.update) == 2:
            nlp_client.update_ner_dict(args)
        elif int(args.update) == 5:
            nlp_client.update_ner_dict_change(args)
        elif int(args.update) == 6:
            nlp_client.update_ner_dict_filter(args)
        elif int(args.update) == 7:
            nlp_client.update_ner_dict_add_and_etc(args)
        elif int(args.update) == 12:
            nlp_client.update_morp_comp_dict(args)
        elif int(args.update) == 13:
            nlp_client.update_morp_pattern_str_dict(args)
        elif int(args.update) == 20:
            nlp_client.update_replace_dict(args)

    elif args.update != None:
        if int(args.update) == 3:
            nlp_client.get_ner_tag_map()
        elif int(args.update) == 4:
            nlp_client.update_ner_tag_map()
        elif int(args.update) == 99:
            nlp_client.update_ner_config()
        elif int(args.update) == 0 and args.text:
            nlp_client.find_rocksdb()
        elif int(args.update) == 20:
            nlp_client.get_replace_dict()

    elif args.replace and args.level and args.text:
        nlp_client.new_run(args)
    elif args.level and args.file:
        nlp_client.multi_run(args)
    elif args.level and args.text:
        nlp_client.run(args)
    else:
        print("Not available. please check help command")
