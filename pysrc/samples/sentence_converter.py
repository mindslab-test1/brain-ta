# -*- coding: utf-8 -*-
from itertools import product
from itertools import permutations
from dic import korean
from dic import nlp
from dic import we

import argparse


class SentenceConverter():
    """
     유사 질의 확장
     1. 동의어 확장
     2. we 확장
     3. 어미 다변화
     4. 어순 도치 확장
    """

    origin_utter = None
    syn_dict = None
    eow_dict = None

    def __init__(self):
        """
         원문(txt) 가져오기
        """
        self.origin_utter = list()
        f = open("./dic/sentence_data.txt", "r")
        lines = f.readlines()
        for line in lines:
            if line != '\n':
                self.origin_utter.append(line.replace('\n', ''))
        f.close()

        """
         동의어사전(tsv) 가져오기
        """
        # 비밀번호	비번, 패스워드
        self.syn_dict = dict()
        f = open("./dic/syn_dict.tsv", "r")
        lines = f.readlines()
        for line in lines:
            if line != '\n':
                syn_dict_values = line.split('\t')[1].replace('\n', '')
                self.syn_dict[line.split('\t')[0]] = syn_dict_values.split(', ')
        f.close()

        """ 
         어미다변화사전(tsv) 가져오기
        # 해줘	해줘라, 해줄래, 해봐
        """
        self.eow_dict = dict()
        f = open("./dic/eow_dict.tsv", "r")
        lines = f.readlines()
        for line in lines:
            if line != '\n':
                eow_dict_values = line.split('\t')[1].replace('\n', '')
                self.eow_dict[line.split('\t')[0]] = eow_dict_values.split(', ')
        f.close()

    ### 1. 동의어 확장 ###
    def syn_dic(self):
        pro_utter = list()
        # 동의어 변환 딕셔너리 저장
        # utter_syn_dict = {"a는 b입니까": [["a", "a1"], ["b", "b1"]]...}
        utter_syn_dict = dict()
        for utter in self.origin_utter:
            for syn_dict_key in self.syn_dict.keys():
                # 동의어 발견
                if utter.find(syn_dict_key) > -1:
                    # 이미 utter_syn_dict가 추가되어 있을 경우
                    if utter_syn_dict.get(utter):
                        # 임시의 리스트에 대표어 저장
                        temp_list = [syn_dict_key]
                        # 임시의 리스트에 동의어 저장 후 dict에 저장
                        for syn_value in self.syn_dict[syn_dict_key]:
                            temp_list.append(syn_value)
                        utter_syn_dict[utter].append(temp_list)
                        utter_syn_dict[utter] = utter_syn_dict[utter]
                    else:
                        # 임시의 리스트에 대표어 저장
                        temp_list = [syn_dict_key]
                        # 임시의 리스트에 동의어 저장 후 dict에 저장
                        for syn_value in self.syn_dict[syn_dict_key]:
                            temp_list.append(syn_value)
                        utter_syn_dict[utter] = [temp_list]

        # 동의어 변환 과정
        for utter in utter_syn_dict.keys():
            # 동의어의 조합이 있을 경우
            if len(utter_syn_dict[utter]) > 1:
                # product_list에 동의어의 조합 관리
                product_list = list(product(*utter_syn_dict[utter]))
                i = 0
                while i < len(product_list):
                    temp_utter = utter
                    o = 0
                    while o < len(product_list[i]):
                        find_cnt = temp_utter.count(product_list[0][o])
                        phoneme = korean.Korean(unicode(product_list[i][o])[-1:])
                        # 동의어 변환
                        temp_utter = temp_utter.replace(product_list[0][o], product_list[i][o], find_cnt)

                        try:
                            # 종성이 있는 경우 (비문 처리)
                            if phoneme[0].phoneme_coda:
                                temp_utter = temp_utter.replace(product_list[i][o] + "는",
                                                                product_list[i][o] + "은",
                                                                find_cnt)
                                temp_utter = temp_utter.replace(product_list[i][o] + "가",
                                                                product_list[i][o] + "이",
                                                                find_cnt)
                                temp_utter = temp_utter.replace(product_list[i][o] + "를",
                                                                product_list[i][o] + "을",
                                                                find_cnt)
                                temp_utter = temp_utter.replace(product_list[i][o] + "와",
                                                                product_list[i][o] + "과",
                                                                find_cnt)
                            # 종성이 없는 경우 (비문 처리)
                            else:
                                temp_utter = temp_utter.replace(product_list[i][o] + "은",
                                                                product_list[i][o] + "는",
                                                                find_cnt)
                                temp_utter = temp_utter.replace(product_list[i][o] + "이",
                                                                product_list[i][o] + "가",
                                                                find_cnt)
                                temp_utter = temp_utter.replace(product_list[i][o] + "을",
                                                                product_list[i][o] + "를",
                                                                find_cnt)
                                temp_utter = temp_utter.replace(product_list[i][o] + "과",
                                                                product_list[i][o] + "와",
                                                                find_cnt)
                        except:
                            pass
                        o += 1
                    pro_utter.append(temp_utter)
                    i += 1
            # 동의어의 조합이 없을 경우
            else:
                for rep_value in utter_syn_dict[utter][0]:
                    find_cnt = utter.count(utter_syn_dict[utter][0][0])
                    phoneme = korean.Korean(unicode(rep_value)[-1:])

                    # 동의어 변환
                    temp_utter = utter.replace(
                        utter_syn_dict[utter][0][0], rep_value, find_cnt)

                    try:
                        # 종성이 있는 경우 (비문 처리)
                        if phoneme[0].phoneme_coda:
                            temp_utter = temp_utter.replace(
                                rep_value + "는", rep_value + "은", find_cnt)
                            temp_utter = temp_utter.replace(
                                rep_value + "가", rep_value + "이", find_cnt)
                            temp_utter = temp_utter.replace(
                                rep_value + "를", rep_value + "을", find_cnt)
                            temp_utter = temp_utter.replace(
                                rep_value + "와", rep_value + "과", find_cnt)
                        # 종성이 없는 경우 (비문 처리)
                        else:
                            temp_utter = temp_utter.replace(
                                rep_value + "은", rep_value + "는", find_cnt)
                            temp_utter = temp_utter.replace(
                                rep_value + "이", rep_value + "가", find_cnt)
                            temp_utter = temp_utter.replace(
                                rep_value + "을", rep_value + "를", find_cnt)
                            temp_utter = temp_utter.replace(
                                rep_value + "과", rep_value + "와", find_cnt)
                    except:
                        pass
                    pro_utter.append(temp_utter)

        f = open("./dic/syn_dict.txt", 'w')
        for utter in list(set(pro_utter)):
            f.write(utter + '\n')
        f.close()
        print("Update syn_dict.txt file")

    ### 어미 다변화 ###
    def eow_dic(self):
        pro_utter = list()

        for utter in self.origin_utter:
            pro_utter.append(utter)
            # print(utter)
            for eow_dict_key in self.eow_dict.keys():
                # print(utter.find(eow_dict_key))
                # print(len(utter) - len(eow_dict_key))
                # print("----------------------")
                if utter.find(eow_dict_key) == len(utter) - len(eow_dict_key) - 1:
                    for eow_value in self.eow_dict[eow_dict_key]:
                        pro_utter.append(utter.replace(eow_dict_key, eow_value))

        f = open("./dic/eow_dict.txt", 'w')
        for utter in list(set(pro_utter)):
            f.write(utter + '\n')
        f.close()
        print("Update eow_dict.txt file")

    ### 문장 도치 ###
    def permu_dic(self):
        pro_utter = list()
        nlp_client = nlp.NlpClient()

        for utter in self.origin_utter:
            split_list = utter.split(' ')
            split_permu = list(permutations(split_list))
            for permu in split_permu:
                temp_utter = str()
                for permu_value in permu:
                    temp_utter += permu_value + ' '
                pro_utter.append(temp_utter)

        f = open("./dic/permu_dict.txt", 'w')
        for utter in list(set(pro_utter)):
            f.write(utter + '\n')
        f.close()
        print("Update permu_dict.txt file")

    ### word embedding ###
    def we_dic(self):
        pro_utter = list()

        nlp_client = nlp.NlpClient()
        we_client = we.WordEmbeddingAnalysis()

        utter_we_dict = dict()

        for utter in self.origin_utter:
            morp_analyze = nlp_client.morp_analyze(utter)
            for morp in morp_analyze:
                # NLP1의 경우
                # if morp['type'] == 'nc':
                # NLP3의 경우
                if morp['type'] == 'NNG':
                    we_results = we_client.get_word_embedding_result(morp['lemma'])
                    if we_results:
                        if utter_we_dict.get(utter):
                            temp_list = [morp['lemma']]
                            for we_result in we_results:
                                temp_list.append(we_result)
                            utter_we_dict[utter].append(temp_list)
                            utter_we_dict[utter] = utter_we_dict[utter]
                        else:
                            temp_list = [morp['lemma']]
                            for we_result in we_results:
                                temp_list.append(we_result)
                            utter_we_dict[utter] = [temp_list]
                    else:
                        pro_utter.append(utter)
        for utter in utter_we_dict.keys():
            if len(utter_we_dict[utter]) > 1:
                product_list = list(product(*utter_we_dict[utter]))
                i = 0
                while i < len(product_list):
                    temp_utter = utter
                    o = 0
                    while o < len(product_list[i]):
                        find_cnt = temp_utter.count(product_list[0][o])
                        phoneme = korean.Korean(unicode(product_list[i][o])[-1:])

                        # 동의어 변환
                        temp_utter = temp_utter.replace(product_list[0][o], product_list[i][o], find_cnt)

                        try:
                            # 종성이 있는 경우 (비문 처리)
                            if phoneme[0].phoneme_coda:
                                temp_utter = temp_utter.replace(
                                    product_list[i][o] + "는",
                                    product_list[i][o] + "은", find_cnt)
                                temp_utter = temp_utter.replace(
                                    product_list[i][o] + "가",
                                    product_list[i][o] + "이", find_cnt)
                                temp_utter = temp_utter.replace(
                                    product_list[i][o] + "를",
                                    product_list[i][o] + "을", find_cnt)
                                temp_utter = temp_utter.replace(
                                    product_list[i][o] + "와",
                                    product_list[i][o] + "과", find_cnt)
                            # 종성이 없는 경우 (비문 처리)
                            else:
                                temp_utter = temp_utter.replace(
                                    product_list[i][o] + "은",
                                    product_list[i][o] + "는", find_cnt)
                                temp_utter = temp_utter.replace(
                                    product_list[i][o] + "이",
                                    product_list[i][o] + "가", find_cnt)
                                temp_utter = temp_utter.replace(
                                    product_list[i][o] + "을",
                                    product_list[i][o] + "를", find_cnt)
                                temp_utter = temp_utter.replace(
                                    product_list[i][o] + "과",
                                    product_list[i][o] + "와", find_cnt)
                        except:
                            pass
                        o += 1
                    pro_utter.append(temp_utter)
                    i += 1
            else:
                for rep_value in utter_we_dict[utter][0]:
                    find_cnt = utter.count(utter_we_dict[utter][0][0])
                    phoneme = korean.Korean(unicode(rep_value)[-1:])

                    # 동의어 변환
                    temp_utter = utter.replace(utter_we_dict[utter][0][0],
                                               rep_value,
                                               find_cnt)
                    try:
                        # 종성이 있는 경우 (비문 처리)
                        if phoneme[0].phoneme_coda:
                            temp_utter = temp_utter.replace(
                                rep_value + "는", rep_value + "은", find_cnt)
                            temp_utter = temp_utter.replace(
                                rep_value + "가", rep_value + "이", find_cnt)
                            temp_utter = temp_utter.replace(
                                rep_value + "를", rep_value + "을", find_cnt)
                            temp_utter = temp_utter.replace(
                                rep_value + "와", rep_value + "과", find_cnt)
                        # 종성이 없는 경우 (비문 처리)
                        else:
                            temp_utter = temp_utter.replace(
                                rep_value + "은", rep_value + "는", find_cnt)
                            temp_utter = temp_utter.replace(
                                rep_value + "이", rep_value + "가", find_cnt)
                            temp_utter = temp_utter.replace(
                                rep_value + "을", rep_value + "를", find_cnt)
                            temp_utter = temp_utter.replace(
                                rep_value + "과", rep_value + "와", find_cnt)
                    except:
                        pass

                    pro_utter.append(temp_utter)


        f = open("./dic/we_dict.txt", 'w')
        for utter in list(set(pro_utter)):
            f.write(utter + '\n')
        f.close()
        print("Update we_dict.txt file")


def main():
    parser = argparse.ArgumentParser(
        description="synonym tool")

    parser.add_argument('-s', '--synonym',
                        help='Synonym',
                        action='store_true')
    parser.add_argument('-w', '--we',
                        help='Word Embedding',
                        action='store_true')
    parser.add_argument('-e', '--eow',
                        help='End of a word',
                        action='store_true')

    args = parser.parse_args()

    sc = SentenceConverter()
    if args.synonym:
        sc.syn_dic()
    elif args.we:
        sc.we_dic()
    elif args.eow:
        sc.eow_dic()
    else:
        print("Not available.")


if __name__ == "__main__":
    main()
