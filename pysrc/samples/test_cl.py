#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys

import grpc
from google.protobuf import json_format
import pprint

from maum.brain.cl import classifier_pb2
from maum.brain.cl import classifier_pb2_grpc
from maum.common import lang_pb2
from maum.common import types_pb2
from common.config import Config
import time

usleep = lambda x: time.sleep(x/1000000.0)

class ClassifierClient:
    model = ''
    lang = 0
    conf = Config()
    cl_stub = None
    resolver_stub = None

    def __init__(self):
        remote = '127.0.0.1:' + conf.get('brain-ta.cl.front.port')
        print remote
        channel = grpc.insecure_channel(remote)
        self.resolver_stub = classifier_pb2_grpc.ClassifierResolverStub(channel)

    def get_server(self, name, lang):
        """Find & Connect servers"""
        # Define model
        model = classifier_pb2.Model()
        if lang == 'eng':
            model.lang = lang_pb2.eng
        else:
            model.lang = lang_pb2.kor

        model.model = name

        try:
            # Find Server
            server_status = self.resolver_stub.Find(model)
        except grpc.RpcError as e:
            print e
            print name + '-' + lang + ' model cannot found'
            return False

        # Remote Classifier service
        channel = grpc.insecure_channel(server_status.server_address)
        self.cl_stub = classifier_pb2_grpc.ClassifierStub(channel)
        print 'SERVER-STATUS', server_status

        # Get server status
        wait = 0
        ## python grpc specific error, some sleep makes all well.
        if server_status.state is classifier_pb2.SERVER_STATE_STARTING:
            print "PREV SLEEP", wait
            usleep(500000)
        while not self.get_status(model):
            usleep(1000)
            print "SLEEP", wait
            wait += 1
            if wait > 100:
                return False
            continue

        return True

    def get_status(self, model):
        """Return Classifier server status"""
        try:
            print 'CALLING PING', model
            status = self.cl_stub.Ping(model)
            print 'STATUS: ', status

            # FORCELY SET MODEL and LANG
            self.model = status.model
            self.lang = status.lang

            return status.running
        except grpc.RpcError as e:
            print e
            #print sys.exc_info()[0]
            return False


    def get_class(self, text):
        try:
            cl_text = classifier_pb2.ClassInputText()
            cl_text.text = text
            cl_text.model = self.model
            cl_text.lang = self.lang

            summary = self.cl_stub.GetClass(cl_text)
            d = json_format.MessageToDict(summary, True, True)
            print 'RESULT:', text, summary.c1_top
            #print 'START DETAIL RESULT -----'
            #print repr(d).decode('utf-8')
            #print 'END DETAIL RESULT -----'
        except grpc.RpcError as e:
            print e
            #print sys.exc_info()[0]

    def read_file_stream(self, filename, chunksize=1024*1024):
        with open(filename, "rb") as f:
            while True:
                chunk = f.read(chunksize)
                if chunk:
                    part = types_pb2.FilePart()
                    part.part = chunk
                    yield part
                else:
                    break

    def set_model(self, filename):
        metadata={(b'in.lang', b'kor'), (b'in.model', 'weather') }
        resp = self.resolver_stub.SetModel(self.read_file_stream(filename),
                                             metadata=metadata)
        d = json_format.MessageToDict(resp, True, True)
        print 'BEGIN RESULT -----'
        print repr(d).decode('unicode-escape')
        print 'END RESULT -----'

    def delete_model(self, name, lang):
        model = classifier_pb2.Model()
        if lang == 'eng':
            model.lang = lang_pb2.eng
        else:
            model.lang = lang_pb2.kor

        model.model = name
        status = self.resolver_stub.DeleteModel(model)
        d = json_format.MessageToDict(status, True, True)
        print 'BEGIN RESULT -----'
        print repr(d).decode('unicode-escape')
        print 'END RESULT -----'


def test_class(cl_client):
    if cl_client.get_server(name='meari', lang='kor'):
        cl_client.get_class('오늘 서울 날씨 어때?')
        cl_client.get_class('삼성전자 주가 어때?')
        cl_client.get_class('나 너 사랑해')
        cl_client.get_class('백두산 높이가 얼마야?')
        cl_client.get_class('오늘 날씨가 왜 이리 꿀꿀하니?')
    else:
        print 'Classifier Service unavailable'

def test_set_model(cl_client):
    cl_client.set_model('/home/gih2yun/weather-kor.tar.gz')

def test_delete_model(cl_client):
    cl_client.delete_model(name='weather', lang='kor')


if __name__ == '__main__':
    conf = Config()
    conf.init('brain-ta.conf')

    cl_client = ClassifierClient()
    for _ in range(0, 300):
        test_class(cl_client)
        usleep(50000)

    #test_set_model(cl_client)
    #test_delete_model(cl_client)
