#ifndef BRAIN_TA_NLP_SERVICE_K_ENG_IMPL_H
#define BRAIN_TA_NLP_SERVICE_K_ENG_IMPL_H

#include "maum/brain/nlp/nlp.grpc.pb.h"
#include <string>
#include <vector>
#include <grpc++/grpc++.h>
#include <memory>
#include <libmaum/brain/nlp/nlp2eng.h>

using grpc::ServerContext;
using grpc::Status;
using grpc::StatusCode;
using google::protobuf::Empty;
using maum::brain::nlp::NaturalLanguageProcessingService;
using maum::brain::nlp::NlpProvider;
using maum::brain::nlp::InputText;
using maum::brain::nlp::Document;
using maum::brain::nlp::NlpFeatures;
using maum::brain::nlp::NlpFeatureSupportList;
using maum::brain::nlp::NlpDict;
using maum::brain::nlp::NlpAnalysisLevel;
using grpc::ServerReaderWriter;

class Eng_analyzer;

/**
 * 대화엔진을 등록하고 대화를 추척할 수 있는 기반을 마련한다.
 */
class NlpEnglish2Impl: public NaturalLanguageProcessingService::Service {
 public:
  NlpEnglish2Impl();
  virtual ~NlpEnglish2Impl();
  const char *Name() {
    return "NLP English #2 Service";
  }

  Status GetProvider(ServerContext *context,
                     const Empty *empty,
                     NlpProvider * provider) override;

  Status Analyze(ServerContext *context,
                 const InputText *text,
                 Document *document) override;

  Status AnalyzeMultiple(ServerContext* context,
                         ServerReaderWriter<Document, InputText>* stream) override;
  Status HasSupport(ServerContext* context,
                    const ::maum::brain::nlp::NlpFeatures* request,
                    NlpFeatureSupportList* response) override;

 private:
  void AnalyzeOne(const InputText * text, Document *document);
  void LoadConfig();

  Nlp2Eng *nlp2Eng_;
  std::string rsc_path_;
};


#endif // BRAIN_TA_NLP_SERVICE_K_ENG_IMPL_H
