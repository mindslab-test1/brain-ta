#include <libmaum/common/config.h>
#include "libmaum/brain/hmd/eng-hmd.h"
#include "hmd-util.h"

using maum::brain::nlp::Sentence;
using maum::brain::nlp::Morpheme;
using maum::brain::nlp::Word;
using maum::brain::hmd::HmdOperator;
using maum::brain::hmd::HmdMatrixItem;
using maum::common::LangCode;

EngHmd::EngHmd()
    : BaseHmd() {
  lang_ = LangCode::eng;
}

EngHmd::~EngHmd() {

}

bool EngHmd::CheckHmdSymbolRule(int wsize) {
  if (wsize < '1' || wsize > '9') {
    return false;
  }
  return true;
}

EngHmd::KeyResult EngHmd::SetResult(const vector<string> &line, const string &out, bool is_found) {
  KeyResult k_res;
  k_res.line = line;
  k_res.key = out;
  if (is_found) {
    k_res.existence = true;
  } else {
    k_res.existence = false;
  }
  return k_res;
}

vector<vector<int>> EngHmd::GetSpaceVec(string str) {
  vector<vector<int>> str_vec;
  vector<int> space(2);

  int start = 0, len = str.length();
  int end = len - 1;
  while (true) {
    end = str.find(" ", start + 1);
    if (end == len - 1) {
      space.at(0) = start;
      space.at(1) = len - 1;
      str_vec.push_back(space);
      break;
    }
    if (start == 1)
      start = 0;
    space.at(0) = start;
    space.at(1) = end;
    str_vec.push_back(space);
    start = end;
  }
  return str_vec;
}

vector<HmdClassified> EngHmd::SearchKey(string model, Document doc) {
  HmdInputText hmd_in_text;
  vector<string> veckey;
  string origin_text, nlp_line;
  vector<string> nlp_vec(3), nlp_tmp_vec;
  vector<vector<vector<int>>> space_vec, tmp_vec;
  vector<HmdClassified> cl_res;
  map<string, vector<string>> hmd_dic = {};
  hmd_in_text.set_model(model);
  hmd_in_text.set_lang(LangCode::eng);
  Sentence sent;
  Morpheme morp;
  Word word;
  KeyResult k_struct;
  std::stringstream tmp_str1, tmp_str2;
  for (auto &sent : doc.sentences()) {
    tmp_str1.str("");
    tmp_str2.str("");
    origin_text = " " + Trim(sent.text()) + " ";
    nlp_line = " ";
    space_vec = {};
    space_vec = {};
    tmp_str1 << ' ';
    tmp_str2 << ' ';
    for (auto &m : sent.morps()) {
      morp = m;
      tmp_str1 << morp.lemma() << ' ';
      tmp_str2 << morp.lemma() << '/' << morp.type() << ' ';
    }
    nlp_vec.at(0) = tmp_str1.str();
    nlp_vec.at(2) = tmp_str2.str();
    tmp_str1.str("");
    tmp_str1 << ' ';
    for (auto &w : sent.words()) {
      word = w;
      tmp_str1 << word.text() << ' ';
    }
    nlp_vec.at(1) = tmp_str1.str();

    for (auto &n : nlp_vec) {
      space_vec.push_back(GetSpaceVec(n));
    }

    nlp_tmp_vec = nlp_vec;

    for (auto const &dic_key : ExtractKeys(hmd_dic)) {
      veckey = SplitTok(dic_key, '$');
      k_struct = SubSearchKey(veckey, origin_text, nlp_tmp_vec, space_vec);

      if (k_struct.existence) {
        for (auto const &item : hmd_dic[dic_key]) {
          HmdClassified cl;
          cl.set_sent_seq(sent.seq());
          cl.set_category(&item[0]);
          cl.set_pattern(dic_key);
          cl.set_search_key(k_struct.key);
          cl.set_sentence(origin_text);
          cl_res.push_back(cl);
        }
      }
    }
  }
  return cl_res;
}

/**
 * 분류 문장 생성하는 함수
 * @param sent nlu 결과 문장
 * @param origin_text 기존 문장에 앞,뒤 공백 추가하여 저장
 * @param nlp_vec 영어버전에 사용할 세 가지 문장을 저장할 vector
 * @param space_vec 문장내에 공백위치 저장할 vector
 */
void EngHmd::MakeSentence(const Sentence &sent, string &origin_text,
                  vector<string> &nlp_vec, vector<vector<int>> &space_vec) {
  std::stringstream tmp_str1, tmp_str2;
  int space1 = 0, space2 = 0, space3= 0;
  vector<int> vec1 = {}, vec2 = {}, vec3 = {};
  origin_text = ' ' + Trim(sent.text()) + " ";
  space_vec = {};
  tmp_str1 << ' ';
  tmp_str2 << ' ';
  for (auto &m : sent.morps()) {
    tmp_str1 << m.lemma() << ' ';
    tmp_str2 << m.lemma() << '/' << m.type() << ' ';
    space1 = tmp_str1.str().size() - 1;
    space3 = tmp_str2.str().size() - 1;
    vec1.push_back(space1);
    vec3.push_back(space3);
  }
  nlp_vec.at(0) = tmp_str1.str();
  nlp_vec.at(2) = tmp_str2.str();
  tmp_str1.str("");
  tmp_str1 << ' ';
  for (auto &w : sent.words()) {
    tmp_str1 << w.text() << ' ';
    space2 = tmp_str1.str().size() - 1;
    vec2.push_back(space2);
  }
  nlp_vec.at(1) = tmp_str1.str();
  space_vec.push_back(vec1);
  space_vec.push_back(vec2);
  space_vec.push_back(vec3);
}

int FindWordId(int pos, int word_size,
               vector<vector<vector<int>>> space_vec, int vec_id) {
  int wid = word_size;
  for (int i = 0; i < word_size; i++) {
    if (space_vec[vec_id][i][0] <= pos and pos < space_vec[vec_id][i][1]) {
      wid = i;
      break;
    }
  }
  return wid;
}

EngHmd::KeyResult EngHmd::SubSearchKey
    (vector<string> key_vec,  string origin_line, vector<string> nlp_vec,
     vector<vector<vector<int>>> space_vec) {
  KeyResult k_res;
  int pos = 0;
  vector<string> not_vec = nlp_vec;
  string not_line = nlp_vec[0];
  vector<int> len_line(3);
  int nlp_vec_size = nlp_vec.size();
  for (int i = 0; i < nlp_vec_size; i++) {
    len_line.at(i) = nlp_vec[i].length();
  }
  int word_size = space_vec[0].size();
  int wid = 0, search_wid = 0, next_wid = 0, t_pos = 0;
  int int_wsize = 0;
  string out;
  bool b_pos, b_sub, b_neg;
  string k_str;
  auto log = LOGGER();
  int vec_id, key_pos, end_pos, wloc;
  int key_vec_size = key_vec.size();
  for (int i = 0; i < key_vec_size; i++) {
    if (key_vec[i].size() == 0)
      continue;
    b_pos = true;
    b_sub = false;
    b_neg = false;
    k_str = "";
    vec_id = 0;
    key_pos = 0;
    end_pos = len_line[vec_id];
    wid = next_wid;
    wloc = -1;
    while (true) {
      wloc += 1;
      if (wloc == (int) key_vec[i].size()) {
        k_res = SetResult(not_vec, out, false);
        return k_res;
      }
      if (key_vec[i][wloc] == '!') {
        if (!b_pos) {
          b_neg = true;
        } else {
          b_pos = false;
        }
      } else if (key_vec[i][wloc] == '@') {
        if (wid >= word_size) {
          key_pos = len_line[vec_id];
        } else {
          key_pos = space_vec[vec_id][wid][0];
        }
      } else if (key_vec[i][wloc] == '+') {
        wloc += 1;
        try {
          int_wsize = key_vec[i][wloc] - '0';
          if (!CheckHmdSymbolRule(int_wsize)) {
            log->warn("{}: Wrong HMD Rule, number(1~9) after '+', cause '{}'",
                      __func__,
                      key_vec[i]);
            k_res = SetResult(not_vec, out, false);
            return k_res;
          }
          if (wid >= word_size) {
            key_pos = len_line[vec_id];
          } else {
            key_pos = space_vec[vec_id][wloc][0];
            if (wid + int_wsize < word_size) {
              end_pos = space_vec[vec_id][wid + int_wsize][0] + 1;
            }
          }
        } catch (const exception &e) {
          log->error("{}: {} ", __func__, e.what());
          k_res = SetResult(not_vec, out, false);
          return k_res;
        }
      } else if (key_vec[i][wloc] == '-') {
        wloc += 1;
        try {
          int_wsize = key_vec[i][wloc] - '0';
          if (!CheckHmdSymbolRule(int_wsize)) {
            log->warn("{}: Wrong HMD Rule, number(1~9) after '-', cause '{}'",
                      __func__,
                      key_vec[i]);
            k_res = SetResult(not_vec, out, false);
            return k_res;
          }
          if (wid - 1 <= 0) {
            end_pos = 0;
          } else if (wid >= word_size) {
            end_pos = len_line[vec_id];
          } else {
            end_pos = space_vec[vec_id][wid - 1][0] + 1;
          }
          if (wid - int_wsize - 1 <= 0) {
            key_pos = 0;
          } else {
            key_pos = space_vec[vec_id][wid - int_wsize - 1][0];
          }
        } catch (const exception &e) {
          log->error("{}: {} ", __func__, e.what());
          k_res = SetResult(not_vec, out, false);
          return k_res;
        }
      } else if (key_vec[i][wloc] == '^') {

        wloc += 1;
        try {
          int int_vec_id = key_vec[i][wloc] - '0';
          if (int_vec_id >= 0 and int_vec_id <= 2) {
            vec_id = int_vec_id;
            end_pos = len_line[vec_id];
          } else {
            log->error("Err: ^ next number(0-2) " + key_vec[i]);
            k_res = SetResult(not_vec, out, false);
            return k_res;
          }
        } catch (const exception &e) {
          log->error("{}: {} ", __func__, e.what());
          k_res = SetResult(not_vec, out, false);
          return k_res;
        }
      } else if (key_vec[i][wloc] == '%') {
        b_sub = true;
      } else {
        k_str = key_vec[i].substr(wloc);
        break;
      }
    }

    t_pos = 0;

    if (b_pos) {
      out += Trim(k_str) + " ";
      if (b_sub) {
        pos = nlp_vec[vec_id].substr(key_pos, end_pos - key_pos).find(k_str);
      } else {
        pos = nlp_vec[vec_id].substr(key_pos, end_pos - key_pos).find(
            " " + k_str + " ");
      }
      if (pos != -1) {
        search_wid = FindWordId(pos + key_pos, word_size, space_vec, vec_id);
        pos = pos + key_pos + k_str.size();
        if (!b_sub) {
          pos -= 1;
        }
        next_wid = FindWordId(pos, word_size, space_vec, vec_id) + 1;
      }
    } else {
      t_pos = key_pos;
      if (b_neg)
        pos = origin_line.find(k_str);
      else if (b_sub)
        pos = not_vec[vec_id].substr(key_pos, end_pos - key_pos).find(k_str);
      else
        pos = not_vec[vec_id].substr(key_pos, end_pos - key_pos).find(
            " " + k_str + " ");
    }

    if (b_pos) {
      if (pos > -1) {
        int s_start = 0, s_end = 0;
        string tmp_str = "";
        s_start = space_vec[vec_id][search_wid][0];
        if (next_wid >= word_size)
          s_end = space_vec[vec_id][word_size - 1][1];
        else
          s_end = space_vec[vec_id][next_wid][0];
        tmp_str = Repeat(s_end - s_start - 1);
        nlp_vec[vec_id] = nlp_vec[vec_id].substr(0, s_start + 1) + tmp_str
            + nlp_vec[vec_id].substr(s_end);

      } else {
        k_res = SetResult(not_vec, out, false);
        return k_res;
      }
    } else {
      if (pos > -1) {
        k_res = SetResult(not_vec, out, false);
        return k_res;
      } else {
        pos = t_pos;
      }
    }
  }

  k_res = SetResult(nlp_vec, out, true);
  return k_res;
}

void inline FindPos(const int &pos, const int &var, const vector<int> &space_vec, int &result) {
  int cnt = -1;
  for (const auto &v : space_vec) {
    if (pos == 0) {
      cnt = 0;
      break;
    } else if (pos >= v) {
        cnt++;
    } else {
      break;
    }
  }
  int len = space_vec.size();
  int calc = cnt + var;
  if (calc < 0 ) {
    result = 0;
  } else if (calc >= len){
    result = space_vec[len -1];
  } else {
    result = space_vec[calc];
  }
}

/**
 * HmdMatrix 기준으로 분류하는 함수.
 * @param model 모델명
 * @param doc nlu 결과
 * @param top 분류값 하나만 뽑아낼 것인지
 * @return 분류결과 vector
 */
vector<HmdClassified> EngHmd::ClassifyWithMatrix(const string &model,
                                                 const Document &doc,
                                                 const bool top) {
  HmdInputText hmd_in_text;
  vector<HmdClassified> cl_res;
  string origin_text;
  vector<string> nlp_vec(3), nlp_tmp_vec;
  vector<vector<int>> space_vec;
  int idx = 0;
  KeyResult k_struct;
  auto logger = LOGGER();

  hmd_in_text.set_model(model);
  hmd_in_text.set_lang(LangCode::eng);

  auto mat_it = matrix_map_.find(model);
  if (mat_it == matrix_map_.end()) {
    return cl_res;
  }

  auto &mat = mat_it->second;
  auto &items = mat.matrix_items();
  for (const auto &sent : doc.sentences()) {
    // 문장생성
    MakeSentence(sent, origin_text, nlp_vec, space_vec);

    // 규칙돌리기전 word단어 중 하나라도 존재하지 않으면 분류 Skip
    bool matched = false;
    auto &words = mat.words();
    for (const auto &w : words) {
      for (const auto &line : nlp_vec) {
        idx = line.find(w);
        if (idx != -1) {
          matched = true;
        }
      }
      if (matched) {
        break;
      }
    }
    if (!matched) {
      return cl_res;
    }
    for (const auto &item : items) {
      SubClassifyWithMatrix(item, nlp_vec, space_vec, k_struct);
      if (k_struct.existence) {
        HmdClassified cl;
        cl.set_sent_seq(sent.seq());
        cl.set_pattern(item.origin_rule());
        cl.set_sentence(sent.text());
        const auto &cates = item.categories();
        for (const auto &cate : cates) {
          cl.set_category(cate);
          cl_res.push_back(cl);
          logger->trace("[Classify - sentence:[{}] : /category: [{}], rule:[{}]", origin_text, cate, item.origin_rule());
          if (top)
            return cl_res;
        }
      }
    }
  }
  return cl_res;
}

/**
 * Hmd Elem 분류하는 함수.
 * @param mat 분류할 기준이될 matrix.
 * @param nlp_line 분류할 문장.
 * @param space_vec 공백 위치
 * @param k_res 결과.
 * @return
 */
void EngHmd::SubClassifyWithMatrix(const HmdMatrixItem &item,
                                   const vector<string> nlp_vec,
                                   const vector<vector<int>> &space_vec,
                                   EngHmd::KeyResult &k_res) {
  int pos = 0, find_pos = 0, param_val = 0, res_pos = 0;
  bool b_not = false;
  auto &elems = item.elems();
  string nlp_line = nlp_vec.at(0);
  vector<int> space = space_vec.at(0);
  k_res = SetResult(nlp_vec, nlp_line, false);
  string k_str;
  for (const auto &e : elems) {
    if (e.use_like()) {
      k_str = e.elem();
    } else {
      k_str = ' ' + e.elem() + ' ';
    }
    if (e.use_not()) {
      b_not = true;
    }
    switch (e.op()) {
      case HmdOperator::HMD_OP_NONE:
        find_pos = nlp_line.find(k_str);
        break;
      case HmdOperator::HMD_OP_NEXT: //+
        param_val = e.param();
        FindPos(pos, param_val, space, res_pos);
        find_pos = nlp_line.substr(pos, res_pos - pos + 1).find(k_str);
        break;
      case HmdOperator::HMD_OP_PREV: //-
        param_val = e.param() * -1;
        FindPos(pos, param_val, space, res_pos);
        find_pos = nlp_line.substr(res_pos, pos - res_pos + 1).find(k_str);
        break;
      case HmdOperator::HMD_OP_POST: //@
        find_pos = nlp_line.substr(pos).find(k_str);
        break;
      case HmdOperator::HMD_OP_NOT: //!
        b_not = true;
        find_pos = nlp_line.find(k_str);
        break;
      case HmdOperator::HMD_OP_LEMMA: //^
        param_val = e.param();
        param_val = (param_val >= 0 && param_val <= 2) ? param_val : 0;
        nlp_line = nlp_line[param_val];
        space = space_vec[param_val];
        find_pos = nlp_line.find(k_str);
        break;
      default:break;
    }
    if (b_not) {
      if (find_pos == -1) {
        b_not = false;
        find_pos = pos;
      } else return;
    }
    if (find_pos == -1) {
      return;
    }
    pos = find_pos;
  }
  k_res = SetResult(nlp_vec, nlp_line, true);
}