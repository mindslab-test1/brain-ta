#include <exception>

#include <libmaum/common/config.h>
#include "libmaum/brain/hmd/kor-hmd.h"
#include "hmd-util.h"

using maum::brain::nlp::Sentence;
using maum::brain::nlp::Morpheme;
using maum::brain::hmd::HmdOperator;
using maum::common::LangCode;

KorHmd::KorHmd()
    : BaseHmd() {
  lang_ = LangCode::kor;
}

KorHmd::~KorHmd() {

}

bool KorHmd::CheckHmdSymbolRule(int wsize) {
  if (wsize < '1' || wsize > '9') {
    return false;
  }
  return true;
}

KorHmd::KeyResult KorHmd::SetResult(const string &line, bool is_found) {
  KeyResult k_res;
  k_res.line = line;
  if (is_found) {
    k_res.existence = true;
  } else {
    k_res.existence = false;
  }
  return k_res;
}

KorHmd::KeyResult KorHmd::SubSearchKey(vector<string> veckey,
                                       string origin_line,
                                       string nlp_line,
                                       vector<int> space_vec) {
  KeyResult k_res;
  int pos = 0;
  string not_line = nlp_line;
  int len_line = nlp_line.size();
  int len_space = space_vec.size();
  bool b_pos, b_sub, b_neg, b_pls;
  int w_loc = -1, key_pos = 0, end_pos = 0, s_i = 0, int_wsize = 0;
  string k_str;
  auto log = LOGGER();
  int vec_size = veckey.size();
  try {
    for (int i = 0; i < vec_size; i++) {
      if (veckey[i].length() == 0) {
        continue;
      }
      b_pos = true;
      b_sub = false;
      b_neg = false;
      b_pls = false;
      k_str = "";
      key_pos = 0;
      end_pos = len_line;
      w_loc = -1;

      while (true) {
        w_loc += 1;
        if (w_loc == (int) veckey[i].size()) {
          k_res = SetResult(not_line, false);
          return k_res;
        } else if (veckey[i][w_loc] == '!') {
          if (b_pos == false)
            b_neg = true;
          else b_pos = false;
        } else if (veckey[i][w_loc] == '@') {
          key_pos = pos;
        } else if (veckey[i][w_loc] == '+') {
          b_pls = true;
          break;
        } else if (veckey[i][w_loc] == '-') {
          int_wsize = 0;
          w_loc += 1;
          try {
            int_wsize = veckey[i][w_loc] - '0';
            if (CheckHmdSymbolRule(int_wsize)) {
              log->warn("{}: Wrong HMD Rule, number(1~9) after '-', cause '{}'",
                        __func__, veckey[i]);
              k_res = SetResult(not_line, false);
              return k_res;
            }
            s_i = 0;
            while (true) {
              if (s_i == len_space or space_vec[s_i] > pos) {
                (s_i == 0) ? s_i = 0 : s_i -= 1;
                break;
              }
              s_i += 1;
            }
            end_pos = space_vec[s_i] + 1;
            if (s_i - int_wsize <= 0) {
              key_pos = 0;
            } else
              key_pos = space_vec[s_i - int_wsize];

          } catch (const exception &e) {
            log->error("{}: {} ", __func__, e.what());
            k_res = SetResult(not_line, false);
            return k_res;
          }
        } else if (veckey[i][w_loc] == '%') {
          b_sub = true;
        } else {
          k_str = veckey[i].substr(w_loc);
          break;
        }
      }

      if (b_pls) {
        continue;
      }
      //  원형 검색
      int t_pos = 0;
      if (b_pos) {
        if (b_sub) {
          pos = nlp_line.substr(key_pos, end_pos - key_pos).find(k_str);
        } else {
          pos = nlp_line.substr(key_pos, end_pos - key_pos).find(' ' + k_str + ' ');
        }
        if (pos != -1) {
          pos = pos + key_pos;
        }
      } else {
        t_pos = key_pos;
        if (b_neg) {
          pos = origin_line.find(k_str);
        } else if (b_sub) {
          pos = not_line.substr(key_pos, end_pos - key_pos).find(k_str);
        } else {
          pos = not_line.substr(key_pos, end_pos - key_pos).find(' ' + k_str + ' ');
        }
      }
      //  결과확인
      if (b_pos) {
        int idx = 0;
        if (pos > -1) {
          string change_str;
          if (b_sub) {
            change_str = Repeat(k_str.length());
            idx = nlp_line.find(k_str, key_pos);
            nlp_line = nlp_line.replace(idx, k_str.length(), change_str);
          } else {
            change_str = ' ' + Repeat(k_str.length()) + ' ';
            k_str = (' ' + k_str + ' ');
            idx = nlp_line.find(k_str, key_pos);
            nlp_line = nlp_line.replace(idx, k_str.length(), change_str);
          }
        } else {
          k_res = SetResult(not_line, false);
          return k_res;
        }
      } else {
        if (pos > -1) {
          k_res = SetResult(not_line, false);
          return k_res;
        } else {
          pos = t_pos;
        }
      }
    }
  } catch (const exception &e) {
    log->error("{}: {}", __func__, e.what());
    k_res = SetResult(not_line, false);
    return k_res;
  }
  k_res = SetResult(nlp_line, true);
  return k_res;
}

/**
 * 분류 문장 생성하는 함수
 * @param sent nlu 결과 문장
 * @param origin_text 기존 문장에 앞,뒤 공백 추가하여 저장
 * @param nlp_vec 영어버전에 사용할 세 가지 문장을 저장할 vector
 * @param space_vec 문장내에 공백위치 저장할 vector
 */
void KorHmd::MakeSentence(const Sentence &sent,
                          string &origin_text,
                          string &result_nlp,
                          vector<int> &space_vec) {
  auto logger = LOGGER();
  origin_text = " " + Trim(sent.text()) + " ";
  ostringstream nlp_line;
  space_vec = {};
  int space_i = 0;
  nlp_line << ' ';
  Morpheme morp;
  string add_str, tmp_str;
  for (const auto m : sent.morps()) {
    morp = m;
    add_str.clear();
    if (morp.type() == "VV" or morp.type() == "VA") {
      add_str = "다";
    }
    if (!add_str.empty()) {
      nlp_line << morp.lemma() << add_str << ' ';
    }
    nlp_line << morp.lemma() << ' ';
    space_i = nlp_line.str().size() - 1;
    space_vec.push_back(space_i);
  }
  result_nlp = nlp_line.str();
  logger->trace("Make Sentence: {}", result_nlp);
}

vector<HmdClassified> KorHmd::SearchKey
    (const string &model, const Document &doc) {
  //  원형분리
  HmdInputText hmd_in_text;
  vector<HmdClassified> cl_res;
  string origin_text, nlp_line;
  vector<int> space_vec;
  map<string, vector<string>> hmd_dic = {};
  hmd_in_text.set_model(model);
  hmd_in_text.set_lang(LangCode::kor);
  for (const auto &sent : doc.sentences()) {
    vector<string> key_vec;
    KeyResult k_struct;
    // 문장생성
    MakeSentence(sent, origin_text, nlp_line, space_vec);
    hmd_dic.insert(dic_.find(hmd_in_text.model())->second.begin(),
                   dic_.find(hmd_in_text.model())->second.end());

    for (auto const &dic_key : ExtractKeys(hmd_dic)) {
      key_vec = SplitTok(dic_key, '$');
      k_struct = SubSearchKey(key_vec, origin_text, nlp_line, space_vec);

      if (k_struct.existence) {
        bool plus_res = PlusKey(key_vec,  nlp_line);
        if (plus_res) {
          for (auto const &item : hmd_dic[dic_key]) {
            HmdClassified cl;
            cl.set_sent_seq(sent.seq());
            cl.set_category(&item[0]);
            cl.set_pattern(dic_key);
            cl.set_sentence(origin_text);
            cl_res.push_back(cl);
          }
        }
      }
    }
  }
  return cl_res;
}

/**
 * hmdmodel 하나를 기준으로, sentence를 탐지시 적합한 카테고리를 찾으면 바로 결과반환.
 * @param model hmdmodel
 * @param doc nlu처리결과
 * @return cl HmdClassified 결과
 */
HmdClassified KorHmd::SearchFirstKey(const string &model, const Document &doc) {
  //  원형분리
  HmdInputText hmd_in_text;
  HmdClassified cl;
  string origin_text, nlp_line;
  vector<int> space_vec;
  map<string, vector<string>> hmd_dic = {};
  hmd_in_text.set_model(model);
  hmd_in_text.set_lang(LangCode::kor);
  for (const auto &sent : doc.sentences()) {
    vector<string> key_vec;
    KeyResult k_struct;
    // 문장생성
    MakeSentence(sent, origin_text, nlp_line, space_vec);
    hmd_dic.insert(dic_.find(hmd_in_text.model())->second.begin(),
                   dic_.find(hmd_in_text.model())->second.end());

    for (auto const &dic_key : ExtractKeys(hmd_dic)) {
      key_vec = SplitTok(dic_key, '$');
      k_struct = SubSearchKey(key_vec, origin_text, nlp_line, space_vec);

      if (k_struct.existence) {
        bool plus_res = PlusKey(key_vec,  nlp_line);
        if (plus_res) {
          for (auto const &item : hmd_dic[dic_key]) {
            cl.set_sent_seq(sent.seq());
            cl.set_category(&item[0]);
            cl.set_pattern(dic_key);
            cl.set_sentence(origin_text);
            return cl;
          }
        }
      }
    }
  }
  return cl;
}

bool KorHmd::PlusKey(vector<string> key_vec, string nlp_line) {
  int plus_size = 0, last_index = 0;
  string word, pre_str, keyword, detc_word;
  vector<string> token;
  auto log = LOGGER();

  int key_vec_size = key_vec.size();
  try {
    bool find = false;
    for (int i = 0; i < key_vec_size; i++) {
      if (key_vec[i].empty())
        continue;
      plus_size = 0;
      if (key_vec[i][0] == '+') {
        plus_size = key_vec[i][1] - '0';
        if (!CheckHmdSymbolRule(plus_size)) {
          log->warn("{}: Wrong HMD Rule, number(1~9) after '+', cause '{}'",
                    __func__, key_vec[i]);
          return false;
        }
        word = key_vec[i].substr(2);
        int idx = i - 1;
        if (idx >= 0) {
          pre_str = key_vec[idx];
        } else {
          pre_str = " ";
        }
      }
      if (plus_size != 0) {
        token = SplitTok(nlp_line, ' ');
        keyword = pre_str + word;
        int tk_size = token.size();
        int gap = 0;
        for (int j = 0; j < tk_size; j++) {
          gap = j + plus_size;
          if (gap >= tk_size) {
            last_index = tk_size - 1;
            detc_word = token[j] + token[last_index];
          } else {
            detc_word = token[j] + token[gap];
          }
          if (Trim(keyword) == Trim(detc_word)) {
            find = true;
          }
        }
        if (find) {
          find = false;
        } else {
          return false;
        }
      }
    }
  } catch (const exception &e) {
    log->error("{}: {} ", __func__, e.what());
    return false;
  }
  return true;
}

/**
 * HmdMatrix 기준으로 분류하는 함수.
 * @param model 모델명
 * @param doc nlu 결과
 * @param top 분류값 하나만 뽑아낼 것인지
 * @return 분류결과 vector
 */
vector<HmdClassified> KorHmd::ClassifyWithMatrix(const string &model, const Document &doc, const bool top) {
  HmdInputText hmd_in_text;
  vector<HmdClassified> cl_res;
  string origin_text, nlp_line;
  vector<int> space_vec;
  KeyResult k_struct;
  auto logger = LOGGER();

  hmd_in_text.set_model(model);
  hmd_in_text.set_lang(LangCode::kor);

  auto mat_it = matrix_map_.find(model);
  if (mat_it == matrix_map_.end()) {
    return cl_res;
  }

  auto &mat = mat_it->second;
  auto &items = mat.matrix_items();
  for (const auto &sent : doc.sentences()) {
    // 문장생성
    MakeSentence(sent, origin_text, nlp_line, space_vec);

    //    // 규칙돌리기전 word단어 중 하나라도 존재하지 않으면 분류 Skip
    //    bool matched = false;
    //    auto &words = mat.words();
    //    for (const auto &w : words) {
    //      idx = nlp_line.find(w);
    //      if (idx != -1) {
    //        matched = true;
    //        break;
    //      }
    //    }
    //    if (!matched) {
    //      return cl_res;
    //    }

    for (const auto &item : items) {
      SubClassifyWithMatrix(item, nlp_line, space_vec, k_struct);
      if (k_struct.existence) {
        HmdClassified cl;
        cl.set_sent_seq(sent.seq());
        cl.set_pattern(item.origin_rule());
        cl.set_sentence(sent.text());
        const auto &cates = item.categories();
        for (const auto &cate : cates) {
          cl.set_category(cate);
          cl_res.push_back(cl);
          logger->trace("[Classify - sentence:[{}] : /category: [{}], rule:[{}]", nlp_line, cate, item.origin_rule());
          if (top)
            break; // 다음 문장 분류
        }
      }
    }
  }
  return cl_res;
}

void inline FindPos(const int &pos, const int &var, const vector<int> &space_vec, int &result) {
  int cnt = -1;
  for (const auto &v : space_vec) {
    if (pos == 0) {
      cnt = 0;
      break;
    } else if (pos >= v) {
      cnt++;
    } else {
      break;
    }
  }
  int len = space_vec.size();
  int calc = cnt + var;
  if (calc < 0 ) {
    result = 0;
  } else if (calc >= len){
    result = space_vec[len -1];
  } else {
    result = space_vec[calc];
  }
}


/**
 * Hmd Elem 분류하는 함수.
 * @param mat 분류할 기준이될 matrix.
 * @param nlp_line 분류할 문장.
 * @param space_vec 공백 위치
 * @param k_res 결과.
 * @return
 */
void KorHmd::SubClassifyWithMatrix(const HmdMatrixItem &item,
                                                    const string &nlp_line,
                                                    const vector<int> &space_vec,
                                                    KeyResult &k_res) {
  int pos = 0, find_pos = 0, param_val = 0, res_pos = 0;
  bool b_not = false;
  auto &elems = item.elems();
  k_res = SetResult(nlp_line, false);
  string k_str;
  for (const auto &e : elems) {
    if (e.use_like()) {
      k_str = e.elem();
    } else {
      k_str = ' ' + e.elem() + ' ';
    }
    if (e.use_not()) {
      b_not = true;
    }
    switch (e.op()) {
      case HmdOperator::HMD_OP_NONE:
        find_pos = nlp_line.find(k_str);
        break;
      case HmdOperator::HMD_OP_NEXT: //+
        param_val = e.param();
        FindPos(pos, param_val, space_vec, res_pos);
        find_pos = nlp_line.substr(pos, res_pos - pos + 1).find(k_str);
        break;
      case HmdOperator::HMD_OP_PREV: //-
        param_val = e.param() * -1;
        FindPos(pos, param_val, space_vec, res_pos);
        find_pos = nlp_line.substr(res_pos, pos - res_pos + 1).find(k_str);
        break;
      case HmdOperator::HMD_OP_POST: //@
        find_pos = nlp_line.substr(pos).find(k_str);
        break;
//      case HmdOperator::HMD_OP_NOT: //!
//        b_not = true;
//        find_pos = nlp_line.find(k_str);
//        break;
      default:break;
    }
    if (b_not) {
      if (find_pos == -1) {
        b_not = false;
        find_pos = pos;
      } else return;
    }
    if (find_pos == -1) {
      return;
    }
    pos = find_pos;

  }
  k_res = SetResult(nlp_line, true);
}