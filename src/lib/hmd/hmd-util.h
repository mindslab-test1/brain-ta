#include <string>
#include <vector>
#include <map>

using namespace std;

string Trim(string s);
vector<string> ExtractKeys(map<string, vector<string>> const &maps);
string Repeat(int n);