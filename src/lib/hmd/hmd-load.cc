#include <exception>

#include <libmaum/common/config.h>
#include "libmaum/brain/error/ta-error.h"
#include "libmaum/brain/hmd/hmd-load.h"
using namespace std::chrono;

ModelLoader::ModelLoader() {
  auto &conf = libmaum::Config::Instance();
  model_dir_= conf.Get("brain-ta.hmd.model.dir", false);
}

ModelLoader::ModelLoader(string dir) {
  model_dir_ = dir;
}

HmdModel ModelLoader::LoadFile(const string &filename) {
  HmdModel hmd_model;
  ifstream is;
  is.open(filename.c_str(), ios::in);
  string f_str = filename;
  auto log = LOGGER();
  if (is.fail()) {
    log->error("open dic file {} fail", f_str);
    exit(1);
  }
  hmd_model.ParseFromIstream(&is);
  log->debug("load dic file {} complete.", f_str);
  return hmd_model;
}

Document ModelLoader::LoadDocument(const string &filename) {
  Document doc;
  try {
    auto log = LOGGER();
    string doc_file = filename;
    ifstream is;
    is.open(doc_file.c_str(), ios::in);
    if (is.fail()) {
      log->error("open doc file {} fail", doc_file);
    }
    doc.ParseFromIstream(&is);
    log->info("load doc file {} complete.", doc_file);
  } catch (std::exception &e) {
    LOGGER()->warn("{}", e.what());
  }
  return doc;
}

void ModelLoader::Save(HmdModel model) {
  string model_file =
      model_dir_ + "/" + model.model() + "__" + to_string(model.lang())
          + ".hmdmodel";
  auto log = LOGGER();
  ofstream ots(model_file);
  model.SerializeToOstream(&ots);
  log->debug("save hmdmodel file {} complete.", model_file);
}

/**
 * HMD model file을 HMD Model 로드
 * @param model hmdmodel 명
 * @param lan hmdmodel 언어정보
 * @param hmd_model hmd_model 정보 저장할 변수
 */
void ModelLoader::LoadHmdModel(const string &model, const LangCode &lan, HmdModel &hmd_model) {
  auto log = LOGGER();
  ostringstream file_name;
  file_name << model_dir_ << '/' << model << "__" << to_string(lan) << ".hmdmodel";
  try {
    ifstream is(file_name.str());
    hmd_model.ParseFromIstream(&is);
  } catch (std::exception &e) {
    ostringstream msg;
    msg << __func__  << " Can't open hmdmodel file '" << file_name.str() << "' : " << e.what();
    log->error(msg.str());
    hmd_model.Clear();
  }
  log->debug("Load {} completely.", file_name.str());
}

 /**
  * 여러 HMD model file을 HMD model vector에 로드 하는 함수
  * @param file_names 로드할 HMD 모델 파일명 set
  * @param lan HmdModel 언어정보
  * @param models HmdModel들이 저장될 변수.
  */
void ModelLoader::LoadHmdModels(const set<string>& file_names, const LangCode &lan, vector<HmdModel> &models) {
  auto log = LOGGER();
  try {
    HmdModel hmd_model;
    for (const auto &m : file_names) {
      hmd_model.Clear();
      LoadHmdModel(m, lan, hmd_model);
      models.push_back(hmd_model);
    }
    if (models.empty())
      log->debug("No hmdmodel file Load.");
  } catch (std::exception &e) {
    models.clear();
    ostringstream msg;
    msg << __func__  << " Can't load Hmd Model Lists : " << e.what();
    log->error(msg.str());
  }
  log->debug("Load hmdmodel files done.");
}


/**
 * HMD matrix file을 로드
 * @param file_name 로드할 HMD Matrix 파일명
 * @return HmdModel HMD Matrix
 */
void ModelLoader::LoadMatrixModel(const string& file_name, HmdMatrix &res_mat) {
  auto log = LOGGER();
  string filename;
  HmdMatrix mat;
  try {
    ifstream is;
    filename = model_dir_ + '/' + file_name + ".hmdmatrix";
    is.open(filename, ios::in);
    mat.ParseFromIstream(&is);
  } catch (std::exception &e) {
    ostringstream msg;
    msg << __func__  << " Can't open hmdmatrix file '" << file_name << "' : " << e.what();
    log->error(msg.str());
    mat.Clear();
  }
  res_mat = mat;
}


/**
 * HMD model dir 내 모든 hmdmatrix file을 로드
 * @param mat_vec 로드된 hmdmatrix 정보를 저장.
 */
void ModelLoader::LoadAllMatrixModels(const LangCode &lang, vector<HmdMatrix> &mat_vec) {
  const system_clock::time_point start = system_clock::now();
  HmdMatrix hmd_mat;
  char dir[2048];
  char *ext;
  struct dirent *file = nullptr;
  DIR *dir_ptr = nullptr;
  strcpy(dir, model_dir_.c_str());
  dir_ptr = opendir(dir);
  file = readdir(dir_ptr);
  auto log = LOGGER();
  int index = 0;

  if (file == nullptr) {
    ostringstream msg;
    msg << __func__ << " Can't Find Hmd Model Path : " << dir;
    std::cerr << msg.str() << endl;
    throw ta::Exception("HMD", msg.str());
  }
  do {
    if (strcmp(file->d_name, "..") == 0 || strcmp(file->d_name, ".") == 0)
      continue;
    if ((ext = strrchr(file->d_name, '.')) == nullptr)
      continue;
    if (strcmp(ext, ".hmdmatrix") == 0) {
      try {
        string file_name(file->d_name);
        index = file_name.find("__");
        if (index < 1 && index >= -1) {
          ostringstream msg;
          msg << __func__ << " Skip the wrong HMD Matrix File : "
              << model_dir_ << '/' << file->d_name;
          log->warn(msg.str());
          continue;
        }
        string model_name = file_name.substr(0, index);
        if ((file_name[index + 2]) == to_string(lang)[0]) {
          LoadMatrixModel(model_name, lang, hmd_mat);
        }
      } catch (std::exception &e) {
        ostringstream msg;
        msg << __func__ << " Stop the wrong HMD Matrix File loading : "
            << model_dir_ << '/' << file->d_name;
        log->error(msg.str());
        continue;
      }
      mat_vec.push_back(hmd_mat);
      hmd_mat.Clear();
    }
  } while ((file = readdir(dir_ptr)) != nullptr);
  closedir(dir_ptr);
  auto took = duration_cast<milliseconds>(system_clock::now() - start);
  log->trace("Load all hmd matrix model complete - {}.(cnt: {}, took: {} ms)",
             lang,
             mat_vec.size(),
             took.count());
}

/**
 * HMD model dir 내 특정 hmdmatrix file을 로드
 * @param model hmdmatrix 파일명
 * @param lang hmdmatrix 언어정보
 * @param mat HMD Matrix
 */
void ModelLoader::LoadMatrixModel(const string &model,
                                  const LangCode &lang,
                                  HmdMatrix &hmd_mat) {
  auto log = LOGGER();
  ostringstream filename;
  try {
    filename << model_dir_ << '/' << model << "__" << to_string(lang)
             << ".hmdmatrix";
    ifstream is(filename.str());
    hmd_mat.ParseFromIstream(&is);
  } catch (std::exception &e) {
    ostringstream msg;
    msg << __func__ << " Stop the wrong HMD Matrix File loading : "
        << model_dir_ << '/' << filename.str();
    log->error(msg.str());
    return;
  }
  log->debug("Load {} complete.", filename.str());
}
/**
 * Hmd model  디렉토리 내에 hmdmodel들을 로딩해온다.
 * @param models
 */

void ModelLoader::LoadAllHmdModels(const LangCode &lang, vector<HmdModel> &models) {
  const system_clock::time_point start = system_clock::now();
  HmdModel hmd_model;
  struct dirent *file = nullptr;
  DIR *dir_ptr = nullptr;
  char dir[2048];
  char *ext;
  auto log = LOGGER();
  strcpy(dir, model_dir_.c_str());
  dir_ptr = opendir(dir);
  file = readdir(dir_ptr);
  int index = 0;
  if (file == nullptr) {
    ostringstream msg;
    msg << __func__ << " Can't Find Hmd Model Path : " << dir;
    std::cerr << msg.str() << endl;
    throw ta::Exception("HMD", msg.str());
  }
  do {
    if (strcmp(file->d_name, "..") == 0 || strcmp(file->d_name, ".") == 0)
      continue;
    if ((ext = strrchr(file->d_name, '.')) == nullptr)
      continue;
    if (strcmp(ext, ".hmdmodel") == 0) {
      try {
        hmd_model.Clear();
        string file_name(file->d_name);
        index = file_name.find("__");
        if (index < 1) {
          ostringstream msg;
          msg << __func__ << " Skip the wrong HMD Matrix File : "
              << model_dir_ << '/' << file->d_name;
          log->error(msg.str());
          continue;
        }
        string model_name = file_name.substr(0, index);
        if ((file_name[index + 2]) != to_string(lang)[0]) {
          ostringstream msg;
          msg << __func__ << " Skip the wrong HMD Matrix File : "
              << model_dir_ << '/' << file->d_name;
          log->error(msg.str());
          continue;
        }
        LoadHmdModel(model_name, lang, hmd_model);
      } catch (std::exception &e) {
        ostringstream msg;
        msg << __func__ << " Stop the wrong HMD Model File loading : "
            << model_dir_ << '/' << file->d_name;
        log->error(msg.str());
        continue;
      }
      models.push_back(hmd_model);
    }
  } while ((file = readdir(dir_ptr)) != nullptr);
  closedir(dir_ptr);
  auto took = duration_cast<milliseconds>(system_clock::now() - start);
  log->trace("Load all hmd model complete - {}.(cnt: {}, took: {} ms)",
             lang,
             models.size(),
             took.count());
}


void ModelLoader::SetModelDir(const string &dir) {
  model_dir_ = dir;
}
