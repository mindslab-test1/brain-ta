#include <libmaum/common/config.h>
#include "libmaum/brain/hmd/base-hmd.h"
#include "hmd-matrix.h"

using namespace std;
using namespace std::chrono;
using maum::brain::hmd::HmdRule;
using maum::brain::hmd::HmdMatrix;
using maum::common::LangCode;

BaseHmd::BaseHmd() {
  dic_ = {};
  models_ = {};
}

BaseHmd::~BaseHmd() {

}

vector<string> BaseHmd::SplitInput(const string &input) {
  vector<string> line;
  int set = -1;
  string tmp;
  int r_p = 0, l_p = 0;
  int input_size = input.length();
  for (int i = 0; i < input_size; i++) {
    if (input.at(i) == '(') {
      set = 1;
      l_p++;
    } else if (input.at(i) == ')') {
      r_p++;
      if (set != 1) {
        line.clear();
        break;
      }
      if (tmp.length() != 0) {
        line.push_back(tmp);
        tmp = "";
        set = 0;
      }
    } else if (set == 1) {
      tmp += input.at(i);
    } else if (set == -1) {
      line.push_back(input);
    }
  }
  if (l_p != r_p) {
    line.clear();
  }
  return line;
}

/**
 *
 * @param result_vec 사전규칙벡터
 * @param rule 사전규칙 카테고리
 * @param def  사전규칙 정규식
 * @param level 레벨
 * @return result_vec  매트릭스화된 사전규칙벡터
 */
vector<string> BaseHmd::CombineVectorWord(vector<string> &result_vec,
                                          const string &rule,
                                          const vector<vector<string>> &def,
                                          int level) {
  auto
      tmp_vec = unique_ptr<vector<string>>(new vector<string>()); //  결과값 담을 변수.
  auto tmp_str = unique_ptr<string>(new string());
  int def_size = 0;
  def_size = def.size();
  int rule_len = rule.length();
  if (level == def_size) { //  마지막 규칙
    result_vec.push_back(rule);
  } else if (level == 0) {  // 첫 기능,  카테고리 + def로 재귀
    tmp_str->clear();
    for (auto &d_l : def[level]) {
      *tmp_str = rule + d_l;
      CombineVectorWord(result_vec, *tmp_str, def, level + 1);
    }
  } else {
    ostringstream oss;
    int rule_sz = rule.length();
    tmp_str->clear();
    for (auto d_l : def[level]) {
      if (rule[rule_sz - 1] == '@') {
        oss << rule.substr(0, rule_sz - 1) << "$@" << d_l;
      } else if (rule[rule_sz - 1] == '%') {
        oss << rule.substr(0, rule_sz - 1) << "$%" << d_l;
      } else if (rule[rule_sz - 2] == '+') {
        oss << rule.substr(0, rule_sz - 2) << "$+" << rule[rule_sz - 1] << d_l;
      } else if (rule[rule_sz - 2] == '-') {
        oss << rule.substr(0, rule_sz - 2) << "$-" << rule[rule_sz - 1] << d_l;
      } else if (rule[rule_len - 2] == '^' and lang_ == LangCode::eng) {
        oss << rule.substr(0, rule_sz - 2) << "$^" << rule[rule_sz - 1] << d_l;
      } else if (rule[rule_len - 1] == '#') {
        oss << rule.substr(0, rule_sz - 1) << "$#" << d_l;
      } else {
        oss << rule << '$' << d_l;
      }
      *tmp_str = oss.str();
      oss.str(std::string());
      *tmp_vec = result_vec;
      result_vec = CombineVectorWord(*tmp_vec, *tmp_str, def, level + 1);
    }
  }
  return result_vec;
}

vector<string> BaseHmd::SplitTok(const string &str, const char &tok) {
  int idx = 0;
  bool exist_check = false;
  string tmp_str;
  vector<string> res_vec;
  int str_size = str.length();
  for (int i = 0; i < str_size; i++) {
    if (str.at(i) != tok)
      tmp_str += str.at(i);
    else if (str.at(i) == tok && tmp_str != "") {
      res_vec.push_back(tmp_str);
      tmp_str = "";
      idx = i + 1;
      exist_check = true;
    }
  }
  if (!exist_check) {
    res_vec.push_back(tmp_str);
  } else
    res_vec.push_back(str.substr(idx));
  return res_vec;
}

void BaseHmd::GetModel(const string &model, const LangCode &lan, HmdModel &hmdmodel) {
  ModelLoader ml;
  ml.LoadHmdModel(model, lan, hmdmodel);
}

void BaseHmd::GetModels(const LangCode &lan, vector<HmdModel> &models) {
  ModelLoader ml;
  ml.LoadAllHmdModels(lan, models);
}

void BaseHmd::SetModel(const HmdModel &hmdmodel) {
  vector<vector<string>> rule_result;
  vector<string> rule_vec, cat_vec, tmp_vec, result;
  HmdRule rules;
  string tmp_str;
  int tot = 0;
  const system_clock::time_point start = system_clock::now();
  for (const auto &rs : hmdmodel.rules()) {
    tmp_str = "";
    rule_result.clear();
    rules = rs;
    rule_vec = SplitInput(rules.rule());
    if (rule_vec.empty()) {
      continue;
    }
    int cnt = 1;
    // 규칙분리
    for (auto &r : rule_vec) {
      tmp_vec = SplitTok(r, '|');
      rule_result.push_back(tmp_vec);
      cnt *= tmp_vec.size();
    }
    tot += cnt;
    LOGGER()->trace("load [{}] counting:{}.", hmdmodel.model(), tot);
    //  카테고리분리
    for (auto &c : rules.categories()) {
      tmp_str += c;
      tmp_str += '\t';
    }
    tmp_vec = {};
    int level = 0;
    tmp_vec = result;
    result = CombineVectorWord(tmp_vec, tmp_str, rule_result, level);
  }

  map<string, vector<string>> hmd_map = {};

  tmp_str = "";
  tmp_vec = {};
  string cate;
  int res_size = result.size();
  for (int i = 0; i < res_size; i++) {
    tmp_str = result[i];
    tmp_vec = SplitTok(tmp_str, '\t');
    cate = "";
    int tmp_vec_size = tmp_vec.size();
    for (int j = 0; j < tmp_vec_size - 1; j++) {
      if (j != 0) {
        cate += '_';
      }
      cate += tmp_vec[j];
    }
    if (hmd_map.find(tmp_vec[tmp_vec_size - 1]) == hmd_map.end()) {
      vector<string> tmp = {cate};
      hmd_map[tmp_vec[tmp_vec_size - 1]] = tmp;
    } else {
      hmd_map[tmp_vec[tmp_vec_size - 1]].push_back(cate);
    }
  }
  dic_.insert(pair<string, map<string, vector<string>>>(hmdmodel.model(),
                                                        hmd_map));
  models_.insert(pair<string, HmdModel>(hmdmodel.model(), hmdmodel));
  auto took = duration_cast<milliseconds>(system_clock::now() - start);
  LOGGER()->debug("load [{}] success! <rule count:{}> took {} ms.",
                  hmdmodel.model(),
                  tot,
                  took.count());
}

string BaseHmd::HasModel(const string &name) {
  string str = name;
  if (models_.find(name) != models_.end()) {
    return str;
  }
  return "";
}

bool BaseHmd::HasMatrix(const string &name) {
  if (matrix_map_.find(name) != matrix_map_.end()) {
    return true;
  }
  return false;
}

/**
 * Hmdmodel을 Matrix로 변환하고, Matrix 파일로 저장
 * @param model 모델명
 * @return
 */
bool BaseHmd::HmdModelToMatrix(const HmdModel &model) {
  Matrix mat(model);
  mat.Compile();
  auto map_it = matrix_map_.find(model.model());
  if (map_it != matrix_map_.end()) {
    map_it->second = mat.GetMatrix();
  } else {
    matrix_map_.insert(pair<string, HmdMatrix>(model.model(), mat.GetMatrix()));
  }
  mat.SaveMatrixFile();
  return true;
}

void BaseHmd::SetMatrix(const HmdMatrix &mat) {
  matrix_map_.emplace(mat.model(), mat);
}