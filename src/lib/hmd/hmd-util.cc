#include "hmd-util.h"

using namespace std;

string Trim(string s) {
  string drop = "\t\n\v";
  string tmp = s.erase(s.find_last_not_of(drop) + 1);
  return tmp.erase(0, tmp.find_first_not_of(drop));
}

vector<string> ExtractKeys(map<string, vector<string>> const &maps) {
  vector<string> result;
  for (auto const &element : maps) {
    result.push_back(element.first);
  }
  return result;
}

string Repeat(int n) {
  string res(n, '_');
  return res;
}