#include <fstream>
#include <string>
#include <sys/types.h>
#include <dirent.h>
#include <set>

#include "maum/brain/nlp/nlp.pb.h"
#include "maum/brain/hmd/hmd.pb.h"

using namespace std;
using maum::brain::hmd::HmdRule;
using maum::brain::hmd::HmdModel;
using maum::brain::hmd::HmdMatrix;
using maum::brain::nlp::Document;
using maum::common::LangCode;

class ModelLoader {
 public:
  ModelLoader();
  ModelLoader(string dir);
  HmdModel LoadFile(const string &filename);
  Document LoadDocument(const string &filename);
  void Save(HmdModel);
  void SetModelDir(const string &dir);

  void LoadAllHmdModels(const LangCode &lan, vector<HmdModel> &models);
  void LoadHmdModels(const set<string>& file_names, const LangCode &lan, vector<HmdModel> &models);
  void LoadHmdModel(const string &model, const LangCode &lan, HmdModel &hmd_model);

  void LoadAllMatrixModels(const LangCode &lang, vector<HmdMatrix> &mat_vec);
  void LoadMatrixModel(const string &model, const LangCode &lang, HmdMatrix &hmd_mat);

  void LoadMatrixModel(const string& file_name, HmdMatrix &res_mat);
  void AllLoadMatrixModels(vector<HmdMatrix> &mat_vec);

 private:
  string model_dir_;
};
