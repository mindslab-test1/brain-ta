#include "base-hmd.h"

using namespace std;
using maum::brain::hmd::HmdModel;
using maum::brain::hmd::HmdRule;
using maum::brain::hmd::HmdInputText;
using maum::brain::hmd::HmdClassified;
using maum::brain::hmd::HmdMatrixItem;
using maum::brain::hmd::HmdResultDocument;
using maum::brain::nlp::Document;
using maum::brain::nlp::Sentence;

int FindWordId(int pos, int word_size,
               vector<vector<vector<int>>> space_vec, int vec_id);

class EngHmd : public BaseHmd {
 public:
  struct KeyResult {
    bool existence;
    vector<string> line;
    string key;
  };
  EngHmd();
  ~EngHmd();
  vector<vector<int>> GetSpaceVec(string str);
  vector<HmdClassified> SearchKey(string model, Document doc);
  KeyResult SubSearchKey(vector<string> key_vec,
                         string origin_line,
                         vector<string> nlp_vec,
                         vector<vector<vector<int>>> space_vec);
  bool CheckHmdSymbolRule(int wsize);
  KeyResult SetResult(const vector<string> &line, const string &out, bool is_found);

  void MakeSentence(const Sentence &sent, string &origin_text,
                    vector<string> &nlp_vec, vector<vector<int>> &space_vec);

  vector<HmdClassified> ClassifyWithMatrix(const string &model,
                                           const Document &doc,
                                           const bool top = false);
  void SubClassifyWithMatrix(const HmdMatrixItem &item,
                             const vector<string> nlp_vec,
                             const vector<vector<int>> &space_vec,
                             KeyResult &k_res);
};
