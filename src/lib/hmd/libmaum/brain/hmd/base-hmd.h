#ifndef __base_hmd_classified_header__
#define __base_hmd_classified_header__
#include "maum/brain/hmd/hmd.grpc.pb.h"
#include "maum/brain/hmd/hmd.pb.h"
#include <libmaum/brain/hmd/hmd-load.h>

using namespace std;
using maum::brain::hmd::HmdModel;
using maum::brain::hmd::HmdMatrix;
using maum::common::LangCode;

class BaseHmd {
 public:
  BaseHmd();
  ~BaseHmd();
  void GetModel(const string &model, const LangCode &lan, HmdModel &hmdmodel);
  void GetModels(const LangCode &lan, vector<HmdModel> &models);
  void SetModel(const HmdModel &hmdmodel);
  string HasModel(const string &name);
  bool HasMatrix(const string &name);

  vector<string> SplitInput(const string &input);
  vector<string> CombineVectorWord(vector<string> &result,
                                   const string &rule,
                                   const vector<vector<string>> &def,
                                   int level);
  vector<string> SplitTok(const string &str, const char &tok);
  bool HmdModelToMatrix(const HmdModel &model);
  void SetMatrix(const HmdMatrix &mat);

 protected:
  std::map<string, std::map<string, vector<string>>> dic_;
  std::map<string, HmdModel> models_;

  // modelname, HmdMatrix 정보 저장할 변수
  std::map<string, HmdMatrix> matrix_map_;

  LangCode lang_;
};
#endif /*__base_hmd_classified_header__ */
