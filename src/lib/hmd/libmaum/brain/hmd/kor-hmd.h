#include "base-hmd.h"

using namespace std;
using maum::brain::hmd::HmdModel;
using maum::brain::hmd::HmdRule;
using maum::brain::hmd::HmdInputText;
using maum::brain::nlp::Document;
using maum::brain::nlp::Sentence;
using maum::brain::hmd::HmdClassified;
using maum::brain::hmd::HmdMatrixItem;
using maum::brain::hmd::HmdResultDocument;

class KorHmd : public BaseHmd {
 public:
  struct KeyResult {
    bool existence;
    string line;
  };
  KorHmd();
  ~KorHmd();
  vector<HmdClassified> SearchKey(const string &model, const Document &doc);
  HmdClassified SearchFirstKey(const string &model, const Document &doc);
  void MakeSentence(const Sentence &sent,
                    string &origin_text,
                    string &nlp_line,
                    vector<int> &space_vec);
  bool PlusKey(vector<string> veckey, string plain_text);
  KeyResult SubSearchKey(vector<string> key_vec,
                         string origin_line,
                         string nlp_line,
                         vector<int> space_vec);
  bool CheckHmdSymbolRule(int wsize);
  KeyResult SetResult(const string &line, bool is_found);

  vector<HmdClassified> ClassifyWithMatrix(const string &model,
                                           const Document &doc,
                                           const bool top = false);
  void SubClassifyWithMatrix(const HmdMatrixItem &item,
                             const string &nlp_line,
                             const vector<int> &space_vec,
                             KeyResult &k_res);
};
