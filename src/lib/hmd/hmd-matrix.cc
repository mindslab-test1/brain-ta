#include <fstream>
#include "hmd-matrix.h"
#include "libmaum/brain/error/ta-error.h"

#include <libmaum/common/util.h> // split

using namespace std;
using namespace std::chrono;
using google::protobuf::Timestamp;
using maum::brain::hmd::HmdRule;
using maum::brain::hmd::HmdModel;
using maum::brain::hmd::HmdMatrix;
using maum::brain::hmd::HmdMatrixItem;
using maum::brain::hmd::HmdMatrixElement;
using maum::brain::hmd::HmdOperator;
using maum::brain::hmd::HmdOperator_Name;
using maum::common::LangCode;

/**
 * make one element to HmdMatixElement
 * @param elem short string
 * @param out
 * @return if unknown operator found, error
 */
bool Matrix::MakeOneElement(const string &elem, HmdMatrixElement &out) {
  auto logger = LOGGER();
  auto it = elem.begin();
  if (*it == HmdOperator::HMD_OP_NOT) {
    out.set_use_not(true);
    ++it;
  }
  switch (*it) {
    // %
    case HmdOperator::HMD_OP_LIKE: {
      out.set_op(HmdOperator::HMD_OP_NONE);
      out.set_use_like(true);
      ++it;
      break;
    }
    // ^
    case HmdOperator::HMD_OP_LEMMA: {
      ++it;
      if (*it >= '0' && *it <= '2') {
        out.set_param(*it - '0');
      } else {
        logger->error(
            "invalid lemma param char '0<=ch<=2, {}",
            elem);
        return false;
      }
      ++it;
      break;
    }
      // @
    case HmdOperator::HMD_OP_POST: {
      ++it;
      if (*it == HmdOperator::HMD_OP_LIKE) {
        out.set_use_like(true);
        ++it;
      }
      out.set_op(HmdOperator::HMD_OP_POST);
      break;
    }
    // -
    case HmdOperator::HMD_OP_PREV: {
      ++it;
      if (*it > '0' && *it <= '9') {
        out.set_param(*it - '0');
      } else {
        logger->error("invalid prev param char '0<ch<=9, {}", elem);
        return false;
      }
      ++it;
      if (*it == HmdOperator::HMD_OP_LIKE) {
        out.set_use_like(true);
        ++it;
      }
      out.set_op(HmdOperator::HMD_OP_PREV);
      break;
    }
    // +
    case HmdOperator::HMD_OP_NEXT: {
      ++it;
      if (*it > '0' && *it <= '9') {
        out.set_param(*it - '0');
      } else {
        logger->error(
            "invalid next param char '0<=ch<9': {}",
            elem);
        return false;
      }
      ++it;
      if (*it == HmdOperator::HMD_OP_LIKE) {
        out.set_use_like(true);
        ++it;
      }
      out.set_op(HmdOperator::HMD_OP_NEXT);
      break;
    }
    default: {
      out.set_op(HmdOperator::HMD_OP_NONE);
      break;
    }
  }

  string word(it, elem.end());
  out.set_elem(word);

  if (out.op() != HmdOperator::HMD_OP_NOT) {
    if (!word.empty()) {
      words_.insert(word);
    }
  } else {
    not_words_.insert(word);
  }
  return true;
}

static int IsPipe(int c) {
  return c == '|';
}

static int IsOpenParen(int c) {
  return c == '(';
}

/**
 * split partial rule to array elem
 * make elem to hmd matrix element
 *
 * @param part ex) a|b|c
 * @param out
 * @return if any error, return false
 */
bool Matrix::PartialRuleToElementVector(const string &part,
                                        Matrix::HmdMatrixElementVector &out) {
  out.clear();
  // a|b|c --> [a, b, c]
  vector<string> elems = split(part, IsPipe);

  for (const auto &elem: elems) {
    HmdMatrixElement mat_elem;
    if (!MakeOneElement(elem, mat_elem)) {
      return false;
    }
    out.push_back(mat_elem);
  }
  return true;
}


bool Matrix::RuleToPartialRule(const string &rule, vector<string> &out) {
  auto logger = LOGGER();
  out.clear();
  vector<string> parts = split(rule, IsOpenParen);
  for (auto & part : parts) {
    string::size_type pos = part.find_last_of(')');
    if (pos != (part.size() - 1)) {
      logger->warn("invalid hmd rule !!");
      return false;
    }
    part.erase(pos, 1);
    logger->trace("   part {}", part);
    out.push_back(part);
  }
  return true;
}


bool Matrix:: SplitRuleToElements(const string &rule,
      Matrix::RuleToHmdMatrixElementVector &out) {
  vector<string> parts;

  if (!RuleToPartialRule(rule, parts)) {
    return false;
  }

  for (const auto &part: parts) {
    Matrix::HmdMatrixElementVector elem_vector;
    PartialRuleToElementVector(part, elem_vector);
    out.push_back(elem_vector);
  }
  return true;
}

HmdMatrixItem *Matrix::MakeItems(const HmdMatrixElementVector &elem_vector) {
  HmdMatrixItem * item = new HmdMatrixItem();
  google::protobuf::RepeatedPtrField<HmdMatrixElement> elems(elem_vector.begin(), elem_vector.end());
  item->mutable_elems()->CopyFrom(elems);

  // matrix 문자열 생성
  stringstream str;
  for (const auto &elem : elem_vector) {
    if ((str.rdbuf()->in_avail()) > 0) {
      str << '$';
    }
    if (elem.op() != HmdOperator::HMD_OP_NONE) {
      if (elem.param() != 0 || elem.op() == HmdOperator::HMD_OP_LEMMA) {
        str << (char)elem.op() << elem.param();
      } else {
        str << (char)elem.op();
      }
    }
    if (elem.use_not()) {
      str << '!';
    }
    if (elem.use_like()) {
      str << '%';
    }
    str << elem.elem();
  }
  string hmd_rule_str = str.str();
  item->set_origin_rule(hmd_rule_str);
  return item;
}

void Matrix::ExpandElemVector(const HmdMatrixElementVector &elem_vector,
                              const int &vec_length,
                              RuleToHmdMatrixElementVector &res_vec,
                              int &level) {

  if (level == 0) {
    for (const auto &elem : elem_vector) {
      HmdMatrixElementVector vec{elem};
      res_vec.push_back(vec);
    }
  } else if (level == vec_length) {
    return;
  } else {
    RuleToHmdMatrixElementVector tmp_vec(res_vec);
    res_vec.clear();
    for (const auto &elem : elem_vector) {
      for (const auto &tmp : tmp_vec) {
        HmdMatrixElementVector vec(tmp.begin(), tmp.end());
        vec.push_back(elem);
        res_vec.push_back(vec);
      }
    }
  }
  ++level;
}

bool Matrix::FindMatrixRule(const string &rule, const string &category) {
  auto item_it = result_mat_.mutable_matrix_items();
  auto it = item_it->begin();
  auto end = item_it->end();

  while (it != end) {
    if (it->origin_rule() == rule) {
      it->add_categories(category);
      return true;
    }
    ++it;
  }
  return false;
}

bool Matrix::Compile() {
  const system_clock::time_point start = system_clock::now();
  auto logger = LOGGER();
  int cnt = 0;
  const auto &rules = model_.rules();
  auto items_it = result_mat_.mutable_matrix_items();
  for (const auto &rule: rules) {
    HmdMatrixItem item;
    Matrix::RuleToHmdMatrixElementVector elem_vector, res_vec;
    if (!SplitRuleToElements(rule.rule(), elem_vector)) {
      logger->warn("skip invalid rule! : {}", rule.rule());
      continue;
    }
    // 카테고리 합치기
    string category;
    ostringstream oss;
    for (const auto &c : rule.categories()) {
      if (!oss.str().empty())
        oss << '_';
      oss << c.c_str();
    }
    category = oss.str();

    // 확장된 룰을 만들기
    int level = 0;
    int len = 1;
    // 벡터의 크기 미리 할당.
    int vec_size = len;
    for (const auto &elems : elem_vector) {
      vec_size *= elems.size();
    }
    res_vec.reserve(vec_size);
    len = elem_vector.size();
    for (const auto &elems : elem_vector) {
      ExpandElemVector(elems, len, res_vec, level);
    }

    items_it->Reserve((items_it->size() + vec_size));
    // MakeItems
    for (const auto &e : res_vec) {
      cnt++;
      // Elems 저장.
      auto item = MakeItems(e);
      // 해당 룰에대하여 같은 Matrix Rule이 존재하는지 체크.
      if (FindMatrixRule(item->origin_rule(), category)) {
//        logger->trace("[{}] rule: {}/ category: {}.", cnt, item->origin_rule(), category);
        continue;
      }
      // 카테고리, 룰 값 저장
      item->add_categories(category);
      items_it->AddAllocated(item);
//      logger->trace("[{}] rule: {}/ category: {}.", cnt, item->origin_rule(), category);
    }
  }

  Timestamp time;
  time.set_seconds(std::time(0));
  time.set_nanos(0);
  result_mat_.mutable_timestamp()->CopyFrom(time);
  result_mat_.set_description("CPP");
  result_mat_.set_lang(model_.lang());
  result_mat_.set_model(model_.model());
  for (const auto& word: words_) {
    result_mat_.add_words(word);
  }

  auto took = duration_cast<milliseconds>(system_clock::now() - start);
  logger->trace("Make [{}] Hmd Matrix done, cnt:[{}], took:[{} ms].", model_.model().c_str(), cnt, took.count());
  if (result_mat_.matrix_items().size() == 0)
    return false;
  return true;
}

bool Matrix::SaveMatrixFile() {
  stringstream str;
  auto logger = LOGGER();
  str << model_dir_ << '/' << result_mat_.model() << "__" << to_string(result_mat_.lang()) << ".hmdmatrix";
  try {
    HmdMatrix mat(result_mat_);
    ofstream ots(str.str());
    mat.SerializeToOstream(&ots);
    logger->debug("Make HmdMatrix file in {} completely.", str.str());
  } catch (ta::Exception e) {
    ostringstream msg;
    msg << __func__ << " Can't Find Hmd Model Path : " << str.str();
    logger->error("Make HmdMatrix file in {} completely.", str.str());
    std::cerr << msg.str() << endl;
    throw ta::Exception("HMD", msg.str());
  }
  return true;
}