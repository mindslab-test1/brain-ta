cmake_minimum_required(VERSION 2.8.12)

project(brain-hmd CXX)
set(OUTLIB brain-hmd)

set(SOURCE_FILES
    hmd-util.h
    hmd-util.cc
    libmaum/brain/hmd/hmd-load.h
    hmd-load.cc
    libmaum/brain/hmd/base-hmd.h
    base-hmd.cc
    libmaum/brain/hmd/kor-hmd.h
    kor-hmd.cc
    libmaum/brain/hmd/eng-hmd.h
    eng-hmd.cc
    hmd-matrix.h
    hmd-matrix.cc)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}  -Wno-unknown-pragmas")
set(CMAKE_CXX_STANDARD 11)
if (CMAKE_COMPILER_IS_GNUCXX)
  add_definitions(-std=c++11)
endif ()

include_directories(.)
include_directories(../../../proto)
include_directories("${CMAKE_INSTALL_PREFIX}/include")
include_directories(../common)
LINK_DIRECTORIES("${CMAKE_INSTALL_PREFIX}/lib")

add_library(${OUTLIB} SHARED ${SOURCE_FILES} )

target_link_libraries(${OUTLIB}
    maum-common
    maum-json
    brain-ta-pb
    grpc++ protobuf
    )

install(DIRECTORY libmaum/ DESTINATION include/libmaum
    FILES_MATCHING PATTERN "*.h")

install(TARGETS ${OUTLIB}
    LIBRARY DESTINATION lib COMPONENT libraries
    )
