#ifndef __HMD_MATRIX_H
#define __HMD_MATRIX_H

#include <set>
#include "maum/brain/hmd/hmd.grpc.pb.h"
#include "maum/brain/hmd/hmd.pb.h"
#include <libmaum/common/config.h>

using maum::brain::hmd::HmdModel;
using maum::brain::hmd::HmdMatrix;
using maum::brain::hmd::HmdMatrixItem;
using maum::brain::hmd::HmdMatrixElement;
using maum::brain::hmd::HmdRule;

class HmdRuleError : public std::exception {
 public:
  HmdRuleError(){};
  void AddErrorRules(const string &rule) {
    error_rules_.insert(rule);
  }
  std::set<string> GetErrorRules() {
    return error_rules_;
  }

 private:
  std::set<string> error_rules_;
};

class Matrix {
 public:
  Matrix(const HmdModel& model)
      : model_(model) {
    auto &conf = libmaum::Config::Instance();
    model_dir_ = conf.Get("brain-ta.hmd.model.dir", false);
  }
  void SetModelDir(const string &path) {
    model_dir_ = path;
  }

  HmdMatrix GetMatrix() {
    return result_mat_;
  }

  bool Compile();
  bool SaveMatrixFile();
 private:

  typedef vector<HmdMatrixElement> HmdMatrixElementVector;
  typedef vector<HmdMatrixElementVector> RuleToHmdMatrixElementVector;
  // RULE (a|b|c)(e|f)(!x|!y) ->  a|b|c, e|f, !x|!y
  bool RuleToPartialRule(const string &rule, vector<string> &out);
  // a|b|c -> [Elem, Elem, Elem]
  bool PartialRuleToElementVector(const string &part,
                                  HmdMatrixElementVector &out);
  // a -> Elem
  bool MakeOneElement(const string &elem, HmdMatrixElement &out);
  // RULE -> [Elem, Elem, Elem] [Elem, Elem, Elem]
  bool SplitRuleToElements(const string &rule,
                           RuleToHmdMatrixElementVector &out);
//  void MakeItems(const HmdMatrixElementVector &elem_vector, HmdMatrixItem &item);
  HmdMatrixItem *MakeItems(const HmdMatrixElementVector &elem_vector);

  void ExpandElemVector(const HmdMatrixElementVector &elem_vector,
                        const int &vec_length,
                        RuleToHmdMatrixElementVector &res_vec,
                        int &level);
  bool FindMatrixRule(const string &rule, const string &category);
  // 원본 규칙 string(안녕)(고객님). HmdMatrixItem
  std::map<string, HmdMatrixItem> item_maps_;
  // matrix화된 규칙 string(안녕$고객님). categories
  std::map<string, string> item_map_;
  // HmdModel 경로
  string model_dir_;
  // 입력으로 사용할 모델
  HmdModel model_;

  // 결과값으로 만들어질 최종 매트릭스
  HmdMatrix result_mat_;
  // rule에 포함 단어 set. 단 operator가 not인 것은 추가하지 않음.
  std::set<string> words_;
  // not인 단어 set.
  std::set<string> not_words_;

};
#endif //__HMD_MATRIX_H
