#include "libmaum/brain/nlp/nlp1kor.h"
#include <libmaum/common/encoding.h>
#include <libmaum/common/config.h>
#include "lm-interface.h"

using maum::brain::nlp::NlpAnalysisLevel;
using maum::brain::nlp::KeywordFrequencyLevel;
using maum::brain::nlp::NlpFeature;

struct node {
  int32_t seq;
  std::string keyword;
  int32_t frequency;
  std::string word_type;
  std::string word_type_nm;
};

void Nlp1Kor::UpdateFeatures() {
  auto logger = LOGGER();
  // 언어 분석을 위한 멀티 쓰레드
  global_lmi_ = new LMInterface();

  unique_ptr<char[]> path(new char[res_path_.size() + 1]);
  strcpy(path.get(), res_path_.c_str());

  // 언어 분석에 해당하는 각 모듈 초기화
  // 띄어쓰기, 형태소 분석, 개체명 인식, Chunk

  if (HasFeature(NlpFeature::NLPF_SPACE)) {
    global_lmi_->init_space(path.get());
    logger->debug("[NLP1] loaded init_space");
  }

  if (HasFeature(NlpFeature::NLPF_MORPHEME)) {
    global_lmi_->init_morp_global(path.get());
    logger->debug("[NLP1] loaded init_morp");
  }

  if (HasFeature(NlpFeature::NLPF_NAMED_ENTITY)) {
    global_lmi_->init_NER_global(path.get(), ALL_DOMAIN);
    logger->debug("[NLP1] loaded init_ner");
  }

  if (HasFeature(NlpFeature::NLPF_CHUNK)) {
    global_lmi_->init_Chunk_global(path.get(), ALL_DOMAIN);
    logger->debug("[NLP1] loaded init_chunk");
  }
}

void Nlp1Kor::Uninit() {

  if (HasFeature(NlpFeature::NLPF_CHUNK)) {
    global_lmi_->close_Chunk_global();
  }

  if (HasFeature(NlpFeature::NLPF_NAMED_ENTITY)) {
    global_lmi_->close_NER_global();
  }

  if (HasFeature(NlpFeature::NLPF_MORPHEME)) {
    global_lmi_->close_morp_global();
  }

  if (HasFeature(NlpFeature::NLPF_SPACE)) {
    global_lmi_->close_space();
  }
}

/**
  @breif 분석된 결과를 Document에 저장시키기 위함 함수
  @param N_Doc &ndoc : 언어분석 결과를 담고 있는 구조체
  @param Document *document : 언어분석 결과를 클라이언트에 보낼 message
  @param NlpAnalysisLevel level : 언어분석 레벨
*/
void Nlp1Kor::ToMessage(const N_Doc &ndoc,
                        Document *document,
                        int &exec_nlp_feature_mask) {
  string result;
  // sentence
  for (const N_Sentence &sent : ndoc.sentence) {
    maum::brain::nlp::Sentence *sentence = document->add_sentences();

    if (HasFeature(NlpFeature::NLPF_MORPHEME)
        & HasExecFeature(exec_nlp_feature_mask, NlpFeature::NLPF_MORPHEME)) {
      // 각각 문장에 대한 정보
      sentence->set_seq(sent.id);
      //string sent_text = my_replace(sent.text, "\\", "\\\\");
      //result += my_replace(sent_text, "\"", "\\\"");
      EuckrToUtf8(sent.text, result);
      sentence->set_text(result);
      // morp, 형태소 분석 결과
      for (const N_Morp &morp : sent.morp) {
        maum::brain::nlp::Morpheme *morpheme = sentence->add_morps();
        morpheme->set_seq(morp.id);
        EuckrToUtf8(morp.lemma, result);
        morpheme->set_lemma(result);
        // string morp_lemma = my_replace(morp.lemma, "\\", "\\\\");
        // result += my_replace(morp_lemma, "\"", "\\\"");
        morpheme->set_type(morp.type);
        morpheme->set_position(morp.position);
      }
      // word, word에 대한 정보 및 분석 결과
      // morp와 결과는 같지만 구조체 내 멤버변수가 다름
      for (const N_Word &word : sent.word) {
        maum::brain::nlp::Word *w = sentence->add_words();
        w->set_seq(word.id);
        // string word_text = my_replace(word.text, "\\", "\\\\");
        // result += my_replace(word_text, "\"", "\\\"");
        EuckrToUtf8(word.text, result);
        w->set_text(result);
        w->set_type(word.type);
        w->set_begin(word.begin);
        w->set_end(word.end);
      }
    }

    // NER, 개체명 인식 결과
    if (HasFeature(NlpFeature::NLPF_NAMED_ENTITY)
        & HasExecFeature(exec_nlp_feature_mask,
                         NlpFeature::NLPF_NAMED_ENTITY)) {
      for (const N_NE &ne : sent.NE) {
        maum::brain::nlp::NamedEntity *entity = sentence->add_nes();
        entity->set_seq(ne.id);
        // string ne_text = my_replace(ne.text, "\\", "\\\\");
        // result += my_replace(ne_text, "\"", "\\\"");
        EuckrToUtf8(ne.text, result);
        if (syn_set_.find(result) != syn_set_.end()) {
          result = syn_set_[result];
        }
        entity->set_text(result);
        entity->set_type(ne.type);
        entity->set_begin(ne.begin);
        entity->set_end(ne.end);
        entity->set_weight(ne.weight);
        entity->set_common_noun(ne.common_noun);
      }
    }

    //Chunk에 대한 분석 결과
    if (HasFeature(NlpFeature::NLPF_CHUNK)
        & HasExecFeature(exec_nlp_feature_mask, NlpFeature::NLPF_CHUNK)) {
      for (const N_Chunk &chunk : sent.chunk) {
        maum::brain::nlp::Chunk *chu = sentence->add_chunks();
        chu->set_seq(chunk.id);
        EuckrToUtf8(chunk.text, result);
        chu->set_text(result);
        chu->set_type(chunk.type);
        chu->set_begin(chunk.begin);
        chu->set_end(chunk.end);
      }
    }
  }
}

/**
  @breif 문장 내에서 명사, 형용사, 동사의 빈도 수를 계산하기 위한 함수
  @param N_Doc &ndoc : 언어분석 결과를 담고 있는 구조체
  @param Document *document : 언어분석 결과를 클라이언트에 보낼 message
*/
void Nlp1Kor::ToMessageWithKeywordFrequency(const N_Doc &ndoc,
                                            Document *document) {
  string idx_str;
  int sentence_size = int(ndoc.sentence.size());

  for (int i = 0; i < sentence_size; i++) {
    const N_Sentence &sent = ndoc.sentence[i];
    vector<string> tmp_vec;
    bool nn_set = false;
    bool jx_set = false;
    bool np_set = true;
    int begin = -1, end = -1;
    int nc_num = 0;
    int m_size = (int) (sent.morp.size());
    map<string, node> dic_keyword;
    for (int mid = 0; mid < m_size; mid++) {
      if (FindStopWord(sent.morp[mid].lemma)) {
        continue;
      }
      // NP => (nn|mm)
      if (sent.morp[mid].type == "nn" || sent.morp[mid].type == "mm") {
        if (!nn_set)
          begin = mid;
        nn_set = true;
        idx_str += sent.morp[mid].lemma;
        // (nn|mm) => clear()
        if (mid + 1 < m_size) {
          nn_set = false;
          end = mid;
        }
#if 0
        // FIXME.. result is same..
        if (!(sent.morp[mid + 1].type == "nb" ||
            sent.morp[mid + 1].type == "nn" ||
            sent.morp[mid + 1].type == "mm")) {
          nn_set = false;
          end = mid;
        } else {
          // (nn|mm) => EOF => clear()
          nn_set = false;
          end = mid;
        }
#endif
      } else if (nn_set) {
        // (nn|mm)+jx+nc => clear()
        if (jx_set) {
          jx_set = false;
          nn_set = false;
          end = mid;
          idx_str += sent.morp[mid].lemma;
        } else if (sent.morp[mid].type[0] == 'j') {
          jx_set = true;
          idx_str += sent.morp[mid].lemma;
        } else if (sent.morp[mid].type == "nb") {
          // (nn|mm) + nb => clear
          nn_set = false;
          end = mid;
          idx_str += sent.morp[mid].lemma;
        }
      } else if (sent.morp[mid].type[0] == 'p') {
        // VP => pv|pa => clear()
        begin = mid;
        end = mid;
        np_set = false;
        idx_str += sent.morp[mid].lemma;
        idx_str += "\xb4\xd9"; // 다
      } else if (sent.morp[mid].type == "nc" || sent.morp[mid].type == "xp") {
        // NP => (nc)
        if (begin == -1) begin = mid;
        // (nc) => insert nc
        if (sent.morp[mid].type == "nc") nc_num += 1;
        idx_str += sent.morp[mid].lemma;
        tmp_vec.push_back(sent.morp[mid].lemma);
        // Check to Making Complex NP
        if (mid + 1 < m_size) {
          // nc+xsm => insert xsm => clear()
          if (sent.morp[mid + 1].type == "xsm") {
            end = mid;
            np_set = false;
            idx_str += sent.morp[mid + 1].lemma;
            idx_str += "\xb4\xd9"; // 다
            tmp_vec.clear();
          } else if (sent.morp[mid + 1].type == "xsv") {
            // VP => nc+xsv => insert xsv => clear()
            end = mid;
            np_set = false;
            idx_str += sent.morp[mid + 1].lemma;
            idx_str += "\xb4\xd9"; // 다
            tmp_vec.clear();
          } else if (sent.morp[mid + 1].type == "xsn") {
            // NP => nc+xsn => insert pop_back+xsn => clear()
            end = mid + 1;
            idx_str += sent.morp[mid + 1].lemma;
            string tmp_str = tmp_vec[(int) (tmp_vec.size()) - 1];
            tmp_vec.pop_back();
            tmp_str += sent.morp[mid + 1].lemma;
            tmp_vec.push_back(tmp_str);
          } else if (sent.morp[mid + 1].type != "nc") {
            end = mid;
          }
        } else if (nc_num >= 1 && mid + 2 < m_size) {
          if (sent.morp[mid + 2].type[0] == 'x'
              && sent.morp[mid + 2].type[1] == 's'
              && ((int) sent.morp[mid + 2].type.size() == 3
                  && sent.morp[mid + 2].type[2] != 'n')) {
            end = mid;
          }
        } else {
          end = mid;
        }
      }
      // print sentence
      if (end != -1) {
        nc_num = 0;
        // print NMP
        if (np_set) {
          // tmp_vec size check
          if ((int) tmp_vec.size() > 1) {
            for (int vec_i = 0; vec_i < (int) tmp_vec.size(); vec_i++) {
              string key = tmp_vec[vec_i];
              if ((tmp_vec[vec_i].size() > 2) ||
                  FindOneWord(tmp_vec[vec_i])) {
                if (dic_keyword.find(key) != dic_keyword.end()) {
                  dic_keyword[key].frequency += 1;
                } else {
                  node tmp_node;
                  tmp_node.seq = i;
                  tmp_node.keyword.append(key);
                  tmp_node.frequency = 1;
                  tmp_node.word_type_nm.append("AT_NOT");
                  tmp_node.word_type.append(ne_tag_["AT_NOT"]);
                  dic_keyword[key] = tmp_node;
                }
              }
            }
          }
          // Check Exist String -> NE
          bool b_NE = false;
          for (int nid = 0; nid < (int) ndoc.sentence[i].NE.size(); nid++) {
            if (ndoc.sentence[i].NE[nid].begin == begin
                && ndoc.sentence[i].NE[nid].end == end) {
              b_NE = true;
              break;
            }
          }
          if (!b_NE &&
              (idx_str.size() > 2 || FindOneWord(idx_str))) {
            if (dic_keyword.find(idx_str) != dic_keyword.end()) {
              dic_keyword[idx_str].frequency += 1;
            } else {
              node tmp_node;
              tmp_node.seq = i;
              tmp_node.keyword.append(idx_str);
              tmp_node.frequency = 1;
              tmp_node.word_type_nm.append("AT_NOT");
              tmp_node.word_type.append(ne_tag_["AT_NOT"]);
              dic_keyword[idx_str] = tmp_node;
            }
          }
        } else {
          // print VP
          string type;
          if (sent.morp[mid].type == "pa") {
            type = "ADJ";
          } else {
            type = "VERB";
          }
          if (dic_keyword.find(idx_str) != dic_keyword.end()) {
            dic_keyword[idx_str].frequency += 1;
          } else {
            node tmp_node;
            tmp_node.seq = i;
            tmp_node.keyword.append(idx_str);
            tmp_node.frequency = 1;
            tmp_node.word_type_nm.append(type);
            tmp_node.word_type.append("-100");
            dic_keyword[idx_str] = tmp_node;
          }
        }
        begin = -1;
        end = -1;
        np_set = true;
        idx_str.clear();
        tmp_vec.clear();
      }
    } // end of keyword print
    map<string, node>::iterator keyword_iter;
    vector<node> vec_keyword_node;
    // keyword sorting
    for (const auto dic_k : dic_keyword) {
      vec_keyword_node.push_back(dic_k.second);
    }
    for (const auto &k_node : vec_keyword_node) {
      auto keyword = document->add_keyword_frequencies();
      keyword->set_seq(k_node.seq);
      keyword->set_keyword(EuckrToUtf8(k_node.keyword));
      keyword->set_frequency(k_node.frequency);
      keyword->set_word_type_nm(k_node.word_type_nm);
      keyword->set_word_type(k_node.word_type);
    }
  } // end of morp.size
}
/**
  @breif 사용자에게 입력받은 분석 레벨에 따라서 해당 모듈 초기화 및 분석하는 함수
  @param InputText *text : 클라이언트에서 보낸 message
  @param Document *document : 언어분석 결과를 클라이언트에 보낼 message
  @return None
*/
void Nlp1Kor::AnalyzeOne(const InputText *text, Document *document) {
  const string &otext = text->text();
  auto nlp_level = text->level();
  int exec_nlp_feature_mask = 0;
  LMInterface lvLMI;
  N_Doc ndoc;
  unique_ptr<char[]> path(new char[res_path_.size() + 1]);
  strcpy(path.get(), res_path_.c_str());
  string sent;
  Utf8ToEuckr(otext.c_str(), sent);
  sent = global_lmi_->do_space(sent);
  switch (nlp_level) {
    // 모든 모듈을 분석
    case NlpAnalysisLevel::NLP_ANALYSIS_ALL: {
      lvLMI.init_morp_thread(path.get());
      lvLMI.init_NER_thread(path.get());
      lvLMI.init_Chunk_thread(path.get());
      lvLMI.do_preNER(sent);
      lvLMI.do_ATR();
      lvLMI.do_Chunk();
      global_lmi_->do_SA(ndoc);
      lvLMI.make_ndoc(&lvLMI.m_Doc, ndoc);

      // set exec Nlpfeature mask
      exec_nlp_feature_mask = (1 << NlpFeature::NLPF_SPACE) |
          (1 << NlpFeature::NLPF_MORPHEME) |
          (1 << NlpFeature::NLPF_NAMED_ENTITY) |
          (1 << NlpFeature::NLPF_CHUNK);

      ToMessage(ndoc, document, exec_nlp_feature_mask);
      lvLMI.clear_Doc(&lvLMI.m_Doc);
      lvLMI.close_morp_thread();
      lvLMI.close_NER_thread();
      lvLMI.close_Chunk_thread();
      break;
    }
      // word, morp 분석
    case NlpAnalysisLevel::NLP_ANALYSIS_MORPHEME: {
      lvLMI.init_morp_thread(path.get());
      lvLMI.do_preNER(sent);
      lvLMI.make_ndoc(&lvLMI.m_Doc, ndoc);

      // set exec Nlpfeature mask
      exec_nlp_feature_mask = (1 << NlpFeature::NLPF_SPACE) |
          (1 << NlpFeature::NLPF_MORPHEME);

      ToMessage(ndoc, document, exec_nlp_feature_mask);
      lvLMI.clear_Doc(&lvLMI.m_Doc);
      lvLMI.close_morp_thread();
      break;
    }
      // word, morp, ner 분석
    case NlpAnalysisLevel::NLP_ANALYSIS_NAMED_ENTITY: {
      lvLMI.init_morp_thread(path.get());
      lvLMI.init_NER_thread(path.get());
      lvLMI.do_preNER(sent);
      lvLMI.do_ATR();
      lvLMI.make_ndoc(&lvLMI.m_Doc, ndoc);

      // set exec Nlpfeature mask
      exec_nlp_feature_mask = (1 << NlpFeature::NLPF_SPACE) |
          (1 << NlpFeature::NLPF_MORPHEME) |
          (1 << NlpFeature::NLPF_NAMED_ENTITY);

      ToMessage(ndoc, document, exec_nlp_feature_mask);
      lvLMI.clear_Doc(&lvLMI.m_Doc);
      lvLMI.close_morp_thread();
      lvLMI.close_NER_thread();
      break;
    }
    default: {
      break;
    }
  }
  // 사용자가 keyword 빈도수 추출을 원하면 수행
  if (text->keyword_frequency_level() ==
      KeywordFrequencyLevel::KEYWORD_FREQUENCY_ALL) {
    ToMessageWithKeywordFrequency(ndoc, document);
  }
  document->set_lang(maum::common::LangCode::kor);
}
