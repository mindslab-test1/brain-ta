/// @file	LMInterface.h
/// @brief	interface for the LMInterface class

/// @mainpage	첫페이지
///	@section intro	소개 및 실행환경
///	- 소개 : ETRI LMInterface 
/// - OS : Ubuntu 11.10 (GNU/Linux 3.0.0-12-server x86_64), 64bit
/// - GCC : gcc version 4.6.1 (Ubuntu/Linaro 4.6.1-9ubuntu3)
/// - 필요사항 : Boost 1.52 설치 필요 (반드시 /usr/local/ 아래에 설치)
///	@section CREATEINFO	작성정보
/// - 작성자 : 윤여찬 (ycyoon@etri.re.kr)
/// - 작성일 : 2013.07.08

// LMInterface.h: interface for the LMInterface class.
//
//////////////////////////////////////////////////////////////////////

#ifndef LM_INTERFACE_H_
#define LM_INTERFACE_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


/**********************************************************************************************
Copyright (c) 2006 Electronics and Telecommunications Research Institute (ETRI) All Rights Reserved.

Following acts are STRICTLY PROHIBITED except when a specific prior written permission is obtained from ETRI or a separate written agreement with ETRI stipulates such permission specifically:
a) Selling, distributing, sublicensing, renting, leasing, transmitting, redistributing or otherwise transferring this software to a third party;
b) Copying, transforming, modifying, creating any derivatives of, reverse engineering, decompiling, disassembling, translating, making any attempt to discover the source code of, the whole or part of this software in source or binary form; 
c) Making any copy of the whole or part of this software other than one copy for backup purposes only; and 
d) Using the name, trademark or logo of ETRI or the names of contributors in order to endorse or promote products derived from this software.

This software is provided "AS IS," without a warranty of any kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

Any permitted redistribution of this software must retain the copyright notice, conditions, and disclaimer as specified above.
**********************************************************************************************/


// LMInterface.h는 언어분석기 통합 담당자가 최종적으로 수정하여 배포함

#undef MIN
#undef MAX

// 개체명 인식기
// #pragma GCC diagnostic push
// #pragma GCC diagnostic ignored "-Wdeprecated-declarations"
// #pragma GCC diagnostic ignored "-Wdeprecated"
#include "ner/NERecognizer.h"
// #pragma GCC diagnostic pop

// 문서 분류기
#include "ner/Classifier.h"
// 띄어쓰기
#include "space/Space.h"
#include "space/SpaceUtil.h"
// 청커
#include "chunk/ChunkAnalyzer.h"
// 파서
#include "parser/tparser.h"
// SRL
#include "srl/srl.h"
// 관계추출
#include "relation/Relation.h"
// 감성분석
#include "SA/SentimentAnalyzer.h"

/// @brief	LMInterface 클래스
class LMInterface {
 public:
  LMInterface();
  virtual ~LMInterface();


  //---------------------------------------
  // 멤버 변수
  //---------------------------------------
  char flag[10];


  // 형태소 분석기
  CMorpAnalyzer *m_scan;

  // 문서 자료구조
  struct Doc m_Doc;            // 일반적으로 사용하는 doc구조
  struct Doc m_Doc2;        // multiple doc을 위한 추가 테스트
  list<Doc *> m_Doc_list;    // multiple doc - document구조를 리스트로 저장

  // ATR
  CNERecognizer *m_NER;

  // 문서분류기
  Classifier *m_cl;

  // 띄어쓰기
  Space *m_space;
  SpaceUtil *m_space_util;

  // CHUNK
  CChunkAnalyzer *m_CHUNK;

  // 의존 관계 분석
  TParser *m_DParse;

  // Semantic Role Labeling
  SRL *m_srl;

  // Relation
  Relation *m_Rel;

  // Sentiment Analysis
  SentimentAnalyzer *m_SA;


  // 형태소 분석
  /**
  @brief	형태소 분석기 생성 및 사전 초기화
  @param	dir	리소스 위치
  @param	qdomain	도메인
  */
  void init_morp(char *dir = (char *) "../rsc",
                 QADOMAIN qdomain = ALL_DOMAIN);    // 형태소 분석기 생성 및 사전 초기화
  /**
  @brief	형태소 분석기 생성 및 사전 초기화 for multi-thread (전역변수 초기화)
  @param	dir	리소스 위치
  @param	qdomain	도메인
  */
  void init_morp_global(char *dir = (char *) "../rsc",
                        QADOMAIN qdomain = ALL_DOMAIN);    // for multi-thread
  /**
  @brief	형태소 분석기 생성 및 사전 초기화 for multi-thread
  @param	dir	리소스 위치
  */
  void init_morp_thread(char *dir = (char *) "../rsc");    // for multi-thread
  /**
  @brief	형태소 분석기 종료 및 finalize
  */
  void close_morp();                // 형태소 분석기 종료 및 finalize
  /**
  @brief	형태소 분석기 종료 및 finalize for multi-thread
  */
  void close_morp_global();        // for multi-thread
  /**
  @brief	형태소 분석기 종료 및 finalize for multi-thread
  */
  void close_morp_thread();        // for multi-thread
  void get_sent(map<long, string> &sentList);  // 이충희 사용코드

  // ATR
  /**
  @brief	개체명인식기 초기화
  @param	dir	리소스 위치
  @param	qdomain	도메인
  */
  void init_NER(char *dir = (char *) "../rsc",
                QADOMAIN qdomain = BLOG_DOMAIN); // AT인식기 초기화, LMInterface내부의 m_doc 사용
  void init_NER_NO_DOC(char *dir = (char *) "../rsc",
                       QADOMAIN qdomain = BLOG_DOMAIN);      // AT인식기 초기화, m_doc변수 지정이 요구됨
  /**
  @brief	개체명인식기 초기화 for multi-thread (전역변수 초기화)
  @param	dir	리소스 위치
  @param	qdomain	도메인
  */
  void init_NER_global(char *dir = (char *) "../rsc",
                       QADOMAIN qdomain = BLOG_DOMAIN);      // AT인식기 초기화 for multi-thread
  /**
  @brief	개체명인식기 초기화 for multi-thread
  @param	dir	리소스 위치
  */
  void init_NER_thread
      (char *dir = (char *) "../rsc");      // AT인식기 초기화 for multi-thread (init_NER_global()이 선행되어야 함)
  /**
  @brief	형태소분석
  @param	text	형태소분석 대상 텍스트
  */
  void
      do_preNER(string &text);                      // 형태소 분석 이후 doc구조 생성, 괄호 포함
  void do_preNER(ESTk_ASR_hypothesis *asr);     // 음성인식기 결과를 받음
  void do_preNER
      (string &, struct Doc *);         // 형태소 분석 이후 지정된 doc구조 생성, 괄호 포함
  void do_preNER_with_P(string &);               // 형태소 분석 이후 doc구조 생성, 괄호 제거
  void do_preNER_with_P
      (string &, struct Doc *);  // 형태소 분석 이후 지정된 doc구조 생성, 괄호 제거
  void do_preNER_no_divide_sentence
      (string &, struct Doc *);        // 형태소 분석 이후 지정된 doc구조 생성, 문장분할 안함, 괄호 포함
  void do_preNER_no_divide_sentence_with_P
      (string &, struct Doc *); // 형태소 분석 이후 지정된 doc구조 생성, 문장분할 안함, 괄호 제거
  /**
  @brief	개체명분석
  */
  void do_ATR();                    // AT인식 진행
  void do_ATR(struct Doc *p);        // 지정된 doc구조를 기반으로 AT인식 진행
  void set_Doc_LM
      (struct Doc *p); // AT인식기와 CHUNK의 대상 m_doc구조 지정, 다중 doc 구조를 대상으로 작업할 때 사용됨
  void print_ATR();                // AT인식 결과 출력
  void print_ATR(struct Doc *p);  // 지정된 doc구조체를 대상으로 AT인식 결과 출력
  string sprint_ATR(bool print_feat = false);            // AT인식 결과 출력
  ///< leeck이 추가
  void print_ATR2(FILE *);                // AT인식 결과 출력

  void close_NER();               // 개체명 인식기 종료
  void close_NER_global();        // 개체명 인식기 종료 for multi-thread
  void close_NER_thread();        // 개체명 인식기 종료 for multi-thread

  // 문서분류기
  void init_cl(char *dir = (char *) "../rsc",
               QADOMAIN qdomain = ALL_DOMAIN,
               bool use_bin_model = true);    // 초기화
  void init_cl_global(char *dir = (char *) "../rsc",
                      QADOMAIN qdomain = ALL_DOMAIN,
                      bool use_bin_model = true);    // 초기화
  void init_cl_thread
      (char *dir = (char *) "../rsc", QADOMAIN qdomain = ALL_DOMAIN);    // 초기화
  void do_cl
      (string text, string cat = "");        // 분류기 실행(bigram): m_doc에 결과 저장
  void do_cl(struct Doc *doc,
             string cat = "");        // 분류기 실행(NP-based bigram): m_doc에 결과 저장
  void do_cl_nbest(string text,
                   string cat = "",
                   int nbest = 2);    // n-best 분류기 실행(bigram): m_doc에 결과 저장
  void do_cl_nbest(struct Doc *doc,
                   string cat = "",
                   int nbest = 2);    // n-best 분류기 실행(NP-based bigram): m_doc에 결과 저장
  string get_parent(string category);    // 부모 클래스 리턴 (최상위일 경우 "" 리턴)
  void close_cl();            // 분류기 종료
  void close_cl_global();            // 분류기 종료
  void close_cl_thread();            // 분류기 종료

  // 띄어쓰기
  void init_space(const char *dir = "../rsc", QADOMAIN qdomain = ALL_DOMAIN);
  string do_space(string input);
  void close_space();

  // 청킹
  void init_Chunk_NO_DOC(char *rsc_dir = (char *)"../rsc",
		  QADOMAIN qdomain = ALL_DOMAIN);
  void init_Chunk_NO_DOC_global(char *rsc_dir = (char *)"../rsc",
		  QADOMAIN qdomain = ALL_DOMAIN);
  void init_Chunk_NO_DOC_thread(char *rsc_dir = (char *)"../rsc");

  void do_Chunk(struct Doc *p);
  void print_Chunk(struct Doc *p);
  void print_EChunk(struct Doc *p);

  void close_Chunk();
  void close_Chunk_global();
  void close_Chunk_thread();

  // 청킹 for test
  void init_Chunk (char *rsc_dir = (char *)"../rsc",
		  QADOMAIN qdomain = ALL_DOMAIN);
  void init_Chunk_global(char *rsc_dir = (char *) "../rsc",
		  QADOMAIN qdomain = ALL_DOMAIN);
  void init_Chunk_thread(char *rsc_dir = (char *) "../rsc");
  void do_Chunk();
  void print_Chunk();
  void print_EChunk();

  // 의존 관계 분석기
  void init_DParse(char *rsc_dir = (char *) "../rsc",
                   QADOMAIN qdomain = ALL_DOMAIN);
  void init_DParse_global(char *rsc_dir = (char *) "../rsc",
                          QADOMAIN qdomain = ALL_DOMAIN);
  void init_DParse_thread(char *rsc_dir = (char *) "../rsc");

  /**
  @brief	의존구분분석
  @param	ndoc	분석할 구조체
  */
  void do_DParse(N_Doc &ndoc);
  void print_DParse(N_Doc &ndoc);
  void print_DParse(FILE *out, N_Doc &ndoc);

  void close_DParse();
  void close_DParse_global();
  void close_DParse_thread();

  // SRL
  void init_srl
      (char *rsc_dir = (char *) "../rsc", QADOMAIN qdomain = ALL_DOMAIN);
  void do_srl(N_Doc &ndoc);
  void do_srl(N_Doc &ndoc, vector<vector<word_t> > &result_doc);
  string sprint_srl(N_Doc &ndoc, vector<vector<word_t> > &result_doc);
  void close_srl();

  // Relation Extraction
  void init_Rel(char *dir, QADOMAIN qdomain = ALL_DOMAIN);
  void init_Rel_global
      (char *dir, QADOMAIN qdomain = ALL_DOMAIN);    // multi-thread
  void init_Rel_thread(char *dir);    // multi-thread

  void close_Rel();
  void close_Rel_global();    // multi-thread
  void close_Rel_thread();    // multi-thread

  void do_Rel(N_Doc &ndoc);

  // Sentiment Analysis
  void init_SA(char *rsc_dir = (char *) "../rsc",
               QADOMAIN qdomain = ALL_DOMAIN);
  void do_SA(N_Doc &ndoc, string ndocJSONStr);
  void do_SA(N_Doc &ndoc);
  void close_SA();


  // 통합
  // Doc 구조체로부터 NDoc 생성
  /**
  @brief	Doc 구조체로부터 NDoc 생성
  @param	doc	Doc 구조체
  @param	ndoc	ndoc 구조체
  */
  void make_ndoc(Doc *doc, N_Doc &ndoc);
  // JSON 포맷
  // string sprint_JSON(N_Doc &ndoc);
  // clear Doc
  void clear_Doc();
  void clear_Doc(struct Doc *p);

  int get_AT_entry(char *p_char, int p_at_list[]);

  CMorpAnalyzer *getMorpAnal() { return m_scan; }
  struct Doc *getDoc() { return &m_Doc; }

  //Utility function

  // 형태소 id에 대응하는 Word id 반환
  int getWordID(int morpid, vector<N_Word> &vecWord);
};


#endif // LM_INTERFACE_H_
