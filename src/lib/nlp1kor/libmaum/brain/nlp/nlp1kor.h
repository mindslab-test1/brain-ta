#ifndef NLP1KOR_H
#define NLP1KOR_H

#include "libmaum/brain/nlp/nlp.h"

using maum::brain::nlp::NlpAnalysisLevel;
using std::vector;

class LMInterface;
class N_Doc;

class Nlp1Kor : public Nlp {
 public:
  Nlp1Kor(const string &res_path, int nlp_feature_mask)
      : Nlp(res_path, nlp_feature_mask) {
  }
  virtual ~Nlp1Kor() {
  }

  void UpdateFeatures() override;
  void AnalyzeOne(const InputText *text, Document *document) override;
  void Uninit() override;

  void SetClassifierModel(const string &classifier_model_path) {};
  void Classify(const Document &document, ClassifiedSummary *sum) {};
  void GetPosTaggedStr(const Document *document,
                       vector<string> *pos_tagged_str) {};
  void GetNerTaggedStr(const Document *document,
                       vector<string> *ner_tagged_str) {};
  void GetPosNerTaggedStr(const Document *document,
                       vector<string> *pos_ner_tagged_str) {};
 private:
  // 멀티쓰레드를 사용하기 위해
  LMInterface *global_lmi_;
  // one-word set
  std::unordered_set<std::string> one_set_;
  // stop-word set
  std::unordered_set<std::string> stop_set_;
  // ne-tag set
  std::map<std::string, std::string> ne_tag_;
  // synonym-tag set
  std::map<std::string, std::string> syn_set_;

  bool HasExecFeature(int &exec_nlp_feature_mask, NlpFeature feature) {
    return (exec_nlp_feature_mask & (1 << int(feature))) > 0;
  }
  void ToMessageWithKeywordFrequency(const N_Doc &ndoc, Document *document);
  void ToMessage(const N_Doc &ndoc,
                 Document *document,
                 int &exec_nlp_feature_mask);

  int FindStopWord(const std::string &str) {
    string word(str, 0, str[0] >= 0 ? 1 : 2);
#if 0
    if (str[0] >= 0) {
      word = str[0];
    } else {
      word += str[0];
      word += str[1];
    }
#endif
    return (stop_set_.find(word) == stop_set_.end()) ? 0 : 1;
  }

  int FindOneWord(const std::string &str) {
    return (one_set_.find(str) == one_set_.end()) ? 0 : 1;
  }
};

#endif //NLP1KOR_H
