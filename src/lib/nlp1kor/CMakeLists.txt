cmake_minimum_required(VERSION 2.8.12)

project(brain-nlp1kor CXX)
set(OUTLIB brain-nlp1kor)

include(import)

msl_import_binary(NLP1_DIR NLP1_MADE "NLP1"
    etri-nlp-kor1-190104.tar.gz)

include_directories(../common)
include_directories(../../proto)
include_directories(${NLP1_INCLUDE})
include_directories(${NLP1_INCLUDE}/Lheader)
link_directories(${NLP1_LIB})
add_definitions(-D_UNIX)


set(CMAKE_CXX_STANDARD 11)
if (CMAKE_COMPILER_IS_GNUCXX)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
endif ()
set(CMAKE_CXX_FLAGS
    "${CMAKE_CXX_FLAGS} -Wno-unknown-pragmas -Wno-sign-compare -Wno-unused-local-typedefs -Wno-deprecated")

include_directories(.)
include_directories(../common)
include_directories(../include)
include_directories(../../../proto)
include_directories("${CMAKE_INSTALL_PREFIX}/include")
LINK_DIRECTORIES("${CMAKE_INSTALL_PREFIX}/lib")

set(SOURCE_FILES
    lm-interface.cc lm-interface.h
    nlp1kor.cc libmaum/brain/nlp/nlp1kor.h
    )

include_directories("${CMAKE_INSTALL_PREFIX}/include")
LINK_DIRECTORIES("${CMAKE_INSTALL_PREFIX}/lib")

add_library(${OUTLIB} SHARED ${SOURCE_FILES} ${NLP1_MADE})

if (APPLE)
  set_target_properties(${OUTLIB}
      PROPERTIES MACOSX_RPATH ${CMAKE_INSTALL_PREFIX}/lib)
endif ()

target_link_libraries(${OUTLIB}
    sa rel srl parser chunk ner scan stag SR space crf
    #GetNDoc #GetSDoc
    maum-common
    maum-json
    brain-ta-pb
    protobuf
    pthread
    )

install(TARGETS ${OUTLIB}
    RUNTIME DESTINATION bin COMPONENT libraries
    LIBRARY DESTINATION lib COMPONENT libraries
    ARCHIVE DESTINATION lib COMPONENT libraries)

install(DIRECTORY libmaum/ DESTINATION include/libmaum
    FILES_MATCHING PATTERN "*.h")

