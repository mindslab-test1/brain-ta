#ifndef TA_ERROR_H
#define TA_ERROR_H

#include <iostream>
#include <sstream>
#include <string>

using namespace std;

namespace ta {

 /**
  * brain-ta 초기 리소스 로딩 시 발생하는 오류에대한 내용을 담은 클래스
  */
 class Exception : public std::exception {
 public:
  Exception();
  Exception(const string &m = "", const string &msg = "") : module_(m), msg_(msg) {};

  void SetModule(const string &module) {
    module_ = module;
  }

  void SetMessage(const string &msg) {
    msg_ = msg;
  }

  void SetInfo(const string &module, const string &msg) {
    module_ = module;
    msg_ = msg;
  }

  string GetModule() {
    return module_;
  }

  string GetMessage() {
    return msg_;
  }

  string ShowMessage() {
    ostringstream res;
    res << '[' << module_ << ']' << msg_;
    cout << res.str() << endl;
    return res.str();
  }

 private:
  string module_; // NLP2, HMD 등 모듈 이름
  string msg_; // 오류 내용
};

}

#endif // TA_ERROR_H
