#ifndef NLP_H
#define NLP_H

#include <string>
#include "maum/brain/nlp/nlp.grpc.pb.h"
#include "maum/brain/cl/classifier.grpc.pb.h"

using std::string;
using std::vector;
using maum::brain::nlp::NlpAnalysisLevel;
using maum::brain::nlp::Document;
using maum::brain::nlp::InputText;
using maum::brain::nlp::NlpFeature;
using maum::brain::cl::ClassifiedSummary;

class Nlp {
 public:
  Nlp(const string &res_path, int nlp_feature_mask) {
    Init(res_path, nlp_feature_mask);
  }
  virtual ~Nlp() {}

  void SetFeatureMask(int nlp_feature_mask) {
    nlp_feature_mask_ = nlp_feature_mask;
  }

  int GetFeatureMask() {
    return nlp_feature_mask_;
  }

  bool HasFeature(NlpFeature feature) {
    return (nlp_feature_mask_ & (1 << int(feature))) > 0;
  }

  void Init(const string &res_path, int nlp_feature_mask) {
    res_path_ = res_path;
    nlp_feature_mask_ = nlp_feature_mask;
  }

  virtual void UpdateFeatures() = 0;
  virtual void Uninit() = 0;

  // nlp2 only
  virtual void SetClassifierModel(const string &classifier_model_path) = 0;
  virtual void Classify(const Document &document, ClassifiedSummary *sum) = 0;
  // all
  virtual void AnalyzeOne(const InputText *text, Document *document) = 0;
  virtual void GetPosTaggedStr(const Document *document,
                               vector<string> *pos_tagged_str) = 0;
  virtual void GetNerTaggedStr(const Document *document,
                               vector<string> *ner_tagged_str) = 0;
  virtual void GetPosNerTaggedStr(const Document *document,
                               vector<string> *pos_ner_tagged_str) = 0;

 protected:
  int nlp_feature_mask_ = 0;
  string res_path_;
};

#endif //NLP_H
