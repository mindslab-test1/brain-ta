# src/lib은 강원대학교(이창기 교수님)에서 전달받은 소스를 library형태로 만들기 위함
# 다음과 같은 폴더로 이루어짐
 - include
 - nlp2eng
 - nlp2kor

# include
 - eng와 kor에서 공통적으로 사용하는 header파일들을 모아두었음

# nlp2eng
 - ``` CMakeLists.txt ``` 를 통해 ``` brain-nlp2eng ``` library 파일을 생성
 - 구성된 모듈
   - ``` tagger ```
   - ``` dnn classification ```
   - ``` ner ```
 - 구성된 유틸리티
   - ``` svm ```
   - ``` gru ```, ``` lstm ``` encoder

# nlp2kor
  - ``` CMakeLists.txt ``` 를 통해 ``` brain-nlp2kor ``` library 파일을 생성
  - 구성된 모듈
    - ``` tagger ```
    - ``` ner ```
    - ``` dnn classification ```
  - 구성된 유틸리티
    - ``` svm ```
    - ``` seq2label ```
