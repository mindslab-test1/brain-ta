#include <string.h>
#include <stdio.h>
#include <stdexcept> //for std::runtime_error
#include <memory>    //for std::bad_alloc
#include "kor-ner.h"

#pragma warning(disable: 4786)
#pragma warning(disable: 4996)
#pragma warning(disable: 4267)
#pragma warning(disable: 4244)
#pragma warning(disable: 4018)

using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Kor_NER::Kor_NER() {
  beam = 0;
  verbose = 0;
}

Kor_NER::~Kor_NER() {
}


int Kor_NER::init(const string &dir) {
  string file;
  FILE *in;
  char strtmp[1001];

  // beam
  ner_ssvm.beam = beam;
  // ssvm
  file = dir + "/" + "kor_ner_model.bin";
  try {
    ner_ssvm.load_bin(file);
  } catch (exception e) {
    cerr << "Error: " << e.what() << endl;
    return 1;
  }

  // POS dic 포함
  file = dir + "/" + "dic.txt";
  cerr << "load " << file << " ... ";
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    return 0;
  }
  while (!feof(in)) {
    fgets(strtmp, 1000, in);
    if (strtmp[0] == ';') continue;
    // fgets의 맨뒤 newline 처리
    strtmp[strlen(strtmp)] = '\0';
    if (strlen(strtmp) > 0 && strtmp[strlen(strtmp) - 1] == '\n')
      strtmp[strlen(strtmp) - 1] = '\0';
    nedic[strtmp] = "n";
  }
  fclose(in);
  cerr << "done." << endl;

  // 대분류 개체명 사전
  file = dir + "/" + "ne_dic.txt";
  cerr << "load " << file << " ... ";
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    return 0;
  }
  while (!feof(in)) {
    fgets(strtmp, 1000, in);
    if (strtmp[0] == ';') continue;
    // fgets의 맨뒤 newline 처리
    strtmp[strlen(strtmp)] = '\0';
    if (strlen(strtmp) > 0 && strtmp[strlen(strtmp) - 1] == '\n')
      strtmp[strlen(strtmp) - 1] = '\0';
    vector<string> token;
    tokenize(strtmp, token, " \t\r\n");
    if (token.size() == 2) {
      nedic[token[0]] = token[1];
    } else {
      cerr << "Warning: " << strtmp << endl;
    }
  }
  fclose(in);
  cerr << "done." << endl;

  // cluster dic: nc tag만 적용함 (nc가 아닌 단어는 삭제함. nc tag는 삭제)
  // 수정됨: cluster.korean.100.txt, nng|nnp|sl|xr|va|vv|mag 에 포함되지 않는 것은 제거, /tag 정보 제거, 1음절 제거
  file = dir + "/" + "cluster_dic.txt";
  cerr << "load " << file << " ... ";
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    return 0;
  }
  while (!feof(in)) {
    fgets(strtmp, 1000, in);
    if (strtmp[0] == ';') continue;
    // fgets의 맨뒤 newline 처리
    strtmp[strlen(strtmp)] = '\0';
    if (strlen(strtmp) > 0 && strtmp[strlen(strtmp) - 1] == '\n')
      strtmp[strlen(strtmp) - 1] = '\0';
    vector<string> token;
    tokenize(strtmp, token, " \t\r\n");
    if (token.size() == 2) {
      cldic[token[1]] = "CL" + token[0];
    } else {
      cerr << "Warning: " << strtmp << endl;
    }
  }
  fclose(in);
  cerr << "done." << endl;

  return 0;
}


void Kor_NER::close() {
  nedic.clear();
  cldic.clear();
  ner_ssvm.clear();
}


/// 입력문장을 NER tagging
double Kor_NER::do_NER(K_Sentence &k_sent) {
  vector<string> &syllable = k_sent.syllable;
  vector<string> &space = k_sent.space;   // 어절의 시작(B), 중간(I) 정보를 저장
  vector<string> &POS_outcome = k_sent.POS_outcome;
  vector<int> &sid2wid = k_sent.sid2wid;  // syllable id --> word id

  // feature for NER
  vector<vector<int> > ner_feature;
  ner_feature.reserve(syllable.size());
  make_ner_feature_int(syllable, space, POS_outcome, ner_feature);

  // 피쳐 출력
  if (verbose) {
    vector<vector<string> > ner_str_feature;
    make_ner_feature(syllable, space, POS_outcome, ner_str_feature);
    for (int k = 0; k < ner_str_feature.size(); k++) {
      for (int l = 0; l < ner_str_feature[k].size(); l++) {
        cout << ner_str_feature[k][l] << " ";
      }
      cout << endl;
    }
  }

  // SSVM for NER
  vector<string> ner_outcome;
  ner_outcome.reserve(ner_feature.size());
  ksent_t ner_sent;
  ner_sent.reserve(ner_feature.size());
  ner_ssvm.make_sent(ner_feature, ner_sent);
  double prob = ner_ssvm.eval(ner_sent, ner_outcome);

  // NER result
  // doc.sent.NE
  K_NE ne;
  ne.id = 0;
  ne.begin = -1;
  ne.end = -1;

  string prev_tag;
  for (int i = 0; i < ner_outcome.size(); i++) {
    vector<string> BI_tag;
    tokenize(ner_outcome[i], BI_tag, "-");
    // B or I
    if (BI_tag.size() == 2) {
      string BI = BI_tag[0];
      string tag = BI_tag[1];
      if (BI == "B") {
        if (i > 0 && ner_outcome[i - 1][0] != 'O') {
          k_sent.NE.push_back(ne);
          ne.id++;
          ne.text = "";
        }
        ne.type = tag;
        ne.text = syllable[i];
        ne.begin = sid2wid[i];
        ne.begin_sid = i;
      } else if (BI == "I") {
        if (space[i] == "B") ne.text += ' ';
        ne.text += syllable[i];
        ne.end = sid2wid[i];
        ne.end_sid = i;
      }
      prev_tag = tag;
    } else {
      // O
      if (i > 0 && ner_outcome[i - 1][0] != 'O') {
        k_sent.NE.push_back(ne);
        ne.id++;
        ne.text = "";
      }
    }
    if (verbose) cout << "# " << syllable[i] + "\t" + ner_outcome[i] << endl;
  }
  if (ner_outcome.size() > 0 && ner_outcome[ner_outcome.size() - 1][0] != 'O') {
    k_sent.NE.push_back(ne);
  }
  if (verbose) cout << endl;
  return prob;
}


/// return the type of syllable: 한글, 한자 등을 검사 (EUC-KR 기준)
string Kor_NER::get_str_type(const string &str) {
  if (str.length() == 1) {
    char c = str[0];
    if (isdigit(c)) {
      return "1";
    } else if (isalpha(c)) {
      if (isupper(c)) return "A";
      else return "a";
    } else if (strchr("~!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?", c) != NULL) {
      return "S";
    }
  } else if (str.length() == 2) {
    unsigned int c = (unsigned char) str.at(0);
    if (strncmp(str.c_str(), "\x0b\xa1"/*"가"*/, 2) >= 0
        && strncmp(str.c_str(), "\xc8\xfe"/*"힝"*/, 2) <= 0) {
      //cerr << str << " " << "Korea ";
      return "K";
    } else if (c >= 0xCA && c <= 0xFD) {
      //cerr << str << " " << "Chinese ";
      return "C";
    }
    //cerr << 0xCA << " " << str << " " << c << " ";
  }
  return "O";
}

/**
    NER feature
    @param syllables, spaces, POS's outcome
    @return feature_vec
*/
void Kor_NER::make_ner_feature(vector<string> &syllables,
                               vector<string> &space,
                               vector<string> &pos,
                               vector<vector<string> > &feature_vec) {
  vector<string> feature;
  feature.reserve(1024);
  // lexical
  string ppp, pp, p, c, n, nn, nnn;
  // space
  string ppps, pps, ps, cs, ns, nns, nnns;
  // type
  string pppt, ppt, pt, ct, nt, nnt, nnnt;
  // pos
  string ppp_pos, pp_pos, p_pos, c_pos, n_pos, nn_pos, nnn_pos;

  for (int i = 0; i < syllables.size(); i++) {
    p = c = n = ps = cs = ns = pt = ct = nt = p_pos = c_pos = n_pos = "";

    c = syllables[i];
    c_pos = pos[i];
    ct = get_str_type(c);
    if (space[i] == "B") cs = "_";

    if (i - 1 >= 0) {
      p = syllables[i - 1];
      p_pos = pos[i - 1];
      pt = get_str_type(p);
      if (space[i - 1] == "B") ps = "_";
    }
    if (i + 1 < syllables.size()) {
      n = syllables[i + 1];
      n_pos = pos[i + 1];
      nt = get_str_type(n);
      if (space[i + 1] == "B") ns = "_";
    }
    // window >= 2
    pp = nn = pps = nns = ppt = nnt = pp_pos = nn_pos = "";
    if (i - 2 >= 0) {
      pp = syllables[i - 2];
      pp_pos = pos[i - 2];
      ppt = get_str_type(pp);
      if (space[i - 2] == "B") pps = "_";
    }
    if (i + 2 < syllables.size()) {
      nn = syllables[i + 2];
      nn_pos = pos[i + 2];
      nnt = get_str_type(nn);
      if (space[i + 2] == "B") nns = "_";
    }
    // window >= 3
    ppp = nnn = ppps = nnns = pppt = nnnt = ppp_pos = nnn_pos = "";
    if (i - 3 >= 0) {
      ppp = syllables[i - 3];
      ppp_pos = pos[i - 3];
      pppt = get_str_type(ppp);
      if (space[i - 3] == "B") ppps = "_";
    }
    if (i + 3 < syllables.size()) {
      nnn = syllables[i + 3];
      nnn_pos = pos[i + 3];
      nnnt = get_str_type(nnn);
      if (space[i + 3] == "B") nnns = "_";
    }

    feature.clear();

    // lexical feature: window = 3
    // unigram
    feature.push_back("L0=" + cs + c);
    feature.push_back("L-1=" + ps + p);
    feature.push_back("L1=" + ns + n);
    // bigram
    feature.push_back("L-2-1=" + pps + pp + ps + p);
    feature.push_back("L-10=" + ps + p + cs + c);
    feature.push_back("L01=" + cs + c + ns + n);
    feature.push_back("L12=" + ns + n + nns + nn);
    // trigram
    feature.push_back("L-3-1=" + ppps + ppp + pps + pp + ps + p);
    feature.push_back("L-20=" + pps + pp + ps + p + cs + c);
    feature.push_back("L-11=" + ps + p + cs + c + ns + n);
    feature.push_back("L02=" + cs + c + ns + n + nns + nn);
    feature.push_back("L13=" + ns + n + nns + nn + nnns + nnn);

    // isalpha, isupper, islower, isdigit features: window = 3
    // window >= 3
    if (ppp != "" && pppt != "O") {
      feature.push_back("p3" + pppt);
      if (ppt != "O") feature.push_back("p3" + pppt + ppt);
      if (pt != "O") feature.push_back("p3" + pppt + ppt + pt);
    }
    // window >= 2
    if (pp != "" && ppt != "O") {
      feature.push_back("p2" + ppt);
      if (pt != "O") feature.push_back("p2" + ppt + pt);
      if (ct != "O") feature.push_back("p2" + ppt + pt + ct);
    }
    if (p != "" && pt != "O") {
      feature.push_back("p" + pt);
      if (ct != "O") feature.push_back("p" + pt + ct);
      if (n != "" && nt != "O") feature.push_back("p" + pt + ct + nt);
    }
    if (c != "" && ct != "O") {
      feature.push_back("c" + ct);
      if (n != "" && nt != "O") feature.push_back("c" + ct + nt);
      if (nn != "" && nnt != "O") feature.push_back("c" + ct + nt + nnt);
    }
    if (n != "" && nt != "O") {
      feature.push_back("n" + nt);
      if (nn != "" && nnt != "O") feature.push_back("n" + nt + nnt);
      // window >= 3
      if (nnn != "" && nnnt != "O") feature.push_back("n" + nt + nnt + nnnt);
    }
    // window >= 3
    if (nn != "" && nnt != "O") {
      feature.push_back("n2" + nnt);
      if (nnn != "" && nnnt != "O") feature.push_back("n2" + nnt + nnnt);
    }
    if (nnn != "" && nnnt != "O") {
      feature.push_back("n3" + nnnt);
    }

    // nedic feature: 2~7음절 검사 (앞뒤 3음절) : : longest-match 적용
    // next_pos는 longest-match 적용 후, 두번째 단어의 시작 위치
    int next_pos = -3;
    // -3 ~
    if (i - 3 >= 0) {
      if (i + 3 < syllables.size()
          && nedic.find(ppp + pp + p + c + n + nn + nnn) != nedic.end()) {
        feature.push_back("-33" + nedic[ppp + pp + p + c + n + nn + nnn]);
        next_pos = 4;
      }
      else if (i + 2 < syllables.size()
          && nedic.find(ppp + pp + p + c + n + nn) != nedic.end()) {
        feature.push_back("-32" + nedic[ppp + pp + p + c + n + nn]);
        next_pos = 3;
      }
      else if (i + 1 < syllables.size()
          && nedic.find(ppp + pp + p + c + n) != nedic.end()) {
        feature.push_back("-31" + nedic[ppp + pp + p + c + n]);
        next_pos = 2;
      }
      else if (nedic.find(ppp + pp + p + c) != nedic.end()) {
        feature.push_back("-30" + nedic[ppp + pp + p + c]);
        next_pos = 1;
      }
      else if (nedic.find(ppp + pp + p) != nedic.end()) {
        feature.push_back("-3-1" + nedic[ppp + pp + p]);
        next_pos = 0;
      }
      else next_pos = -2;
    }
    // -2 ~
    if (i - 2 >= 0 && next_pos <= -2) {
      if (i + 3 < syllables.size()
          && nedic.find(pp + p + c + n + nn + nnn) != nedic.end()) {
        feature.push_back("-23" + nedic[pp + p + c + n + nn + nnn]);
        next_pos = 4;
      }
      else if (i + 2 < syllables.size()
          && nedic.find(pp + p + c + n + nn) != nedic.end()) {
        feature.push_back("-22" + nedic[pp + p + c + n + nn]);
        next_pos = 3;
      }
      else if (i + 1 < syllables.size()
          && nedic.find(pp + p + c + n) != nedic.end()) {
        feature.push_back("-21" + nedic[pp + p + c + n]);
        next_pos = 2;
      }
      else if (nedic.find(pp + p + c) != nedic.end()) {
        feature.push_back("-20" + nedic[pp + p + c]);
        next_pos = 1;
      }
      else if (nedic.find(pp + p) != nedic.end()) {
        feature.push_back("-2-1" + nedic[pp + p]);
        next_pos = 0;
      }
      else next_pos = -1;
    }
    // -1 ~
    if (i - 1 >= 0 && next_pos <= -1) {
      if (i + 3 < syllables.size()
          && nedic.find(p + c + n + nn + nnn) != nedic.end()) {
        feature.push_back("-13" + nedic[p + c + n + nn + nnn]);
        next_pos = 4;
      }
      else if (i + 2 < syllables.size()
          && nedic.find(p + c + n + nn) != nedic.end()) {
        feature.push_back("-12" + nedic[p + c + n + nn]);
        next_pos = 3;
      }
      else if (i + 1 < syllables.size()
          && nedic.find(p + c + n) != nedic.end()) {
        feature.push_back("-11" + nedic[p + c + n]);
        next_pos = 2;
      }
      else if (nedic.find(p + c) != nedic.end()) {
        feature.push_back("-10" + nedic[p + c]);
        next_pos = 1;
      }
      else next_pos = 0;
    }
    // 0 ~
    if (next_pos <= 0) {
      if (i + 3 < syllables.size()
          && nedic.find(c + n + nn + nnn) != nedic.end()) {
        feature.push_back("03" + nedic[c + n + nn + nnn]);
        next_pos = 4;
      }
      else if (i + 2 < syllables.size()
          && nedic.find(c + n + nn) != nedic.end()) {
        feature.push_back("02" + nedic[c + n + nn]);
        next_pos = 3;
      }
      else if (i + 1 < syllables.size() && nedic.find(c + n) != nedic.end()) {
        feature.push_back("01" + nedic[c + n]);
        next_pos = 2;
      }
      else next_pos = 1;
    }
    // 1 ~
    if (next_pos <= 1) {
      if (i + 3 < syllables.size() && nedic.find(n + nn + nnn) != nedic.end()) {
        feature.push_back("13" + nedic[n + nn + nnn]);
        next_pos = 4;
      }
      else if (i + 2 < syllables.size() && nedic.find(n + nn) != nedic.end()) {
        feature.push_back("12" + nedic[n + nn]);
        next_pos = 3;
      }
      else next_pos = 2;
    }

    // CL dic feature: 2~7음절 검사 (앞뒤 3음절) : longest-match 적용
    // next_pos는 longest-match 적용 후, 두번째 단어의 시작 위치
    next_pos = -3;
    // -3 ~
    if (i - 3 >= 0) {
      if (i + 3 < syllables.size()
          && cldic.find(ppp + pp + p + c + n + nn + nnn) != cldic.end()) {
        feature.push_back("-33" + cldic[ppp + pp + p + c + n + nn + nnn]);
        next_pos = 4;
      }
      else if (i + 2 < syllables.size()
          && cldic.find(ppp + pp + p + c + n + nn) != cldic.end()) {
        feature.push_back("-32" + cldic[ppp + pp + p + c + n + nn]);
        next_pos = 3;
      }
      else if (i + 1 < syllables.size()
          && cldic.find(ppp + pp + p + c + n) != cldic.end()) {
        feature.push_back("-31" + cldic[ppp + pp + p + c + n]);
        next_pos = 2;
      }
      else if (cldic.find(ppp + pp + p + c) != cldic.end()) {
        feature.push_back("-30" + cldic[ppp + pp + p + c]);
        next_pos = 1;
      }
      else if (cldic.find(ppp + pp + p) != cldic.end()) {
        feature.push_back("-3-1" + cldic[ppp + pp + p]);
        next_pos = 0;
      }
      else next_pos = -2;
    }
    // -2 ~
    if (i - 2 >= 0 && next_pos <= -2) {
      if (i + 3 < syllables.size()
          && cldic.find(pp + p + c + n + nn + nnn) != cldic.end()) {
        feature.push_back("-23" + cldic[pp + p + c + n + nn + nnn]);
        next_pos = 4;
      }
      else if (i + 2 < syllables.size()
          && cldic.find(pp + p + c + n + nn) != cldic.end()) {
        feature.push_back("-22" + cldic[pp + p + c + n + nn]);
        next_pos = 3;
      }
      else if (i + 1 < syllables.size()
          && cldic.find(pp + p + c + n) != cldic.end()) {
        feature.push_back("-21" + cldic[pp + p + c + n]);
        next_pos = 2;
      }
      else if (cldic.find(pp + p + c) != cldic.end()) {
        feature.push_back("-20" + cldic[pp + p + c]);
        next_pos = 1;
      }
      else if (cldic.find(pp + p) != cldic.end()) {
        feature.push_back("-2-1" + cldic[pp + p]);
        next_pos = 0;
      }
      else next_pos = -1;
    }
    // -1 ~
    if (i - 1 >= 0 && next_pos <= -1) {
      if (i + 3 < syllables.size()
          && cldic.find(p + c + n + nn + nnn) != cldic.end()) {
        feature.push_back("-13" + cldic[p + c + n + nn + nnn]);
        next_pos = 4;
      }
      else if (i + 2 < syllables.size()
          && cldic.find(p + c + n + nn) != cldic.end()) {
        feature.push_back("-12" + cldic[p + c + n + nn]);
        next_pos = 3;
      }
      else if (i + 1 < syllables.size()
          && cldic.find(p + c + n) != cldic.end()) {
        feature.push_back("-11" + cldic[p + c + n]);
        next_pos = 2;
      }
      else if (cldic.find(p + c) != cldic.end()) {
        feature.push_back("-10" + cldic[p + c]);
        next_pos = 1;
      }
      else next_pos = 0;
    }
    // 0 ~
    if (next_pos <= 0) {
      if (i + 3 < syllables.size()
          && cldic.find(c + n + nn + nnn) != cldic.end()) {
        feature.push_back("03" + cldic[c + n + nn + nnn]);
        next_pos = 4;
      }
      else if (i + 2 < syllables.size()
          && cldic.find(c + n + nn) != cldic.end()) {
        feature.push_back("02" + cldic[c + n + nn]);
        next_pos = 3;
      }
      else if (i + 1 < syllables.size() && cldic.find(c + n) != cldic.end()) {
        feature.push_back("01" + cldic[c + n]);
        next_pos = 2;
      }
      else next_pos = 1;
    }
    // 1 ~
    if (next_pos <= 1) {
      if (i + 3 < syllables.size() && cldic.find(n + nn + nnn) != cldic.end()) {
        feature.push_back("13" + cldic[n + nn + nnn]);
        next_pos = 4;
      }
      else if (i + 2 < syllables.size() && cldic.find(n + nn) != cldic.end()) {
        feature.push_back("12" + cldic[n + nn]);
        next_pos = 3;
      }
      else next_pos = 2;
    }

    // POS feature: window = 3
    // unigram
    feature.push_back("P0=" + c_pos);
    feature.push_back("P-1=" + p_pos);
    feature.push_back("P1=" + n_pos);
    // bigram
    feature.push_back("P-2-1=" + pp_pos + "_" + p_pos);
    feature.push_back("P-10=" + p_pos + "_" + c_pos);
    feature.push_back("P01=" + c_pos + "_" + n_pos);
    feature.push_back("P12=" + n_pos + "_" + nn_pos);
    // trigram
    feature.push_back("P-3-1=" + ppp_pos + "_" + pp_pos + "_" + p_pos);
    feature.push_back("P-20=" + pp_pos + "_" + p_pos + "_" + c_pos);
    feature.push_back("P02=" + c_pos + "_" + n_pos + "_" + nn_pos);
    feature.push_back("P13=" + n_pos + "_" + nn_pos + "_" + nnn_pos);
    // get current, previous, next morpheme
    string cur_morp, prev_morp, next_morp;
    cur_morp = get_cur_morp(syllables, pos, i);
    prev_morp = get_prev_morp(syllables, pos, i);
    next_morp = get_next_morp(syllables, pos, i);
    // morp feature
    feature.push_back("M0=" + c_pos + "_" + cur_morp);
    feature.push_back("M-1=" + prev_morp);
    feature.push_back("M1=" + next_morp);
    feature.push_back("M-10=" + c_pos + "_" + prev_morp + "_" + cur_morp);
    feature.push_back("M01=" + c_pos + "_" + cur_morp + "_" + next_morp);
    // morp nedic feature
    if (nedic.find(cur_morp) != nedic.end()) {
      feature.push_back("M0=" + c_pos + "_" + nedic[cur_morp]);
    }
    if (prev_morp != "") {
      if (nedic.find(prev_morp) != nedic.end()) {
        feature.push_back("M-1=" + nedic[prev_morp]);
      }
      if (nedic.find(prev_morp + cur_morp) != nedic.end()) {
        feature.push_back("M-10=" + c_pos + "_" + nedic[prev_morp + cur_morp]);
      }
    }
    if (next_morp != "") {
      if (nedic.find(next_morp) != nedic.end()) {
        feature.push_back("M1=" + nedic[next_morp]);
      }
      if (nedic.find(cur_morp + next_morp) != nedic.end()) {
        feature.push_back("M01=" + c_pos + "_" + nedic[cur_morp + next_morp]);
      }
    }
    if (prev_morp != "" && next_morp != "") {
      if (nedic.find(prev_morp + cur_morp + next_morp) != nedic.end()) {
        feature.push_back(
            "M-11=" + c_pos + "_" + nedic[prev_morp + cur_morp + next_morp]);
      }
    }
    // morp cldic feature
    if (cldic.find(cur_morp) != cldic.end()) {
      feature.push_back("M0=" + c_pos + "_" + cldic[cur_morp]);
    }
    if (prev_morp != "") {
      if (cldic.find(prev_morp) != cldic.end()) {
        feature.push_back("M-1=" + cldic[prev_morp]);
      }
      if (cldic.find(prev_morp + cur_morp) != cldic.end()) {
        feature.push_back("M-10=" + c_pos + "_" + cldic[prev_morp + cur_morp]);
      }
    }
    if (next_morp != "") {
      if (cldic.find(next_morp) != cldic.end()) {
        feature.push_back("M1=" + cldic[next_morp]);
      }
      if (cldic.find(cur_morp + next_morp) != cldic.end()) {
        feature.push_back("M01=" + c_pos + "_" + cldic[cur_morp + next_morp]);
      }
    }
    if (prev_morp != "" && next_morp != "") {
      if (cldic.find(prev_morp + cur_morp + next_morp) != cldic.end()) {
        feature.push_back(
            "M-11=" + c_pos + "_" + cldic[prev_morp + cur_morp + next_morp]);
      }
    }

    feature_vec.push_back(feature);
  }
}


/**
    NER int feature : 속도를 빠르게 하기 위해서
    @param syllables, spaces, POS's outcome
    @return feature_vec
*/
void Kor_NER::make_ner_feature_int(vector<string> &syllables,
                                   vector<string> &space,
                                   vector<string> &pos,
                                   vector<vector<int> > &feature_vec) {
  vector<int> feature;
  feature.reserve(1024);
  // lexical
  string ppp, pp, p, c, n, nn, nnn;
  // space
  string ppps, pps, ps, cs, ns, nns, nnns;
  // type
  string pppt, ppt, pt, ct, nt, nnt, nnnt;
  // pos
  string ppp_pos, pp_pos, p_pos, c_pos, n_pos, nn_pos, nnn_pos;

  for (int i = 0; i < syllables.size(); i++) {
    p = c = n = ps = cs = ns = pt = ct = nt = p_pos = c_pos = n_pos = "";

    c = syllables[i];
    c_pos = pos[i];
    ct = get_str_type(c);
    if (space[i] == "B") cs = "_";

    if (i - 1 >= 0) {
      p = syllables[i - 1];
      p_pos = pos[i - 1];
      pt = get_str_type(p);
      if (space[i - 1] == "B") ps = "_";
    }
    if (i + 1 < syllables.size()) {
      n = syllables[i + 1];
      n_pos = pos[i + 1];
      nt = get_str_type(n);
      if (space[i + 1] == "B") ns = "_";
    }
    // window >= 2
    pp = nn = pps = nns = ppt = nnt = pp_pos = nn_pos = "";
    if (i - 2 >= 0) {
      pp = syllables[i - 2];
      pp_pos = pos[i - 2];
      ppt = get_str_type(pp);
      if (space[i - 2] == "B") pps = "_";
    }
    if (i + 2 < syllables.size()) {
      nn = syllables[i + 2];
      nn_pos = pos[i + 2];
      nnt = get_str_type(nn);
      if (space[i + 2] == "B") nns = "_";
    }
    // window >= 3
    ppp = nnn = ppps = nnns = pppt = nnnt = ppp_pos = nnn_pos = "";
    if (i - 3 >= 0) {
      ppp = syllables[i - 3];
      ppp_pos = pos[i - 3];
      pppt = get_str_type(ppp);
      if (space[i - 3] == "B") ppps = "_";
    }
    if (i + 3 < syllables.size()) {
      nnn = syllables[i + 3];
      nnn_pos = pos[i + 3];
      nnnt = get_str_type(nnn);
      if (space[i + 3] == "B") nnns = "_";
    }

    feature.clear();

    // lexical feature: window = 3
    // unigram
    feature.push_back(ner_ssvm.make_pid("L0=" + cs + c));
    feature.push_back(ner_ssvm.make_pid("L-1=" + ps + p));
    feature.push_back(ner_ssvm.make_pid("L1=" + ns + n));
    // bigram
    feature.push_back(ner_ssvm.make_pid("L-2-1=" + pps + pp + ps + p));
    feature.push_back(ner_ssvm.make_pid("L-10=" + ps + p + cs + c));
    feature.push_back(ner_ssvm.make_pid("L01=" + cs + c + ns + n));
    feature.push_back(ner_ssvm.make_pid("L12=" + ns + n + nns + nn));
    // trigram
    feature.push_back(ner_ssvm.make_pid(
        "L-3-1=" + ppps + ppp + pps + pp + ps + p));
    feature.push_back(ner_ssvm.make_pid("L-20=" + pps + pp + ps + p + cs + c));
    feature.push_back(ner_ssvm.make_pid("L-11=" + ps + p + cs + c + ns + n));
    feature.push_back(ner_ssvm.make_pid("L02=" + cs + c + ns + n + nns + nn));
    feature.push_back(ner_ssvm.make_pid(
        "L13=" + ns + n + nns + nn + nnns + nnn));

    // isalpha, isupper, islower, isdigit features: window = 3
    // window >= 3
    if (ppp != "" && pppt != "O") {
      feature.push_back(ner_ssvm.make_pid("p3" + pppt));
      if (ppt != "O") feature.push_back(ner_ssvm.make_pid("p3" + pppt + ppt));
      if (pt != "O")
        feature.push_back(ner_ssvm.make_pid("p3" + pppt + ppt + pt));
    }
    // window >= 2
    if (pp != "" && ppt != "O") {
      feature.push_back(ner_ssvm.make_pid("p2" + ppt));
      if (pt != "O") feature.push_back(ner_ssvm.make_pid("p2" + ppt + pt));
      if (ct != "O") feature.push_back(ner_ssvm.make_pid("p2" + ppt + pt + ct));
    }
    if (p != "" && pt != "O") {
      feature.push_back(ner_ssvm.make_pid("p" + pt));
      if (ct != "O") feature.push_back(ner_ssvm.make_pid("p" + pt + ct));
      if (n != "" && nt != "O")
        feature.push_back(ner_ssvm.make_pid("p" + pt + ct + nt));
    }
    if (c != "" && ct != "O") {
      feature.push_back(ner_ssvm.make_pid("c" + ct));
      if (n != "" && nt != "O")
        feature.push_back(ner_ssvm.make_pid("c" + ct + nt));
      if (nn != "" && nnt != "O")
        feature.push_back(ner_ssvm.make_pid("c" + ct + nt + nnt));
    }
    if (n != "" && nt != "O") {
      feature.push_back(ner_ssvm.make_pid("n" + nt));
      if (nn != "" && nnt != "O")
        feature.push_back(ner_ssvm.make_pid("n" + nt + nnt));
      // window >= 3
      if (nnn != "" && nnnt != "O")
        feature.push_back(ner_ssvm.make_pid("n" + nt + nnt + nnnt));
    }
    // window >= 3
    if (nn != "" && nnt != "O") {
      feature.push_back(ner_ssvm.make_pid("n2" + nnt));
      if (nnn != "" && nnnt != "O")
        feature.push_back(ner_ssvm.make_pid("n2" + nnt + nnnt));
    }
    if (nnn != "" && nnnt != "O") {
      feature.push_back(ner_ssvm.make_pid("n3" + nnnt));
    }

    // nedic feature: 2~7음절 검사 (앞뒤 3음절) : : longest-match 적용
    // next_pos는 longest-match 적용 후, 두번째 단어의 시작 위치
    int next_pos = -3;
    // -3 ~
    if (i - 3 >= 0) {
      if (i + 3 < syllables.size()
          && nedic.find(ppp + pp + p + c + n + nn + nnn) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "-33" + nedic[ppp + pp + p + c + n + nn + nnn]));
        next_pos = 4;
      }
      else if (i + 2 < syllables.size()
          && nedic.find(ppp + pp + p + c + n + nn) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "-32" + nedic[ppp + pp + p + c + n + nn]));
        next_pos = 3;
      }
      else if (i + 1 < syllables.size()
          && nedic.find(ppp + pp + p + c + n) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "-31" + nedic[ppp + pp + p + c + n]));
        next_pos = 2;
      }
      else if (nedic.find(ppp + pp + p + c) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid("-30" + nedic[ppp + pp + p + c]));
        next_pos = 1;
      }
      else if (nedic.find(ppp + pp + p) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid("-3-1" + nedic[ppp + pp + p]));
        next_pos = 0;
      }
      else next_pos = -2;
    }
    // -2 ~
    if (i - 2 >= 0 && next_pos <= -2) {
      if (i + 3 < syllables.size()
          && nedic.find(pp + p + c + n + nn + nnn) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "-23" + nedic[pp + p + c + n + nn + nnn]));
        next_pos = 4;
      }
      else if (i + 2 < syllables.size()
          && nedic.find(pp + p + c + n + nn) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "-22" + nedic[pp + p + c + n + nn]));
        next_pos = 3;
      }
      else if (i + 1 < syllables.size()
          && nedic.find(pp + p + c + n) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid("-21" + nedic[pp + p + c + n]));
        next_pos = 2;
      }
      else if (nedic.find(pp + p + c) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid("-20" + nedic[pp + p + c]));
        next_pos = 1;
      }
      else if (nedic.find(pp + p) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid("-2-1" + nedic[pp + p]));
        next_pos = 0;
      }
      else next_pos = -1;
    }
    // -1 ~
    if (i - 1 >= 0 && next_pos <= -1) {
      if (i + 3 < syllables.size()
          && nedic.find(p + c + n + nn + nnn) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "-13" + nedic[p + c + n + nn + nnn]));
        next_pos = 4;
      }
      else if (i + 2 < syllables.size()
          && nedic.find(p + c + n + nn) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid("-12" + nedic[p + c + n + nn]));
        next_pos = 3;
      }
      else if (i + 1 < syllables.size()
          && nedic.find(p + c + n) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid("-11" + nedic[p + c + n]));
        next_pos = 2;
      }
      else if (nedic.find(p + c) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid("-10" + nedic[p + c]));
        next_pos = 1;
      }
      else next_pos = 0;
    }
    // 0 ~
    if (next_pos <= 0) {
      if (i + 3 < syllables.size()
          && nedic.find(c + n + nn + nnn) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid("03" + nedic[c + n + nn + nnn]));
        next_pos = 4;
      }
      else if (i + 2 < syllables.size()
          && nedic.find(c + n + nn) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid("02" + nedic[c + n + nn]));
        next_pos = 3;
      }
      else if (i + 1 < syllables.size() && nedic.find(c + n) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid("01" + nedic[c + n]));
        next_pos = 2;
      }
      else next_pos = 1;
    }
    // 1 ~
    if (next_pos <= 1) {
      if (i + 3 < syllables.size() && nedic.find(n + nn + nnn) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid("13" + nedic[n + nn + nnn]));
        next_pos = 4;
      }
      else if (i + 2 < syllables.size() && nedic.find(n + nn) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid("12" + nedic[n + nn]));
        next_pos = 3;
      }
      else next_pos = 2;
    }

    // CL dic feature: 2~7음절 검사 (앞뒤 3음절) : longest-match 적용
    // next_pos는 longest-match 적용 후, 두번째 단어의 시작 위치
    next_pos = -3;
    // -3 ~
    if (i - 3 >= 0) {
      if (i + 3 < syllables.size()
          && cldic.find(ppp + pp + p + c + n + nn + nnn) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "-33" + cldic[ppp + pp + p + c + n + nn + nnn]));
        next_pos = 4;
      }
      else if (i + 2 < syllables.size()
          && cldic.find(ppp + pp + p + c + n + nn) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "-32" + cldic[ppp + pp + p + c + n + nn]));
        next_pos = 3;
      }
      else if (i + 1 < syllables.size()
          && cldic.find(ppp + pp + p + c + n) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "-31" + cldic[ppp + pp + p + c + n]));
        next_pos = 2;
      }
      else if (cldic.find(ppp + pp + p + c) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid("-30" + cldic[ppp + pp + p + c]));
        next_pos = 1;
      }
      else if (cldic.find(ppp + pp + p) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid("-3-1" + cldic[ppp + pp + p]));
        next_pos = 0;
      }
      else next_pos = -2;
    }
    // -2 ~
    if (i - 2 >= 0 && next_pos <= -2) {
      if (i + 3 < syllables.size()
          && cldic.find(pp + p + c + n + nn + nnn) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "-23" + cldic[pp + p + c + n + nn + nnn]));
        next_pos = 4;
      }
      else if (i + 2 < syllables.size()
          && cldic.find(pp + p + c + n + nn) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "-22" + cldic[pp + p + c + n + nn]));
        next_pos = 3;
      }
      else if (i + 1 < syllables.size()
          && cldic.find(pp + p + c + n) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid("-21" + cldic[pp + p + c + n]));
        next_pos = 2;
      }
      else if (cldic.find(pp + p + c) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid("-20" + cldic[pp + p + c]));
        next_pos = 1;
      }
      else if (cldic.find(pp + p) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid("-2-1" + cldic[pp + p]));
        next_pos = 0;
      }
      else next_pos = -1;
    }
    // -1 ~
    if (i - 1 >= 0 && next_pos <= -1) {
      if (i + 3 < syllables.size()
          && cldic.find(p + c + n + nn + nnn) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "-13" + cldic[p + c + n + nn + nnn]));
        next_pos = 4;
      }
      else if (i + 2 < syllables.size()
          && cldic.find(p + c + n + nn) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid("-12" + cldic[p + c + n + nn]));
        next_pos = 3;
      }
      else if (i + 1 < syllables.size()
          && cldic.find(p + c + n) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid("-11" + cldic[p + c + n]));
        next_pos = 2;
      }
      else if (cldic.find(p + c) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid("-10" + cldic[p + c]));
        next_pos = 1;
      }
      else next_pos = 0;
    }
    // 0 ~
    if (next_pos <= 0) {
      if (i + 3 < syllables.size()
          && cldic.find(c + n + nn + nnn) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid("03" + cldic[c + n + nn + nnn]));
        next_pos = 4;
      }
      else if (i + 2 < syllables.size()
          && cldic.find(c + n + nn) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid("02" + cldic[c + n + nn]));
        next_pos = 3;
      }
      else if (i + 1 < syllables.size() && cldic.find(c + n) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid("01" + cldic[c + n]));
        next_pos = 2;
      }
      else next_pos = 1;
    }
    // 1 ~
    if (next_pos <= 1) {
      if (i + 3 < syllables.size() && cldic.find(n + nn + nnn) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid("13" + cldic[n + nn + nnn]));
        next_pos = 4;
      }
      else if (i + 2 < syllables.size() && cldic.find(n + nn) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid("12" + cldic[n + nn]));
        next_pos = 3;
      }
      else next_pos = 2;
    }

    // POS feature: window = 3
    // unigram
    feature.push_back(ner_ssvm.make_pid("P0=" + c_pos));
    feature.push_back(ner_ssvm.make_pid("P-1=" + p_pos));
    feature.push_back(ner_ssvm.make_pid("P1=" + n_pos));
    // bigram
    feature.push_back(ner_ssvm.make_pid("P-2-1=" + pp_pos + "_" + p_pos));
    feature.push_back(ner_ssvm.make_pid("P-10=" + p_pos + "_" + c_pos));
    feature.push_back(ner_ssvm.make_pid("P01=" + c_pos + "_" + n_pos));
    feature.push_back(ner_ssvm.make_pid("P12=" + n_pos + "_" + nn_pos));
    // trigram
    feature.push_back(ner_ssvm.make_pid(
        "P-3-1=" + ppp_pos + "_" + pp_pos + "_" + p_pos));
    feature.push_back(ner_ssvm.make_pid(
        "P-20=" + pp_pos + "_" + p_pos + "_" + c_pos));
    feature.push_back(ner_ssvm.make_pid(
        "P02=" + c_pos + "_" + n_pos + "_" + nn_pos));
    feature.push_back(ner_ssvm.make_pid(
        "P13=" + n_pos + "_" + nn_pos + "_" + nnn_pos));
    // get current, previous, next morpheme
    string cur_morp, prev_morp, next_morp;
    cur_morp = get_cur_morp(syllables, pos, i);
    prev_morp = get_prev_morp(syllables, pos, i);
    next_morp = get_next_morp(syllables, pos, i);
    // morp feature
    feature.push_back(ner_ssvm.make_pid("M0=" + c_pos + "_" + cur_morp));
    feature.push_back(ner_ssvm.make_pid("M-1=" + prev_morp));
    feature.push_back(ner_ssvm.make_pid("M1=" + next_morp));
    feature.push_back(ner_ssvm.make_pid(
        "M-10=" + c_pos + "_" + prev_morp + "_" + cur_morp));
    feature.push_back(ner_ssvm.make_pid(
        "M01=" + c_pos + "_" + cur_morp + "_" + next_morp));
    // morp nedic feature
    if (nedic.find(cur_morp) != nedic.end()) {
      feature.push_back(ner_ssvm.make_pid(
          "M0=" + c_pos + "_" + nedic[cur_morp]));
    }
    if (prev_morp != "") {
      if (nedic.find(prev_morp) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid("M-1=" + nedic[prev_morp]));
      }
      if (nedic.find(prev_morp + cur_morp) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "M-10=" + c_pos + "_" + nedic[prev_morp + cur_morp]));
      }
    }
    if (next_morp != "") {
      if (nedic.find(next_morp) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid("M1=" + nedic[next_morp]));
      }
      if (nedic.find(cur_morp + next_morp) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "M01=" + c_pos + "_" + nedic[cur_morp + next_morp]));
      }
    }
    if (prev_morp != "" && next_morp != "") {
      if (nedic.find(prev_morp + cur_morp + next_morp) != nedic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "M-11=" + c_pos + "_" + nedic[prev_morp + cur_morp + next_morp]));
      }
    }
    // morp cldic feature
    if (cldic.find(cur_morp) != cldic.end()) {
      feature.push_back(ner_ssvm.make_pid(
          "M0=" + c_pos + "_" + cldic[cur_morp]));
    }
    if (prev_morp != "") {
      if (cldic.find(prev_morp) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid("M-1=" + cldic[prev_morp]));
      }
      if (cldic.find(prev_morp + cur_morp) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "M-10=" + c_pos + "_" + cldic[prev_morp + cur_morp]));
      }
    }
    if (next_morp != "") {
      if (cldic.find(next_morp) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid("M1=" + cldic[next_morp]));
      }
      if (cldic.find(cur_morp + next_morp) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "M01=" + c_pos + "_" + cldic[cur_morp + next_morp]));
      }
    }
    if (prev_morp != "" && next_morp != "") {
      if (cldic.find(prev_morp + cur_morp + next_morp) != cldic.end()) {
        feature.push_back(ner_ssvm.make_pid(
            "M-11=" + c_pos + "_" + cldic[prev_morp + cur_morp + next_morp]));
      }
    }

    feature_vec.push_back(feature);
  }
}


/**
    현재 음절이 속한 형태소를 반환
    @param syllables, POS's outcome, i (current position)
    @return morpheme
*/
string Kor_NER::get_cur_morp(vector<string> &syllables,
                             vector<string> &pos,
                             int i) {
  string morp = syllables[i];
  int j = i;
  // 앞 부분
  if (strncmp(pos[j].c_str(), "I-", 2) == 0) {
    j--;
    while (j >= 0) {
      morp = syllables[j] + morp;
      if (strncmp(pos[j].c_str(), "B-", 2) == 0) break;
      j--;
    }
  }
  // 뒷 부분
  j = i + 1;
  while (j < syllables.size() && strncmp(pos[j].c_str(), "I-", 2) == 0) {
    morp += syllables[j];
    j++;
  }
  return morp;
}

/**
    현재 음절이 속한 형태소의 앞 형태소를 반환
    @parma syllables, POS's outcome, i (current position)
    @return morpheme
*/
string Kor_NER::get_prev_morp(vector<string> &syllables,
                              vector<string> &pos,
                              int i) {
  if (strncmp(pos[i].c_str(), "B-", 2) == 0) {
    if (i >= 1) return get_cur_morp(syllables, pos, i - 1);
    else return "";
  } else {
    int j = i - 1;
    while (j >= 0 && strncmp(pos[j].c_str(), "I-", 2) == 0) j--;
    if (j >= 1) return get_cur_morp(syllables, pos, j - 1);
    else return "";
  }
}

/**
    현재 음절이 속한 형태소의 뒤 형태소를 반환
    @param syllables, POS's outcome, i (current position)
    @return morpheme
*/
string Kor_NER::get_next_morp(vector<string> &syllables,
                              vector<string> &pos,
                              int i) {
  int j = i + 1;
  while (j < syllables.size() && strncmp(pos[j].c_str(), "I-", 2) == 0) j++;
  if (j < syllables.size()) return get_cur_morp(syllables, pos, j);
  else return "";
}

