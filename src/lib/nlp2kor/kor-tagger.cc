#include <string.h>
#include <stdio.h>
#include <stdexcept> //for std::runtime_error
#include <memory> //for std::bad_alloc
#include "kor-tagger.h"

#pragma warning(disable: 4786)
#pragma warning(disable: 4996)
#pragma warning(disable: 4267)
#pragma warning(disable: 4244)
#pragma warning(disable: 4018)

#include <libmaum/common/encoding.h>

using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Kor_tagger::Kor_tagger() {
  restore = 1;
  beam = 0;
  verbose = 0;
}

Kor_tagger::~Kor_tagger() {
}

extern "C" Kor_tagger * extern_kor_tagger_create() {
  return new Kor_tagger;
}

extern "C" int extern_kor_tagger_init(Kor_tagger *me, const string &dir) {
  return me->init(dir);
}

extern "C" double extern_kor_tagger_do_tagging_sentense(Kor_tagger * me, string &input, K_Sentence &k_sent) {
  return me->do_tagging(input, k_sent);
}

extern "C" double extern_kor_tagger_do_tagging(Kor_tagger * me, string &input, K_Doc &k_doc) {
  k_doc.sentence.clear();
  k_doc.text = input;
  K_Sentence k_sent;
  double score = me->do_tagging(input, k_sent);
  k_doc.sentence.push_back(k_sent);

  return score;
}

extern "C" vector<string> get_postagged_str(Kor_tagger * me, K_Doc &k_doc) {
  vector<string> ret ;
  for (auto &k_sent : k_doc.sentence) {
    string result ;
    for (const auto& word : k_sent.word) {
      if (result !="") result+= " " ;
      result = my_replace_all(word.tagged_text, " ", "+") ;
    }
    ret.push_back(result);
  }
  return ret ;
}

/**
  POS tagger 초기화
  @param dir  리소스 디렉토리
  @return   초기화 성공(0) or 실패(1)
*/
int Kor_tagger::init(const string &dir) {
  string file;
  FILE *in, *out;
  char strtmp[1001];

  // time check
  /*
  if (1) {
      struct tm *t;
      time_t t0;
      time(&t0);
      t = localtime(&t0);
      int year = t->tm_year + 1900;
      int month = t->tm_mon + 1;
      int day = t->tm_mday;
      //printf("%d/%d/%d\n", year, month, day);
      if (year >= 2106 || year <= 2013) {
          printf("%s\n", "본 프로그램의 사용기간이 만료 되었습니다.");
          printf("%s\n", "leeck@kangwon.ac.kr(강원대 이창기, 010-3308-0337)으로 문의해주시기 바랍니다.");
          exit(1);
      }
  }
  */

  // beam
  ssvm.beam = beam;
  // ssvm
  file = dir + "/" + "kor_tagger_model.bin";
  try {
    ssvm.load_bin(file);
  } catch (exception e) {
    cerr << "Error1: " << e.what() << endl;
    return 1;
  }

  // dic
  if (1) {
    file = dir + "/" + "dic.txt";
    cerr << "load " << file << " ... ";
    in = fopen(file.c_str(), "r+");
    if (in == NULL) {
      cerr << "File open error: " << file << endl;
      return 1;
    }
    while (!feof(in)) {
      fgets(strtmp, 1000, in);
      if (strtmp[0] == ';') continue;
      // fgets의 맨뒤 newline 처리
      strtmp[strlen(strtmp)] = '\0';
      if (strlen(strtmp) > 0 && strtmp[strlen(strtmp) - 1] == '\n')
        strtmp[strlen(strtmp) - 1] = '\0';
      dic[strtmp] = "n";
    }
    fclose(in);
    cerr << "done." << endl;
  }

  // NE dic
  if (1) {
    // encryption
    char key[] = {'l', 'c', 'k', '!', '3', '%', '7'};
    file = dir + "/" + "ne_dic4pos.enc.txt";
    cerr << "load " << file << " ... ";
    in = fopen(file.c_str(), "r+");
    if (in != NULL) {
      while (!feof(in)) {
        strtmp[0] = '\0';
        fgets(strtmp, 1000, in);
        if (strtmp[0] == ';' || strtmp[0] == '\0') continue;
        // fgets의 맨뒤 newline 처리
        strtmp[strlen(strtmp)] = '\0';
        if (strlen(strtmp) > 0 && strtmp[strlen(strtmp) - 1] == '\n')
          strtmp[strlen(strtmp) - 1] = '\0';
        // decoding
        for (int i = 0; strtmp[i] != '\0'; i++) {
          char c = strtmp[i];
          if (c != ' ' && c != '\t' && c != '\n' && c != '\r') {
            c ^= key[i % 3];
            if (c != '\0' && c != EOF && c != ' ' && c != '\t' && c != '\n'
                && c != '\r')
              strtmp[i] = c;
          }
        }
        vector<string> token;
        tokenize(strtmp, token, " \t\r\n");
        if (token.size() == 2) {
          dic[token[0]] = token[1];
        } else {
          cerr << "Warning: " << strtmp << endl;
        }
        // test
        //cout << strtmp << endl;
      }
      fclose(in);
      cerr << "done." << endl;
    } else {
      // no encryption
      string enc_file = dir + "/" + "ne_dic4pos.enc.txt";
      file = dir + "/" + "ne_dic4pos.txt";
      cerr << "load " << file << " ... ";
      in = fopen(file.c_str(), "r+");
      out = fopen(enc_file.c_str(), "w");
      if (in == NULL) {
        cerr << "File open error: " << file << endl;
        return 1;
      }
      if (out == NULL) {
        cerr << "File open error: " << enc_file << endl;
        return 1;
      }
      while (!feof(in)) {
        strtmp[0] = '\0';
        fgets(strtmp, 1000, in);
        if (strtmp[0] == ';' || strtmp[0] == '\0') continue;
        // fgets의 맨뒤 newline 처리
        strtmp[strlen(strtmp)] = '\0';
        if (strlen(strtmp) > 0 && strtmp[strlen(strtmp) - 1] == '\n')
          strtmp[strlen(strtmp) - 1] = '\0';
        vector<string> token;
        tokenize(strtmp, token, " \t\r\n");
        if (token.size() == 2) {
          dic[token[0]] = token[1];
        } else {
          cerr << "Warning: " << strtmp << endl;
        }
        // encoding
        for (int i = 0; strtmp[i] != '\0'; i++) {
          char c = strtmp[i];
          if (c != ' ' && c != '\t' && c != '\n' && c != '\r') {
            c ^= key[i % 3];
            if (c != '\0' && c != EOF && c != ' ' && c != '\t' && c != '\n'
                && c != '\r')
              strtmp[i] = c;
          }
        }
        fprintf(out, "%s\n", strtmp);
      }
      fclose(in);
      fclose(out);
      cerr << "done." << endl;
    }
  }


  // restore morpheme
  if (restore) {
    // morph_rule.txt
    file = dir + "/" + "morph_rule.txt";
    cerr << "load " << file << " ... ";
    in = fopen(file.c_str(), "r+");
    if (in == NULL) {
      cerr << "File open error: " << file << endl;
      return 1;
    }
    while (!feof(in)) {
      fgets(strtmp, 1000, in);
      if (strtmp[0] == ';' || strtmp[0] == '#') continue;
      // fgets의 맨뒤 newline 처리
      strtmp[strlen(strtmp)] = '\0';
      if (strlen(strtmp) > 0 && strtmp[strlen(strtmp) - 1] == '\n')
        strtmp[strlen(strtmp) - 1] = '\0';
      vector<string> token;
      tokenize(strtmp, token, " \t\r\n");
      if (token.size() == 3) {
        if (morph_rule.find(token[0]) != morph_rule.end()) {
          // 애매성이 존재할 때는 확률이 큰 쪽 선택
          int freq = atoi(token[2].c_str());
          if (morph_rule_freq[token[0]] < freq) {
            morph_rule[token[0]] = token[1];
            morph_rule_freq[token[0]] = freq;
            if (verbose)
              cerr << "중복존재: "
                   << EuckrToUtf8(token[0]) << " "
                   << EuckrToUtf8(token[1]) << "=" << freq
                   << endl;
          }
        } else {
          morph_rule[token[0]] = token[1];
          morph_rule_freq[token[0]] = atoi(token[2].c_str());
        }
      } else {
        cerr << "Warning: " << strtmp << endl;
      }
    }
    fclose(in);
    cerr << "done." << endl;

    // morph_rule_lp.txt
    morph_rule_freq.clear();
    file = dir + "/" + "morph_rule_lp.txt";
    cerr << "load " << file << " ... ";
    in = fopen(file.c_str(), "r+");
    if (in == NULL) {
      cerr << "File open error: " << file << endl;
      return 1;
    }
    while (!feof(in)) {
      fgets(strtmp, 1000, in);
      if (strtmp[0] == ';' || strtmp[0] == '#') continue;
      // fgets의 맨뒤 newline 처리
      strtmp[strlen(strtmp)] = '\0';
      if (strlen(strtmp) > 0 && strtmp[strlen(strtmp) - 1] == '\n')
        strtmp[strlen(strtmp) - 1] = '\0';
      vector<string> token;
      tokenize(strtmp, token, " \t\r\n");
      if (token.size() == 3) {
        if (morph_rule_lp.find(token[0]) != morph_rule_lp.end()) {
          // 애매성이 존재할 때는 확률이 큰 쪽 선택
          int freq = atoi(token[2].c_str());
          if (morph_rule_freq[token[0]] < freq) {
            morph_rule_lp[token[0]] = token[1];
            morph_rule_freq[token[0]] = freq;
            cerr << "중복존재: " << token[0] << " " << token[1] << "=" << freq
                 << endl;
          }
        } else {
          morph_rule_lp[token[0]] = token[1];
          morph_rule_freq[token[0]] = atoi(token[2].c_str());
        }
      } else {
        cerr << "Warning: " << strtmp << endl;
      }
    }
    fclose(in);
    cerr << "done." << endl;
  }

  return 0;
}

/**
  POS tagger 종료
*/
void Kor_tagger::close() {
  dic.clear();
  morph_rule.clear();
  morph_rule_freq.clear();
  morph_rule_lp.clear();
  ssvm.clear();
}

/**
  입력문장을 POS tagging
  @param input  입력 텍스트
  @param k_sent   형태소분석 및 품사태깅 결과가 저장될 K_Sentence 구조체
  @return score
*/
double Kor_tagger::do_tagging(string &input, K_Sentence &k_sent) {
  vector<string> e_syllable;
  vector<string> eojeol;
  vector<vector<int> > feature;

  vector<string> &syllable = k_sent.syllable;
  vector<string> &space = k_sent.space;   // 어절의 시작(B), 중간(I) 정보를 저장
  vector<int> &sid2wid = k_sent.sid2wid;  // syllable id --> word id

  // sent
  k_sent.id = 0;
  k_sent.text = input;
  K_Word word;

  // 각 어절로부터 syllable을 만든다
  tokenize(input, eojeol, " \t\r\n");
  for (int i = 0; i < eojeol.size(); i++) {
    word.begin_sid = sid2wid.size();
    e_syllable.clear();
    make_syllable(eojeol[i], e_syllable);
    for (int j = 0; j < e_syllable.size(); j++) {
      syllable.push_back(e_syllable[j]);
      // 어절 정보
      if (j == 0) space.push_back("B");
      else space.push_back("I");
      // syllable id --> word id
      sid2wid.push_back(i);
    }
    // doc.sent.word
    word.end_sid = sid2wid.size() - 1;
    word.id = i;
    word.text = eojeol[i];
    word.begin_mid = -1;
    word.end_mid = -1;
    k_sent.word.push_back(word);
  }

  // 음절로부터 feature를 만든다
  if (verbose) {
    vector<vector<string> > str_feature;
    make_feature(syllable, space, str_feature);
    for (int i = 0; i < str_feature.size(); i++) {
      for (int j = 0; j < str_feature[i].size(); j++) {
        cout << " " << str_feature[i][j];
      }
      cout << endl;
    }
  }
  make_feature_int(syllable, space, feature);

  // SSVM for POS tagging
  vector<string> &POS_outcome = k_sent.POS_outcome;
  POS_outcome.reserve(feature.size());
  ksent_t sent;
  sent.reserve(feature.size());
  ssvm.make_sent(feature, sent);
  double prob = ssvm.eval(sent, POS_outcome);

  // doc.sent.morp
  K_Morp morp;
  morp.id = 0;
  morp.wid = -1;
  int word_id = 0;

  // POS result
  vector<string> BI_tag;
  string result, prev_tag;
  // POS result
  for (int i = 0; i < POS_outcome.size(); i++) {
    BI_tag.clear();
    tokenize(POS_outcome[i], BI_tag, "-");
    if (BI_tag.size() == 2) {
      string BI = BI_tag[0];
      string tag = BI_tag[1];
      // 띄어쓰기
      if (BI == "B") {
        if (result == "") {
          result = syllable[i];
        } else {
          if (space[i] == "B") {
            result += "/" + prev_tag;
            k_sent.word[word_id++].tagged_text = result;
            result = syllable[i];
          } else {
            result += "/" + prev_tag + " " + syllable[i];
          }
        }
        // doc.sent.morp
        if (morp.lemma != "") {
          morp.type = prev_tag;
          k_sent.morp.push_back(morp);
          // new morp
          morp.id++;
        }
        // new morp
        morp.lemma = syllable[i];
        if (space[i] == "B") morp.wid++;
      }
        // modified by leeck 2014.5
        // 붙여쓰기 오류(space="B", BI=="I") --> BI를 "B"로 수정
      else if (BI == "I" && space[i] == "B") {
        if (result == "") {
          result = syllable[i];
        } else {
          // space[i] == "B"
          result += "/" + prev_tag;
          k_sent.word[word_id++].tagged_text = result;
          result = syllable[i];
        }
        // doc.sent.morp
        if (morp.lemma != "") {
          morp.type = prev_tag;
          k_sent.morp.push_back(morp);
          // new morp
          morp.id++;
        }
        // new morp
        morp.lemma = syllable[i];
        // space[i] == "B"
        morp.wid++;
      }
        // 붙여쓰기
      else if (BI == "I") {
        result += syllable[i];
        // doc.sent.morp
        morp.lemma += syllable[i];
      }
      prev_tag = tag;
    } else {
      cerr << "Error2: " << POS_outcome[i] << endl;
    }
    if (verbose) cout << "# " << syllable[i] + "\t" + POS_outcome[i] << endl;
  }
  if (result != "") {
    result += "/" + prev_tag;
    k_sent.word[word_id++].tagged_text = result;
  }
  if (verbose) cout << endl;

  // doc.sent.morp
  if (morp.lemma != "") {
    morp.type = prev_tag;
    k_sent.morp.push_back(morp);
  }

  // modified by leeck 2014.5
  // 후처리 대상: ex. 김정은
  if (0) {
    for (int i = 0; i < k_sent.word.size(); i++) {
      K_Word &word = k_sent.word[i];
      // 현재 어절이 3음절이며, 개체명 사전에 "PS"로 등록된 경우
      if (word.text.size() == 6 && dic.find(word.text) != dic.end()
          && dic[word.text] == "PS") {
        string two_syllable1 = word.text.substr(0, 4);
        string two_syllable2 = word.text.substr(2, 4);
        string tagged_text2 = word.text + "/NNP";
        // 현재 어절 전체가 NNP로 태깅이 되지 않고, 앞 2음절이 사전에 없는 경우
        if (word.tagged_text != tagged_text2
            && word.tagged_text.size() != tagged_text2.size()
            && dic.find(two_syllable1) == dic.end()
            && dic.find(two_syllable2) == dic.end()) {
          cerr << "Post-processing candidate: " << word.tagged_text << " -->? "
               << word.text + "/NNP" << endl;
        }
      }
    }
  }

  // doc.sent.word: begin_mid, end_mid
  for (int i = 0; i < k_sent.morp.size(); i++) {
    int mid = k_sent.morp[i].id;
    int wid = k_sent.morp[i].wid;
    if (k_sent.word[wid].begin_mid == -1) k_sent.word[wid].begin_mid = mid;
    k_sent.word[wid].end_mid = mid;
  }
  return prob;
}

/**
  문장 분리 (어절의 마지막 태그가 SF이면 한 문장의 끝)
  @param k_sent   형태소분석 및 품사태깅 결과가 저장된 K_Sentence 구조체
  @param k_doc  문장 분리 결과가 저장될 K_Doc 구조체
  @return score
*/
void Kor_tagger::split_sentence(K_Sentence &k_sent, K_Doc &k_doc) {
  K_Sentence new_sent;
  new_sent.id = 0;
  int begin_wid = 0, begin_mid = 0, begin_sid = 0;
  int max_wid = 0, max_mid = 0, max_sid = 0;
  // 어절 단위로 문장 분리
  for (int i = 0; i < k_sent.word.size(); i++) {
    K_Word &word = k_sent.word[i];
    K_Morp &last_morp = k_sent.morp[word.end_mid];
    string prev_last_type;
    if (word.end_mid - 1 >= 0) {
      prev_last_type = k_sent.morp[word.end_mid - 1].type;
    }
    // text
    if (new_sent.text != "") new_sent.text += " ";
    new_sent.text += word.text;
    // max_wid, ...
    max_wid = word.id;
    max_mid = word.end_mid;
    max_sid = word.end_sid;
    // syllable, space, POS_outcome, sid2wid 추가
    //cerr << word.begin_sid << " , " << word.end_sid << endl;
    for (int j = word.begin_sid; j <= word.end_sid; j++) {
      new_sent.syllable.push_back(k_sent.syllable[j]);
      new_sent.space.push_back(k_sent.space[j]);
      new_sent.POS_outcome.push_back(k_sent.POS_outcome[j]);
      int wid = k_sent.sid2wid[j] - begin_wid;
      new_sent.sid2wid.push_back(wid);
    }
    // 형태소 추가
    for (int j = word.begin_mid; j <= word.end_mid; j++) {
      K_Morp &morp = k_sent.morp[j];
      if (morp.lemma.size() == 0)
        continue;
      morp.id = morp.id - begin_mid;
      morp.wid = morp.wid - begin_wid;
      new_sent.morp.push_back(morp);
    }
    // 어절 추가
    word.id = word.id - begin_wid;
    word.begin_mid = word.begin_mid - begin_mid;
    word.end_mid = word.end_mid - begin_mid;
    word.begin_sid = word.begin_sid - begin_sid;
    word.end_sid = word.end_sid - begin_sid;
    new_sent.word.push_back(word);
    // 문장 분리
    if ((prev_last_type == "EF" || prev_last_type == "ETN")
        && last_morp.type == "SF") {
      k_doc.sentence.push_back(new_sent);
      new_sent.syllable.clear();
      new_sent.space.clear();
      new_sent.POS_outcome.clear();
      new_sent.sid2wid.clear();
      new_sent.word.clear();
      new_sent.morp.clear();
      new_sent.text.clear();
      new_sent.id++;
      begin_wid = max_wid + 1;
      begin_mid = max_mid + 1;
      begin_sid = max_sid + 1;
    }
  }
  if (!new_sent.word.empty()) {
    k_doc.sentence.push_back(new_sent);
  }
}

/**
  preprocess0: 구문 분석을 위한 전처리: ex) ~다...아이폰...
  @param k_sent   형태소분석 및 품사태깅 결과가 저장된 K_Sentence 구조체. 전처리 결과가 저장됨
*/
void Kor_tagger::preprocess0(K_Sentence &k_sent) {
  vector<K_Word> new_words;
  for (int i = 0, w_id = 0; i < k_sent.word.size(); i++) {
    K_Word &word = k_sent.word[i];
    int pos1 = -1, pos2 = -1;
    for (int j = word.begin_mid; j <= word.end_mid; j++) {
      K_Morp &morp = k_sent.morp[j];
      char c_lemma = morp.lemma.at(0);
      if ((morp.type == "SF" || morp.type == "SE" || morp.type == "SP") \
 && (c_lemma == '.' || c_lemma == '?' || c_lemma == '!')) {
        if (pos1 < 0 || pos1 + 1 == j) pos1 = j;
        else if (pos2 < 0 || pos2 + 1 == j) pos2 = j;
      }
    }
    int pos_syl1 = -1, pos_syl2 = -1;
    for (int j = word.begin_sid; j <= word.end_sid; j++) {
      string &syl = k_sent.syllable[j];
      if (syl == "." || syl == "?" || syl == "!") {
        if (pos_syl1 < 0 || pos_syl1 + 1 == j) pos_syl1 = j;
        else if (pos_syl2 < 0 || pos_syl2 + 1 == j) pos_syl2 = j;
      }
    }
    // ~다...
    if (pos1 < 0 || pos1 == word.end_mid \
 || (pos1 == word.end_mid - 1 && k_sent.morp[pos1 + 1].type.at(0) == 'S')) {
      word.id = w_id++;
      new_words.push_back(word);
      continue;
    }
    // ~다...아이폰...
    // [word] ... [word]
    if (pos1 < word.end_mid) {
      // word ... word ...
      // word ...
      K_Word word1 = word;
      word1.id = w_id++;
      word1.end_mid = pos1;
      if (pos_syl1 >= 0) word1.end_sid = pos_syl1;
      else
        cerr << " Preprocess0-1: pos_syl1=" << pos_syl1 << " " << word.text
             << endl;
      word1.text = word1.tagged_text = "";
      for (int j = word1.begin_mid; j <= word1.end_mid; j++) {
        K_Morp &morp = k_sent.morp[j];
        if (j > word1.begin_mid) word1.tagged_text += " ";
        word1.text += morp.lemma;
        word1.tagged_text += morp.lemma + "/" + morp.type;
      }
      new_words.push_back(word1);
      // word ...
      K_Word word2 = word;
      word2.id = w_id++;
      word2.begin_mid = pos1 + 1;
      if (pos_syl1 >= 0) word2.begin_sid = pos_syl1 + 1;
      else
        cerr << " Preprocess0-2: pos_syl1=" << pos_syl1 << " " << word.text
             << endl;
      word2.text = word2.tagged_text = "";
      for (int j = word2.begin_mid; j <= word2.end_mid; j++) {
        K_Morp &morp = k_sent.morp[j];
        if (j > word2.begin_mid) word2.tagged_text += " ";
        word2.text += morp.lemma;
        word2.tagged_text += morp.lemma + "/" + morp.type;
      }
      new_words.push_back(word2);
    } else {
      word.id = w_id++;
      new_words.push_back(word);
      continue;
    }
  }
  k_sent.word = new_words;
  // k_sent.sid2wid
  for (int i = 0; i < k_sent.word.size(); i++) {
    K_Word &word = k_sent.word[i];
    for (int j = word.begin_sid; j <= word.end_sid; j++) {
      k_sent.sid2wid[j] = word.id;
    }
  }
  // k_sent.text
  k_sent.raw_text = k_sent.text;
  k_sent.text = "";
  /*
  for (int i=0; i < k_sent.word.size(); i++) {
      K_Word &word = k_sent.word[i];
      // text
      if (k_sent.text != "") k_sent.text += " ";
      k_sent.text += word.text;
  }
  */
  for (int i = 0; i < k_sent.syllable.size(); i++) {
    // text
    if (i > 0 && k_sent.space[i] == "B") k_sent.text += " ";
    k_sent.text += k_sent.syllable[i];
  }
}

/**
  preprocess1: 구문 분석을 위한 ( ) { } [ ] 전처리
  @param k_sent   형태소분석 및 품사태깅 결과가 저장된 K_Sentence 구조체. 전처리 결과가 저장됨
*/
void Kor_tagger::preprocess1(K_Sentence &k_sent) {
  vector<K_Word> new_words;
  for (int i = 0, w_id = 0; i < k_sent.word.size(); i++) {
    K_Word &word = k_sent.word[i];
    int pos1 = -1, pos2 = -1;
    for (int j = word.begin_mid; j <= word.end_mid; j++) {
      K_Morp &morp = k_sent.morp[j];
      if (morp.lemma == "(" || morp.lemma == "{" || morp.lemma == "[") pos1 = j;
      if (morp.lemma == ")" || morp.lemma == "}" || morp.lemma == "]") pos2 = j;
    }
    int pos_syl1 = -1, pos_syl2 = -1;
    for (int j = word.begin_sid; j <= word.end_sid; j++) {
      string &syl = k_sent.syllable[j];
      if (syl == "(" || syl == "{" || syl == "[") pos_syl1 = j;
      if (syl == ")" || syl == "}" || syl == "]") pos_syl2 = j;
    }
    // no symbol or 2 symbols
    if ((pos1 < 0 && pos2 < 0) || (pos1 >= 0 && pos2 >= 0)) {
      word.id = w_id++;
      new_words.push_back(word);
      continue;
    }
    // 1 symbol
    // [word] ( [word]
    if (pos1 >= word.begin_mid) {
      // word ( word
      if (pos1 > word.begin_mid) {
        // word
        K_Word word1 = word;
        word1.id = w_id++;
        word1.end_mid = pos1 - 1;
        if (pos_syl1 - 1 >= 0) word1.end_sid = pos_syl1 - 1;
        else
          cerr << " Preprocess1-1: pos_syl1=" << pos_syl1 << " " << word.text
               << endl;
        word1.text = word1.tagged_text = "";
        for (int j = word1.begin_mid; j <= word1.end_mid; j++) {
          K_Morp &morp = k_sent.morp[j];
          if (j > word1.begin_mid) word1.tagged_text += " ";
          word1.text += morp.lemma;
          word1.tagged_text += morp.lemma + "/" + morp.type;
        }
        new_words.push_back(word1);
      }
      // (
      K_Word word2 = word;
      word2.id = w_id++;
      word2.text = k_sent.morp[pos1].lemma;
      word2.tagged_text = word2.text + "/" + k_sent.morp[pos1].type;
      word2.begin_mid = word2.end_mid = pos1;
      if (pos_syl1 >= 0) word2.begin_sid = word2.end_sid = pos_syl1;
      else
        cerr << " Preprocess1-2: pos_syl1=" << pos_syl1 << " " << word.text
             << endl;
      new_words.push_back(word2);
      // word
      if (pos1 < word.end_mid) {
        K_Word word3 = word;
        word3.id = w_id++;
        word3.begin_mid = pos1 + 1;
        if (pos_syl1 >= 0) word3.begin_sid = pos_syl1 + 1;
        else
          cerr << " Preprocess1-3: pos_syl1=" << pos_syl1 << " " << word.text
               << endl;
        word3.text = word3.tagged_text = "";
        for (int j = word3.begin_mid; j <= word3.end_mid; j++) {
          K_Morp &morp = k_sent.morp[j];
          if (j > word3.begin_mid) word3.tagged_text += " ";
          word3.text += morp.lemma;
          word3.tagged_text += morp.lemma + "/" + morp.type;
        }
        new_words.push_back(word3);
      }
    }
      // [word] ) [word]
    else if (pos2 >= word.begin_mid && pos2 <= word.end_mid) {
      // word ) word
      if (pos2 > word.begin_mid) {
        // word
        K_Word word1 = word;
        word1.id = w_id++;
        word1.end_mid = pos2 - 1;
        if (pos_syl2 - 1 >= 0) word1.end_sid = pos_syl2 - 1;
        else
          cerr << " Preprocess1-4: pos_syl2=" << pos_syl2 << " " << word.text
               << endl;
        word1.text = word1.tagged_text = "";
        for (int j = word1.begin_mid; j <= word1.end_mid; j++) {
          K_Morp &morp = k_sent.morp[j];
          if (j > word1.begin_mid) word1.tagged_text += " ";
          word1.text += morp.lemma;
          word1.tagged_text += morp.lemma + "/" + morp.type;
        }
        new_words.push_back(word1);
      }
      // )
      K_Word word2 = word;
      word2.id = w_id++;
      word2.text = k_sent.morp[pos2].lemma;
      word2.tagged_text = word2.text + "/" + k_sent.morp[pos2].type;
      word2.begin_mid = word2.end_mid = pos2;
      if (pos_syl2 >= 0) word2.begin_sid = word2.end_sid = pos_syl2;
      else
        cerr << " Preprocess1-5: pos_syl2=" << pos_syl2 << " " << word.text
             << endl;
      new_words.push_back(word2);
      // word
      if (pos2 < word.end_mid) {
        K_Word word3 = word;
        word3.id = w_id++;
        word3.begin_mid = pos2 + 1;
        if (pos_syl2 >= 0) word3.begin_sid = pos_syl2 + 1;
        else
          cerr << " Preprocess1-6: pos_syl2=" << pos_syl2 << " " << word.text
               << endl;
        word3.text = word3.tagged_text = "";
        for (int j = word3.begin_mid; j <= word3.end_mid; j++) {
          K_Morp &morp = k_sent.morp[j];
          if (j > word3.begin_mid) word3.tagged_text += " ";
          word3.text += morp.lemma;
          word3.tagged_text += morp.lemma + "/" + morp.type;
        }
        new_words.push_back(word3);
      }
    }
  }
  k_sent.word = new_words;
  // k_sent.sid2wid
  for (int i = 0; i < k_sent.word.size(); i++) {
    K_Word &word = k_sent.word[i];
    for (int j = word.begin_sid; j <= word.end_sid; j++) {
      k_sent.sid2wid[j] = word.id;
    }
  }
  // k_sent.text
  k_sent.raw_text = k_sent.text;
  k_sent.text = "";
  /*
  for (int i=0; i < k_sent.word.size(); i++) {
      K_Word &word = k_sent.word[i];
      // text
      if (k_sent.text != "") k_sent.text += " ";
      k_sent.text += word.text;
  }
  */
  for (int i = 0; i < k_sent.syllable.size(); i++) {
    // text
    if (i > 0 && k_sent.space[i] == "B") k_sent.text += " ";
    k_sent.text += k_sent.syllable[i];
  }
}

/**
  preprocess2: 구문 분석을 위한 " ` ' 전처리
  @param k_sent   형태소분석 및 품사태깅 결과가 저장된 K_Sentence 구조체. 전처리 결과가 저장됨
*/
void Kor_tagger::preprocess2(K_Sentence &k_sent) {
  vector<K_Word> new_words;
  // two-byte symbol
  for (int i = 0; i < k_sent.word.size(); i++) {
    K_Word &word = k_sent.word[i];
    for (int j = word.begin_mid; j <= word.end_mid; j++) {
      K_Morp &morp = k_sent.morp[j];
      if (morp.lemma == "‘") morp.lemma = "'";
      if (morp.lemma == "’") morp.lemma = "'";
      if (morp.lemma == "“") morp.lemma = "\"";
      if (morp.lemma == "”") morp.lemma = "\"";
    }
    for (int j = word.begin_sid; j <= word.end_sid; j++) {
      string &syl = k_sent.syllable[j];
      if (syl == "‘") syl = "'";
      if (syl == "’") syl = "'";
      if (syl == "“") syl = "\"";
      if (syl == "”") syl = "\"";
    }
  }
  for (int i = 0, w_id = 0; i < k_sent.word.size(); i++) {
    K_Word &word = k_sent.word[i];
    int pos1 = -1, pos2 = -1;
    for (int j = word.begin_mid; j <= word.end_mid; j++) {
      K_Morp &morp = k_sent.morp[j];
      if (morp.lemma == "\"" || morp.lemma == "`" || morp.lemma == "'") {
        pos1 = j;
        break;
      }
    }
    for (int j = word.end_mid; j >= word.begin_mid; j--) {
      K_Morp &morp = k_sent.morp[j];
      if (morp.lemma == "\"" || morp.lemma == "`" || morp.lemma == "'") {
        pos2 = j;
        break;
      }
    }
    int pos_syl1 = -1, pos_syl2 = -1;
    for (int j = word.begin_sid; j <= word.end_sid; j++) {
      string &syl = k_sent.syllable[j];
      if (syl == "\"" || syl == "`" || syl == "'") {
        pos_syl1 = j;
        break;
      }
    }
    for (int j = word.end_sid; j >= word.begin_sid; j--) {
      string &syl = k_sent.syllable[j];
      if (syl == "\"" || syl == "`" || syl == "'") {
        pos_syl2 = j;
        break;
      }
    }
    // no symbol or 2 symbols
    if ((pos1 < 0 && pos2 < 0) || (pos1 >= 0 && pos1 < pos2)) {
      word.id = w_id++;
      new_words.push_back(word);
      continue;
    }
    // 1 symbol
    // [word] " [word]
    if (pos1 >= word.begin_mid && pos1 == pos2 && pos_syl1 >= word.begin_sid
        && pos_syl1 == pos_syl2) {
      // word
      if (pos1 > word.begin_mid && pos_syl1 > word.begin_sid) {
        K_Word word1 = word;
        word1.id = w_id++;
        word1.end_mid = pos1 - 1;
        if (pos_syl1 - 1 >= 0) word1.end_sid = pos_syl1 - 1;
        else
          cerr << " Preprocess2-1: pos_syl1=" << pos_syl1 << " pos_syl2="
               << pos_syl2 << " pos1=" << pos1 << " pos2=" << pos2
               << " begin_mid=" << word.begin_mid << " " << word.text << endl;
        word1.text = word1.tagged_text = "";
        for (int j = word1.begin_mid; j <= word1.end_mid; j++) {
          K_Morp &morp = k_sent.morp[j];
          if (j > word1.begin_mid) word1.tagged_text += " ";
          word1.text += morp.lemma;
          word1.tagged_text += morp.lemma + "/" + morp.type;
        }
        new_words.push_back(word1);
      }
      // "
      K_Word word2 = word;
      word2.id = w_id++;
      word2.text = k_sent.morp[pos2].lemma;
      word2.tagged_text = word2.text + "/" + k_sent.morp[pos2].type;
      word2.begin_mid = word2.end_mid = pos1;
      if (pos_syl1 >= 0) word2.begin_sid = word2.end_sid = pos_syl1;
      else
        cerr << " Preprocess2-2: pos_syl1=" << pos_syl1 << " " << word.text
             << endl;
      new_words.push_back(word2);
      // word
      if (pos1 < word.end_mid) {
        K_Word word3 = word;
        word3.id = w_id++;
        word3.begin_mid = pos1 + 1;
        if (pos_syl1 >= 0) word3.begin_sid = pos_syl1 + 1;
        else
          cerr << " Preprocess2-3: pos_syl2=" << pos_syl1 << " " << word.text
               << endl;
        word3.text = word3.tagged_text = "";
        for (int j = word3.begin_mid; j <= word3.end_mid; j++) {
          K_Morp &morp = k_sent.morp[j];
          if (j > word3.begin_mid) word3.tagged_text += " ";
          word3.text += morp.lemma;
          word3.tagged_text += morp.lemma + "/" + morp.type;
        }
        new_words.push_back(word3);
      }
    } else {
      word.id = w_id++;
      new_words.push_back(word);
    }
  }
  k_sent.word = new_words;
  // k_sent.sid2wid
  for (int i = 0; i < k_sent.word.size(); i++) {
    K_Word &word = k_sent.word[i];
    for (int j = word.begin_sid; j <= word.end_sid; j++) {
      k_sent.sid2wid[j] = word.id;
    }
  }
  // k_sent.text
  if (k_sent.raw_text == "") k_sent.raw_text = k_sent.text;
  k_sent.text = "";
  /*
  for (int i=0; i < k_sent.word.size(); i++) {
      K_Word &word = k_sent.word[i];
      // text
      if (k_sent.text != "") k_sent.text += " ";
      k_sent.text += word.text;
  }
  */
  for (int i = 0; i < k_sent.syllable.size(); i++) {
    // text
    if (i > 0 && k_sent.space[i] == "B") k_sent.text += " ";
    k_sent.text += k_sent.syllable[i];
  }
}

/**
  입력 텍스트로부터 음절 리스트 생성
  @param input  입력 텍스트
  @param syllable 입력 텍스트로부터 생성한 음절 리스트가 저장될 vector<string>
*/
void Kor_tagger::make_syllable(string &input, vector<string> &syllable) {
  syllable.clear();

  for (int i = 0; i < input.size(); i++) {
    char c = *(input.c_str() + i);
    // 공백 문자 skip
    if (c == ' ' || c == '\t' || c == '\n' || c == '\r') {
      continue;
    }
      // 1-byte 처리
    else if (c >= 0 && c < 128) {
      syllable.push_back(input.substr(i, 1));
    }
      // 나머지 2-byte 처리 : 한글, 한자 포함
    else {
      syllable.push_back(input.substr(i, 2));
      i++;
    }
  }
}

/**
  Feature 변환(입력=문장의 음절 리스트): fixed window = 3
  @param syllables  입력 텍스트로부터 생성된 음절 리스트
  @param space    입력 텍스트의 띄어쓰기 정보 리스트 ("B" or "I" 리스트)
  @param feature_vec  음절별로 생성된 feature들(vector<string>)의 리스트
*/
void Kor_tagger::make_feature(vector<string> &syllables,
                              vector<string> &space,
                              vector<vector<string> > &feature_vec) {
  vector<string> feature;
  feature_vec.reserve(syllables.size());
  // lexical
  string ppp, pp, p, c, n, nn, nnn;
  // type
  string pppt, ppt, pt, ct, nt, nnt, nnnt;
  // space
  string ppps, pps, ps, cs, ns, nns, nnns;

  for (int i = 0; i < syllables.size(); i++) {
    ppp = pp = p = c = n = nn = nnn = "";
    pppt = ppt = pt = ct = nt = nnt = nnnt = "";
    ppps = pps = ps = cs = ns = nns = nnns = "";

    c = syllables[i];
    ct = get_str_type(c);
    if (space[i] == "B") cs = "_";

    if (i - 3 >= 0) {
      ppp = syllables[i - 3];
      pppt = get_str_type(ppp);
      if (space[i - 3] == "B") ppps = "_";
    }
    if (i - 2 >= 0) {
      pp = syllables[i - 2];
      ppt = get_str_type(pp);
      if (space[i - 2] == "B") pps = "_";
    }
    if (i - 1 >= 0) {
      p = syllables[i - 1];
      pt = get_str_type(p);
      if (space[i - 1] == "B") ps = "_";
    }
    if (i + 1 < syllables.size()) {
      n = syllables[i + 1];
      nt = get_str_type(n);
      if (space[i + 1] == "B") ns = "_";
    }
    if (i + 2 < syllables.size()) {
      nn = syllables[i + 2];
      nnt = get_str_type(nn);
      if (space[i + 2] == "B") nns = "_";
    }
    if (i + 3 < syllables.size()) {
      nnn = syllables[i + 3];
      nnnt = get_str_type(nnn);
      if (space[i + 3] == "B") nnns = "_";
    }

    feature.clear();

    // lexical feature
    // unigram
    feature.push_back("L0=" + c);
    feature.push_back("L-1=" + p + cs);
    feature.push_back("L1=" + ns + n);
    // bigram
    feature.push_back("L-2-1=" + pp + ps + p + cs);
    feature.push_back("L-10=" + p + cs + c);
    feature.push_back("L01=" + c + ns + n);
    feature.push_back("L12=" + ns + n + nns + nn);
    // trigram
    feature.push_back("L-3-1=" + ppp + pps + pp + ps + p + cs);
    feature.push_back("L-20=" + pp + ps + p + cs + c);
    feature.push_back("L-11=" + p + cs + c + ns + n);
    feature.push_back("L02=" + c + ns + n + nns + nn);
    feature.push_back("L13=" + ns + n + nns + nn + nnns + nnn);

    // isalpha, isupper, islower, isdigit 관련 피쳐
    if (ppp != "") {
      feature.push_back("p3" + ppps + pppt);
      feature.push_back("p3" + ppps + pppt + pps + ppt);
      feature.push_back("p3" + ppps + pppt + pps + ppt + ps + pt);
    }
    if (pp != "") {
      feature.push_back("p2" + pps + ppt);
      feature.push_back("p2" + pps + ppt + ps + pt);
      feature.push_back("p2" + pps + ppt + ps + pt + cs + ct);
    }
    if (p != "") {
      feature.push_back("p" + ps + pt);
      feature.push_back("p" + ps + pt + cs + ct);
      if (n != "") feature.push_back("p" + ps + pt + cs + ct + ns + nt);
    }
    if (c != "") {
      feature.push_back("c" + cs + ct);
      if (n != "") feature.push_back("c" + cs + ct + ns + nt);
      if (nn != "") feature.push_back("c" + cs + ct + ns + nt + nns + nnt);
    }
    if (n != "") {
      feature.push_back("n" + ns + nt);
      if (nn != "") feature.push_back("n" + ns + nt + nns + nnt);
      if (nnn != "") feature.push_back("n" + ns + nt + nns + nnt + nnns + nnnt);
    }
    if (nn != "") {
      feature.push_back("n2" + nns + nnt);
      if (nnn != "") feature.push_back("n2" + nns + nnt + nnns + nnnt);
    }
    if (nnn != "") {
      feature.push_back("n3" + nnns + nnnt);
    }

    // 어절 사전 피쳐: 현재 어절이 사전에 있는지 검사 - modified by leeck 2014.5
    string eojeol;
    int e_i, e_begin, e_end;
    char temp[100];
    // 현재 음절이 속한 어절의 스트링을 구함
    e_i = e_begin = e_end = i;
    while (e_i >= 0) {
      eojeol = syllables[e_i] + eojeol;
      e_begin = e_i;
      if (space[e_i] == "B") break;
      e_i--;
    }
    e_i = i + 1;
    while (e_i < syllables.size() && space[e_i] == "I") {
      eojeol = eojeol + syllables[e_i];
      e_end = e_i;
      e_i++;
    }
    if (dic.find(eojeol) != dic.end()) {
      snprintf(temp, sizeof temp, "e%ld", eojeol.size());
      string e_feat = temp + space[i] + dic[eojeol];
      feature.push_back(e_feat);
    }

    // 현재 어절 안에서만 사전 검사 - modified by leeck 2014.5
    // dic feature: 2~7음절 검사 (앞뒤 3음절): longest-match 적용
    // next_pos는 longest-match 적용 후, 두번째 단어의 시작 위치
    int next_pos = -3;
    if (1) {
      // -3 ~
      if (i - 3 >= 0 && i - 3 >= e_begin) {
        if (i + 3 < syllables.size() && i + 3 <= e_end
            && dic.find(ppp + pp + p + c + n + nn + nnn) != dic.end()) {
          feature.push_back("-33" + dic[ppp + pp + p + c + n + nn + nnn]);
          next_pos = 4;
        } else if (i + 2 < syllables.size() && i + 2 <= e_end
            && dic.find(ppp + pp + p + c + n + nn) != dic.end()) {
          feature.push_back("-32" + dic[ppp + pp + p + c + n + nn]);
          next_pos = 3;
        } else if (i + 1 < syllables.size() && i + 1 <= e_end
            && dic.find(ppp + pp + p + c + n) != dic.end()) {
          feature.push_back("-31" + dic[ppp + pp + p + c + n]);
          next_pos = 2;
        } else if (dic.find(ppp + pp + p + c) != dic.end()) {
          feature.push_back("-30" + dic[ppp + pp + p + c]);
          next_pos = 1;
        } else if (dic.find(ppp + pp + p) != dic.end()) {
          feature.push_back("-3-1" + dic[ppp + pp + p]);
          next_pos = 0;
        } else next_pos = -2;
      }
      // -2 ~
      if (i - 2 >= 0 && i - 2 >= e_begin && next_pos <= -2) {
        if (i + 3 < syllables.size() && i + 3 <= e_end
            && dic.find(pp + p + c + n + nn + nnn) != dic.end()) {
          feature.push_back("-23" + dic[pp + p + c + n + nn + nnn]);
          next_pos = 4;
        } else if (i + 2 < syllables.size() && i + 2 <= e_end
            && dic.find(pp + p + c + n + nn) != dic.end()) {
          feature.push_back("-22" + dic[pp + p + c + n + nn]);
          next_pos = 3;
        } else if (i + 1 < syllables.size() && i + 1 <= e_end
            && dic.find(pp + p + c + n) != dic.end()) {
          feature.push_back("-21" + dic[pp + p + c + n]);
          next_pos = 2;
        } else if (dic.find(pp + p + c) != dic.end()) {
          feature.push_back("-20" + dic[pp + p + c]);
          next_pos = 1;
        } else if (dic.find(pp + p) != dic.end()) {
          feature.push_back("-2-1" + dic[pp + p]);
          next_pos = 0;
        } else next_pos = -1;
      }
      // -1 ~
      if (i - 1 >= 0 && i - 1 >= e_begin && next_pos <= -1) {
        if (i + 3 < syllables.size() && i + 3 <= e_end
            && dic.find(p + c + n + nn + nnn) != dic.end()) {
          feature.push_back("-13" + dic[p + c + n + nn + nnn]);
          next_pos = 4;
        } else if (i + 2 < syllables.size() && i + 2 <= e_end
            && dic.find(p + c + n + nn) != dic.end()) {
          feature.push_back("-12" + dic[p + c + n + nn]);
          next_pos = 3;
        } else if (i + 1 < syllables.size() && i + 1 <= e_end
            && dic.find(p + c + n) != dic.end()) {
          feature.push_back("-11" + dic[p + c + n]);
          next_pos = 2;
        } else if (dic.find(p + c) != dic.end()) {
          feature.push_back("-10" + dic[p + c]);
          next_pos = 1;
        } else next_pos = 0;
      }
      // 0 ~
      if (next_pos <= 0) {
        if (i + 3 < syllables.size() && i + 3 <= e_end
            && dic.find(c + n + nn + nnn) != dic.end()) {
          feature.push_back("03" + dic[c + n + nn + nnn]);
          next_pos = 4;
        } else if (i + 2 < syllables.size() && i + 2 <= e_end
            && dic.find(c + n + nn) != dic.end()) {
          feature.push_back("02" + dic[c + n + nn]);
          next_pos = 3;
        } else if (i + 1 < syllables.size() && i + 1 <= e_end
            && dic.find(c + n) != dic.end()) {
          feature.push_back("01" + dic[c + n]);
          next_pos = 2;
        } else next_pos = 1;
      }
      // 1 ~
      if (next_pos <= 1) {
        if (i + 3 < syllables.size() && i + 3 <= e_end
            && dic.find(n + nn + nnn) != dic.end()) {
          feature.push_back("13" + dic[n + nn + nnn]);
          next_pos = 4;
        } else if (i + 2 < syllables.size() && i + 2 <= e_end
            && dic.find(n + nn) != dic.end()) {
          feature.push_back("12" + dic[n + nn]);
          next_pos = 3;
        } else next_pos = 2;
      }
    }

    feature_vec.push_back(feature);
  }
}

/**
  Feature 변환: 속도를 빠르게 하기 위해 feature를 int로 표현
  @param syllables  입력 텍스트로부터 생성된 음절 리스트
  @param space    입력 텍스트의 띄어쓰기 정보 리스트 ("B" or "I" 리스트)
  @param feature_vec  음절별로 생성된 feature들(vector<int>)의 리스트
*/
void Kor_tagger::make_feature_int(vector<string> &syllables,
                                  vector<string> &space,
                                  vector<vector<int> > &feature_vec) {
  vector<int> feature;
  feature_vec.reserve(syllables.size());
  // lexical
  string ppp, pp, p, c, n, nn, nnn;
  // type
  string pppt, ppt, pt, ct, nt, nnt, nnnt;
  // space
  string ppps, pps, ps, cs, ns, nns, nnns;

  for (int i = 0; i < syllables.size(); i++) {
    ppp = pp = p = c = n = nn = nnn = "";
    pppt = ppt = pt = ct = nt = nnt = nnnt = "";
    ppps = pps = ps = cs = ns = nns = nnns = "";

    c = syllables[i];
    ct = get_str_type(c);

    c = syllables[i];
    ct = get_str_type(c);
    if (space[i] == "B") cs = "_";

    if (i - 3 >= 0) {
      ppp = syllables[i - 3];
      pppt = get_str_type(ppp);
      if (space[i - 3] == "B") ppps = "_";
    }
    if (i - 2 >= 0) {
      pp = syllables[i - 2];
      ppt = get_str_type(pp);
      if (space[i - 2] == "B") pps = "_";
    }
    if (i - 1 >= 0) {
      p = syllables[i - 1];
      pt = get_str_type(p);
      if (space[i - 1] == "B") ps = "_";
    }
    if (i + 1 < syllables.size()) {
      n = syllables[i + 1];
      nt = get_str_type(n);
      if (space[i + 1] == "B") ns = "_";
    }
    if (i + 2 < syllables.size()) {
      nn = syllables[i + 2];
      nnt = get_str_type(nn);
      if (space[i + 2] == "B") nns = "_";
    }
    if (i + 3 < syllables.size()) {
      nnn = syllables[i + 3];
      nnnt = get_str_type(nnn);
      if (space[i + 3] == "B") nnns = "_";
    }

    feature.clear();

    // lexical feature
    // unigram
    feature.push_back(ssvm.make_pid("L0=" + c));
    feature.push_back(ssvm.make_pid("L-1=" + p + cs));
    feature.push_back(ssvm.make_pid("L1=" + ns + n));
    // bigram
    feature.push_back(ssvm.make_pid("L-2-1=" + pp + ps + p + cs));
    feature.push_back(ssvm.make_pid("L-10=" + p + cs + c));
    feature.push_back(ssvm.make_pid("L01=" + c + ns + n));
    feature.push_back(ssvm.make_pid("L12=" + ns + n + nns + nn));
    // trigram
    feature.push_back(ssvm.make_pid("L-3-1=" + ppp + pps + pp + ps + p + cs));
    feature.push_back(ssvm.make_pid("L-20=" + pp + ps + p + cs + c));
    feature.push_back(ssvm.make_pid("L-11=" + p + cs + c + ns + n));
    feature.push_back(ssvm.make_pid("L02=" + c + ns + n + nns + nn));
    feature.push_back(ssvm.make_pid("L13=" + ns + n + nns + nn + nnns + nnn));

    // isalpha, isupper, islower, isdigit 관련 피쳐
    if (ppp != "") {
      feature.push_back(ssvm.make_pid("p3" + ppps + pppt));
      feature.push_back(ssvm.make_pid("p3" + ppps + pppt + pps + ppt));
      feature.push_back(ssvm.make_pid(
          "p3" + ppps + pppt + pps + ppt + ps + pt));
    }
    if (pp != "") {
      feature.push_back(ssvm.make_pid("p2" + pps + ppt));
      feature.push_back(ssvm.make_pid("p2" + pps + ppt + ps + pt));
      feature.push_back(ssvm.make_pid("p2" + pps + ppt + ps + pt + cs + ct));
    }
    if (p != "") {
      feature.push_back(ssvm.make_pid("p" + ps + pt));
      feature.push_back(ssvm.make_pid("p" + ps + pt + cs + ct));
      feature.push_back(ssvm.make_pid("p" + ps + pt + cs + ct + ns + nt));
    }
    if (c != "") {
      feature.push_back(ssvm.make_pid("c" + cs + ct));
      feature.push_back(ssvm.make_pid("c" + cs + ct + ns + nt));
      feature.push_back(ssvm.make_pid("c" + cs + ct + ns + nt + nns + nnt));
    }
    if (n != "") {
      feature.push_back(ssvm.make_pid("n" + ns + nt));
      feature.push_back(ssvm.make_pid("n" + ns + nt + nns + nnt));
      feature.push_back(ssvm.make_pid("n" + ns + nt + nns + nnt + nnns + nnnt));
    }
    if (nn != "") {
      feature.push_back(ssvm.make_pid("n2" + nns + nnt));
      feature.push_back(ssvm.make_pid("n2" + nns + nnt + nnns + nnnt));
    }
    if (nnn != "") {
      feature.push_back(ssvm.make_pid("n3" + nnns + nnnt));
    }

    // 어절 사전 피쳐: 현재 어절이 사전에 있는지 검사 - modified by leeck 2014.5
    string eojeol;
    int e_i, e_begin, e_end;
    char temp[100];
    // 현재 음절이 속한 어절의 스트링을 구함
    e_i = e_begin = e_end = i;
    while (e_i >= 0) {
      eojeol = syllables[e_i] + eojeol;
      e_begin = e_i;
      if (space[e_i] == "B") break;
      e_i--;
    }
    e_i = i + 1;
    while (e_i < syllables.size() && space[e_i] == "I") {
      eojeol = eojeol + syllables[e_i];
      e_end = e_i;
      e_i++;
    }
    if (dic.find(eojeol) != dic.end()) {
      snprintf(temp, sizeof temp, "e%ld", eojeol.size());
      string e_feat = temp + space[i] + dic[eojeol];
      feature.push_back(ssvm.make_pid(e_feat));
    }

    // 현재 어절 안에서만 사전 검사 - modified by leeck 2014.5
    // dic feature: 2~7음절 검사 (앞뒤 3음절) : longest-match 적용
    // next_pos는 longest-match 적용 후, 두번째 단어의 시작 위치
    int next_pos = -3;
    if (1) {
      // -3 ~
      if (i - 3 >= 0 && i - 3 >= e_begin) {
        if (i + 3 < syllables.size() && i + 3 <= e_end
            && dic.find(ppp + pp + p + c + n + nn + nnn) != dic.end()) {
          feature.push_back(ssvm.make_pid(
              "-33" + dic[ppp + pp + p + c + n + nn + nnn]));
          next_pos = 4;
        } else if (i + 2 < syllables.size() && i + 2 <= e_end
            && dic.find(ppp + pp + p + c + n + nn) != dic.end()) {
          feature.push_back(ssvm.make_pid(
              "-32" + dic[ppp + pp + p + c + n + nn]));
          next_pos = 3;
        } else if (i + 1 < syllables.size() && i + 1 <= e_end
            && dic.find(ppp + pp + p + c + n) != dic.end()) {
          feature.push_back(ssvm.make_pid("-31" + dic[ppp + pp + p + c + n]));
          next_pos = 2;
        } else if (dic.find(ppp + pp + p + c) != dic.end()) {
          feature.push_back(ssvm.make_pid("-30" + dic[ppp + pp + p + c]));
          next_pos = 1;
        } else if (dic.find(ppp + pp + p) != dic.end()) {
          feature.push_back(ssvm.make_pid("-3-1" + dic[ppp + pp + p]));
          next_pos = 0;
        } else next_pos = -2;
      }
      // -2 ~
      if (i - 2 >= 0 && i - 2 >= e_begin && next_pos <= -2) {
        if (i + 3 < syllables.size() && i + 3 <= e_end
            && dic.find(pp + p + c + n + nn + nnn) != dic.end()) {
          feature.push_back(ssvm.make_pid(
              "-23" + dic[pp + p + c + n + nn + nnn]));
          next_pos = 4;
        } else if (i + 2 < syllables.size() && i + 2 <= e_end
            && dic.find(pp + p + c + n + nn) != dic.end()) {
          feature.push_back(ssvm.make_pid("-22" + dic[pp + p + c + n + nn]));
          next_pos = 3;
        } else if (i + 1 < syllables.size() && i + 1 <= e_end
            && dic.find(pp + p + c + n) != dic.end()) {
          feature.push_back(ssvm.make_pid("-21" + dic[pp + p + c + n]));
          next_pos = 2;
        } else if (dic.find(pp + p + c) != dic.end()) {
          feature.push_back(ssvm.make_pid("-20" + dic[pp + p + c]));
          next_pos = 1;
        } else if (dic.find(pp + p) != dic.end()) {
          feature.push_back(ssvm.make_pid("-2-1" + dic[pp + p]));
          next_pos = 0;
        } else next_pos = -1;
      }
      // -1 ~
      if (i - 1 >= 0 && i - 1 >= e_begin && next_pos <= -1) {
        if (i + 3 < syllables.size() && i + 3 <= e_end
            && dic.find(p + c + n + nn + nnn) != dic.end()) {
          feature.push_back(ssvm.make_pid("-13" + dic[p + c + n + nn + nnn]));
          next_pos = 4;
        } else if (i + 2 < syllables.size() && i + 2 <= e_end
            && dic.find(p + c + n + nn) != dic.end()) {
          feature.push_back(ssvm.make_pid("-12" + dic[p + c + n + nn]));
          next_pos = 3;
        } else if (i + 1 < syllables.size() && i + 1 <= e_end
            && dic.find(p + c + n) != dic.end()) {
          feature.push_back(ssvm.make_pid("-11" + dic[p + c + n]));
          next_pos = 2;
        } else if (dic.find(p + c) != dic.end()) {
          feature.push_back(ssvm.make_pid("-10" + dic[p + c]));
          next_pos = 1;
        } else next_pos = 0;
      }
      // 0 ~
      if (next_pos <= 0) {
        if (i + 3 < syllables.size() && i + 3 <= e_end
            && dic.find(c + n + nn + nnn) != dic.end()) {
          feature.push_back(ssvm.make_pid("03" + dic[c + n + nn + nnn]));
          next_pos = 4;
        } else if (i + 2 < syllables.size() && i + 2 <= e_end
            && dic.find(c + n + nn) != dic.end()) {
          feature.push_back(ssvm.make_pid("02" + dic[c + n + nn]));
          next_pos = 3;
        } else if (i + 1 < syllables.size() && i + 1 <= e_end
            && dic.find(c + n) != dic.end()) {
          feature.push_back(ssvm.make_pid("01" + dic[c + n]));
          next_pos = 2;
        } else next_pos = 1;
      }
      // 1 ~
      if (next_pos <= 1) {
        if (i + 3 < syllables.size() && i + 3 <= e_end
            && dic.find(n + nn + nnn) != dic.end()) {
          feature.push_back(ssvm.make_pid("13" + dic[n + nn + nnn]));
          next_pos = 4;
        } else if (i + 2 < syllables.size() && i + 2 <= e_end
            && dic.find(n + nn) != dic.end()) {
          feature.push_back(ssvm.make_pid("12" + dic[n + nn]));
          next_pos = 3;
        } else next_pos = 2;
      }
    }

    feature_vec.push_back(feature);
  }
}

/**
  @param str  음절 스트링
  @return the type of syllable: 숫자, 영어, 기호, 한글, 한자 등을 검사 (EUC-KR 기준)
*/
string Kor_tagger::get_str_type(const string &str) {
  if (str.length() == 1) {
    char c = str[0];
    if (isdigit(c)) {
      return "1";
    } else if (isalpha(c)) {
      if (isupper(c)) return "A";
      else return "a";
    } else if (strchr("~!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?", c) != NULL) {
      return "S";
    }
  } else if (str.length() == 2) {
    unsigned int c = (unsigned char) str.at(0);
    if (strncmp(str.c_str(), "\x0b\xa1"/*"가"*/, 2) >= 0
        && strncmp(str.c_str(), "\xc8\xfe"/*"힝"*/, 2) <= 0) {
      //cerr << str << " " << "Korea ";
      return "K";
    } else if (c >= 0xCA && c <= 0xFD) {
      //cerr << str << " " << "Chinese ";
      return "C";
    }
    //cerr << 0xCA << " " << str << " " << c << " ";
  }
  return "O";
}

/**
  restore morpheme: 음절기반 품사 태깅결과로부터 형태소 원형 복원
  @param k_sent   음절기반 품사 태깅결과가 저장된 K_Sentence 구조체. 여기에 결과가 저장됨
*/
void Kor_tagger::restore_morpheme(K_Sentence &k_sent) {
  vector<string> morph_tag, morph, tag, temp_vec;

  k_sent.morp.clear();

  // 어절별로 구분
  for (int i = 0; i < k_sent.word.size(); i++) {
    string &eojeol_i = k_sent.word[i].tagged_text;
    if (verbose) cerr << i << ": " << eojeol_i << endl;

    // 형태소별 구분
    morph_tag.clear();
    morph.clear();
    tag.clear();
    tokenize(eojeol_i, morph_tag, " ");
    for (int j = 0; j < morph_tag.size(); j++) {
      // 형태소, 태그 구별 저장
      tokenize(morph_tag[j], temp_vec, "/");
      if (temp_vec.size() == 2) {
        morph.push_back(temp_vec[0]);
        tag.push_back(temp_vec[1]);
      } else if (temp_vec.size() == 1) {
        //cerr << morph_tag[j] << " size=1 ";
        morph.push_back("/");
        tag.push_back(temp_vec[0]);
      } else {
        cerr << morph_tag[j] << " error ";
        morph.push_back("XXX");
        tag.push_back("XXX");
      }
    }
    // 한개의 morph로 된 어절은 morph_rule_lp 먼저 적용
    if (morph_tag.size() == 1
        && morph_rule_lp.find(morph_tag[0]) != morph_rule_lp.end()) {
      string rule_key = morph_tag[0];
      string
          rule_result = my_replace_all(morph_rule_lp[morph_tag[0]], "_", " ");
      eojeol_i = my_replace(eojeol_i, rule_key, rule_result);
    } else {
      // 형태소별 규칙 적용
      for (int j = 0; j < morph.size(); j++) {
        // morph_rule 적용 (최대 4개 형태소)
        string key, partial_key, partial_key2, partial_key3, partial_key4;
        string rule_key, rule_key2, rule_key3, rule_key4, rule_result,
            rule_result2, rule_result3, rule_result4;
        int j_position = 0;
        for (int k = 0; k <= 4 && j + k < morph_tag.size(); k++) {
          if (k == 0) {
            key = morph_tag[j + k];   // 형태소와 태그 둘다 봄
            partial_key = tag[j + k]; // 첫 형태소의 태그만 봄
            // modified by leeck 2014.5
            if (tag[j][0] == 'V') { // 첫 형태소가 용언 일때
              int size = morph[j + k].length();
              // 첫 형태소의 마지막 3음절과 태그를 봄: ex. 얄미워한/VV --> 미워한/VV
              if (size > 6)
                partial_key2 = (morph[j + k].substr(size - 6, 6) + "/"
                    + tag[j + k]);
              // 첫 형태소의 마지막 2음절과 태그를 봄: ex. 감겨드는/VV --> 드는/VV
              if (size > 4)
                partial_key3 = (morph[j + k].substr(size - 4, 4) + "/"
                    + tag[j + k]);
              // 첫 형태소의 마지막 1음절과 태그를 봄: ex. 기뻐해/VV --> 해/VV
              if (size > 2)
                partial_key4 = (morph[j + k].substr(size - 2, 2) + "/"
                    + tag[j + k]);
            }
          } else {
            key += ("_" + morph_tag[j + k]);
            partial_key += ("_" + morph_tag[j + k]);
            if (partial_key2 != "") partial_key2 += ("_" + morph_tag[j + k]);
            // modified by leeck 2014.5
            if (partial_key3 != "") partial_key3 += ("_" + morph_tag[j + k]);
            if (partial_key4 != "") partial_key4 += ("_" + morph_tag[j + k]);
          }
          if (verbose)
            cerr << "key:" << key << " p_key:" << partial_key << " p_key2:"
                 << partial_key2 << " p_key3:" << partial_key3 << " p_key4:"
                 << partial_key4 << endl;
          // morph_rule 검사
          if (morph_rule.find(key) != morph_rule.end()) {
            if (verbose)
              cerr << eojeol_i << " : " << key << " -->(key) "
                   << morph_rule[key] << endl;
            rule_key = my_replace_all(key, "_", " ");
            rule_result = my_replace_all(morph_rule[key], "_", " ");
            j_position = j + k;
          }
            // partial key (첫 형태소에서 품사만 보는 규칙 적용)
            //else if (k > 0 && morph_rule.find(partial_key) != morph_rule.end()) {}
          else if (k > 0 && morph_rule.find(partial_key) != morph_rule.end()) {
            if (verbose)
              cerr << eojeol_i << " : " << partial_key << " -->(p_key) "
                   << morph_rule[partial_key] << endl;
            rule_key = my_replace_all(partial_key, "_", " ");
            rule_result = my_replace_all(morph_rule[partial_key], "_", " ");
            j_position = j + k;
          }
            // modified by leeck 2014.5
            // partial_key2 적용 (첫 형태소의 마지막 3음절+태그를 보는 규칙)
          else if ((k > 0 || j + k == morph_tag.size() - 1)
              && partial_key2 != ""
              && morph_rule.find(partial_key2) != morph_rule.end()) {
            if (verbose)
              cerr << eojeol_i << " : " << partial_key2 << " -->(p_key2) "
                   << morph_rule[partial_key2] << endl;
            rule_key2 = my_replace_all(partial_key2, "_", " ");
            rule_result2 = my_replace_all(morph_rule[partial_key2], "_", " ");
            //j_position = j+k;
          }
            // partial_key3 적용 (첫 형태소의 마지막 2음절+태그를 보는 규칙)
          else if ((k > 0 || j + k == morph_tag.size() - 1)
              && partial_key3 != ""
              && morph_rule.find(partial_key3) != morph_rule.end()) {
            if (verbose)
              cerr << eojeol_i << " : " << partial_key3 << " -->(p_key3) "
                   << morph_rule[partial_key3] << endl;
            rule_key3 = my_replace_all(partial_key3, "_", " ");
            rule_result3 = my_replace_all(morph_rule[partial_key3], "_", " ");
            //j_position = j+k;
          }
            // partial_key4 적용 (첫 형태소의 마지막 1음절+태그를 보는 규칙)
          else if ((k > 0 || j + k == morph_tag.size() - 1)
              && partial_key4 != ""
              && morph_rule.find(partial_key4) != morph_rule.end()) {
            if (verbose)
              cerr << eojeol_i << " : " << partial_key4 << " -->(p_key4) "
                   << morph_rule[partial_key4] << endl;
            rule_key4 = my_replace_all(partial_key4, "_", " ");
            rule_result4 = my_replace_all(morph_rule[partial_key4], "_", " ");
            //j_position = j+k;
          }
        }
        // 최장일치 한 개의 규칙만 적용
        if (rule_key != "" && rule_key != rule_result) {
          eojeol_i = my_replace(eojeol_i, rule_key, rule_result);
          j = j_position;
        }
          // morph_rule_lp 적용
        else if (j == morph_tag.size() - 1) {
          if (morph_rule_lp.find(morph_tag[j]) != morph_rule_lp.end()) {
            rule_key = morph_tag[j];
            rule_result = my_replace_all(morph_rule_lp[morph_tag[j]], "_", " ");
            j_position = j;
            eojeol_i = my_replace(eojeol_i, rule_key, rule_result);
          } else {
            // partial_key2 적용
            if (morph_rule_lp.find(partial_key2) != morph_rule_lp.end()) {
              rule_key = partial_key2;
              rule_result =
                  my_replace_all(morph_rule_lp[partial_key2], "_", " ");
              if (verbose)
                cerr << eojeol_i << " :lp " << rule_key << " --> "
                     << rule_result << endl;
              j_position = j;
              eojeol_i = my_replace(eojeol_i, rule_key, rule_result);
            }
              // modified by leeck 2014.5
              // partial_key3 적용
            else if (morph_rule_lp.find(partial_key3) != morph_rule_lp.end()) {
              rule_key = partial_key3;
              rule_result =
                  my_replace_all(morph_rule_lp[partial_key3], "_", " ");
              if (verbose)
                cerr << eojeol_i << " :lp " << rule_key << " --> "
                     << rule_result << endl;
              j_position = j;
              eojeol_i = my_replace(eojeol_i, rule_key, rule_result);
            }
              // partial_key4 적용
            else if (morph_rule_lp.find(partial_key4) != morph_rule_lp.end()) {
              rule_key = partial_key4;
              rule_result =
                  my_replace_all(morph_rule_lp[partial_key4], "_", " ");
              if (verbose)
                cerr << eojeol_i << " :lp " << rule_key << " --> "
                     << rule_result << endl;
              j_position = j;
              eojeol_i = my_replace(eojeol_i, rule_key, rule_result);
            }
          }
        }
        // modified by leeck 2014.5
        // 위 규칙들이 적용이 안될 경우, partial_key2, partial_key3 적용 (첫 형태소의 마지막 음절+태그를 보는 규칙)
        if (rule_key == "") {
          if (rule_key2 != "" && rule_key2 != rule_result2) {
            if (verbose)
              cerr << eojeol_i << " : " << rule_key2 << " -->(p_key2_apply) "
                   << rule_result2 << endl;
            eojeol_i = my_replace(eojeol_i, rule_key2, rule_result2);
          } else if (rule_key3 != "" && rule_key3 != rule_result3) {
            if (verbose)
              cerr << eojeol_i << " : " << rule_key3 << " -->(p_key3_apply) "
                   << rule_result3 << endl;
            eojeol_i = my_replace(eojeol_i, rule_key3, rule_result3);
          } else if (rule_key4 != "" && rule_key4 != rule_result4) {
            if (verbose)
              cerr << eojeol_i << " : " << rule_key4 << " -->(p_key4_apply) "
                   << rule_result4 << endl;
            eojeol_i = my_replace(eojeol_i, rule_key4, rule_result4);
          }
        }
      } // end for
    } // end if

    // 수정된 형태소 구분
    morph_tag.clear();
    morph.clear();
    tag.clear();
    tokenize(eojeol_i, morph_tag, " ");
    for (int j = 0; j < morph_tag.size(); j++) {
      // 형태소, 태그 구별
      string lemma, type;
      tokenize(morph_tag[j], temp_vec, "/");
      if (temp_vec.size() == 2) {
        lemma = temp_vec[0];
        type = temp_vec[1];
      } else if (temp_vec.size() == 1) {
        //cerr << morph_tag[j] << " size=1 ";
        lemma = "/";
        type = temp_vec[0];
      } else {
        cerr << endl << " Error3: " << morph_tag[j] << endl;
        for (vector<string>::size_type lem_i = 0; lem_i < temp_vec.size() - 1; lem_i++) {
          if (lem_i != 0)
            lemma += "/";
          lemma += temp_vec[lem_i];
        }
        type = temp_vec[(int) temp_vec.size() - 1];
      }
      // doc.sent.morp
      K_Morp morp;
      morp.lemma = lemma;
      morp.type = type;
      morp.wid = i;
      morp.id = k_sent.morp.size();
      k_sent.morp.push_back(morp);
    }
  }

  // doc.sent.word: begin_mid, end_mid
  int prev_wid = -1;
  for (int i = 0; i < k_sent.morp.size(); i++) {
    int mid = k_sent.morp[i].id;
    int wid = k_sent.morp[i].wid;
    if (wid != prev_wid) k_sent.word[wid].begin_mid = mid;
    k_sent.word[wid].end_mid = mid;
    prev_wid = wid;
  }
}
