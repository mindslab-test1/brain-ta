#include "libmaum/brain/nlp/nlp2kor.h"
#include "kor-tagger.h"
#include <libmaum/common/encoding.h>
#include <libmaum/brain/nlp/kor-analysis.h>
#include <libmaum/common/config.h>
#include "libmaum/brain/error/ta-error.h"
#include <fstream>

void Nlp2Kor::UpdateFeatures() {
  auto logger = LOGGER();
  anal_ = std::make_shared<Kor>();

  if (HasFeature(NlpFeature::NLPF_MORPHEME)) {
    anal_->init_tagger(res_path_);
    logger->debug("[NLP2] loaded init_tagger");
  }

  if (HasFeature(NlpFeature::NLPF_NAMED_ENTITY)) {
    anal_->init_NER(res_path_);
    logger->debug("[NLP2] loaded init_ner");
  }

  if (HasFeature(NlpFeature::NLPF_PARSER)) {
    anal_->init_parser(res_path_);
    logger->debug("[NLP2] loaded init_parser");
  }
}

void Nlp2Kor::Uninit() {
  anal_.reset();
}

/**
  @breif 분석된 결과를 Document에 저장시키기 위한 함수
  @param K_Doc &kdoc : 언어분석 결과를 담고 있는 구조체
  @param Document *document : 언어분석 결과를 클라이언트에 보낼 message
  @param NlpAnalysis level : 언어분석 레벨
  @return None
*/
void ToMessage(const K_Doc &kdoc, Document *document, NlpAnalysisLevel level) {
  string result;
  // sentence
  for (const K_Sentence &sent: kdoc.sentence) {
    // 각 문장에 대한 정보
    maum::brain::nlp::Sentence *sentence = document->add_sentences();
    sentence->set_seq(sent.id);
    //string sent_text = my_replace(sent.text, "\\", "\\\\");
    //result += my_replace(sent_text, "\"", "\\\"");
    EuckrToUtf8(sent.text, result);
    sentence->set_text(result);
    // morp, 형태소 분석 결과
    for (const K_Morp &morp: sent.morp) {
      maum::brain::nlp::Morpheme *morpheme = sentence->add_morps();
      morpheme->set_seq(morp.id);
      EuckrToUtf8(morp.lemma, result);
      morpheme->set_lemma(result);
      // string morp_lemma = my_replace(morp.lemma, "\\", "\\\\");
      // result += my_replace(morp_lemma, "\"", "\\\"");
      morpheme->set_type(morp.type);
      morpheme->set_wid(morp.wid);
    }
    // word, word에 대한 정보 및 분석결과
    // morp와 결과는 같지만 구조체 내 멤버변수가 다름
    for (const K_Word &word: sent.word) {
      maum::brain::nlp::Word *w = sentence->add_words();
      w->set_seq(word.id);
      // string word_text = my_replace(word.text, "\\", "\\\\");
      // result += my_replace(word_text, "\"", "\\\"");
      EuckrToUtf8(word.text, result);
      w->set_text(result);
      EuckrToUtf8(word.tagged_text, result);
      w->set_tagged_text(result);
      w->set_begin(word.begin_mid);
      w->set_end(word.end_mid);
    }
    if (level == NlpAnalysisLevel::NLP_ANALYSIS_MORPHEME)
      continue;
    // NER, 개체명 인식 결과
    for (const K_NE &ne: sent.NE) {
      maum::brain::nlp::NamedEntity *entity = sentence->add_nes();
      entity->set_seq(ne.id);
      // string ne_text = my_replace(ne.text, "\\", "\\\\");
      // result += my_replace(ne_text, "\"", "\\\"");
      EuckrToUtf8(ne.text, result);
      entity->set_text(result);
      entity->set_type(ne.type);
      entity->set_begin(ne.begin);
      entity->set_end(ne.end);
      entity->set_begin_sid(ne.begin_sid);
      entity->set_end_sid(ne.end_sid);
    }
    if (level == NlpAnalysisLevel::NLP_ANALYSIS_NAMED_ENTITY)
      continue;
  }
}

void ToKDoc(const Document *document, K_Doc &kdoc) {
  string result;
  string original_text = "";
  for (auto &sent : document->sentences()) {
    K_Sentence sentence;
    sentence.id = sent.seq();
    Utf8ToEuckr(sent.text(), result);
    sentence.text = result;
    original_text += result;

    // morp 형태소 분석 결과
    for (const auto &morp : sent.morps()) {
      K_Morp morpheme;
      morpheme.id = morp.seq();
      Utf8ToEuckr(morp.lemma(), result);
      morpheme.lemma = result;
      morpheme.type = morp.type();
      morpheme.wid = morp.wid();
      sentence.morp.push_back(morpheme);
    }

    // word 분석결과
    for (const auto &word : sent.words()) {
      K_Word w;
      w.id = word.seq();
      Utf8ToEuckr(word.text(), result);
      w.text = result;
      Utf8ToEuckr(word.tagged_text(), result);
      w.tagged_text = result;
      w.begin_mid = word.begin();
      w.end_mid = word.end();
      sentence.word.push_back(w);
    }
    kdoc.sentence.push_back(sentence);
  }
  kdoc.text = original_text;
}

void Nlp2Kor::AnalyzeOne(const InputText *text, Document *document) {
  const string &otext = text->text();
  auto nlp_level = text->level();
  anal_->use_preprocess = text->split_sentence();

  string sent;
  Utf8ToEuckr(otext.c_str(), sent);
  K_Doc kdoc;
  switch (nlp_level) {
    // 모든 모듈을 분석
    case NlpAnalysisLevel::NLP_ANALYSIS_ALL: {
      anal_->do_tagging(sent, kdoc);
      anal_->do_NER(kdoc);
      anal_->do_parsing(kdoc);
      ToMessage(kdoc, document, NlpAnalysisLevel::NLP_ANALYSIS_ALL);
      break;
    }
      // word, morp 분석
    case NlpAnalysisLevel::NLP_ANALYSIS_MORPHEME: {
      anal_->do_tagging(sent, kdoc);
      ToMessage(kdoc, document, NlpAnalysisLevel::NLP_ANALYSIS_MORPHEME);
      break;
    }
      // word, morp, ner 분석
    case NlpAnalysisLevel::NLP_ANALYSIS_NAMED_ENTITY: {
      anal_->do_tagging(sent, kdoc);
      anal_->do_NER(kdoc);
      ToMessage(kdoc, document, NlpAnalysisLevel::NLP_ANALYSIS_NAMED_ENTITY);
      break;
    }
    default: {
      break;
    }
  }

  document->set_lang(maum::common::LangCode::kor);
}

void Nlp2Kor::SetClassifierModel(const string &classifier_model_path) {
  auto logger = LOGGER();
  auto &c = libmaum::Config::Instance();
  string sa_model_root;
  sa_model_root = c.Get("brain-ta.cl.model.dir");
  string full_path = sa_model_root;

  full_path += '/';
  full_path += classifier_model_path;
  logger->debug("[NLP2] model full path : {}", full_path);

  if (access(full_path.c_str(), F_OK) != 0) {
    ostringstream msg;
    msg << " Can't Load DNN models in " << sa_model_root;
    std::cerr << msg.str() << endl;
    throw ta::Exception("NLP2", msg.str());
  }
  anal_->init_sa(full_path, res_path_);
  logger->debug("[NLP2] loaded init_sa");
}

void Nlp2Kor::Classify(const Document &document, ClassifiedSummary *sum) {

  auto logger = LOGGER();

  K_Doc kdoc;
  float prob;

  // NlpDocument to K_Doc
  ToKDoc(&document, kdoc);

  vector<vector<float>> vec_prob;
  vector<vector<string>> vec_sa;
  vec_sa = anal_->do_multy_sa(kdoc, vec_prob);

  for (vector<K_Sentence>::size_type i = 0; i < kdoc.sentence.size(); i++) {
    auto sent = sum->add_sentences();
    sent->set_text(EuckrToUtf8(kdoc.sentence[i].text));
    for (vector<vector<string>>::size_type j = 0; j < vec_sa[i].size(); j++) {
      // 이를 소팅할 수 있는 map 에 추가하여 가장 높은 값이 나오도록
      // 순서는 float, string map 으로 처리한다.
      auto cl = sent->add_cls();
      cl->set_sa(EuckrToUtf8(vec_sa[i][j]));
      cl->set_probability(vec_prob[i][j]);
    }
  }
  prob = vec_prob[0][0];
  for (auto sent : kdoc.sentence) {
    logger->debug("sent: {} , found SA: {} ",
                  EuckrToUtf8(sent.text), EuckrToUtf8(sent.SA));
  }

  // 출력
  // 문장이 여러개면 여러개의 결과물이 나올 수 있겠으나, 하나만 취한다.
  if (kdoc.sentence.empty()) {
    logger->debug<const char *>("Doc has no sentences, return []");
    sum->set_c1_top("");
    sum->set_cl_top("");
  } else if (vec_prob[0][0] > 0) {
    // confidence level modify
    auto v =  EuckrToUtf8(kdoc.sentence[0].SA);
    sum->set_c1_top(v);
    sum->set_cl_top(v);
    sum->set_probability_top(prob);
  } else {
    logger->debug("found result is not proper! {}", vec_prob[0][0]);
    sum->set_c1_top("");
    sum->set_cl_top("");
  }
}

void Nlp2Kor::GetPosTaggedStr(const Document *document,
                              vector<string> *pos_tagged_str) {
  K_Doc k_doc;
  ToKDoc(document, k_doc);
  for (auto &k_sent : k_doc.sentence) {
    string result;
    for (const auto &word : k_sent.word) {
      result += ' ';
      result += my_replace_all(word.tagged_text, " ", "+");
    }
    pos_tagged_str->push_back(EuckrToUtf8(result));
  }
};

void ReplaceAll(std::string &sentence,
                const std::string &from,
                std::string &to) {
  size_t start_pos = 0;
  while ((start_pos = sentence.find(from, start_pos)) != std::string::npos) {

    if (sentence[start_pos + from.length()] != '/') {
      // 개체명 뒤에 공백을 제외한 문자는 +를 붙임
      if (start_pos + from.length() != sentence.length()
          && sentence[start_pos + from.length()] != ' ') {
        to += '+';
      }
      sentence.replace(start_pos, from.length(), to);
      break;
    }
    start_pos += to.length();
  }
}

void Nlp2Kor::GetNerTaggedStr(const Document *document,
                              vector<string> *ner_tagged_str) {
  for (auto &sent: document->sentences()) {
    string result;
    string temp = sent.text();
    if (result != "") result += ' ';
    result += temp;
    if (sent.nes().size()) {
      for (auto &ner: sent.nes()) {
        string ner_word;
        ner_word += ner.text();
        ner_word += '/';
        ner_word += ner.type();
        ReplaceAll(temp, ner.text(), ner_word);
        result = temp;
      }
    }
    ner_tagged_str->push_back(result);
  }
}