/**
 * @file mynn.cpp
 * @brief (Word embedding-based) Neural Network class
 * @author Changki Lee (leeck@kangwon.ac.kr)
 * @date 2014/10/23
 */
#include <stdlib.h>
#include <math.h>
#include <ctime>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include "mynn.h"

using namespace std;

/// load model
void MyNN::load_model(const string &model) {
  ifstream f(model.c_str());
  if (!f) {
    cerr << "Fail to open file: " << model << endl;
    exit(1);
  }

  float max, min;
  int count, size;
  string line;
  timer t;

  // word_embed_num
  getline(f, line);
  cerr << line;
  getline(f, line);
  word_embed_num = atoi(line.c_str());
  cerr << word_embed_num << endl;
  // feat_embed_num
  getline(f, line);
  cerr << line;
  getline(f, line);
  feat_embed_num = atoi(line.c_str());
  cerr << feat_embed_num << endl;
  // hidden_num
  getline(f, line);
  cerr << line;
  getline(f, line);
  hidden_num = atoi(line.c_str());
  cerr << hidden_num << endl;
  // output_num
  getline(f, line);
  cerr << line;
  getline(f, line);
  output_num = atoi(line.c_str());
  cerr << output_num << endl;
  // word_vocab_num
  getline(f, line);
  cerr << line;
  getline(f, line);
  word_vocab_num = atoi(line.c_str());
  cerr << word_vocab_num << endl;
  // feat_vocab_num
  getline(f, line);
  cerr << line;
  getline(f, line);
  feat_vocab_num = atoi(line.c_str());
  cerr << feat_vocab_num << endl;
  // input_word_num
  getline(f, line);
  cerr << line;
  getline(f, line);
  input_word_num = atoi(line.c_str());
  cerr << input_word_num << endl;
  // input_feat_num
  getline(f, line);
  cerr << line;
  getline(f, line);
  input_feat_num = atoi(line.c_str());
  cerr << input_feat_num << endl;
  // word_embedding_weights
  getline(f, line);
  cerr << line;
  count = 0;
  max = -1e10;
  min = 1e10;
  word_embedding_weights.clear();
  for (int i = 0; i < word_vocab_num; i++) {
    getline(f, line);
    vector<float> weights;
    vector<string> tokens;
    tokenize(line, tokens, " \t\r\n");
    vector<string>::iterator it = tokens.begin();
    for (; it != tokens.end(); it++) {
      float w = atof(it->c_str());
      weights.push_back(w);
      if (w > max) max = w;
      if (w < min) min = w;
      count++;
    }
    word_embedding_weights.push_back(weights);
  }
  cerr << count << " = " << word_embedding_weights.size() << " X " <<
      word_embed_num << " : min=" << min << ", max=" << max << endl;
  // feat_embedding_weights
  getline(f, line);
  cerr << line;
  count = 0;
  max = -1e10;
  min = 1e10;
  feat_embedding_weights.clear();
  for (int i = 0; i < feat_vocab_num; i++) {
    getline(f, line);
    vector<float> weights;
    vector<string> tokens;
    tokenize(line, tokens, " \t\r\n");
    vector<string>::iterator it = tokens.begin();
    for (; it != tokens.end(); it++) {
      float w = atof(it->c_str());
      weights.push_back(w);
      if (w > max) max = w;
      if (w < min) min = w;
      count++;
    }
    feat_embedding_weights.push_back(weights);
  }
  cerr << count << " = " << feat_embedding_weights.size() << " X " <<
      feat_embed_num << " : min=" << min << ", max=" << max << endl;
  // word_embed_to_hid_weights
  getline(f, line);
  cerr << line;
  count = 0;
  max = -1e10;
  min = 1e10;
  word_embed_to_hid_weights.clear();
  size = word_embed_num * input_word_num;
  for (int i = 0; i < size; i++) {
    getline(f, line);
    vector<float> weights;
    vector<string> tokens;
    tokenize(line, tokens, " \t\r\n");
    vector<string>::iterator it = tokens.begin();
    for (; it != tokens.end(); it++) {
      float w = atof(it->c_str());
      weights.push_back(w);
      if (w > max) max = w;
      if (w < min) min = w;
      count++;
    }
    word_embed_to_hid_weights.push_back(weights);
  }
  cerr << count << " = " << word_embed_to_hid_weights.size() << " X " <<
      hidden_num << " : min=" << min << ", max=" << max << endl;
  // feat_embed_to_hid_weights
  getline(f, line);
  cerr << line;
  count = 0;
  max = -1e10;
  min = 1e10;
  feat_embed_to_hid_weights.clear();
  size = feat_embed_num * input_feat_num;
  for (int i = 0; i < size; i++) {
    getline(f, line);
    vector<float> weights;
    vector<string> tokens;
    tokenize(line, tokens, " \t\r\n");
    vector<string>::iterator it = tokens.begin();
    for (; it != tokens.end(); it++) {
      float w = atof(it->c_str());
      weights.push_back(w);
      if (w > max) max = w;
      if (w < min) min = w;
      count++;
    }
    feat_embed_to_hid_weights.push_back(weights);
  }
  cerr << count << " = " << feat_embed_to_hid_weights.size() << " X " <<
      hidden_num << " : min=" << min << ", max=" << max << endl;
  // hid_to_output_weights
  getline(f, line);
  cerr << line;
  count = 0;
  max = -1e10;
  min = 1e10;
  hid_to_output_weights.clear();
  for (int i = 0; i < hidden_num; i++) {
    getline(f, line);
    vector<float> weights;
    vector<string> tokens;
    tokenize(line, tokens, " \t\r\n");
    vector<string>::iterator it = tokens.begin();
    for (; it != tokens.end(); it++) {
      float w = atof(it->c_str());
      weights.push_back(w);
      if (w > max) max = w;
      if (w < min) min = w;
      count++;
    }
    hid_to_output_weights.push_back(weights);
  }
  cerr << count << " = " << hid_to_output_weights.size() << " X " <<
      output_num << " : min=" << min << ", max=" << max << endl;
  // hid_bias
  getline(f, line);
  cerr << line;
  count = 0;
  max = -1e10;
  min = 1e10;
  hid_bias.clear();
  {
    getline(f, line);
    vector<string> tokens;
    tokenize(line, tokens, " \t\r\n");
    vector<string>::iterator it = tokens.begin();
    for (; it != tokens.end(); it++) {
      float w = atof(it->c_str());
      hid_bias.push_back(w);
      if (w > max) max = w;
      if (w < min) min = w;
      count++;
    }
  }
  cerr << count << " : min=" << min << ", max=" << max << endl;
  // output_bias
  getline(f, line);
  cerr << line;
  count = 0;
  max = -1e10;
  min = 1e10;
  output_bias.clear();
  {
    getline(f, line);
    vector<string> tokens;
    tokenize(line, tokens, " \t\r\n");
    vector<string>::iterator it = tokens.begin();
    for (; it != tokens.end(); it++) {
      float w = atof(it->c_str());
      output_bias.push_back(w);
      if (w > max) max = w;
      if (w < min) min = w;
      count++;
    }
  }
  cerr << count << " : min=" << min << ", max=" << max << endl;
  cerr << "(" << t.elapsed() << ") done." << endl;

  // init cache
  cerr << "cache(" << cache_size << " words, " <<
      cache_size * input_word_num * hidden_num << " float, " <<
      sizeof(float) * cache_size * input_word_num * hidden_num / (1024 * 1024)
      << "MB) ... ";
  timer t2;
  cache.clear();
  int total_size = cache_size * input_word_num * hidden_num;
  for (int i = 0; i < total_size; i++) {
    cache.push_back(0);
  }
#define CACHE_IDX(x, y, z) (((x)*input_word_num*hidden_num)+((y)*hidden_num)+(z))
  // caculate cache
  for (int index = 0; index < cache_size; index++) {
    for (int i = 0; i < hidden_num; i++) {
      for (int w = 0; w < input_word_num; w++) {
        int begin_j = word_embed_num * w;
        float local_sum = 0;
        for (int j = 0; j < word_embed_num; j++) {
          local_sum += word_embed_to_hid_weights[begin_j + j][i]
              * word_embedding_weights[index][j];
        }
        //cache[index][w][i] = local_sum;
        cache[CACHE_IDX(index, w, i)] = local_sum;
      }
    }
  }
  cerr << "(" << t2.elapsed() << ") done." << endl;
}

/// classify
pair<int, float> MyNN::classify(vector<int> &word_input,
                                vector<int> &feat_input) {
  vector<float> word_embedding_layer_state;
  vector<float> feat_embedding_layer_state;
  vector<float> hidden_layer_state;
  vector<float> output_layer_state;

  fprop(word_input, feat_input, word_embedding_layer_state,
        feat_embedding_layer_state, hidden_layer_state,
        output_layer_state);
  float max = -1e10;
  int max_i = 0;
  for (int i = 0; i < output_num; i++) {
    if (output_layer_state[i] > max) {
      max = output_layer_state[i];
      max_i = i;
    }
  }
  return make_pair(max_i, max);
}

/// forward propagation
void MyNN::fprop(vector<int> &word_input, vector<int> &feat_input,
                 vector<float> &word_embedding_layer_state,
                 vector<float> &feat_embedding_layer_state,
                 vector<float> &hidden_layer_state,
                 vector<float> &output_layer_state) {
  // word_embedding_layer_state
  word_embedding_layer_state.clear();
  for (auto &index : word_input) {
    if (index >= word_vocab_num) {
      cerr << "Error(mynn.frop.word_embed): index=" << index << ">="
          << word_vocab_num << endl;
    }
    for (int j = 0; j < word_embed_num; j++) {
      word_embedding_layer_state.push_back(word_embedding_weights[index][j]);
    }
  }

  // feat_embedding_layer_state
  feat_embedding_layer_state.clear();
  for (auto & index : feat_input) {
    if (index >= feat_vocab_num) {
      cerr << "Error(mynn.frop.feat_embed): index=" << index << ">="
          << feat_vocab_num << endl;
    }
    for (int j = 0; j < feat_embed_num; j++) {
      feat_embedding_layer_state.push_back(feat_embedding_weights[index][j]);
    }
  }

  // hidden_layer_state
  hidden_layer_state.clear();
  // hidden_layer_state = word_embed_to_hid_weights' X word_embedding_layer_state
  //#pragma omp parallel for
  for (int i = 0; i < hidden_num; i++) {
    float sum = 0;
    // use cache
    if (cache_size > 0) {
      for (vector<int>::size_type w = 0; w < word_input.size(); w++) {
        int &index = word_input[w];
        int begin_j = int(word_embed_num * w);
        int end_j = int(word_embed_num * (w + 1));
        if (index < cache_size) {
          //sum += cache[index][w][i];
          sum += cache[CACHE_IDX(index, w, i)];
        } else {
          for (int j = begin_j; j < end_j; j++) {
            sum +=
                word_embed_to_hid_weights[j][i] * word_embedding_layer_state[j];
          }
        }
      }
    } else {
      // no cache
      for (vector<float>::size_type j = 0; j < word_embedding_layer_state.size(); j++) {
        sum += word_embed_to_hid_weights[j][i] * word_embedding_layer_state[j];
      }
    }
    hidden_layer_state.push_back(sum);
  }
  // hidden_layer_state += feat_embed_to_hid_weights' X feat_embedding_layer_state
  // hidden_layer_state += hid_bias
  //#pragma omp parallel for
  for (int i = 0; i < hidden_num; i++) {
    float sum = 0;
    for (vector<float>::size_type j = 0; j < feat_embedding_layer_state.size(); j++) {
      sum += feat_embed_to_hid_weights[j][i] * feat_embedding_layer_state[j];
    }
    sum += hid_bias[i];
    hidden_layer_state[i] += sum;
  }
  // activation func.
  if (activation_func == SIGM) {
    for (int i = 0; i < hidden_num; i++) {
      hidden_layer_state[i] = sigmoid(hidden_layer_state[i]);
    }
  } else if (activation_func == RELU) {
    for (int i = 0; i < hidden_num; i++) {
      hidden_layer_state[i] = relu(hidden_layer_state[i]);
    }
  } else {
    cerr << "Error: Unknown activation function: " << activation_func << endl;
    exit(1);
  }

  // dropout
  if (dropout_fraction > 0) {
    for (int i = 0; i < hidden_num; i++) {
      hidden_layer_state[i] = (1 - dropout_fraction) * hidden_layer_state[i];
    }
  }

  // output_layer_state
  output_layer_state.clear();
  // output_layer_state = hid_to_output_weights' X hidden_layer_state
  // output_layer_state += output_bias
  float max = 0;
  for (int i = 0; i < output_num; i++) {
    float sum = 0;
    for (vector<float>::size_type j = 0; j < hidden_layer_state.size(); j++) {
      sum += hid_to_output_weights[j][i] * hidden_layer_state[j];
    }
    sum += output_bias[i];
    output_layer_state.push_back(sum);
    if (sum > max) max = sum;
  }
  // subtract maximum
  for (int i = 0; i < output_num; i++) {
    output_layer_state[i] -= max;
  }
  // softmax
  float sum = 0;
  for (int i = 0; i < output_num; i++) {
    output_layer_state[i] = exp(output_layer_state[i]);
    sum += output_layer_state[i];
  }
  for (int i = 0; i < output_num; i++) {
    output_layer_state[i] /= sum;
  }
}

/** Tokenize string to words.
    Tokenization of string and assignment to word vector.
    Delimiters are set of char.
    @param str string
    @param tokens token vector
    @param delimiters delimiters to divide string
    @return none
*/
void MyNN::tokenize(const string &str,
                    vector<string> &tokens,
                    const string &delimiters) {
  tokens.clear();
  string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  string::size_type pos = str.find_first_of(delimiters, lastPos);
  while (string::npos != pos || string::npos != lastPos) {
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    lastPos = str.find_first_not_of(delimiters, pos);
    pos = str.find_first_of(delimiters, lastPos);
  }
}

/** Split string to words.
    Tokenization of string and assignment to word vector.
    Delimiter is string.
    @param str string
    @param tokens token vector
    @param delimiter delimiter to divide string
    @return none
*/
void MyNN::split(const string &str,
                 vector<string> &tokens,
                 const string &delimiter) {
  tokens.clear();
  string::size_type pos = str.find(delimiter, 0);
  string::size_type lastPos = 0;
  while (0 <= pos && str.size() > pos) {
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    lastPos = pos + delimiter.size();
    pos = str.find(delimiter, lastPos);
  }
  tokens.push_back(str.substr(lastPos, str.size() - lastPos));
}

