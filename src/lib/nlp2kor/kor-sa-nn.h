#if !defined(KOR_SA_NN_INCLUDED_)
#define KOR_SA_NN_INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma warning( disable : 4786 )

#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <set>
#include <map>
//#include <unordered_map>
#include <deque>

#include "seq2label.h"
#include "libmaum/brain/nlp/kdoc.h"

using namespace std;

/// Korean Sentiment Analyzer using LSTM RNN
class Kor_SA_NN {
 public:
  Kor_SA_NN();
  virtual ~Kor_SA_NN();

  /// 초기화 함수: load_model
  int init(const string &dir, const string &res);
#if 0
  int init(const string &dir, const string &proj);
#endif
  void load_model(const string &file);
  void load_outcome_vec(const string &file);
  void load_word_map(const string &file);

  /// 종료 함수
  void close();

  /// Sentiment Analyzer
  string do_SA(K_Sentence &ksent, double &prob);

  vector<string> do_multy_SA(K_Sentence &k_sent, vector<float> &vec_prob);

  /////////////////////////////////////////////
  // public member variables
  /////////////////////////////////////////////

  int verbose;
  int model_loaded;
  int binary_model;

 private:
  /// normalize digit and alphabet: 0~9 --> 0, lower, '|' --> '_'
  inline string normalize(const string &word) {
    char result[1000];
    result[0] = 0;
    for (string::size_type i = 0; i < word.length() && i < 999; i++) {
      char c = word.at(i);
      if (c >= '0' && c <= '9') c = '0';
      if (c == '|') c = '_';
      if (isupper(c))
        c = tolower(c);
      result[i] = c;
    }
    if (word.length() < 999) {
      result[word.length()] = 0;
    } else {
      result[999] = 0;
    }
    return result;
  }

  /// normalize digit: 0~9 --> 0
  inline string normalize_num(const string &word) {
    char result[1000];
    result[0] = 0;
    for (string::size_type i = 0; i < word.length() && i < 999; i++) {
      char c = word.at(i);
      if (c >= '0' && c <= '9') c = '0';
      if (c == '|') c = '_';  // 영향을 안 미침
      result[i] = c;
    }
    if (word.length() < 999) {
      result[word.length()] = 0;
    } else {
      result[999] = 0;
    }
    return result;
  }

  /** Tokenize string to words.
  Tokenization of string and assignment to word vector.
  Delimiters are set of char.
  @param str string
  @param tokens token vector
  @param delimiters delimiters to divide string
  @return none
  */
  inline void tokenize(const string &str,
                       vector<string> &tokens,
                       const string &delimiters) {
    tokens.clear();
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    string::size_type pos = str.find_first_of(delimiters, lastPos);
    while (string::npos != pos || string::npos != lastPos) {
      tokens.push_back(str.substr(lastPos, pos - lastPos));
      lastPos = str.find_first_not_of(delimiters, pos);
      pos = str.find_first_of(delimiters, lastPos);
    }
  }

  /////////////////////////////////////////////
  // private member variables
  /////////////////////////////////////////////

  Seq2label nn;  ///< NN
  vector<string> outcome_vec;     ///< outcome vector
  map<string, int> word_map;      ///< word dic
};

#endif // KOR_SA_INCLUDED_
