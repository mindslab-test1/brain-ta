#include <stdio.h>
#include <algorithm>
#include <sstream>
#include <libmaum/brain/error/ta-error.h>
#include "kor-sa-nn.h"
#include "kor-sa-nn.h"

#ifdef _OPENMP
#include <omp.h>
#endif

/**
  Kor_SA_NN 인스턴스 생성 
*/
Kor_SA_NN::Kor_SA_NN() {
  verbose = 0;
  model_loaded = 0;
  binary_model = 0;

  // nn
//  nn.activation = RELU;
  nn.activation = TANH;
  nn.x_drop_out_rate = 0.8;
  nn.h_drop_out_rate = 0.5;
}

/// Destruction
Kor_SA_NN::~Kor_SA_NN() {
  outcome_vec.clear();
  word_map.clear();
}

/**
  Kor_SA_NN 초기화
  @param dir  리소스 디렉토리
  @return 초기화 성공 (0) 여부
*/
int Kor_SA_NN::init(const string &dir, const string &rsc) {
  string file;
  // nn
  file = dir + "/sa_model.txt";
  load_model(file);
  // outcome vocab.
  file = dir + "/vocab_sa.txt";
  load_outcome_vec(file);
  // word vocab.
  file = rsc + "/vocab_sa_word.txt";
  load_word_map(file);

  return 0;
}
#if 0
int Kor_SA_NN::init(const string &dir, const string &proj) {
  string file;
  // nn
  file = dir + "/" + proj + "_sa_model.txt";
  load_model(file);
  // outcome vocab.
  file = dir + "/" + proj + "_vocab_sa.txt";
  load_outcome_vec(file);
  // word vocab.
  file = dir + "/" + proj + "_vocab_sa_word.txt";
  load_word_map(file);
  return 0;
}
#endif

/**
  Neural Network model loading
  @param file Neural Network model file
*/
void Kor_SA_NN::load_model(const string &file) {
  try {
    nn.load_model(file);
  } catch (exception e) {
    cerr << "Error: " << e.what() << endl;
    exit(1);
  }
  model_loaded = 1;
}

/**
  Outcome dictionary loading
  @param file Outcome dictionary file
*/
void Kor_SA_NN::load_outcome_vec(const string &file) {
  FILE *in;
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    exit(1);
  }

  cerr << "loading " << file << " ... ";
  char temp[1000];
  while (!feof(in)) {
    fgets(temp, 1000, in);
    if (temp[0] == ';') continue;
    vector<string> token;
    tokenize(temp, token, " \t\n\r");
    if (token.size() == 1) {
      outcome_vec.push_back(token[0]);
    } else if (token.size() > 1) {
      cerr << "Error(load_outcome_vec): " << temp << endl;
    }
    temp[0] = '\0';
  }
  fclose(in);
  cerr << "done: " << outcome_vec.size() << endl;
}

/**
  Word dictionary loading
  @param file word dictionary file
*/
void Kor_SA_NN::load_word_map(const string &file) {
  FILE *in;
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    exit(1);
  }

  cerr << "loading " << file << " ... ";
  char temp[1000];
  int idx = 0;
  while (!feof(in)) {
    fgets(temp, 1000, in);
    vector<string> token;
    tokenize(temp, token, " \t\n\r");
    if (token.size() == 1) {
      word_map[token[0]] = idx++;
    } else if (token.size() > 1) {
      cerr << "Error(load_word_map):" << idx << " [" << temp << "]" << endl;
    }
    temp[0] = '\0';
  }
  fclose(in);
  cerr << "done: " << idx << endl;
}


/// close parser
void Kor_SA_NN::close() {
}


/**
  SA 수행
  @param k_sent   형태소 분석된 문장
  @param prob   SA score가 저장됨
  @return string  SA 결과 (NEG or POS), k_sent에도 업데이트 됨
*/
string Kor_SA_NN::do_SA(K_Sentence &k_sent, double &prob) {
  vector<int> word_input;
  for (vector<K_Morp>::size_type i = 0; i < k_sent.morp.size(); i++) {
    string w = k_sent.morp[i].lemma + "/" + k_sent.morp[i].type;
    w = normalize(w);
    if (word_map.find(w) != word_map.end()) {
      word_input.push_back(word_map[w]);
    } else {
      //cerr << "word_map(UNK): " << w << endl;
      word_input.push_back(word_map["UNK"]);
    }
  }
  prob = 0;
  int result = nn.classify(word_input);
  k_sent.SA = outcome_vec[result];

  return k_sent.SA;
}

vector<string> Kor_SA_NN::do_multy_SA(K_Sentence &k_sent,
                                      vector<float> &vec_prob) {
  vector<int> word_input;
  for (vector<K_Morp>::size_type i = 0; i < k_sent.morp.size(); i++) {
    string w = k_sent.morp[i].lemma + "/" + k_sent.morp[i].type;
    w = normalize(w);
    if (word_map.find(w) != word_map.end()) {
      word_input.push_back(word_map[w]);
    } else {
      //cerr << "word_map(UNK): " << w << endl;
      word_input.push_back(word_map["UNK"]);
    }
  }
  vector<int> vecResult = nn.multy_classify(word_input, vec_prob);
  vector<string> vecSA;
  for (auto item : vecResult) {
    if (item == -1)
      break;
    else
      vecSA.push_back(outcome_vec[item]);
  }
  k_sent.SA = vecSA[0];

  return vecSA;
}