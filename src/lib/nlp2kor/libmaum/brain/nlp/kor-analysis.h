#ifndef LIBMAUM_BRAIN_NLP2KOR_KOR_ANALYSIS_H_
#define LIBMAUM_BRAIN_NLP2KOR_KOR_ANALYSIS_H_

#include <vector>
#include <cassert>
#include <stdexcept> //for std::runtime_error
#include <memory>  //for std::bad_alloc

class K_Doc;
class K_Sentence;
class Kor_tagger;
class Kor_NER;
class Kor_Parser_NN;
class Kor_SA_NN;

using std::vector;
using std::string;

class Kor {
 public:
  Kor();
  virtual ~Kor();
  // initialize each module
  /// init Korean POS tagger
  int init_tagger(const string &dir);

  /// init Korean Named Entity Recognizer
  int init_NER(const string &dir);
  /// init Korean dependency parser
  int init_parser(const string &dir);
  /// init Korean SA
  int init_sa(const string &dir, const string &rsc);
/*
  inline int init_sa(const string &dir, const string &proj) {
    return sa->init(dir, proj);
  }
*/
// POS tagging
  double do_tagging(string &input, K_Doc &k_doc);
  double do_tagging(string &input, K_Sentence &k_sent);
  // NER
  double do_NER(K_Doc &k_doc);
  double do_NER(K_Sentence &k_sent);
  double do_parsing(K_Doc &k_doc, int beam = 1);
  double do_parsing(K_Sentence &k_sent, int beam = 1);
  double do_sa(K_Doc &k_doc);
  double do_sa(K_Sentence &k_sent);
  vector<vector<string> > do_multy_sa(K_Doc &k_doc,
									  vector<vector<float> > &vec_prob);
  vector<string> do_multy_sa(K_Sentence &k_sent,
							 vector<float> &vec_prob);
  void print_POS(K_Doc &k_doc, int format = 0);
  void print_POS(K_Sentence &k_sent, int format = 0);
  void print_parse(K_Doc &k_doc, int format = 0);
  void print_parse(K_Sentence &k_sent, int format = 0);
  void print_sa(K_Doc &k_doc);
  void print_sa(K_Sentence &k_sent);
  /// make K_Doc as JSON format string
  string sprint_JSON(K_Doc &k_doc, int human_readable = 1);
  // yghwang 2017.11.30
  vector<string> get_postagged_str(K_Doc &k_doc);
 public:
  // member variable
  int use_preprocess;
 private:
  // each module
  Kor_tagger *tagger;
  Kor_NER *ner;
  Kor_Parser_NN *parser;
  Kor_SA_NN *sa;
};

#endif // LIBMAUM_BRAIN_NLP2KOR_KOR_ANALYSIS_H_
