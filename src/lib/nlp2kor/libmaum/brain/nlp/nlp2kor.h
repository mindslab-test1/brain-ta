#ifndef NLP2KOR_H
#define NLP2KOR_H

#include "libmaum/brain/nlp/nlp.h"

class Kor;

class Nlp2Kor : public Nlp {
 public:
  Nlp2Kor(const string &res_path, int nlp_feature_mask)
      : Nlp(res_path, nlp_feature_mask) {

  }

  virtual ~Nlp2Kor() {

  }

  void UpdateFeatures() override;
  void AnalyzeOne(const InputText *text, Document *document) override;
  void Uninit() override;

  void SetClassifierModel(const string &classifier_model_path) override;
  void Classify(const Document &document, ClassifiedSummary *sum) override;
  void GetPosTaggedStr(const Document *document,
                       vector <string> *pos_tagged_str) override;
  void GetNerTaggedStr(const Document *document,
                       vector<string> *ner_tagged_str) override;
  void GetPosNerTaggedStr(const Document *document,
                       vector<string> *pos_ner_tagged_str) {};

 private:
  std::shared_ptr<Kor> anal_;

};

#endif //NLP2KOR_H
