#if !defined(KDOC_H)
#define KDOC_H

#include <string>
#include <vector>

using namespace std;

/// 한국어 형태소 
struct K_Morp {
  int id;            ///< 각 문장마다 0부터 시작
  int wid;        ///< 형태소가 속한 어절(word)의 id
  string lemma;    ///< 형태소
  string type;    ///< 품사
};

/// 한국어 어절
struct K_Word {
  int id;            ///< 각 문장마다 0부터 시작
  string text;    ///< 어절
  string tagged_text;  ///< "lemma/POS lemma/POS ..." 형식의 결과
  int begin_mid;    ///< 어절의 처음 morp id
  int end_mid;    ///< 어절의 마지막 morp id
  int begin_sid;    ///< 어절의 처음 syllable id
  int end_sid;    ///< 어절의 마지막 syllable id
};

/// 한국어 의존파싱
struct K_Dep {
  int id;            ///< 각 문장마다 0부터 시작
  int head;        ///< head: word id (주의 morp id 아님), root = -1
  string label;    ///< dependency label
  vector<int> modifier;   ///< modifier id
};

/// 한국어 구구조 트리 (Phrase Structure Tree: binary tree)
struct K_PS {
  int id;            ///< 각 문장마다 0부터 시작
  string label;    ///< dependency label
  int head_wid;   ///< head's word id
  int
      left_child;        ///< left child: phrase structure (K_PS) id, -1 if leaf node
  int
      right_child;    ///< left child: phrase structure (K_PS) id, -1 if leaf node
};

/// 한국어 개체명 (NE)
struct K_NE {
  int id;            ///< 각 문장마다 0부터 시작
  string text;    ///< NE
  string type;    ///< NE tag
  int begin;        ///< NE의 처음 word id
  int end;        ///< NE의 마지막 word id
  int begin_sid;    ///< NE의 처음 syllable id
  int end_sid;    ///< NE의 마지막 syllable id
};

/// SRL
struct K_SRL {
  string verb;    ///< 용언
  int sense;        ///< 용언의 의미 번호 (1부터 시작)
  int verb_wid;    ///< 용언의 word id
  vector<pair<string, int> >
      arg;    ///< 용언의 argument(semantic role, word id) vector
};

/// Relation
struct K_Relation {
  string type;    ///< Relation type
  int arg1;        ///< argument1의 NE id
  int arg2;        ///< argument2의 NE id
};

/// Event
struct K_Event {
  string type;    ///< Event type
  string trigger;    ///< Event trigger
  int trigger_wid;
  ///< trigger word id
  vector<pair<string, int> > arg;    ///< Event의 argument(type, NE id) vector
};

/// SA (감성분석)
struct K_SA {
  string type;        ///< 감성 분류
  // anchor
  string anchor;        ///< 감성표현 자체 string
  int anchor_begin;    ///< word id
  int anchor_end;        ///< word id
  // target
  string target;        ///< SA 대상 string
  int target_begin;    ///< word id
  int target_end;        ///< word id
};

/// 한국어 문장
struct K_Sentence {
  int id;                        ///< 문장 id (0부터 시작)
  string raw_text;            ///< 문장 입력 string
  string text;                ///< 문장 string (preprocessing)
  vector<string> syllable;    ///< 음절 vector
  vector<string> space;       ///< 음절별 띄어쓰기 정보 (B, I)
  vector<int> sid2wid;        ///< syllable id --> word id
  vector<string> POS_outcome; ///< 음절단위 POS tagging result
  vector<K_Morp> morp;        ///< 형태소
  vector<K_Word> word;        ///< 어절
  vector<K_NE> NE;            ///< NE
  vector<vector<K_NE> > NE_nbest; ///< NE n-best list
  vector<double> NE_nbest_score;  ///< NE n-best score list
  vector<K_Dep> dependency;   ///< dependency parsing
  vector<K_PS> PST;           ///< phrase structure tree
  vector<K_SRL> SRL;            ///< SRL
  vector<K_Relation> relation;
  ///< relation
  vector<K_Event> event;        ///< event
  //vector<K_SA> SA;            ///< SA
  string SA;                    ///< SA for sentence
};

/// 한국어 문서 
struct K_Doc {
  string text;                    ///< 원문
  vector<K_Sentence> sentence;    ///< 문장
};

#endif
