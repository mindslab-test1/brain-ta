#if !defined(KOR_PARSER_NN_INCLUDED_)
#define KOR_PARSER_NN_INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma warning( disable : 4786 )

#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <set>
#include <map>
//#include <unordered_map>
#include <deque>

#include "mynn.h"
#include "libmaum/brain/nlp/kdoc.h"
#include "kor-tagger.h"
#include "util.h"

using namespace std;

/// word type: parsing에 필요한 정보를 추출함: conll2008 정보와 매칭
typedef struct k_word_struct {
  // id (1부터 시작)
  int id;
  // form: 어절의 첫 단어: lexical/pos
  string form;
  // form2(추가됨): 어절의 첫 두 단어: lexical/pos (구분자 '|')
  string form2;
  // lemma: 어절의 끝 단어: lexical/pos (끝이 symbol이면 그 앞 단어)
  string lemma;
  // gpostag: 어절의 첫 단어: pos
  string gpostag;
  // ppostag: 어절의 끝 단어: pos (끝이 symbol이면 그 앞 단어)
  string ppostag;
  // split form: 어절의 끝 두 단어: lexical/pos (구분자 '|')
  string split_form;
  // split lemma: 어절의 끝 두 단어: pos (구분자 '|')
  string split_lemma;
  // split postag: 어절의 전체: 전체 pos (구분자 '|')
  string split_postag;
  // head (dependency parsing): root = 0, 1부터 시작
  int head;
  // dependency label
  string dep_label;
} Word_t;

/// Agenda의 element
struct Config {
  double score;
  // input의 id 저장 (1부터 시작)
  deque<int> stack;
  // buffer id (1부터 시작)
  int b_id;
  // arc에 해당
  vector<int> head;
  vector<string> label;
  // action history
  vector<string> action;
  // feature history
  vector<vector<int> > feature;
};

/// to compare config
class Cmp_Config {
 public:
  bool operator()(Config x, Config y) const {
    return x.score > y.score;
  }
};

/// Transition-based Korean dependency parser
class Kor_Parser_NN {
 public:
  Kor_Parser_NN();
  virtual ~Kor_Parser_NN();

  /// 초기화 함수: load_model, load_cluster_dic, load_embedding, load_ngram_dic
  int init(const string &dir);
  void load_model(const string &file);
  void load_outcome_vec(const string &file);
  void load_word_map(const string &file);
  void load_feat_map(const string &file);
  void load_cluster_dic(const string &file);
  void load_ngram_dic(const string &file);

  /// 종료 함수
  void close();

  /// K_Sentence to vector<Word_t>
  void convert_ksent2wordt(K_Sentence &ksent, vector<Word_t> &input);
  void convert_wordt2ksent(vector<Word_t> &input, K_Sentence &ksent);

  /// dependency parsing
  vector<Word_t> do_parsing(vector<Word_t> input, double &prob);

  /// parsing 결과를 string으로 바꿔줌
  string print_parse(vector<Word_t> &input);
  string print_parse(K_Sentence &ksent);
  string print_parse_format(K_Sentence &ksent, int begin_id = 1);
  string print_parse_tree(vector<Word_t> &input, int depth = 0, int head = 0);
  string print_parse_tree(K_Sentence &ksent, int depth = 0, int head = -1);
  string print_PST(K_Sentence &ksent, int root_id, int depth = 0);

  /////////////////////////////////////////////
  // public member variables
  /////////////////////////////////////////////

  int verbose;
  int print;
  int model_loaded;
  int binary_model;
  /// pre-process bracket
  int bracket;

 private:
  /// 실제 파싱: head와 label의 size는 n+1 (맨 처음은 ROOT)
  Config parse_sentence(vector<Word_t> input,
                        deque<int> stack,
                        vector<int> head,
                        vector<string> label);

  /// arc-eager action
  inline void shift(Config &a, string &action, double prob) {
    // reverse
    //a.stack.push_back(a.b_id++);
    a.stack.push_back(a.b_id--);
    a.score += prob;
    a.action.push_back(action);
  }
  inline void reduce(Config &a, string &action, double prob) {
    a.stack.pop_back();
    a.score += prob;
    a.action.push_back(action);
  }
  inline void left_arc(Config &a, string &action, double prob) {
    string label = my_replace(action, "Left-Arc:", "");
    int s_id = a.stack.back();
    a.head[s_id] = a.b_id;
    a.label[s_id] = label;
    a.stack.pop_back();
    a.score += prob;
    a.action.push_back(action);
  }
  inline void right_arc(Config &a, string &action, double prob) {
    string label = my_replace(action, "Right-Arc:", "");
    int s_id = a.stack.back();
    a.head[a.b_id] = s_id;
    a.label[a.b_id] = label;
    // reverse
    //a.stack.push_back(a.b_id++);
    a.stack.push_back(a.b_id--);
    a.score += prob;
    a.action.push_back(action);
  }

  /// feature 생성 (all feature)
  void make_feature(vector<int> &word_input,
                    vector<int> &feat_input,
                    vector<Word_t> input,
                    deque<int> &stack,
                    int b_id,
                    vector<int> &head,
                    vector<string> &label);

  /// get left dependents' label set
  void get_ld_set
      (vector<int> &head, vector<string> &label, int id, string &ld_set);
  /// get left dependents' important label set: sbj, obj, cmp
  void get_ld_set2
      (vector<int> &head, vector<string> &label, int id, string &ld_set);
  /// get right dependents' label set
  void get_rd_set
      (vector<int> &head, vector<string> &label, int id, string &rd_set);

  void print_config(vector<Word_t> &input, deque<int> &stack, int b_id);

  /// normalize digit and alphabet: 0~9 --> 0, lower, '|' --> '_'
  inline string normalize(const string &word) {
    char result[1000];
    result[0] = 0;
    for (string::size_type i = 0; i < word.length() && i < 999; i++) {
      char c = word.at(i);
      if (c >= '0' && c <= '9') c = '0';
      if (c == '|') c = '_';
      if (isupper(c)) c = (char)tolower(c);
      result[i] = c;
    }
    if (word.length() < 999) {
      result[word.length()] = 0;
      result[999] = 0;
    }
    return string(result);
  }

  /// normalize digit: 0~9 --> 0
  inline string normalize_num(const string &word) {
    char result[1000];
    result[0] = 0;
    for (string::size_type i = 0; i < word.length() && i < 999; i++) {
      char c = word.at(i);
      if (c >= '0' && c <= '9') c = '0';
      if (c == '|') c = '_';  // 영향을 안 미침
      result[i] = c;
    }
    if (word.length() < 999) {
      result[word.length()] = 0;
    } else {
      result[999] = 0;
    }
    return string(result);
  }

  /////////////////////////////////////////////
  // private member variables
  /////////////////////////////////////////////

  MyNN mynn;  ///< NN
  vector<string> outcome_vec;     ///< outcome vector
  map<string, int> word_map;      ///< word dic
  map<string, int> feat_map;      ///< feature dic
  map<string, string> cluster;    ///< word cluster
  map<string, int> ngram;         ///< ngram
  //unordered_map<string, int> ngram;
};

#endif // KOR_PARSER_INCLUDED_
