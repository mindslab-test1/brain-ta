#include <stdio.h>
#include <string.h>
#include <deque>
#include <algorithm>
#include <ctime>
#include "kor-parser-nn.h"

#ifdef _OPENMP
#include <omp.h>
#endif

/**
    Kor_Parser 인스턴스 생성 
*/
Kor_Parser_NN::Kor_Parser_NN() {
  verbose = 0;
  model_loaded = 0;
  binary_model = 0;
  bracket = 0;

  // mynn
  mynn.activation_func = RELU;
  mynn.dropout_fraction = 0.5;
}

/// Destruction
Kor_Parser_NN::~Kor_Parser_NN() {
}

/**
    Kor_Parser 초기화
    @param dir  리소스 디렉토리
    @return 초기화 성공 (0) 여부
*/
int Kor_Parser_NN::init(const string &dir) {
  string file;
  // mynn
  file = dir + "/" + "parser_model.txt";
  load_model(file);
  // outcome vocab.
  file = dir + "/" + "vocab_output.txt";
  load_outcome_vec(file);
  // word vocab.
  file = dir + "/" + "vocab_word.txt";
  load_word_map(file);
  // feat vocab.
  file = dir + "/" + "vocab_feat.txt";
  load_feat_map(file);
  // cluster dic
  //file = dir + "/" + "cluster.kor_phrase.txt";
  //load_cluster_dic(file);
  // ngram dic
  file = dir + "/" + "bigram.korean.freq.txt";
  load_ngram_dic(file);
  return 0;
}

/**
    Neural Network model loading
    @param file Neural Network model file
*/
void Kor_Parser_NN::load_model(const string &file) {
  try {
    mynn.load_model(file);
  } catch (exception e) {
    cerr << "Error: " << e.what() << endl;
    exit(1);
  }
  model_loaded = 1;
}

/**
    Outcome dictionary loading
    @param file Outcome dictionary file
*/
void Kor_Parser_NN::load_outcome_vec(const string &file) {
  FILE *in;
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    exit(1);
  }

  cerr << "loading " << file << " ... ";
  char temp[1000];
  while (!feof(in)) {
    fgets(temp, 1000, in);
    if (temp[0] == ';') continue;
    vector<string> token;
    mynn.tokenize(temp, token, " \t\n\r");
    if (token.size() == 1) {
      outcome_vec.push_back(token[0]);
    } else if (token.size() > 1) {
      cerr << "Error(load_outcome_vec): " << temp << endl;
    }
    temp[0] = '\0';
  }
  fclose(in);
  cerr << "done: " << outcome_vec.size() << endl;
}

/**
    Word dictionary loading
    @param file word dictionary file
*/
void Kor_Parser_NN::load_word_map(const string &file) {
  FILE *in;
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    exit(1);
  }

  cerr << "loading " << file << " ... ";
  char temp[1000];
  int idx = 0;
  while (!feof(in)) {
    fgets(temp, 1000, in);
    vector<string> token;
    mynn.tokenize(temp, token, " \t\n\r");
    if (token.size() == 1) {
      word_map[token[0]] = idx++;
    } else if (token.size() > 1) {
      cerr << "Error(load_word_map):" << idx << " [" << temp << "]" << endl;
    }
    temp[0] = '\0';
  }
  fclose(in);
  cerr << "done: " << idx << endl;
}

/**
    Feature dictionary loading
    @param file feature dictionary file
*/
void Kor_Parser_NN::load_feat_map(const string &file) {
  FILE *in;
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    exit(1);
  }

  cerr << "loading " << file << " ... ";
  char temp[1000];
  int idx = 0;
  while (!feof(in)) {
    fgets(temp, 1000, in);
    vector<string> token;
    mynn.tokenize(temp, token, " \t\n\r");
    if (token.size() == 1) {
      feat_map[token[0]] = idx++;
    } else if (token.size() > 1) {
      cerr << "Error(load_feat_map):" << idx << " [" << temp << "]" << endl;
    }
    temp[0] = '\0';
  }
  fclose(in);
  cerr << "done: " << idx << endl;
}

/**
    Word cluster dictionary loading (자질로 사용)
    @param file word cluster file
*/
void Kor_Parser_NN::load_cluster_dic(const string &file) {
  FILE *in;
  // cluster dic
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    exit(1);
  }

  // time
  clock_t start, finish;
  start = clock();

  cerr << "loading " << file << " ... ";
  char temp[1000];
  while (!feof(in)) {
    fgets(temp, 1000, in);
    if (temp[0] == ';') continue;
    vector<string> token;
    mynn.tokenize(temp, token, " \t\n\r");
    if (token.size() == 3) {
      // cluster prob word
      cluster[get_lower(token[2])] = token[0];
    } else if (token.size() == 2) {
      // word cluster
      cluster[get_lower(token[0])] = token[1];
    } else if (token.size() > 0) {
      cerr << "Error(load_cluster_dic): " << temp << endl;
    }
    temp[0] = '\0';
  }
  fclose(in);
  finish = clock();
  cerr << "(" << (double) (finish - start) / CLOCKS_PER_SEC << ") done."
      << endl;
}

/**
    N-gram dictionary dictionary loading (자질로 사용)
    @param file N-gram dictionary file
*/
void Kor_Parser_NN::load_ngram_dic(const string &file) {
  FILE *in;
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    exit(1);
  }

  // time
  clock_t start, finish;
  start = clock();

  cerr << "loading " << file << " ... ";
  char temp[1000];
  while (!feof(in)) {
    fgets(temp, 1000, in);
    if (temp[0] == ';') continue;
    vector<string> token;
    mynn.tokenize(temp, token, " \t\n\r");
    if (token.size() == 2) {
      // word count
      ngram[token[0]] = atoi(token[1].c_str());
    } else if (token.size() > 0) {
      cerr << "Error(load_ngram_dic): " << temp << endl;
    }
    temp[0] = '\0';
  }
  fclose(in);
  finish = clock();
  cerr << "(" << (double) (finish - start) / CLOCKS_PER_SEC << ") done."
      << endl;
}

/// close parser
void Kor_Parser_NN::close() {
  cluster.clear();
  ngram.clear();
}

/**
    K_Sentence 구조체를 vector<Word_t>로 변환 (구문분석은 vector<Word_t>로 수행)\n
    vector<Word_t>로 변환해서 구문분석을 수행하는 이유는 영어 구문분석과 동일한 모듈을 사용하기 위함
    @param ksent    형태소분석/품사태깅 결과가 저장된 K_Sentence 구조체
    @param input    구문분석에 사용될 vector<Word_t> - 형태소분석/품사태깅 결과를 포함
*/
void Kor_Parser_NN::convert_ksent2wordt(K_Sentence &ksent,
                                        vector<Word_t> &input) {
  if (verbose) cerr << "Begin convert_ksent2wordt." << endl;
  input.clear();
  for (int i = 0; i < ksent.word.size(); i++) {
    Word_t word;
    int begin = ksent.word[i].begin_mid;
    int end = ksent.word[i].end_mid;
    // id
    word.id = i + 1;
    // form: 어절의 첫 단어: lexical/pos
    word.form = ksent.morp[begin].lemma + "/" + ksent.morp[begin].type;
    // form2: 어절의 첫 두 단어: lexical/pos (구분자 '|')
    word.form2 = "_";
    if (begin + 1 < end) {
      word.form2 = ksent.morp[begin].lemma + "/" + ksent.morp[begin].type;
      word.form2 += "|";
      word.form2 +=
          ksent.morp[begin + 1].lemma + "/" + ksent.morp[begin + 1].type;
    }
    // lemma: 어절의 끝 단어: lexical/pos (끝이 symbol이면 그 앞 단어)
    // ppos: 어절의 끝 단어: pos (끝이 symbol이면 그 앞 단어)
    word.lemma = "_";
    word.ppostag = "_";
    if (end > begin) {
      word.lemma = ksent.morp[end].lemma + "/" + ksent.morp[end].type;
      word.ppostag = ksent.morp[end].type;
      //if (ksent.morp[end].type == "s" && end-1 > begin) {}
      if (ksent.morp[end].type == "SP" || ksent.morp[end].type == "SF"
          || ksent.morp[end].type == "SY" || ksent.morp[end].type == "s") {
        if (end - 1 > begin) {
          word.lemma =
              ksent.morp[end - 1].lemma + "/" + ksent.morp[end - 1].type;
          word.ppostag = ksent.morp[end - 1].type;
        }
      }
    }
    // gpos: 어절의 첫 단어: pos
    word.gpostag = ksent.morp[begin].type;
    // s_form: 어절의 끝 두 단어: lexical/pos
    if (end - 1 > begin) {
      word.split_form =
          ksent.morp[end - 1].lemma + "/" + ksent.morp[end - 1].type;
      word.split_form += "|";
      word.split_form += ksent.morp[end].lemma + "/" + ksent.morp[end].type;
    }
    // s_lemma: 어절의 끝 두 단어: pos
    if (end - 1 > begin) {
      word.split_lemma = ksent.morp[end - 1].type + "|" + ksent.morp[end].type;
    }
    // s_pos: 어절의 전체 pos
    for (int j = begin; j <= end; j++) {
      if (word.split_postag != "") word.split_postag += "|";
      word.split_postag += ksent.morp[j].type;
    }
    // parsing이 되어 있으면 파싱 결과도 저장: 파싱 결과 출력에 쓰임
    if (ksent.dependency.size() == ksent.word.size()) {
      // head: id = i + 1
      word.head = ksent.dependency[i].head + 1;
      // dep_label
      word.dep_label = ksent.dependency[i].label;
    }
    input.push_back(word);
  }
  if (verbose) cerr << "End convert_ksent2wordt." << endl;
}

/**
    구문분석 결과가 저장된 vector<Word_t>를 K_Sentence 구조체로 변환. 여기에 구문분석 결과를 추가함 (dependency tree and phrase structure tree)
    @param input    구문분석 결과가 저장된 vector<Word_t>
    @param ksent    형태소분석/품사태깅 결과가 저장된 K_Sentence 구조체. 여기에 구문분석 결과를 추가함 (dependency tree and phrase structure tree)
*/
void Kor_Parser_NN::convert_wordt2ksent(vector<Word_t> &input,
                                        K_Sentence &ksent) {
  if (verbose) cerr << "Begin convert_wordt2ksent." << endl;
  // dependency
  ksent.dependency.clear();
  for (int i = 0; i < input.size(); i++) {
    Word_t &word = input[i];
    K_Dep dep;
    dep.id = i;
    // head: id = i + 1
    dep.head = word.head - 1;
    // dep_label
    dep.label = word.dep_label;
    // modifier
    for (int j = 0; j < input.size(); j++) {
      Word_t &word2 = input[j];
      int head_id = word2.head - 1;
      if (head_id == i) dep.modifier.push_back(j);
    }
    // ksent
    ksent.dependency.push_back(dep);
  }
  if (verbose) cerr << "Middle convert_wordt2ksent." << endl;

  // phrase structure tree
  vector<K_PS> temp_PST;
  K_PS ps_node;
  ksent.PST.clear();
  // leaf node
  for (int i = 0; i < ksent.dependency.size(); i++) {
    K_Dep &dep = ksent.dependency[i];
    ps_node.id = dep.id;
    ps_node.label = dep.label;
    ps_node.head_wid = dep.id;
    ps_node.left_child = -1;
    ps_node.right_child = -1;
    temp_PST.push_back(ps_node);
    ksent.PST.push_back(ps_node);
  }
  // internal node
  while (temp_PST.size() >= 2) {
    int is_changed = 0;
    for (int i = 0; i + 1 < temp_PST.size(); i++) {
      K_PS &ps1 = temp_PST[i];
      K_PS &ps2 = temp_PST[i + 1];
      if (ksent.dependency[ps1.head_wid].head == ps2.head_wid) {
        // add new node to PST
        ps_node.id = (int)ksent.PST.size();
        ps_node.label = ps2.label;
        ps_node.head_wid = ps2.head_wid;
        ps_node.left_child = ps1.id;
        ps_node.right_child = ps2.id;
        ksent.PST.push_back(ps_node);
        // replace new node with ps2
        ps2.id = ps_node.id;
        ps2.left_child = ps_node.left_child;
        // remove ps1 from temp_PST
        temp_PST.erase(temp_PST.begin() + i);
        is_changed = 1;
        break;
      }
    }
    if (!is_changed) {
      if (temp_PST.size() >= 2) {
        // if incomplet dependency tree
//                cerr << "Warning: Incomplet dependency tree." << endl;
        if (verbose)
          cerr
              << "convert_wordt2ksent: incomplet dependency tree: temp_PST.size()="
              << temp_PST.size() << endl;
        K_PS &ps1 = temp_PST[0];
        K_PS &ps2 = temp_PST[1];
        // add new node to PST
        ps_node.id = (int)ksent.PST.size();
        ps_node.label = ps2.label;
        ps_node.head_wid = ps2.head_wid;
        ps_node.left_child = ps1.id;
        ps_node.right_child = ps2.id;
        ksent.PST.push_back(ps_node);
        // replace new node with ps2
        ps2.id = ps_node.id;
        ps2.left_child = ps_node.left_child;
        // remove ps1 from temp_PST
        temp_PST.erase(temp_PST.begin());
      } else {
        cerr
            << "Warning(convert_wordt2ksent): incomplet dependency tree: temp_PST.size()="
            << temp_PST.size() << endl;
        break;
      }
    }
  }
  if (verbose) cerr << "End convert_wordt2ksent." << endl;
}


/**
    Dependency parsing 수행
    @param input    형태소분석/품사태깅 결과가 저장된 vector<Word_t>
    @param prob     구문분석 score가 저장됨
    @return 구문분석 결과가 저장된 vector<Word_t>
*/
vector<Word_t> Kor_Parser_NN::do_parsing(vector<Word_t> input, double &prob) {
  deque<int> stack;
  vector<int> head(input.size() + 1, -1);
  vector<string> label(input.size() + 1, "");

  // stack 초기화
  stack.push_back(0);

  // 실제 파싱
  if (!input.empty()) {
    Config agenda = parse_sentence(input, stack, head, label);
    head = agenda.head;
    label = agenda.label;
    prob = agenda.score;

    for (int i = 0; i < input.size(); i++) {
      // dependency
      int id = input[i].id;
      if (head[id] == -1) {
        input[i].head = 0;
        input[i].dep_label = "root";
      } else if (head[id] >= 0 && head[id] <= input.size()) {
        input[i].head = head[id];
        input[i].dep_label = label[id];
      } else {
        cerr << "Error(do_parse):" << id << " " << head[id] << endl;
        input[i].head = 0;
        input[i].dep_label = "root";
      }
    }
  }
  if (verbose) cerr << "End do_parsing." << endl;

  return input;
}


/**
    Parsing using greedy local search: arc-eager (head와 label의 size는 n+1 (맨 처음은 ROOT))
    @param input    품사 태깅결과가 저장된 vector<Word_t>
    @param stack    초기화된 stack
    @param head     초기화된 head
    @param label    초기화된 label
    @return 구문분석 결과가 저장된 Beam (Config에 구문분석 결과가 저장됨)
*/
Config Kor_Parser_NN::parse_sentence(vector<Word_t> input,
                                     deque<int> stack,
                                     vector<int> head,
                                     vector<string> label) {
  Config agenda;
  agenda.score = 0;
  agenda.stack = stack;
  // backward
  //init_a.b_id = 1;    // forward
  agenda.b_id = (int)input.size(); // backward
  agenda.head = head;
  agenda.label = label;
  // 2 * n - 1 iteration
  for (int iter = 0; iter < 2 * input.size() - 1; iter++) {
    Config &a = agenda;
    // stack 검사
    if (a.stack.empty()) {
      cerr << " stack is empty: " << iter << "/" << 2 * input.size() - 1
          << endl;
      if (!a.action.empty())
        cerr << " s prev_act=" << a.action[a.action.size() - 1];
      print_config(input, a.stack, a.b_id);
      continue;
    }
    if (verbose) {
      cerr << endl << "## input:" << iter << " score: " << a.score;
      print_config(input, a.stack, a.b_id);
    }
    // stack s와 buffer b가 서로 dependency 정보가 있는지 검사
    // feature 생성
    vector<int> word_input, feat_input;
    make_feature(word_input,
                 feat_input,
                 input,
                 a.stack,
                 a.b_id,
                 a.head,
                 a.label);
    if (word_input.size() != 7 || feat_input.size() != 28) {
      cerr << "Error(feature num error): " << word_input.size() << ", "
          << feat_input.size() << endl;
      exit(1);
    }

    // NN
    pair<int, float> outcome_prob = mynn.classify(word_input, feat_input);
    string action = outcome_vec[outcome_prob.first];
    float prob = outcome_prob.second;
    // action ...
    if (action == "Shift") {
      // reverse
      //if (a.b_id > input.size()) {}
      if (a.b_id <= 0) {
        if (verbose) cerr << endl << "Can't shift!" << endl;
        continue;
      }
      if (!a.action.empty() && a.action[a.action.size() - 1] == "Reduce") {
        if (verbose) cerr << endl << "Can't shift2!" << endl;
        continue;
      }
      if (verbose) cerr << endl << "Shift " << a.b_id << " " << prob;
      shift(a, action, prob);
      if (verbose) print_config(input, a.stack, a.b_id);
    } else if (action == "Reduce") {
      if (a.b_id <= input.size() && a.head[a.stack.back()] < 0) {
        if (verbose) cerr << endl << "Can't reduce!" << endl;
        continue;
      }
      if (verbose) cerr << endl << "Reduce " << a.stack.back() << " " << prob;
      reduce(a, action, prob);
      if (verbose) print_config(input, a.stack, a.b_id);
    } else if (strncmp(action.c_str(), "Left-Arc", 8) == 0) {
      // reverse
      //if (a.b_id > input.size() || a.stack.size() <= 1 || head[a.stack.back()] > 0) {}
      if (a.b_id <= 0 || a.stack.size() <= 1 || head[a.stack.back()] > 0) {
        if (verbose) cerr << endl << "Can't left-arc!" << endl;
        if (head[a.stack.back()] > 0) cerr << "e";
        continue;
      }
      if (verbose)
        cerr << endl << action << " " << a.stack.back() << "<-" << a.b_id << " "
            << prob;
      left_arc(a, action, prob);
      if (verbose) print_config(input, a.stack, a.b_id);
    } else if (strncmp(action.c_str(), "Right-Arc", 9) == 0) {
      // reverse
      //if (a.b_id > input.size() || head[a.b_id] > 0) {}
      if (a.b_id <= 0) {
        if (verbose) cerr << endl << "Can't right-arc!" << endl;
        if (a.b_id <= input.size() && head[a.b_id] > 0)
          cerr << " e:" << a.b_id << "," << head[a.b_id];
        continue;
      }
      if (verbose)
        cerr << endl << action << " " << a.stack.back() << "->" << a.b_id << " "
            << prob;
      right_arc(a, action, prob);
      if (verbose) print_config(input, a.stack, a.b_id);
    } else {
      cerr << "Error: unknown action: " << action << endl;
      exit(1);
    }
  }

  return agenda;
}


/**
    feature generation for Korean all feature: standard + struct \n
    Word_t 구성: form, lemma, gpos, ppos, s_form, s_lemma, s_pos, head, deprel \n
    form: 어절의 첫 단어: lexical/pos \n
    form2: 어절의 첫 두 단어: lexical/pos (구분자 '|') \n
    lemma: 어절의 끝 단어: lexical/pos (끝이 symbol이면 그 앞 단어) \n
    gpos: 어절의 첫 단어: pos \n
    ppos: 어절의 끝 단어: pos (끝이 symbol이면 그 앞 단어) \n
    s_form: 어절의 끝 두 단어: lexical/pos (구분자 '|') \n
    s_lemma: 어절의 끝 두 단어: pos (구분자 '|') \n
    s_pos: 어절의 전체: 전체 pos (구분자 '|') \n
    @param feature  현재 상태에서 action을 결정하기 위해 사용되는 feature를 저장함
    @param input    형태소분석/품사태깅 결과가 저장된 vector<Word_t>
    @param stack    현재 상태의 stack
    @param b_id     구문분석할 어절의 id
    @param head     구문분석 결과 중의 head 정보
    @param label    구문분석 결과 중의 label 정보
*/
void Kor_Parser_NN::make_feature(vector<int> &word_input,
                                 vector<int> &feat_input,
                                 vector<Word_t> input,
                                 deque<int> &stack,
                                 int b_id,
                                 vector<int> &head,
                                 vector<string> &label) {
  Word_t root, init_word;
  root.id = 0;
  root.form = "ROOT";
  root.form2 = "ROOT";
  root.lemma = "ROOT";
  root.gpostag = "ROOT";
  root.ppostag = "ROOT";
  root.split_form = "ROOT";
  root.split_lemma = "_";
  root.split_postag = "_";
  init_word.id = 0;
  init_word.form = "_";
  init_word.form2 = "_";
  init_word.lemma = "_";
  init_word.gpostag = "_";
  init_word.ppostag = "_";
  init_word.split_form = "_";
  init_word.split_lemma = "_";
  init_word.split_postag = "_";

  // s, s2, s3
  Word_t s, s2, s3;
  s = s2 = s3 = init_word;
  int s_id, s2_id, s3_id;
  s.id = s2.id = s3.id = 0;
  s_id = s2_id = s3_id = 0;
  if (stack.size() >= 1) {
    s_id = stack.back();
    if (s_id == 0) s = root;
    else s = input[s_id - 1];
  }
  if (stack.size() >= 2) {
    s2_id = stack[stack.size() - 2];
    if (s2_id == 0) s2 = root;
    else s2 = input[s2_id - 1];
  }
  if (stack.size() >= 3) {
    s3_id = stack[stack.size() - 3];
    if (s3_id == 0) s3 = root;
    else s3 = input[s3_id - 1];
  }
  // q, q2
  Word_t q, q2;
  q = q2 = init_word;
  int q_id, q2_id;
  q.id = q2.id = 0;
  q_id = q2_id = 0;
  if (b_id <= input.size() && b_id - 1 >= 0) {
    q_id = b_id;
    q = input[q_id - 1];
  }
  // reverse
  /*
  if (b_id + 1 <= input.size()) {
      q2_id = b_id + 1;
      q2 = input[q2_id-1];
  }
  */
  if (b_id - 1 >= 1) {
    q2_id = b_id - 1;
    q2 = input[q2_id - 1];
  } else { // 검증 필요. ssvm 버전과의 차이 검증 필요
    //q2 = end;
  }
  // dist
  string dist;
  char temp[1000];
  sprintf(temp, "d~%d", ABS(q.id - s.id));
  if (ABS(q.id - s.id) < 4) dist = temp;
  else if (ABS(q.id - s.id) < 7) dist = "d<7";
  else if (ABS(q.id - s.id) < 15) dist = "d<15";
  else dist = "d>~15";

  // word input
  word_input.clear();
  vector<string> word_str_input;
  // s
  word_str_input.push_back(s.form);
  word_str_input.push_back(s.lemma);
  // q
  word_str_input.push_back(q.form);
  word_str_input.push_back(q.lemma);
  // q2
  word_str_input.push_back(q2.form);
  // s2
  word_str_input.push_back(s2.form);
  // s3
  word_str_input.push_back(s3.form);

  for (int i = 0; i < word_str_input.size(); i++) {
    string w = normalize_num(word_str_input[i]);
    if (w == "ROOT") w = "PADDING";
    if (word_map.find(w) != word_map.end()) {
      word_input.push_back(word_map[w]);
    } else {
      //cerr << "word_map(UNK): " << w << endl;
      word_input.push_back(word_map["UNK"]);
    }
  }

  // feat input
  feat_input.clear();
  vector<string> feat_str_input;
  // s
  feat_str_input.push_back("POS=" + s.gpostag);
  feat_str_input.push_back("POS=" + s.ppostag);
  feat_str_input.push_back("POS=" + s.split_postag);
  //cerr << s.gpostag << " " << s.ppostag << " " << s.split_postag << endl;
  // q
  feat_str_input.push_back("POS=" + q.gpostag);
  feat_str_input.push_back("POS=" + q.ppostag);
  feat_str_input.push_back("POS=" + q.split_postag);
  // q2
  feat_str_input.push_back("POS=" + q2.gpostag);
  feat_str_input.push_back("POS=" + q2.ppostag);
  feat_str_input.push_back("POS=" + q2.split_postag);

  // dist
  feat_str_input.push_back(dist);

  // label set
  // s
  string s_ld_set, s_ld_set2;
  get_ld_set(head, label, s.id, s_ld_set);
  //get_ld_set2(head, label, s.id, s_ld_set2);
  // s2
  string s2_ld_set, s2_ld_set2;
  if (stack.size() > 1) {
    get_ld_set(head, label, s2.id, s2_ld_set);
    //get_ld_set2(head, label, s2.id, s2_ld_set2);
  }
  // s3
  string s3_ld_set, s3_ld_set2;
  if (stack.size() > 2) {
    get_ld_set(head, label, s3.id, s3_ld_set);
    //get_ld_set2(head, label, s3.id, s3_ld_set2);
  }
  if (s_ld_set == "") s_ld_set = "_";
  if (s2_ld_set == "") s2_ld_set = "_";
  if (s3_ld_set == "") s3_ld_set = "_";
  // label
  string s_label, s2_label, s3_label;
  s_label = s2_label = s3_label = "_";
  if (s_id >= 0) s_label = label[s_id];
  if (s2_id >= 0) s2_label = label[s2_id];
  if (s3_id >= 0) s3_label = label[s3_id];
  if (s_label == "") s_label = "_";
  if (s2_label == "") s2_label = "_";
  if (s3_label == "") s3_label = "_";

  // label
  feat_str_input.push_back("LB=" + s_label);
  feat_str_input.push_back("LB=" + s2_label);
  feat_str_input.push_back("LB=" + s3_label);
  // label set
  feat_str_input.push_back("LB=" + s_ld_set);
  feat_str_input.push_back("LB=" + s2_ld_set);
  feat_str_input.push_back("LB=" + s3_ld_set);

  // valency: 빈 스트링일때 valency가 1인 버그 수정 (2014.03.04)
  int s_lvi = 0, s2_lvi = 0, s3_lvi = 0;
  vector<string> token;
  if (s_ld_set != "") {
    mynn.split(s_ld_set, token, "_");
    s_lvi = (int)token.size();
    token.clear();
  }
  if (s2_ld_set != "") {
    mynn.split(s2_ld_set, token, "_");
    s2_lvi = (int)token.size();
    token.clear();
  }
  if (s3_ld_set != "") {
    mynn.split(s3_ld_set, token, "_");
    s3_lvi = (int)token.size();
    token.clear();
  }
  string s_lv, s2_lv, s3_lv;
  if (s_lvi <= 5) {
    sprintf(temp, "v~%d", s_lvi);
    s_lv = temp;
  }
  else if (s_lvi < 10) { s_lv = "v<10"; }
  else { s_lv = "v>~10"; }
  if (s2_lvi <= 5) {
    sprintf(temp, "v~%d", s2_lvi);
    s2_lv = temp;
  }
  else if (s2_lvi < 10) { s2_lv = "v<10"; }
  else { s2_lv = "v>~10"; }
  if (s3_lvi <= 5) {
    sprintf(temp, "v~%d", s3_lvi);
    s3_lv = temp;
  }
  else if (s3_lvi < 10) { s3_lv = "v<10"; }
  else { s3_lv = "v>~10"; }

  // valency
  feat_str_input.push_back(s_lv);
  feat_str_input.push_back(s2_lv);
  feat_str_input.push_back(s3_lv);

  // ngram
  if (!ngram.empty()) {
    int all = 1369640000;
    if (ngram.find("@@all@@") != ngram.end()) all = ngram["@@all@@"];
    string s3f_key = normalize(s3.form);
    string s2f_key = normalize(s2.form);
    string sf_key = normalize(s.form);
    string qf_key = normalize(q.form);
    string ql_key = normalize(q.lemma);
    // q_form q_lemma
    string fl_q = qf_key + "_" + ql_key;
    // s_form q_form q_lemma, s_form q_form, s_form q_lemma
    string ffl_sq = "FFL_" + sf_key + "_" + qf_key + "_" + ql_key;
    string ff_sq = "FF_" + sf_key + "_" + qf_key;
    string fl_sq = "FL_" + sf_key + "_" + ql_key;
    // s2_form q_form q_lemma, s2_form q_form, s2_form q_lemma
    string ffl_s2q = "FFL_" + s2f_key + "_" + qf_key + "_" + ql_key;
    string ff_s2q = "FF_" + s2f_key + "_" + qf_key;
    string fl_s2q = "FL_" + s2f_key + "_" + ql_key;
    // s3_form q_form q_lemma, s3_form q_form, s3_form q_lemma
    string ffl_s3q = "FFL_" + s3f_key + "_" + qf_key + "_" + ql_key;
    string ff_s3q = "FF_" + s3f_key + "_" + qf_key;
    string fl_s3q = "FL_" + s3f_key + "_" + ql_key;
    // MI: s_form q_form q_lemma
    if (ngram.find(ffl_sq) != ngram.end() && ngram.find(sf_key) != ngram.end()
        && ngram.find(fl_q) != ngram.end()) {
      double p_x_given_y = (ngram[ffl_sq] + 1e-10) / (ngram[fl_q] + 1.0);
      double p_x = (ngram[sf_key] + 1e-10) / (all + 1.0);
      double mi = log(p_x_given_y / p_x);
      string mi_str2;
      if (mi >= 10) {
        mi_str2 = ">5";
      } else if (mi >= 0) {
        sprintf(temp, "%d", int(0.5 * mi));
        mi_str2 = temp;
      } else {
        sprintf(temp, "%d", -int(-0.5 * mi));
        mi_str2 = temp;
      }
      if (mi_str2 == "0" && mi < 0) mi_str2 = "-0";
      //cerr << ffl_sq << " p=" << p_x_given_y / p_x << " mi=" << mi << "," << mi_str2 << " ";
      feat_str_input.push_back("MI~" + mi_str2);
    } else {
      feat_str_input.push_back("MI~");
    }
    // MI: s_form q_form
    if (ngram.find(ff_sq) != ngram.end() && ngram.find(qf_key) != ngram.end()
        && ngram.find(sf_key) != ngram.end()) {
      double p_x_given_y = (ngram[ff_sq] + 1e-10) / (ngram[qf_key] + 1.0);
      double p_x = (ngram[sf_key] + 1e-10) / (all + 1.0);
      double mi = log(p_x_given_y / p_x);
      string mi_str2;
      if (mi >= 10) {
        mi_str2 = ">5";
      } else if (mi >= 0) {
        sprintf(temp, "%d", int(0.5 * mi));
        mi_str2 = temp;
      } else {
        sprintf(temp, "%d", -int(-0.5 * mi));
        mi_str2 = temp;
      }
      if (mi_str2 == "0" && mi < 0) mi_str2 = "-0";
      //cerr << " mi=" << mi << " " << p_x_given_y << " " << p_x;
      feat_str_input.push_back("MI~" + mi_str2);
    } else {
      feat_str_input.push_back("MI~");
    }
    // MI: s_form q_lemma
    if (ngram.find(fl_sq) != ngram.end() && ngram.find(sf_key) != ngram.end()
        && ngram.find(ql_key) != ngram.end()) {
      double p_x_given_y = (ngram[fl_sq] + 1e-10) / (ngram[ql_key] + 1.0);
      double p_x = (ngram[sf_key] + 1e-10) / (all + 1.0);
      double mi = log(p_x_given_y / p_x);
      string mi_str2;
      if (mi >= 10) {
        mi_str2 = ">5";
      } else if (mi >= 0) {
        sprintf(temp, "%d", int(0.5 * mi));
        mi_str2 = temp;
      } else {
        sprintf(temp, "%d", -int(-0.5 * mi));
        mi_str2 = temp;
      }
      if (mi_str2 == "0" && mi < 0) mi_str2 = "-0";
      //cerr << " mi=" << mi << " " << p_x_given_y << " " << p_x;
      feat_str_input.push_back("MI~" + mi_str2);
    } else {
      feat_str_input.push_back("MI~");
    }
    // MI: s2_form q_form q_lemma
    if (ngram.find(ffl_s2q) != ngram.end() && ngram.find(s2f_key) != ngram.end()
        && ngram.find(fl_q) != ngram.end()) {
      double p_x_given_y = (ngram[ffl_s2q] + 1e-10) / (ngram[fl_q] + 1.0);
      double p_x = (ngram[s2f_key] + 1e-10) / (all + 1.0);
      double mi = log(p_x_given_y / p_x);
      string mi_str2;
      if (mi >= 10) {
        mi_str2 = ">5";
      } else if (mi >= 0) {
        sprintf(temp, "%d", int(0.5 * mi));
        mi_str2 = temp;
      } else {
        sprintf(temp, "%d", -int(-0.5 * mi));
        mi_str2 = temp;
      }
      if (mi_str2 == "0" && mi < 0) mi_str2 = "-0";
      //cerr << " mi=" << mi << " " << p_x_given_y << " " << p_x;
      feat_str_input.push_back("MI~" + mi_str2);
    } else {
      feat_str_input.push_back("MI~");
    }
    // MI: s2_form q_form
    if (ngram.find(ff_s2q) != ngram.end() && ngram.find(s2f_key) != ngram.end()
        && ngram.find(qf_key) != ngram.end()) {
      double p_x_given_y = (ngram[ff_s2q] + 1e-10) / (ngram[qf_key] + 1.0);
      double p_x = (ngram[s2f_key] + 1e-10) / (all + 1.0);
      double mi = log(p_x_given_y / p_x);
      string mi_str2;
      if (mi >= 10) {
        mi_str2 = ">5";
      } else if (mi >= 0) {
        sprintf(temp, "%d", int(0.5 * mi));
        mi_str2 = temp;
      } else {
        sprintf(temp, "%d", -int(-0.5 * mi));
        mi_str2 = temp;
      }
      if (mi_str2 == "0" && mi < 0) mi_str2 = "-0";
      //cerr << " mi=" << mi << " " << p_x_given_y << " " << p_x;
      feat_str_input.push_back("MI~" + mi_str2);
    } else {
      feat_str_input.push_back("MI~");
    }
    // MI: s2_form q_lemma
    if (ngram.find(fl_s2q) != ngram.end() && ngram.find(s2f_key) != ngram.end()
        && ngram.find(ql_key) != ngram.end()) {
      double p_x_given_y = (ngram[fl_s2q] + 1e-10) / (ngram[ql_key] + 1.0);
      double p_x = (ngram[s2f_key] + 1e-10) / (all + 1.0);
      double mi = log(p_x_given_y / p_x);
      string mi_str2;
      if (mi >= 10) {
        mi_str2 = ">5";
      } else if (mi >= 0) {
        sprintf(temp, "%d", int(0.5 * mi));
        mi_str2 = temp;
      } else {
        sprintf(temp, "%d", -int(-0.5 * mi));
        mi_str2 = temp;
      }
      if (mi_str2 == "0" && mi < 0) mi_str2 = "-0";
      //cerr << " mi=" << mi << " " << p_x_given_y << " " << p_x;
      feat_str_input.push_back("MI~" + mi_str2);
    } else {
      feat_str_input.push_back("MI~");
    }
    // MI: s3_form q_form q_lemma
    if (ngram.find(ffl_s3q) != ngram.end() && ngram.find(s3f_key) != ngram.end()
        && ngram.find(fl_q) != ngram.end()) {
      double p_x_given_y = (ngram[ffl_s3q] + 1e-10) / (ngram[fl_q] + 1.0);
      double p_x = (ngram[s3f_key] + 1e-10) / (all + 1.0);
      double mi = log(p_x_given_y / p_x);
      string mi_str2;
      if (mi >= 10) {
        mi_str2 = ">5";
      } else if (mi >= 0) {
        sprintf(temp, "%d", int(0.5 * mi));
        mi_str2 = temp;
      } else {
        sprintf(temp, "%d", -int(-0.5 * mi));
        mi_str2 = temp;
      }
      if (mi_str2 == "0" && mi < 0) mi_str2 = "-0";
      //cerr << " mi=" << mi << " " << p_x_given_y << " " << p_x;
      feat_str_input.push_back("MI~" + mi_str2);
    } else {
      feat_str_input.push_back("MI~");
    }
    // MI: s3_form q_form
    if (ngram.find(ff_s3q) != ngram.end() && ngram.find(s3f_key) != ngram.end()
        && ngram.find(qf_key) != ngram.end()) {
      double p_x_given_y = (ngram[ff_s3q] + 1e-10) / (ngram[qf_key] + 1.0);
      double p_x = (ngram[s3f_key] + 1e-10) / (all + 1.0);
      double mi = log(p_x_given_y / p_x);
      string mi_str2;
      if (mi >= 10) {
        mi_str2 = ">5";
      } else if (mi >= 0) {
        sprintf(temp, "%d", int(0.5 * mi));
        mi_str2 = temp;
      } else {
        sprintf(temp, "%d", -int(-0.5 * mi));
        mi_str2 = temp;
      }
      if (mi_str2 == "0" && mi < 0) mi_str2 = "-0";
      //cerr << " mi=" << mi << " " << p_x_given_y << " " << p_x;
      feat_str_input.push_back("MI~" + mi_str2);
    } else {
      feat_str_input.push_back("MI~");
    }
    // MI: s3_form q_lemma
    if (ngram.find(fl_s3q) != ngram.end() && ngram.find(s3f_key) != ngram.end()
        && ngram.find(ql_key) != ngram.end()) {
      double p_x_given_y = (ngram[fl_s3q] + 1e-10) / (ngram[ql_key] + 1.0);
      double p_x = (ngram[s3f_key] + 1e-10) / (all + 1.0);
      double mi = log(p_x_given_y / p_x);
      string mi_str2;
      if (mi >= 10) {
        mi_str2 = ">5";
      } else if (mi >= 0) {
        sprintf(temp, "%d", int(0.5 * mi));
        mi_str2 = temp;
      } else {
        sprintf(temp, "%d", -int(-0.5 * mi));
        mi_str2 = temp;
      }
      if (mi_str2 == "0" && mi < 0) mi_str2 = "-0";
      //cerr << " mi=" << mi << " " << p_x_given_y << " " << p_x;
      feat_str_input.push_back("MI~" + mi_str2);
    } else {
      feat_str_input.push_back("MI~");
    }
  } else {
    for (int i = 0; i < 9; i++) {
      feat_str_input.push_back("MI~");
    }
  }

  for (int i = 0; i < feat_str_input.size(); i++) {
    string &f = feat_str_input[i];
    if (feat_map.find(f) != feat_map.end()) {
      feat_input.push_back(feat_map[f]);
    } else if (strncmp(f.c_str(), "POS=", 4) == 0) {
      //cerr << "feat_map(POS=UNK): " << f << endl;
      feat_input.push_back(feat_map["POS=UNK"]);
    } else if (strncmp(f.c_str(), "LB=", 3) == 0) {
      //cerr << "feat_map(LB=UNK): " << f << endl;
      feat_input.push_back(feat_map["LB=UNK"]);
    } else {
      cerr << "feat_map(UNK): " << f << endl;
      feat_input.push_back(feat_map["UNK"]);
    }
  }
}


/**
    Get left dependents' label set (왼쪽 자식 노드들의 label들을 가져옴)
    @param head     구문분석 결과 중의 head 정보
    @param label    구문분석 결과 중의 label 정보
    @param id       대상 어절 id
    @param ld_set   왼쪽 자식 노드들의 label들이 저장됨
*/
void Kor_Parser_NN::get_ld_set(vector<int> &head,
                               vector<string> &label,
                               int id,
                               string &ld_set) {
  ld_set = "";
  for (int i = id; i >= 0; i--) {
    if (id == head[i]) {
      if (ld_set != "") ld_set += "_";
      ld_set += label[i];
    }
  }
}

/**
    Get left dependents' important label set: subject, object, complement \n
    (왼쪽 자식 노드들의 label들 중에서 문법적으로 중요한 label들만 가져옴)
    @param head     구문분석 결과 중의 head 정보
    @param label    구문분석 결과 중의 label 정보
    @param id       대상 어절 id
    @param ld_set   왼쪽 자식 노드들의 중요한 label들이 저장됨
*/
void Kor_Parser_NN::get_ld_set2(vector<int> &head,
                                vector<string> &label,
                                int id,
                                string &ld_set) {
  ld_set = "";
  for (int i = id; i >= 0; i--) {
    if (id == head[i]) {
      if (label[i].find("SBJ") != string::npos) {
        if (ld_set != "") ld_set += "_";
        ld_set += "SBJ";
      } else if (label[i].find("OBJ") != string::npos) {
        if (ld_set != "") ld_set += "_";
        ld_set += "OBJ";
      } else if (label[i].find("CMP") != string::npos) {
        if (ld_set != "") ld_set += "_";
        ld_set += "CMP";
      }
    }
  }
}

/**
    Get right dependents' label set (오른쪽 자식 노드들의 label들을 가져옴)
    @param head     구문분석 결과 중의 head 정보
    @param label    구문분석 결과 중의 label 정보
    @param id       대상 어절 id
    @param rd_set   오른쪽 자식 노드들의 label들이 저장됨
*/
void Kor_Parser_NN::get_rd_set(vector<int> &head,
                               vector<string> &label,
                               int id,
                               string &rd_set) {
  rd_set = "";
  for (int i = id; i < head.size(); i--) {
    if (id == head[i]) {
      if (rd_set != "") rd_set += "_";
      rd_set += label[i];
    }
  }
}


/**
    Print stack & buffer: for debugging
    @param input    형태소분석/품사태깅 결과가 저장된 vector<Word_t>
    @param stack    현재 상태의 stack
    @param b_id     구문분석 대상 어절 id (buffer id)
*/
void Kor_Parser_NN::print_config(vector<Word_t> &input,
                                 deque<int> &stack,
                                 int b_id) {
  cerr << endl << "#Stack:";
  for (int i = 0; i < stack.size(); i++) {
    int id = stack[i];
    if (id == 0) cerr << " root";
    else cerr << " " << input[id - 1].form;
  }
  if (b_id - 1 >= 0)
    cerr << endl << "#Buffer: " << b_id << " " << input[b_id - 1].form << endl;
  else cerr << endl << "#Buffer: " << b_id << " ." << endl;
}


/**
    Print parsing result
    @param input    구문분석 결과가 저장된 vector<Word_t>
    @return 구문분석 결과를 출력한 string
*/
string Kor_Parser_NN::print_parse(vector<Word_t> &input) {
  string result;
  char temp[1000];
  for (int i = 0; i < input.size(); i++) {
    sprintf(temp,
            "%d\t%s\t%d\t%s\n",
            input[i].id,
            input[i].form.c_str(),
            input[i].head,
            input[i].dep_label.c_str());
    result += temp;
  }
  return result;
}

/**
    Print parsing result
    @param ksent    구문분석 결과가 저장된 K_Sentence 구조체
    @return 구문분석 결과를 출력한 string
*/
string Kor_Parser_NN::print_parse(K_Sentence &ksent) {
  string result;
  char temp[1000];
  for (int i = 0; i < ksent.dependency.size(); i++) {
    sprintf(temp, "%d", ksent.dependency[i].id);
    result = temp;
    result += "\t";
    result += ksent.word[i].text;
    result += "\t";
    sprintf(temp, "%d", ksent.dependency[i].head);
    result += temp;
    result += "\t";
    result += ksent.dependency[i].label;
    result += "\n";
  }
  return result;
}

/**
    Print parsing result (format: id head label POS_list)
    @param ksent    구문분석 결과가 저장된 K_Sentence 구조체
    @param begin_id 구문분석 결과가 출력시에 어절의 시작 id (일반적으로 0 or 1)
    @return 구문분석 결과를 출력한 string
*/
string Kor_Parser_NN::print_parse_format(K_Sentence &ksent, int begin_id) {
  string result;
  char temp[1000];
  for (int i = 0; i < ksent.dependency.size(); i++) {
    sprintf(temp,
            "%d\t%d\t%s\t",
            ksent.dependency[i].id + begin_id,
            ksent.dependency[i].head + begin_id,
            ksent.dependency[i].label.c_str());
    result += temp;
    // morpheme
    for (int j = ksent.word[i].begin_mid; j <= ksent.word[i].end_mid; j++) {
      if (j != ksent.word[i].begin_mid) result += "|";
      result += ksent.morp[j].lemma;
      result += "/";
      result += ksent.morp[j].type;
    }
    result += "\n";
  }
  return result;
}

/**
    Print parsing result as tree
    @param input    구문분석 결과가 저장된 vector<Word_t>
    @param depth    출력할 sub tree의 root의 depth
    @param head     출력할 sub tree의 root
    @return 구문분석 결과를 출력한 string
*/
string Kor_Parser_NN::print_parse_tree(vector<Word_t> &input,
                                       int depth,
                                       int head) {
  string result;
  char temp[1000];
  string depth_str;
  for (int i = 0; i < depth; i++) depth_str += "  ";
  for (vector<Word_t>::size_type i = input.size() - 1; i >= 0; i--) {
    if (input[i].head == head) {
      result += depth_str;
      string form = input[i].form2;
      if (form == "_") form = input[i].form;
      sprintf(temp,
              "(%d\t%s\t%d\t%s)\n",
              input[i].id,
              form.c_str(),
              input[i].head,
              input[i].dep_label.c_str());
      result += temp;
      // child
      result += print_parse_tree(input, depth + 1, input[i].id);
    }
  }
  return result;
}

/**
    Print parsing result as tree
    @param ksent    구문분석 결과가 저장된 K_Sentence 구조체
    @param depth    출력할 sub tree의 root의 depth
    @param head     출력할 sub tree의 root
    @return 구문분석 결과를 출력한 string
*/
string Kor_Parser_NN::print_parse_tree(K_Sentence &ksent, int depth, int head) {
  string result;
  char temp[1000];
  string depth_str;
  for (int i = 0; i < depth; i++) depth_str += "  ";
  for (vector<K_Dep>::size_type i = ksent.dependency.size() - 1;
       i >= 0; i--) {
    if (ksent.dependency[i].head == head) {
      result += depth_str;
      sprintf(temp,
              "(%d\t%s\t%d\t%s)\n",
              ksent.dependency[i].id,
              ksent.word[i].text.c_str(),
              ksent.dependency[i].head,
              ksent.dependency[i].label.c_str());
      result += temp;
      // child
      result += print_parse_tree(ksent, depth + 1, ksent.dependency[i].id);
    }
  }
  return result;
}

/**
    Print parsing result as phrase structure tree (PST)
    @param ksent    구문분석 결과가 저장된 K_Sentence 구조체
    @param depth    출력할 sub tree의 root의 depth
    @param head     출력할 sub tree의 root
    @return 구문분석 결과를 출력한 string
*/
string Kor_Parser_NN::print_PST(K_Sentence &ksent, int root_id, int depth) {
  string result;
  char temp[1000];
  string depth_str;
  if (root_id != ksent.PST.size() - 1) result += "\n";
  for (int i = 0; i < depth; i++) depth_str += "    ";
  // root
  K_PS &ps = ksent.PST[root_id];
  result += depth_str;
  if (ps.left_child == -1 || ps.right_child == -1) {
    sprintf(temp,
            "(%s %s)",
            ps.label.c_str(),
            ksent.word[ps.head_wid].text.c_str());
    result += temp;
  } else {
    sprintf(temp, "(%s", ps.label.c_str());
    result += temp;
    // children
    result += print_PST(ksent, ps.left_child, depth + 1);
    result += print_PST(ksent, ps.right_child, depth + 1);
    result += ")";
  }
  if (root_id == ksent.PST.size() - 1) result += "\n";
  return result;
}

