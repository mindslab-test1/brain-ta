#include <string.h>
#include <stdio.h>
#include <stdexcept> //for std::runtime_error
#include <memory>  //for std::bad_alloc

#include "libmaum/brain/nlp/kor-analysis.h"
#include "libmaum/brain/nlp/kdoc.h"
#include "kor-tagger.h"
#include "kor-ner.h"
#include "kor-parser-nn.h"
#include "kor-sa-nn.h"

using namespace std;

Kor::Kor() {
  tagger = new Kor_tagger;
  ner = new Kor_NER;
  parser = new Kor_Parser_NN();
  sa = new Kor_SA_NN();
  use_preprocess = 0;
}

Kor::~Kor() {
  delete tagger;
  delete ner;
  delete parser;
  delete sa;
}

// initialize each module
/// init Korean POS tagger
int Kor::init_tagger(const string &dir) {
  return tagger->init(dir);
}

/// init Korean Named Entity Recognizer
int Kor::init_NER(const string &dir) {
  return ner->init(dir);
}

/// init Korean dependency parser
int Kor::init_parser(const string &dir) {
  use_preprocess = 1;
  return parser->init(dir);
}

/// init Korean SA
int Kor::init_sa(const string &dir, const string &rsc) {
  return sa->init(dir, rsc);
}
/*
   int init_sa(const string &dir, const string &proj) {
   return sa->init(dir, proj);
   }
   */
// POS tagging
/**
  Korean POS tagging
  @param input  입력 텍스트
  @param k_doc  태깅 결과가 저장될 K_Doc 구조체 (split sentence 수행)
  @return score
  */
double Kor::do_tagging(string &input, K_Doc &k_doc) {
  k_doc.sentence.clear();
  k_doc.text = input;
  K_Sentence k_sent;
  double score = do_tagging(input, k_sent);
  if (use_preprocess) {
    // split sentence
    tagger->split_sentence(k_sent, k_doc);
    for (auto &sent : k_doc.sentence) {
      // K_Sentence &k_sent = k_doc.sentence[i];
      tagger->preprocess0(sent);
      tagger->preprocess1(sent);
      tagger->preprocess2(sent);
    }
  } else {
    k_doc.sentence.push_back(k_sent);
  }
  return score;
}
/**
  Korean POS tagging
  @param input  입력 텍스트
  @param k_sent   태깅 결과가 저장될 K_Sentence 구조체
  @return score
  */
double Kor::do_tagging(string &input, K_Sentence &k_sent) {
  k_sent.text.clear();
  k_sent.syllable.clear();
  k_sent.space.clear();
  k_sent.morp.clear();
  k_sent.word.clear();
  // POS tagging
  double score = tagger->do_tagging(input, k_sent);
  tagger->restore_morpheme(k_sent);
  return score;
}

// NER
/**
 * Korean Named Entity Recognition
 * @param K_Doc
 * @return K_Doc
 */
double Kor::do_NER(K_Doc &k_doc) {
  double score = 0;
  for (auto &k_sent : k_doc.sentence) {
    score += do_NER(k_sent);
  }
  return score;
}

/**
 * Korean Named Entity Recognition
 * @param K_Sentence
 * @return K_Sentence
 */
double Kor::do_NER(K_Sentence &k_sent) {
  k_sent.NE.clear();
  return ner->do_NER(k_sent);
}

// dependency parsing
/**
 * Korean dependency parsing
 * @param k_doc  품사태깅 결과가 저장된 K_Doc 구조체, 파싱 결과도 여기에 저장됨
 * @param beam   Beam search에서 beam size (default 1)
 * @return score
 */
double Kor::do_parsing(K_Doc &k_doc, int beam) {
  double score = 0;
  for (auto &k_sent : k_doc.sentence) {
    score += do_parsing(k_sent, beam);
  }
  return score;
}

/**
 * Korean dependency parsing
 * @param k_sent   품사태깅 결과가 저장된 K_Sentence 구조체, 파싱 결과도 여기에 저장됨
 * @param beam   Beam search에서 beam size (default 1)
 * @return score
 */
double Kor::do_parsing(K_Sentence &k_sent, int beam) {
  double score = 0;
  k_sent.dependency.clear();
  vector<Word_t> input;
  parser->convert_ksent2wordt(k_sent, input);
  vector<Word_t> output = parser->do_parsing(input, score);
  parser->convert_wordt2ksent(output, k_sent);
  return score;
}

// SA
/**
  Korean SA
  @param k_doc  품사태깅 결과가 저장된 K_Doc 구조체, SA 결과도 여기에 저장됨
  @return score
  */
double Kor::do_sa(K_Doc &k_doc) {
  double score = 0;
  for (auto &k_sent : k_doc.sentence) {
    score += do_sa(k_sent);
  }
  return score;
}

double Kor::do_sa(K_Sentence &k_sent) {
  double score = 0;
  k_sent.SA.clear();
  sa->do_SA(k_sent, score);
  return score;
}

vector<vector<string> > Kor::do_multy_sa(K_Doc &k_doc,
                                         vector<vector<float> > &vec_prob) {
  vector<vector<string> > vec_sa;
  for (int i = 0; i < (int) k_doc.sentence.size(); i++) {
    K_Sentence &k_sent = k_doc.sentence[i];
    vector<float> tmp_float;
    vector<string> tmp_sa = do_multy_sa(k_sent, tmp_float);
    vec_sa.push_back(tmp_sa);
    vec_prob.push_back(tmp_float);
  }
  return vec_sa;
}

vector<string> Kor::do_multy_sa(K_Sentence &k_sent,
                                vector<float> &vec_prob) {
  k_sent.SA.clear();
  vector<string> vec_sa = sa->do_multy_SA(k_sent, vec_prob);
  return vec_sa;
}

///////////////////////////////////////////////////////////
// Print results
///////////////////////////////////////////////////////////

/// print Korean POS tagging result
void Kor::print_POS(K_Doc &k_doc, int format) {
  for (auto &k_sent : k_doc.sentence) {
    print_POS(k_sent, format);
  }
}
void Kor::print_POS(K_Sentence &k_sent, int format) {
  int prev_wid = 0;
  int i = 0;
  for (const auto &morp : k_sent.morp) {
    // 한 어절별 한 문장으로 출력 (format == 1)
    if (format && morp.wid != prev_wid) {
      cout << endl;
    } else if (i > 0) {
      // 한 문장에 모든 형태소를 출력 (format == 0)
      cout << " ";
    }
    cout << morp.lemma << "/" << morp.type;
    prev_wid = morp.wid;
    i++;
  }
  cout << endl;
}

/// print Korean dependency parsing result
void Kor::print_parse(K_Doc &k_doc, int format) {
  for (auto &k_sent : k_doc.sentence)
    print_parse(k_sent, format);
}

void Kor::print_parse(K_Sentence &k_sent, int format) {
  if (k_sent.raw_text != "" && k_sent.raw_text != k_sent.text) {
    cout << "; " << k_sent.raw_text << endl;
    cout << ";; " << k_sent.text << endl;
  } else {
    cout << "; " << k_sent.text << endl;
  }
  if (format)
    cout << parser->print_parse_format(k_sent) << endl;
  else {
    cout << endl << parser->print_parse_tree(k_sent) << endl;
    cout << parser->print_PST(k_sent, (int) k_sent.PST.size() - 1) << endl;
  }
}

/// print Korean SA result
void Kor::print_sa(K_Doc &k_doc) {
  for (auto &k_sent : k_doc.sentence) {
    print_sa(k_sent);
  }
}
void Kor::print_sa(K_Sentence &k_sent) {
  cout << k_sent.SA << endl;
}

/////////////////////////////////////////////
/// make K_Doc as JSON format string
string Kor::sprint_JSON(K_Doc &k_doc, int human_readable) {
  string result;
  char temp[1000];
  temp[999] = 0;

  // doc
  string doc_text = my_replace_all(k_doc.text, "\\", "\\\\");
  doc_text = my_replace_all(doc_text, "\"", "\\\"");
  result += "{\"text\" : \"" + doc_text + "\",";
  if (human_readable) result += "\n"; else result += ' ';
  result += " \"sentence\" : [";
  if (human_readable) result += "\n";


  // sentence
  for (size_t i = 0; i < k_doc.sentence.size(); i++) {
    K_Sentence &sent = k_doc.sentence[i];
    if (human_readable) result += "\t";
    result += '{';
    if (human_readable) result += "\n\t";
    // id
    result += "\"id\" : ";
    sprintf(temp, "%d", sent.id);
    result += temp;
    result += ',';
    if (human_readable) result += "\n\t"; else result += ' ';
    // txt
    result += "\"text\" : \"";
    string sent_text = my_replace_all(sent.text, "\\", "\\\\");
    result += my_replace_all(sent_text, "\"", "\\\"");
    result += "\",";
    if (human_readable) result += "\n"; else result += ' ';
    /*
    // syllable
    if (human_readable) result += "\t";
    result += "\"syllable\" : [";
    if (human_readable) result += "\n";
    for (size_t j=0; j < sent.syllable.size(); j++) {
    // 음절 단위로 출력
    string &syl = sent.syllable[j];
    string &pos = sent.POS_outcome[j];
    if (human_readable) result += "\t\t";
    result += "{";
    // id
    result += "\"id\" : ";
    sprintf(temp, "%d", j);
    result += temp;
    result += ", ";
    // str
    result += "\"syl\" : \"";
    string syl2 = my_replace_all(syl, "\\", "\\\\");
    result += my_replace_all(syl2, "\"", "\\\"");
    result += "\", ";
    // type
    result += "\"type\" : \"";
    result += pos;
    result += "\", ";
    // wid
    result += "\"wid\" : ";
    sprintf(temp, "%d", sent.sid2wid[j]);
    result += temp;
    if (j+1 == sent.syllable.size()) {
    result += "}";
    } else {
    result += "},";
    }
    if (human_readable) result += "\n"; else result += " ";
    }
    if (human_readable) result += "\t";
    result += "],";
    if (human_readable) result += "\n"; else result += " ";
    */
    // morp
    if (human_readable) result += "\t";
    result += "\"morp\" : [";
    if (human_readable) result += "\n";
    for (size_t j = 0; j < sent.morp.size(); j++) {
      // 형태소 단위로 출력
      K_Morp &morp = sent.morp[j];
      if (human_readable) result += "\t\t";
      result += '{';
      // id
      result += "\"id\" : ";
      sprintf(temp, "%d", morp.id);
      result += temp;
      result += ", ";
      // str
      result += "\"lemma\" : \"";
      string morp_lemma = my_replace_all(morp.lemma, "\\", "\\\\");
      result += my_replace_all(morp_lemma, "\"", "\\\"");
      result += "\", ";
      // type
      result += "\"type\" : \"";
      result += morp.type;
      result += "\"";
      if (j + 1 == sent.morp.size()) {
        result += '}';
      } else {
        result += "},";
      }
      if (human_readable) result += "\n"; else result += ' ';
    }
    if (human_readable) result += "\t";
    result += "],";
    if (human_readable) result += "\n"; else result += ' ';

    // word
    if (human_readable) result += "\t";
    result += "\"word\" : [";
    if (human_readable) result += "\n";
    for (size_t j = 0; j < sent.word.size(); j++) {
      K_Word &word = sent.word[j];
      if (human_readable) result += "\t\t";
      result += '{';
      // id
      result += "\"id\" : ";
      sprintf(temp, "%d", word.id);
      result += temp;
      result += ", ";
      // str
      result += "\"text\" : \"";
      string Word_text = my_replace_all(word.text, "\\", "\\\\");
      result += my_replace_all(Word_text, "\"", "\\\"");
      result += "\", ";
      // tagged str
      result += "\"tagged_text\" : \"";
      Word_text = my_replace_all(word.tagged_text, "\\", "\\\\");
      result += my_replace_all(Word_text, "\"", "\\\"");
      result += "\", ";
      // begin
      result += "\"begin\" : ";
      sprintf(temp, "%d", word.begin_mid);
      result += temp;
      result += ", ";
      // end
      result += "\"end\" : ";
      sprintf(temp, "%d", word.end_mid);
      result += temp;
      if (j + 1 == sent.word.size()) {
        result += '}';
      } else {
        result += "},";
      }
      if (human_readable) result += "\n"; else result += ' ';
    }
    if (human_readable) result += "\t";
    result += "],";
    if (human_readable) result += "\n"; else result += ' ';

    // NER
    if (human_readable) result += "\t";
    result += "\"NE\" : [";
    if (human_readable) result += "\n";
    for (size_t j = 0; j < sent.NE.size(); j++) {
      K_NE &ne = sent.NE[j];
      if (human_readable) result += "\t\t";
      result += '{';
      // id
      result += "\"id\" : ";
      sprintf(temp, "%d", ne.id);
      result += temp;
      result += ", ";
      // text
      result += "\"text\" : \"";
      string ne_text = my_replace_all(ne.text, "\\", "\\\\");
      result += my_replace_all(ne_text, "\"", "\\\"");
      result += "\", ";
      // type
      result += "\"type\" : \"";
      result += ne.type;
      result += "\", ";
      // begin
      result += "\"begin\" : ";
      sprintf(temp, "%d", ne.begin);
      result += temp;
      result += ", ";
      // end
      result += "\"end\" : ";
      sprintf(temp, "%d", ne.end);
      result += temp;
      result += ", ";
      // begin_sid
      result += "\"begin_sid\" : ";
      sprintf(temp, "%d", ne.begin_sid);
      result += temp;
      result += ", ";
      // end_sid
      result += "\"end_sid\" : ";
      sprintf(temp, "%d", ne.end_sid);
      result += temp;
      if (j + 1 == sent.NE.size()) {
        result += '}';
      } else {
        result += "},";
      }
      if (human_readable) result += "\n"; else result += " ";
    }
    if (human_readable) result += "\t";
    result += "],";
    if (human_readable) result += "\n"; else result += " ";
#if 0
    // NER N-best
    if (! sent.NE_nbest.empty()) {
      if (human_readable) result += "\t";
      result += "\"NE_nbest\" : [";
      if (human_readable) result += "\n";
      for (size_t n=0; n < sent.NE_nbest.size(); n++) {
        if (human_readable) result += "\t\t";
        result += "[";
        if (human_readable) result += "\n";
        for (size_t j=0; j < sent.NE_nbest[n].size(); j++) {
          K_NE &ne = sent.NE_nbest[n][j];
          if (human_readable) result += "\t\t\t";
          result += "{";
          // id
          result += "\"id\" : ";
          sprintf(temp, "%d", ne.id);
          result += temp;
          result += ", ";
          // text
          result += "\"text\" : \"";
          string ne_text = my_replace_all(ne.text, "\\", "\\\\");
          result += my_replace_all(ne_text, "\"", "\\\"");
          result += "\", ";
          // type
          result += "\"type\" : \"";
          result += ne.type;
          result += "\", ";
          // begin
          result += "\"begin\" : ";
          sprintf(temp, "%d", ne.begin);
          result += temp;
          result += ", ";
          // end
          result += "\"end\" : ";
          sprintf(temp, "%d", ne.end);
          result += temp;
          result += ", ";
          // begin_sid
          result += "\"begin_sid\" : ";
          sprintf(temp, "%d", ne.begin_sid);
          result += temp;
          result += ", ";
          // end_sid
          result += "\"end_sid\" : ";
          sprintf(temp, "%d", ne.end_sid);
          result += temp;
          if (j+1 == sent.NE_nbest[n].size()) {
            result += "}";
          } else {
            result += "},";
          }
          if (human_readable) result += "\n"; else result += " ";
        }
        if (human_readable) result += "\t\t";
        if (n+1 == sent.NE_nbest.size()) {
          result += "]";
        } else {
          result += "],";
        }
        if (human_readable) result += "\n"; else result += " ";
      }
      if (human_readable) result += "\t";
      result += "],";
      if (human_readable) result += "\n"; else result += " ";
    }

    // dependency parsing
    if (human_readable) result += "\t";
    result += "\"dependency\" : [";
    if (human_readable) result += "\n";
    for (size_t j=0; j < sent.dependency.size(); j++) {
      K_Dep &dependency = sent.dependency[j];
      if (human_readable) result += "\t\t";
      result += "{";
      // id
      result += "\"id\" : ";
      sprintf(temp, "%d", dependency.id);
      result += temp;
      result += ", ";
      // text
      result += "\"text\" : \"";
      string dep_text = my_replace_all(sent.word[j].text, "\\", "\\\\");
      result += my_replace_all(dep_text, "\"", "\\\"");
      result += "\", ";
      // head
      result += "\"head\" : ";
      sprintf(temp, "%d", dependency.head);
      result += temp;
      result += ", ";
      // label
      result += "\"label\" : \"";
      result += dependency.label;
      result += "\"";
      result += ", ";
      // mod
      result += "\"mod\" : [";
      for (size_t k=0; k < dependency.modifier.size(); k++) {
        sprintf(temp, "%d", dependency.modifier[k]);
        result += temp;
        if (k <  dependency.modifier.size()-1) result += ", ";
      }
      result += "] ";
      // end
      if (j+1 == sent.dependency.size()) {
        result += "}";
      } else {
        result += "},";
      }
      if (human_readable) result += "\n"; else result += " ";
    }
    if (human_readable) result += "\t";
    result += "],";
    if (human_readable) result += "\n"; else result += " ";

    // phrase structure tree
    if (human_readable) result += "\t";
    result += "\"PST\" : [";
    if (human_readable) result += "\n";
    for (size_t j=0; j < sent.PST.size(); j++) {
      K_PS &ps = sent.PST[j];
      if (human_readable) result += "\t\t";
      result += "{";
      // id
      result += "\"id\" : ";
      sprintf(temp, "%d", ps.id);
      result += temp;
      result += ", ";
      // label
      result += "\"label\" : \"";
      result += ps.label;
      result += "\"";
      result += ", ";
      // head_wid
      result += "\"head_wid\" : ";
      sprintf(temp, "%d", ps.head_wid);
      result += temp;
      result += ", ";
      // head text
      result += "\"head_text\" : \"";
      string dep_text = my_replace_all(sent.word[ps.head_wid].text, "\\", "\\\\");
      result += my_replace_all(dep_text, "\"", "\\\"");
      result += "\", ";
      // left child
      result += "\"left_child\" : ";
      sprintf(temp, "%d", ps.left_child);
      result += temp;
      result += ", ";
      // right child
      result += "\"right_child\" : ";
      sprintf(temp, "%d", ps.right_child);
      result += temp;
      // end
      if (j+1 == sent.PST.size()) {
        result += "}";
      } else {
        result += "},";
      }
      if (human_readable) result += "\n"; else result += " ";
    }
    if (human_readable) result += "\t";
    result += "],";
    if (human_readable) result += "\n"; else result += " ";

    // SRL
    if (human_readable) result += "\t";
    result += "\"SRL\" : [";
    if (human_readable) result += "\n";
    for (size_t j=0; j < sent.SRL.size(); j++) {
      K_SRL &srl = sent.SRL[j];
      if (human_readable) result += "\t\t";
      result += "{";
      // verb
      result += "\"verb\" : \"";
      string verb_text = my_replace_all(srl.verb, "\\", "\\\\");
      result += my_replace_all(verb_text, "\"", "\\\"");
      result += "\", ";
      // sense
      result += "\"sense\" : ";
      sprintf(temp, "%d", srl.sense);
      result += temp;
      result += ", ";
      // verb_wid
      result += "\"word_id\" : ";
      sprintf(temp, "%d", srl.verb_wid);
      result += temp;
      result += ", ";
      // argument
      if (human_readable) result += "\n\t\t\t";
      result += "\"argument\" : [";
      if (human_readable) result += "\n";
      for (size_t k=0; k < srl.arg.size(); k++) {
        if (human_readable) result += "\t\t\t\t";
        result += "{";
        // type
        result += "\"type\" : \"";
        result += srl.arg[k].first;
        result += "\", ";
        // word_id
        result += "\"word_id\" : ";
        int word_id = srl.arg[k].second;
        sprintf(temp, "%d", word_id);
        result += temp;
        result += ", ";
        // text
        result += "\"text\" : \"";
        string arg_text = my_replace_all(sent.word[word_id].text, "\\", "\\\\");
        result += my_replace_all(arg_text, "\"", "\\\"");
        if (k+1 < srl.arg.size()) result += "\"},";
        else result += "\"}";
        if (human_readable) result += "\n"; else result += " ";
      }
      if (human_readable) result += "\t\t\t";
      result += "] ";
      if (j+1 == sent.SRL.size()) {
        result += "}";
      } else {
        result += "},";
      }
      if (human_readable) result += "\n"; else result += " ";
    }
    if (human_readable) result += "\t";
    result += "],";
    if (human_readable) result += "\n"; else result += " ";

    // relation
    if (human_readable) result += "\t";
    result += "\"relation\" : [";
    if (human_readable) result += "\n";
    for (size_t j=0; j < sent.relation.size(); j++) {
      K_Relation &rel = sent.relation[j];
      if (human_readable) result += "\t\t";
      result += "{";
      // type
      result += "\"type\" : \"";
      result += rel.type;
      result += "\", ";
      // arg1
      result += "\"arg1\" : ";
      sprintf(temp, "%d", rel.arg1);
      result += temp;
      result += ", ";
      // arg1_text
      result += "\"arg1_text\" : \"";
      int ne_id = rel.arg1;
      string arg_text = my_replace_all(sent.NE[ne_id].text, "\\", "\\\\");
      result += my_replace_all(arg_text, "\"", "\\\"");
      result += "\", ";
      // arg2
      result += "\"arg2\" : ";
      sprintf(temp, "%d", rel.arg2);
      result += temp;
      result += ", ";
      // arg2_text
      result += "\"arg2_text\" : \"";
      ne_id = rel.arg2;
      arg_text = my_replace_all(sent.NE[ne_id].text, "\\", "\\\\");
      result += my_replace_all(arg_text, "\"", "\\\"");
      result += "\"";
      if (j+1 == sent.relation.size()) {
        result += "}";
      } else {
        result += "},";
      }
      if (human_readable) result += "\n"; else result += " ";
    }
    if (human_readable) result += "\t";
    result += "],";
    if (human_readable) result += "\n"; else result += " ";

    // event
    if (human_readable) result += "\t";
    result += "\"event\" : [";
    if (human_readable) result += "\n";
    for (size_t j=0; j < sent.event.size(); j++) {
      K_Event &event = sent.event[j];
      if (human_readable) result += "\t\t";
      result += "{";
      // type
      result += "\"type\" : \"";
      result += event.type;
      result += "\", ";
      // trigger
      result += "\"trigger\" : \"";
      string trigger_text = my_replace_all(event.trigger, "\\", "\\\\");
      result += my_replace_all(trigger_text, "\"", "\\\"");
      result += "\", ";
      // trigger_wid
      result += "\"trigger_wid\" : ";
      sprintf(temp, "%d", event.trigger_wid);
      result += temp;
      result += ", ";
      // argument
      if (human_readable) result += "\n\t\t\t";
      result += "\"argument\" : [";
      if (human_readable) result += "\n";
      for (size_t k=0; k < event.arg.size(); k++) {
        if (human_readable) result += "\t\t\t\t";
        result += "{";
        // type
        result += "\"type\" : \"";
        result += event.arg[k].first;
        result += "\", ";
        // NE_id
        result += "\"NE_id\" : ";
        sprintf(temp, "%d", event.arg[k].second);
        result += temp;
        result += ", ";
        // text
        result += "\"text\" : \"";
        int ne_id = event.arg[k].second;
        string arg_text = my_replace_all(sent.NE[ne_id].text, "\\", "\\\\");
        result += my_replace_all(arg_text, "\"", "\\\"");
        result += "\"";
        if (k+1 < event.arg.size()) result += "},";
        else result += "}";
        if (human_readable) result += "\n"; else result += " ";
      }
      if (human_readable) result += "\t\t\t";
      result += "] ";
      if (j+1 == sent.event.size()) {
        result += "}";
      } else {
        result += "},";
      }
      if (human_readable) result += "\n"; else result += " ";
    }
    if (human_readable) result += "\t";
    result += "],";
    if (human_readable) result += "\n"; else result += " ";
#endif
    // SA for sentence
    if (human_readable) result += "\t";
    result += "\"SA\" : \"";
    result += sent.SA;
    result += "\"";
    if (human_readable) result += "\n"; else result += ' ';

    if (i + 1 == k_doc.sentence.size()) {
      if (human_readable) result += "\t";
      result += '}';
      if (human_readable) result += "\n";
    } else {
      if (human_readable) result += "\t";
      result += "},";
      if (human_readable) result += "\n";
    }
  }
  result += "] }";
  if (human_readable) result += "\n";

  return result;
}

vector<string> Kor::get_postagged_str(K_Doc &k_doc) {
  vector<string> ret;
  for (auto &k_sent : k_doc.sentence) {
    string result;
    for (const auto &word : k_sent.word) {
      result += ' ';
      result += my_replace_all(word.tagged_text, " ", "+");
    }
    ret.push_back(result);
  }
  return ret;
}
