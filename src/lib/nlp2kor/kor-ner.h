#if !defined(KOR_NER_INCLUDED_)
#define KOR_NER_INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma warning( disable : 4786 )

#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <set>
#include <map>
#include "libmaum/brain/nlp/kdoc.h"
#include "kor-ssvm.h"
#include "util.h"

using namespace std;


/// 음절 기반 한국어 개체명인식기 - by leeck
class Kor_NER {
 public:
  Kor_NER();
  virtual ~Kor_NER();

  /// 초기화
  int init(const string &dir);

  /// 종료
  void close();

  /// NER feature: fixed window = 3 : input=syllables and space and POS, output=feature_vec
  void make_ner_feature(vector<string> &syllable,
                        vector<string> &space,
                        vector<string> &pos,
                        vector<vector<string> > &feature_vec);
  /// fast version
  void make_ner_feature_int(vector<string> &syllable,
                            vector<string> &space,
                            vector<string> &pos,
                            vector<vector<int> > &feature_vec);

  /// 입력문장을 NER tagging
  double do_NER(K_Sentence &k_sent);

  /// check string type
  string get_str_type(const string &str);
  /// 현재 음절이 속한 형태소를 반환
  string get_cur_morp(vector<string> &syllables, vector<string> &pos, int i);
  /// 현재 음절이 속한 형태소의 앞 형태소를 반환
  string get_prev_morp(vector<string> &syllables, vector<string> &pos, int i);
  /// 현재 음절이 속한 형태소의 뒤 형태소를 반환
  string get_next_morp(vector<string> &syllables, vector<string> &pos, int i);

  /// parameter
  int beam;
  int verbose;

 private:
  /// S-SVM
  K_SSVM ner_ssvm;
  /// NER dic
  map<string, string> nedic;
  map<string, string> cldic;
};


#endif // KOR_NER_INCLUDED_
