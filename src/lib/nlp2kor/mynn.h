/**
  @file mynn.hpp
  @brief (Word embedding-based) Neural Network class
  @author Changki Lee (leeck@kangwon.ac.kr)
  @date 2014/10/23
*/
#ifndef MYNN_H
#define MYNN_H

#include <math.h>
#include <ctime>
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include "timer.h"

// activation function
#define SIGM    1
#define TANH    2
#define RELU    3

using namespace std;

/** (Word embedding-based) Neural Network class
 @class mynn
 */
class MyNN {
 public:
  int activation_func;    ///< activation function type
  float dropout_fraction; ///< dropout fraction
  int cache_size;         ///< cache size

 private:
  // number of neurons
  int input_word_num;     ///< size of the input word
  int input_feat_num;     ///< size of the input feature
  int word_embed_num;     ///< size of the word embedding layer
  int feat_embed_num;     ///< size of the feature embedding layer
  int hidden_num;         ///< size of the hidden layer
  int output_num;         ///< size of the output

  // number of vocab.
  int word_vocab_num;     ///< size of the word vocab.
  int feat_vocab_num;     ///< size of the feature vocab.

  // weights
  vector<vector<float> > word_embedding_weights;
  vector<vector<float> > feat_embedding_weights;
  vector<vector<float> > word_embed_to_hid_weights;
  vector<vector<float> > feat_embed_to_hid_weights;
  vector<vector<float> > hid_to_output_weights;
  vector<float> hid_bias;
  vector<float> output_bias;

  // cache for word_embedding to hid
  vector<float> cache;

 public:
  /// constructor
  MyNN(int activation_func = RELU) {
    MyNN::activation_func = activation_func;
    dropout_fraction = 0.5;
    cache_size = 10000;
  }

  /// destructor
  ~MyNN() {
  }

  /// load model
  void load_model(const string &model);

  /// classify
  pair<int, float> classify(vector<int> &word_input, vector<int> &feat_input);

  /// forward propagation
  void fprop(vector<int> &word_input, vector<int> &feat_input,
             vector<float> &word_embedding_layer_state,
             vector<float> &feat_embedding_layer_state,
             vector<float> &hidden_layer_state,
             vector<float> &output_layer_state);

  /** Tokenize string to words.
      Tokenization of string and assignment to word vector.
      Delimiters are set of char.
      @param str string
      @param tokens token vector
      @param delimiters delimiters to divide string
      @return none
  */
  void tokenize
      (const string &str, vector<string> &tokens, const string &delimiters);

  /** Split string to words.
      Tokenization of string and assignment to word vector.
      Delimiter is string.
      @param str string
      @param tokens token vector
      @param delimiter delimiter to divide string
      @return none
  */
  void
      split(const string &str, vector<string> &tokens, const string &delimiter);

 private:
  /// activation function: sigmoid
  inline double sigmoid(double x) {
    return 1 / (1 + exp(-x));
  }
  inline float sigmoid(float x) {
    return 1 / (1 + exp(-x));
  }

  /// activation function: ReLU
  inline double relu(double x) {
    return x < 0 ? 0 : x;
  }
  inline float relu(float x) {
    return x < 0 ? 0 : x;
  }
};

#endif
