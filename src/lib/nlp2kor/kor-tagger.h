#if !defined(KOR_TAGGER_INCLUDED_)
#define KOR_TAGGER_INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma warning( disable : 4786 )

#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <set>
#include <map>
#include "libmaum/brain/nlp/kdoc.h"
#include "kor-ssvm.h"
#include "util.h"

using namespace std;


/// 음절 기반 한국어 태거 - by leeck
class Kor_tagger {
 public:
  Kor_tagger();
  virtual ~Kor_tagger();

  /// 초기화
  int init(const string& dir);

  /// 종료
  void close();

  /// POS tagging
  double do_tagging(string &input, K_Sentence &k_sent);

  /// restore morpheme: 형태소 원형 복원
  void restore_morpheme(K_Sentence &k_sent);

  /// split sentence: 문장 분리
  void split_sentence(K_Sentence &k_sent, K_Doc &k_doc);

  /// preprocess0: 구문 분석을 위한 word...word 전처리
  void preprocess0(K_Sentence &k_sent);
  /// preprocess1: 구문 분석을 위한 ( ) < > { } [ ] 전처리
  void preprocess1(K_Sentence &k_sent);
  /// preprocess2: 구문 분석을 위한 " ` ' 전처리
  void preprocess2(K_Sentence &k_sent);

  /// syllables 생성
  void make_syllable(string &input, vector<string> &syllable);

  /// POS feature 생성: fixed window = 3: input=syllables and space, output=feature_vec
  void make_feature(vector<string> &syllable,
                    vector<string> &space,
                    vector<vector<string> > &feature_vec);
  /// fast version
  void make_feature_int(vector<string> &syllable,
                        vector<string> &space,
                        vector<vector<int> > &feature_vec);

  /// check string type
  string get_str_type(const string& str);

  /// parameter
  int restore;
  int beam;
  int verbose;

 private:
  /// S-SVM
  K_SSVM ssvm;
  /// POS dic
  map<string, string> dic;

  /// morpheme restoration: 애매성이 존재할 때는 현재 확률이 높은 쪽 선택
  map<string, string> morph_rule;    ///< partial matching 허용
  map<string, int> morph_rule_freq;  ///< morph rule frequency
  map<string, string> morph_rule_lp; ///< morph rule at last position in ejeol
};


#endif // KOR_TAGGER_INCLUDED_
