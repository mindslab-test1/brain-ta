#include <stdlib.h>
#include <math.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <stdexcept>

#include "nn-util.h"
#include "seq2label.h"

using namespace std;

/**
 *
 * @author hhs, modified by leeck
 *
 *
 */
void Seq2label::load_model(const string &file_name) {
  ifstream is1;
  is1.open(file_name.c_str(), ios::in);
  if (is1.fail()) {
    cerr << file_name << " open fail" << endl;
    exit(1);
  }

  cerr << "load " << file_name << "... ";

  this->emb.clear();
  this->Wgx.clear();
  this->Ugh.clear();
  this->bg.clear();
  this->h0.clear();
  this->cell0.clear();
  this->Wyc.clear();
  this->by.clear();

  string s1;
  vector<string> s_temp1;
  vector<float> s_temp2;

  s_temp1.clear();
  s_temp2.clear();

  getline(is1, s1);
  if (s1.find("emb") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        s_temp1.clear();
        s_temp2.clear();
        tokenize(s1, s_temp1, " \t\r\n");
        for (int i = 0; i < (int) s_temp1.size(); i++) {
          s_temp2.push_back((float)atof(s_temp1[i].c_str()));
        }
        this->emb.push_back(s_temp2);
      } else {
        break;
      }
    }
  } else {
    cerr << "emb error!" << endl;
    throw std::invalid_argument( "model error :: emb error!" );
  }

  getline(is1, s1);
  if (s1.find("Wgx") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        s_temp1.clear();
        s_temp2.clear();
        tokenize(s1, s_temp1, " \t\r\n");
        for (int i = 0; i < (int) s_temp1.size(); i++) {
          s_temp2.push_back((float)atof(s_temp1[i].c_str()));
        }
        this->Wgx.push_back(s_temp2);
      } else {
        break;
      }

    }
  } else {
    cerr << "Wgx error!" << endl;
    throw std::invalid_argument( "model error :: Wgx error!" );
  }

  getline(is1, s1);
  if (s1.find("Ugh") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        s_temp1.clear();
        s_temp2.clear();
        tokenize(s1, s_temp1, " \t\r\n");
        for (int i = 0; i < (int) s_temp1.size(); i++) {
          s_temp2.push_back((float)atof(s_temp1[i].c_str()));
        }
        this->Ugh.push_back(s_temp2);
      } else {
        break;
      }

    }
  } else {
    cerr << "Ugh error!" << endl;
    throw std::invalid_argument( "model error :: Ugh error!" );
  }

  getline(is1, s1);
  if (s1.find("Wyc") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        s_temp1.clear();
        s_temp2.clear();
        tokenize(s1, s_temp1, " \t\r\n");
        for (int i = 0; i < (int) s_temp1.size(); i++) {
          s_temp2.push_back((float)atof(s_temp1[i].c_str()));
        }
        this->Wyc.push_back(s_temp2);
      } else {
        break;
      }

    }
  } else {
    cerr << "Wyc error!" << endl;
    throw std::invalid_argument( "model error :: Wyc error!" );
  }

  getline(is1, s1);
  if (s1.find("bg") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        this->bg.push_back((float)atof(s1.c_str()));
      } else {
        break;
      }

    }
  } else {
    cerr << "bg error!" << endl;
    throw std::invalid_argument( "model error :: bg error!" );
  }

  getline(is1, s1);
  if (s1.find("by") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        this->by.push_back((float)atof(s1.c_str()));
      } else {
        break;
      }

    }
  } else {
    cerr << "by error!" << endl;
    throw std::invalid_argument( "model error :: by error!" );
  }

  getline(is1, s1);
  if (s1.find("h0") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        this->h0.push_back((float)atof(s1.c_str()));
      } else {
        break;
      }

    }
  } else {
    cerr << "h0 error!" << endl;
    throw std::invalid_argument( "model error :: h0 error!" );
  }

  getline(is1, s1);
  if (s1.find("cell0") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        this->cell0.push_back((float)atof(s1.c_str()));
      } else {
        break;
      }

    }
  } else {
    cerr << "cell0 error!" << endl;
    throw std::invalid_argument( "model error :: cell0 error!" );
  }

  this->de = (int) (this->emb[0].size());
  this->ne = (int) (this->emb.size());
  this->nall = (int) (this->Wgx[0].size());
  this->nx = (int) (this->Wgx.size());
  this->nh = (int) (this->Ugh.size());
  this->nc = (int) (this->Wyc[0].size());

  cerr << "done." << endl;
  cerr << "emb size : " << this->emb.size() << " * " << this->emb[0].size()
      << endl;
  cerr << "Wgx size : " << this->Wgx.size() << " * " << this->Wgx[0].size()
      << endl;
  cerr << "Ugh size : " << this->Ugh.size() << " * " << this->Ugh[0].size()
      << endl;
  cerr << "Wyc size : " << this->Wyc.size() << " * " << this->Wyc[0].size()
      << endl;
  cerr << "bg size : " << this->bg.size() << endl;
  cerr << "h0 size : " << this->h0.size() << endl;
  cerr << "cell0 size : " << this->cell0.size() << endl;
  cerr << "by size : " << this->by.size() << endl;

  trans_matrix(Wgx, Wgx);
  trans_matrix(Ugh, Ugh);
  trans_matrix(Wyc, Wyc);
}

Seq2label::Seq2label() {
  this->activation = TANH;
  this->x_drop_out_rate = 0.8;
  this->h_drop_out_rate = 0.5;
}

int Seq2label::classify(vector<int> input_vec) {
  vector<vector<float> > xt;

  float max_temp = -99999999.99999999F;
  int max_out = -1;

  xt.clear();

  vector<float> h_tm1;
  vector<float> c_tm1;

  vector_copy(this->h0, h_tm1);
  vector_copy(this->cell0, c_tm1);

  //projection
  for (int i = 0; i < (int) input_vec.size(); i++) {
    vector<float> temp_x;
    temp_x.clear();

    for (int k = 0; k < this->de; k++) {
      temp_x.push_back((this->x_drop_out_rate) * (this->emb[input_vec[i]][k]));
    }
    xt.push_back(temp_x);
  }

  vector<float> i_t;
  vector<float> f_t;
  vector<float> o_t;
  vector<float> cc_t;
  vector<float> c_t;
  vector<float> c2_t;
  vector<float> h_t;
  vector<float> y_t;

  for (int wordi = 0; wordi < (int) input_vec.size(); wordi++) {
    vector<float> allt1;
    vector<float> allt2;

    allt1.clear();
    allt2.clear();

    matrix_vector_mul(this->Wgx, xt[wordi], allt1);
    matrix_vector_mul(this->Ugh, h_tm1, allt2);

    vector_add(allt2, allt1);
    vector_add(this->bg, allt1);

    i_t.clear();
    f_t.clear();
    o_t.clear();
    cc_t.clear();
    c_t.clear();
    c2_t.clear();
    h_t.clear();
    y_t.clear();

    for (int i = 0; i < this->nh; i++) {
      i_t.push_back(allt1[i]);
      f_t.push_back(allt1[i + this->nh]);
      o_t.push_back(allt1[i + this->nh + this->nh]);
      cc_t.push_back(allt1[i + this->nh + this->nh + this->nh]);
    }

    vector_sigm(i_t, i_t);
    vector_sigm(f_t, f_t);
    vector_sigm(o_t, o_t);
    vector_tanh(cc_t, cc_t);

    vector_vector_mul(i_t, cc_t, i_t);
    vector_vector_mul(f_t, c_tm1, f_t);
    vector_add(i_t, f_t);

    vector_copy(f_t, c2_t);

    if (activation == SIGM) {
      vector_sigm(f_t, c_t);
    } else if (activation == TANH) {
      vector_tanh(f_t, c_t);
    } else {
      vector_relu(f_t, c_t);
    }

    vector_vector_mul(o_t, c_t, h_t);

    vector_copy(h_t, h_tm1);
    vector_copy(c2_t, c_tm1);
  }

  for (int i = 0; i < (int) h_t.size(); i++) {
    h_t[i] = this->h_drop_out_rate * h_t[i];
  }

  matrix_vector_mul(this->Wyc, h_t, y_t);
  vector_add(this->by, y_t);

  for (int i = 0; i < this->nc; i++) {
    if (y_t[i] > max_temp) {
      max_temp = y_t[i];
      max_out = i;
    }
    //cout << "classsss : " << i << " " << y_t[i] << endl;
  }

  return max_out;
}

vector<int> Seq2label::multy_classify(vector<int> input_vec,
                                      vector<float> &vec_prob) {
  vector<vector<float> > xt;
  // ISSUE: m2u#278
  // static int MAX_NUM = 3;
  int MAX_NUM = this->nc;
  vector<int> vecID;

  xt.clear();

  vector<float> h_tm1;
  vector<float> c_tm1;

  vector_copy(this->h0, h_tm1);
  vector_copy(this->cell0, c_tm1);

  //projection
  for (int i = 0; i < (int) input_vec.size(); i++) {
    vector<float> temp_x;
    temp_x.clear();

    for (int k = 0; k < this->de; k++) {
      temp_x.push_back((this->x_drop_out_rate) * (this->emb[input_vec[i]][k]));
    }
    xt.push_back(temp_x);
  }

  vector<float> i_t;
  vector<float> f_t;
  vector<float> o_t;
  vector<float> cc_t;
  vector<float> c_t;
  vector<float> c2_t;
  vector<float> h_t;
  vector<float> y_t;

  for (int wordi = 0; wordi < (int) input_vec.size(); wordi++) {
    vector<float> allt1;
    vector<float> allt2;

    allt1.clear();
    allt2.clear();

    matrix_vector_mul(this->Wgx, xt[wordi], allt1);
    matrix_vector_mul(this->Ugh, h_tm1, allt2);

    vector_add(allt2, allt1);
    vector_add(this->bg, allt1);

    i_t.clear();
    f_t.clear();
    o_t.clear();
    cc_t.clear();
    c_t.clear();
    c2_t.clear();
    h_t.clear();
    y_t.clear();

    for (int i = 0; i < this->nh; i++) {
      i_t.push_back(allt1[i]);
      f_t.push_back(allt1[i + this->nh]);
      o_t.push_back(allt1[i + this->nh + this->nh]);
      cc_t.push_back(allt1[i + this->nh + this->nh + this->nh]);
    }

    vector_sigm(i_t, i_t);
    vector_sigm(f_t, f_t);
    vector_sigm(o_t, o_t);
    vector_tanh(cc_t, cc_t);

    vector_vector_mul(i_t, cc_t, i_t);
    vector_vector_mul(f_t, c_tm1, f_t);
    vector_add(i_t, f_t);

    vector_copy(f_t, c2_t);

    if (activation == SIGM) {
      vector_sigm(f_t, c_t);
    } else if (activation == TANH) {
      vector_tanh(f_t, c_t);
    } else {
      vector_relu(f_t, c_t);
    }

    vector_vector_mul(o_t, c_t, h_t);

    vector_copy(h_t, h_tm1);
    vector_copy(c2_t, c_tm1);
  }

  for (int i = 0; i < (int) h_t.size(); i++) {
    h_t[i] = this->h_drop_out_rate * h_t[i];
  }

  matrix_vector_mul(this->Wyc, h_t, y_t);
  vector_add(this->by, y_t);

  for (int i = 0; i < MAX_NUM; i++) {
    vec_prob.push_back(-999999.999999F);
    vecID.push_back(-1);
  }

  for (int i = 0; i < this->nc; i++) {
    for (int j = 0; j < MAX_NUM; j++) {
      if (y_t[i] > vec_prob[j]) {
        for (int k = MAX_NUM - 1; k > j; k--) {
          vec_prob[k] = vec_prob[k - 1];
          vecID[k] = vecID[k - 1];
        }
        vec_prob[j] = y_t[i];
        vecID[j] = i;
        break;
      }
    }
    // cout << "classsss : " << i << " " << y_t[i] << endl;
  }

  for (int i = 0; i < MAX_NUM; i++) {
    float sum = 0.0;
    for (int j = 0; j < this->nc; j++)
      sum += expf(y_t[j] - vec_prob[i]);
    vec_prob[i] = 1.0F / sum;
    // cout << "vec_prob[" << i << "] = " << vec_prob[i] << endl ;
  }

  return vecID;
}
