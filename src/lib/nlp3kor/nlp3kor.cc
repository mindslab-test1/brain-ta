#include "libmaum/brain/nlp/nlp3kor.h"
#include <fstream>
#include <sstream>

#undef MAX
#undef MIN
#include "Lheader/LMInterface.h"
#include <curl/curl.h>
#include <libmaum/common/config.h>
#include <libmaum/common/encoding.h>
#include <libmaum/brain/error/ta-error.h>

#include "rocksdb/db.h"

#include "rwlock.h"

using maum::brain::nlp::ChangedNerDict;

const char *kMltCallbackApiKey = "7c3a778e29ac522de62c4e22aa90c9";

const int32_t kLevelToFeatures[8] = {
    // NLP_ANALYSIS_ALL
    (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION |
        1 << NlpFeature::NLPF_MORPHEME |
        1 << NlpFeature::NLPF_NAMED_ENTITY |
        1 << NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION |
        1 << NlpFeature::NLPF_DEPENDENCY_PARSER |
        1 << NlpFeature::NLPF_SEMANTIC_ROLE_LABELING |
        1 << NlpFeature::NLPF_ZERO_ANAPHORA),
    // NLP_ANALYSIS_MORPHEME
    (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION |
        1 << NlpFeature::NLPF_MORPHEME),
    //NLP_ANALYSIS_NAMED_ENTITY
    (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION |
        1 << NlpFeature::NLPF_MORPHEME |
        1 << NlpFeature::NLPF_NAMED_ENTITY),
    // NLP_ANALYSIS_WORD_SENSE_DISAMBIGUATION
    (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION |
        1 << NlpFeature::NLPF_MORPHEME |
        1 << NlpFeature::NLPF_NAMED_ENTITY |
        1 << NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION),
    // NLP_ANALYSIS_DEPENDENCY_PARSER
    (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION |
        1 << NlpFeature::NLPF_MORPHEME |
        1 << NlpFeature::NLPF_NAMED_ENTITY |
        1 << NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION |
        1 << NlpFeature::NLPF_DEPENDENCY_PARSER),
    // NLP_ANALYSIS_SEMANTIC_ROLE_LABELING
    (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION |
        1 << NlpFeature::NLPF_MORPHEME |
        1 << NlpFeature::NLPF_NAMED_ENTITY |
        1 << NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION |
        1 << NlpFeature::NLPF_DEPENDENCY_PARSER |
        1 << NlpFeature::NLPF_SEMANTIC_ROLE_LABELING),
    // NLP_ANALYSIS_ZERO_ANAPHORA
    (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION |
        1 << NlpFeature::NLPF_MORPHEME |
        1 << NlpFeature::NLPF_NAMED_ENTITY |
        1 << NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION |
        1 << NlpFeature::NLPF_DEPENDENCY_PARSER |
        1 << NlpFeature::NLPF_SEMANTIC_ROLE_LABELING |
        1 << NlpFeature::NLPF_ZERO_ANAPHORA),
    0
};

/**
 * `level`과 `feature_mask`를 고려하고 현재 지원가능한 feature를 감안하여
 * 처리할 feature를 내보낸다.
 *
 * @param level 사용자로부터 요청된 level, feature_mask가 지정되어 있으면
 *    이 값은 무시된다.
 * @param feature_mask
 *    요청할 feature 들, 필수적으로 채워야할 feature를 채운다.
 *    이 과정 이후에 이 서버가 현재 처리가능한 feature와 mask 처리한다.
 * @return 지원가능한 feature 처리
 */
int32_t Nlp3Kor::LevelToFeature(NlpAnalysisLevel level, int32_t feature_mask) {
  if (level > NlpAnalysisLevel::NLP_ANALYSIS_ZERO_ANAPHORA)
    level = NlpAnalysisLevel::NLP_ANALYSIS_ZERO_ANAPHORA;

  // `feature_mask`가 값이 0이고 `level`이 있는 경우에는
  // 레벨을 `feature`로 변경하여 처리한다.
  if (feature_mask == 0) {
    feature_mask = kLevelToFeatures[int(level)];
  }

  // 매우 중요한 한줄,
  // 현재 서버의 구성에서 지원 가능한 feature와 masking한 결과
  feature_mask &= nlp_feature_mask_;

  // 분석의 최소 단계에서는 아래 2개, 즉, 문장인식과 형태소 분석은 필수이므로
  // 이를 무조건 수행하도록 한다.
  feature_mask |= (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION);
  feature_mask |= (1 << NlpFeature::NLPF_MORPHEME);

  // 구해진 마스크를 반환하도록 한다.
  return feature_mask;
}

void Nlp3Kor::UpdateFeatures() {
  auto logger = LOGGER();
  unique_ptr<char[]> path(new char[res_path_.size() + 1]);
  strcpy(path.get(), res_path_.c_str());

  // TODO (gih2yun)
  // Features should has flags with SR & MORP to run nlu properly.

  lock_ = new AtomicRWlock();

  // 멀티쓰레드를 사용하기 위해
  LMInterface *glob_lmi = new LMInterface();
  global_lmi_ptr_ = glob_lmi;

  if (HasFeature(NlpFeature::NLPF_SPACE)) {
    logger->debug("[NLP3] init_space is called");
    glob_lmi->init_space(path.get());
  }
  if (HasFeature(NlpFeature::NLPF_SENTENCE_RECOGNITION)) {
    logger->debug("[NLP3] init_SR_global is called");
    glob_lmi->init_SR_global(path.get());
  }
  if (HasFeature(NlpFeature::NLPF_MORPHEME)) {
    logger->debug("[NLP3] init_morp_global is called");
    glob_lmi->init_morp_global(path.get());
  }

  if (HasFeature(NlpFeature::NLPF_NAMED_ENTITY)) {
    logger->debug("[NLP3] init_NER_global is called");
    glob_lmi->init_NER_global(path.get());

    // init ne tag map
    LoadNeTagMap();
  }

  if (HasFeature(NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION)) {
    logger->debug("[NLP3] init_WSD_global is called");
    glob_lmi->init_WSD_global(path.get());
  }

  if (HasFeature(NlpFeature::NLPF_DEPENDENCY_PARSER)) {
    logger->debug("[NLP3] init_DParse_global is called");
    glob_lmi->init_DParse_global(path.get());
  }

  if (HasFeature(NlpFeature::NLPF_SEMANTIC_ROLE_LABELING)) {
    logger->debug("[NLP3] init_srl_global is called");
    glob_lmi->init_srl_global(path.get());
  }

  if (HasFeature(NlpFeature::NLPF_ZERO_ANAPHORA)) {
    logger->debug("[NLP3] init_ZA_global is called");
    glob_lmi->init_ZA_global(path.get());
  }
}

void Nlp3Kor::Uninit() {
  auto logger = LOGGER();
  LMInterface *glob_lmi =
      reinterpret_cast<LMInterface *>(this->global_lmi_ptr_);

  if (HasFeature(NlpFeature::NLPF_ZERO_ANAPHORA)) {
    logger->debug("[NLP3] close_ZA is called");
    glob_lmi->close_ZA_global();
  }

  if (HasFeature(NlpFeature::NLPF_SEMANTIC_ROLE_LABELING)) {
    logger->debug("[NLP3] close_srl is called");
    glob_lmi->close_srl_global();
  }

  if (HasFeature(NlpFeature::NLPF_DEPENDENCY_PARSER)) {
    logger->debug("[NLP3] close_DParse is called");
    glob_lmi->close_DParse_global();
  }

  if (HasFeature(NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION)) {
    logger->debug("[NLP3] cloase_WSD is called");
    glob_lmi->close_WSD_global();
  }

  if (HasFeature(NlpFeature::NLPF_NAMED_ENTITY)) {
    logger->debug("[NLP3] cloase_NER is called");
    glob_lmi->close_NER_global();
  }

  if (HasFeature(NlpFeature::NLPF_MORPHEME)) {
    logger->debug("[NLP3] close_morp is called");
    glob_lmi->close_morp_global();
  }

  if (HasFeature(NlpFeature::NLPF_SENTENCE_RECOGNITION)) {
    logger->debug("[NLP3] close_SR is called");
    glob_lmi->close_SR_global();
  }

  if (HasFeature(NlpFeature::NLPF_SPACE)) {
    logger->debug("[NLP3] close_space is called");
    glob_lmi->close_space();
  }
}

/**
  @breif 분석된 결과를 Document에 저장시키기 위한 함수
  @param N_Doc &ndoc : 언어분석 결과를 담고 있는 구조체
  @param Document *document : 언어분석 결과를 클라이언트에 보낼 message
  @param NlpAnalysisLevel : 언어분석 레벨
  @return None
*/
void Nlp3Kor::ToMessage(const N_Doc &ndoc, Document *document, int32_t mask) {
  string result;

  auto logger = LOGGER();
  string morp_log;

  for (const N_Sentence &sent : ndoc.sentence) {
    maum::brain::nlp::Sentence *sentence = document->add_sentences();
    sentence->set_seq(sent.id);
    sentence->set_text(sent.text);
    // word
    for (const N_Word &word : sent.word) {
      maum::brain::nlp::Word *w = sentence->add_words();
      w->set_seq(word.id);
      w->set_text(word.text);
      w->set_type(word.type);
      w->set_begin(word.begin);
      w->set_end(word.end);
    }
    // morpheme
    // 형태소 분석
    for (const N_Morp &morp : sent.morp) {
      maum::brain::nlp::Morpheme *morpheme = sentence->add_morps();
      morpheme->set_seq(morp.id);
      morpheme->set_lemma(morp.lemma);
      morpheme->set_type(morp.type);
      morpheme->set_position(morp.position);
      morpheme->set_weight(morp.weight);
      morp_log += morp.lemma;
      morp_log += '/';
      morp_log += morp.type;
      morp_log += ' ';
    }
    // morpheme eval
    // 형태소 분석 결과 평가
    for (const N_Morp_eval &morp_eval : sent.morp_eval) {
      maum::brain::nlp::MorphemeEval *morph_eval = sentence->add_morph_evals();
      morph_eval->set_seq(morp_eval.id);
      morph_eval->set_target(morp_eval.target);
      morph_eval->set_result(morp_eval.result);
      morph_eval->set_word_id(morp_eval.word_id);
      morph_eval->set_m_begin(morp_eval.m_begin);
      morph_eval->set_m_end(morp_eval.m_end);
    }

    if (CanFeature(mask, NlpFeature::NLPF_NAMED_ENTITY)) {
      // named entity
      // 개체명
      for (const N_NE &ne : sent.NE) {
        maum::brain::nlp::NamedEntity *entity = sentence->add_nes();
        entity->set_seq(ne.id);
        entity->set_text(ne.text);
        entity->set_type(ne.type);
        entity->set_begin(ne.begin);
        entity->set_end(ne.end);
        entity->set_weight(ne.weight);
        entity->set_common_noun(ne.common_noun);

        // Add NE type name
        if (ne.type.length() != 0) {
          auto result = ne_tag_map.find(ne.type);
          if (result != ne_tag_map.end()) {
            entity->set_type_name((*result).second.name);
          }
        }
      }
    }

    if (CanFeature(mask, NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION)) {
      // word sense disambiguation
      // 어휘 의미 분석
      for (const N_WSD &wsd : sent.wsd) {
        maum::brain::nlp::WordSenseDisambiguation
            *word_sense_dis = sentence->add_wsds();
        word_sense_dis->set_seq(wsd.id);
        word_sense_dis->set_text(wsd.text);
        word_sense_dis->set_type(wsd.type);
        word_sense_dis->set_scode(wsd.scode);
        word_sense_dis->set_weight(wsd.weight);
        word_sense_dis->set_position(wsd.position);
        word_sense_dis->set_begin(wsd.begin);
        word_sense_dis->set_end(wsd.end);
      }
    }

    if (CanFeature(mask, NlpFeature::NLPF_DEPENDENCY_PARSER)) {
      // dependency parser
      // 의존 구문 분석
      for (const N_Dependency &dependency : sent.dependency) {
        maum::brain::nlp::DependencyParser
            *dp = sentence->add_dependency_parsers();
        dp->set_seq(dependency.id);
        dp->set_text(dependency.text);
        dp->set_head(dependency.head);
        dp->set_label(dependency.label);
        //dependency mod
        if (dependency.mod.size() != 0) {
          for (int i = 0; i < dependency.mod.size(); i++) {
            dp->add_mods(dependency.mod[i]);
          }
        }
        dp->set_weight(dependency.weight);
      }
    }

    if (CanFeature(mask, NlpFeature::NLPF_SEMANTIC_ROLE_LABELING)) {
      // semantic role labeling(srl)
      // 의미역 인식
      for (const N_SRL &srl : sent.SRL) {
        maum::brain::nlp::SRL *sr = sentence->add_srls();
        sr->set_verb(srl.verb);
        sr->set_word_id(srl.word_id);
        sr->set_weight(srl.weight);
      }
    }

    if (CanFeature(mask, NlpFeature::NLPF_ZERO_ANAPHORA)) {
      // zero anaphora
      // 무형대용어 생략복원
      for (const N_ZA &za : sent.ZA) {
        maum::brain::nlp::ZeroAnaphora *zero = sentence->add_zas();
        zero->set_seq(za.id);
        zero->set_verb_wid(za.verb_wid);
        zero->set_ant_sid(za.ant_sid);
        zero->set_ant_wid(za.ant_wid);
        zero->set_type(za.type);
        zero->set_istitle(za.istitle);
        zero->set_weight(za.weight);
      }
    }
  }

  logger->trace("[NLP3] morpheme result: {}", morp_log);
}

/**
 @brief 문장 분리
 @param lmThread : thread pointer
 @param input_text : 입력 문장
 @param split_sentence : 문장 분리 여부
 @param sentVec : 분리 후 결과
 @return None
 */
void SplitSentence(LMInterface &lmThread,
                   string input_text,
                   bool split_sentence,
                   std::vector<string> &sentVec) {
  if (split_sentence == true) {
    lmThread.do_sentence_boundary_detection(input_text, sentVec);
  } else {
    auto it = sentVec.begin();
    sentVec.insert(it, input_text);
  }
}

void Nlp3Kor::AnalyzeOne(const InputText *text, Document *document) {
  LMInterface *glob_lmi =
      reinterpret_cast<LMInterface *>(this->global_lmi_ptr_);

  const string &otext = text->text();
  string input_text = otext;
  auto nlp_level = text->level();
  auto nlp_mask = text->feature_mask();
  int32_t local_mask = LevelToFeature(nlp_level, nlp_mask);

  auto split_sentence = text->split_sentence();
  std::vector<string> sentVec;

  unique_ptr<char[]> path(new char[res_path_.size() + 1]);
  strcpy(path.get(), res_path_.c_str());

  LMInterface lmThread(*glob_lmi);

  // sentence space 처리
  bool use_space = text->use_space();

  if (use_space) {
    string sent;
    Utf8ToEuckr(otext.c_str(), sent);
    sent = glob_lmi->do_space(sent);
    EuckrToUtf8(sent, input_text);
  }


  // FIXME(gih2yun)
  // 아래의 코드를 보게 되면, thread init, proc, thread term 구조로 동작한다.
  // 하나의 Thread에서 복수개의 문서를 처리하는 AnalyzeMultiple 함수를 처리할 때
  // thread init, thread term 함수는 아마도 매우 낭비가 심한 방식으로 보인다.
  // 이를 개선할 수 있는 논리구조가 필요하다.

  // FIXME
  // 아래와 같은 구조를 NLP1, NLP2에 각각 확대할 필요가 있다.

  ReaderLock rlock(*this->lock_);

  //
  // THREAD INIT BLOCK
  //
  {
    lmThread.init_SR_thread(path.get(), *glob_lmi);
    lmThread.init_morp_thread(path.get(), *glob_lmi);

    if (CanFeature(local_mask, NlpFeature::NLPF_NAMED_ENTITY))
      lmThread.init_NER_thread(path.get(), *glob_lmi);
    if (CanFeature(local_mask, NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION))
      lmThread.init_WSD_thread(path.get(), *glob_lmi);
    if (CanFeature(local_mask, NlpFeature::NLPF_DEPENDENCY_PARSER))
      lmThread.init_DParse_thread(path.get(), *glob_lmi);
    if (CanFeature(local_mask, NlpFeature::NLPF_SEMANTIC_ROLE_LABELING))
      lmThread.init_srl_thread(path.get(), *glob_lmi);
    if (CanFeature(local_mask, NlpFeature::NLPF_ZERO_ANAPHORA))
      lmThread.init_ZA_thread(path.get(), *glob_lmi);
  }

  //
  // 처리 블럭
  //
  {
    N_Doc *ndoc(new N_Doc());
    // 문장 쪼개기
    SplitSentence(lmThread, input_text, split_sentence, sentVec);

    // 기본작업
    lmThread.make_NDoc(sentVec, *ndoc);

    // 형태소 분석
    lmThread.do_analyze_morph(*ndoc);

    if (CanFeature(local_mask, NlpFeature::NLPF_NAMED_ENTITY))
      lmThread.do_NER(*ndoc);
    if (CanFeature(local_mask, NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION))
      lmThread.do_WSD(*ndoc);
    if (CanFeature(local_mask, NlpFeature::NLPF_DEPENDENCY_PARSER))
      lmThread.do_DParse(*ndoc);
    if (CanFeature(local_mask, NlpFeature::NLPF_SEMANTIC_ROLE_LABELING))
      lmThread.do_srl(*ndoc);
    if (CanFeature(local_mask, NlpFeature::NLPF_ZERO_ANAPHORA))
      lmThread.do_ZA(*ndoc, false);

    ToMessage(*ndoc, document, local_mask);
    delete ndoc;
  }

  //
  // 정리 블럭
  //
  {
    if (CanFeature(local_mask, NlpFeature::NLPF_ZERO_ANAPHORA))
      lmThread.close_ZA_thread();
    if (CanFeature(local_mask, NlpFeature::NLPF_SEMANTIC_ROLE_LABELING))
      lmThread.close_srl_thread();
    if (CanFeature(local_mask, NlpFeature::NLPF_DEPENDENCY_PARSER))
      lmThread.close_DParse_thread();
    if (CanFeature(local_mask, NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION))
      lmThread.close_WSD_thread();
    if (CanFeature(local_mask, NlpFeature::NLPF_NAMED_ENTITY))
      lmThread.close_NER_thread();
    lmThread.close_morp_thread();
    lmThread.close_SR_thread();
  }
}

void Nlp3Kor::GetPosTaggedString(const Document *document,
                              vector<string> *pos_tagged_str) {
  for (auto &sent: document->sentences()) {
    string result;
    for (auto &morph_eval: sent.morph_evals()) {
      if (result != "") result += ' ';
      result += morph_eval.result();
    }
    pos_tagged_str->push_back(result);
  }
}

void ReplaceAll(std::string &sentence,
                const std::string &from,
                std::string &to) {
  size_t start_pos = 0;
  while ((start_pos = sentence.find(from, start_pos)) != std::string::npos) {

    if (sentence[start_pos + from.length()] != '/') {
      // 개체명 뒤에 공백을 제외한 문자는 +를 붙임
      if (start_pos + from.length() != sentence.length()
          && sentence[start_pos + from.length()] != ' ') {
        to += '+';
      }
      sentence.replace(start_pos, from.length(), to);
      break;
    }
    start_pos += to.length();
  }
}

void Nlp3Kor::GetNerTaggedString(const Document *document,
                              vector<string> *ner_tagged_str) {
  for (auto &sent: document->sentences()) {
    string result;
    string temp = sent.text();
    if (result != "") result += ' ';
    result += temp;
    if (sent.nes().size()) {
      for (auto &ner: sent.nes()) {
        string ner_word;
        ner_word += ner.text();
        ner_word += '/';
        ner_word += ner.type();
        ReplaceAll(temp, ner.text(), ner_word);
        result = temp;
      }
    }
    ner_tagged_str->push_back(result);
  }
}

/**
 * nlp document에서 pos, ner 정보를 합쳐서 문장으로 return
 * @param document Nlp Document
 * @param pos_ner_tagged_str pos + ner 된 결과물
 */
void Nlp3Kor::GetPosNerTaggedString(const Document *document,
                                 vector<string> *pos_ner_tagged_str) {

  for (auto &sent: document->sentences()) {
    string result;
    bool ner_check = false;

    // ner index
    int ner_index = 0;
    // morph_evals index
    int morph_evals_index = 0;

    for (int i = 0; i < sent.morps().size(); i++) {
      if (result != "") result += ' ';

      if (ner_index < sent.nes().size()) {
        if (sent.nes(ner_index).begin() == i) {
          result += sent.nes(ner_index).text();
          result += '/';
          result += sent.nes(ner_index).type();
          // NER 이 여러개 단어로 구성되어 있는경우, i 값을 end로 변경
          if (sent.nes(ner_index).end() != i) {
            i = sent.nes(ner_index).end();
          }
          ner_index++;
          ner_check = true;
        }
      }
      // 개쳬명 추가
      if (!ner_check) {
        result += sent.morps(i).lemma();
        result += '/';
        result += sent.morps(i).type();
      }

      // 단어 뒤에 어절이 나오는지 공백이 나오는지 판단에 따라 + 추가
      if (sent.morph_evals(morph_evals_index).m_end() == i) {
        morph_evals_index++;
      } else {
        result += '+';
      }
      ner_check = false;
    }
    pos_ner_tagged_str->push_back(result);
  }
}

static uint ReadData(char *in, uint size, uint nmemb, void *out) {
  string *body = static_cast<string *>(out);
  body->append(in, size * nmemb);
  return size * nmemb;
}

/**
 * @brief 개체명 사전을 처리후 callback url 처리
 * @param nerDict 처리 할 사전 정보
 * @param type DictType
 * @return curl callback 호출이 성공하면 0, 실패하면 -1 반환
 */
int Nlp3Kor::Callback(const NerDict *nerDict, DictType type) {
  const auto &url = nerDict->callback_url();
  auto logger = LOGGER();

  if (!url.empty()) {
    string full(url);
    full += kMltCallbackApiKey;
    logger->debug("[dict callback url] {}", full);

    CURL *curl;
    std::string resp_body;
    char err_buf[CURL_ERROR_SIZE];
    curl = curl_easy_init();
    if (!curl) {
      logger->debug<const char *>("curl init failed {}");
      return -1;
    }

    curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "Accept: application/json");
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "charsets: utf-8");

    ostringstream oss;
    if (type == DictType::POS_DICT) {
      oss << "{"
          << "\"message\": \""
          << "nlp3 ner post dictionary has been applied!"
          << "\"}";
    } else if (type == DictType::PRE_DICT) {
      oss << "{"
          << "\"message\": \""
          << "nlp3 ner pre dictionary has been applied!"
          << "\"}";
    }
    string body = oss.str();

    curl_easy_setopt(curl, CURLOPT_URL, full.c_str());
    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, err_buf);
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
    curl_easy_setopt(curl, CURLOPT_POST, 1L);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, ReadData);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &resp_body);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, body.c_str());
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, body.length());

    int err = curl_easy_perform(curl);
    curl_slist_free_all(headers);
    curl_easy_cleanup(curl);

    logger->debug("response body [{}]", resp_body);

    if (err) {
      logger->debug("curl perform failed {}",
                    curl_easy_strerror((CURLcode) err));
      return -1;
    }
  }
  logger->debug("callback url is empty");
  return 0;
}

/**
 * @brief NLP3 개체명 태그 변경 사전 처리후 callback url 처리
 * @param nerDict 처리 할 개체명 태그 변경 사전 정보
 * @return curl callback 호출이 성공하면 0, 실패하면 -1 반환
 */
int CallbackCh(const ChangedNerDict *nerDict) {
  const auto &url = nerDict->callback_url();
  auto logger = LOGGER();
  if (!url.empty()) {
    string full(url);
    full += kMltCallbackApiKey;
    logger->debug("[dict callback url] {}", full);

    CURL *curl;
    std::string resp_body;
    char err_buf[CURL_ERROR_SIZE];
    curl = curl_easy_init();
    if (!curl) {
      logger->debug<const char *>("curl init failed {}");
      return -1;
    }

    curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "Accept: application/json");
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "charsets: utf-8");

    ostringstream oss;
    oss << "{"
        << "\"message\": \""
        << "nlp3 category dictionary has been applied!"
        << "\"}";
    string body = oss.str();

    curl_easy_setopt(curl, CURLOPT_URL, full.c_str());
    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, err_buf);
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
    curl_easy_setopt(curl, CURLOPT_POST, 1L);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, ReadData);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &resp_body);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, body.c_str());
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, body.length());

    int err = curl_easy_perform(curl);
    curl_slist_free_all(headers);
    curl_easy_cleanup(curl);

    logger->debug("response body [{}]", resp_body);

    if (err) {
      logger->debug("curl perform failed {}",
                    curl_easy_strerror((CURLcode) err));
      return -1;
    }
  }
  logger->debug("callback url is empty");
  return 0;
}

/**
 @brief 특정 파일을 백업하고, 전달받은 내용을 개체명 사전 양식으로 저장
 @param NerDict 적용할 할 사전 정보
 @param path  복사 할 파일 full 경로
 @return None
 */
void BackUpAndSaveFile(const NerDict *nerDict, const string &path) {
  auto logger = LOGGER();

  // 기존 파일 백업
  std::ifstream src_path(path, std::ios::binary);
  std::ofstream dst_path(path + ".ori", std::ios::binary);
  dst_path << src_path.rdbuf();
  dst_path.close();
  src_path.close();

  FILE *file;
  file = fopen(path.c_str(), "w");

  for (auto &dic : nerDict->text()) {
    for (auto &reg : dic.values()) {
      string line = dic.category() + "=1=" + reg;
      fprintf(file, "%s", line.c_str());
    }
  }
  fclose(file);
}

/**
 @brief 특정 파일을 백업하고, 전달받은 내용을 개체명 태그 변경 사전 양식으로 저장
 @param chNerDict 적용할 할 개체명 태그 변경 파일 내용
 @param path  복사 할 파일 full 경로
 @return None
 */
void BackUpAndChangeFile(const ChangedNerDict *chNerDict, const string &path) {
  auto logger = LOGGER();

  // 기존 파일 백업
  std::ifstream src_path(path, std::ios::binary);
  std::ofstream dst_path(path + ".ori", std::ios::binary);
  dst_path << src_path.rdbuf();
  dst_path.close();
  src_path.close();

  FILE *file;
  file = fopen(path.c_str(), "w");

  auto txt = chNerDict->text();
  for (auto it = txt.begin(); it != txt.end();) {
    const string &word = it->word();
    string line = word + "^^" + it->old_category() + "\t->\t" + word + "^^"
        + it->category();
    it++;
    if (it != txt.end())
      line += '\n';
    fprintf(file, "%s", line.c_str());
  }
  fclose(file);
}

/**
 @brief 후처리 패턴 개체명 사전 적용
 @param NerDict
 @return None
 */
void Nlp3Kor::UpdateNerDict(const NerDict *nerDict) {
  LMInterface *glob_lmi =
      reinterpret_cast<LMInterface *>(this->global_lmi_ptr_);
  unique_ptr<char[]> path(new char[res_path_.size() + 1]);
  strcpy(path.get(), res_path_.c_str());

  auto &c = libmaum::Config::Instance();
  auto logger = LOGGER();
  string post_path(c.Get("brain-ta.resource.3.kor.path"));
  string file_path("/ner/at_post_pattern.txt");
  post_path += file_path;

  BackUpAndSaveFile(nerDict, post_path);

  // ner global reload 과정
  glob_lmi->close_NER_global();
  logger->debug("[NLP3] close_NER_global is called");
  glob_lmi->init_NER_global(path.get());
  logger->debug("[NLP3] init_NER_global is called");

  Callback(nerDict);
}

/**
 @brief 후처리 패턴 개체명 사전 적용
 @param NerDict 적용할 후처리 개체명 사전 정보
 @return None
 */
void Nlp3Kor::UpdatePostNerDict(const NerDict *nerDict) {

  LMInterface *glob_lmi =
      reinterpret_cast<LMInterface *>(this->global_lmi_ptr_);
  unique_ptr<char[]> path(new char[res_path_.size() + 1]);
  strcpy(path.get(), res_path_.c_str());

  auto &c = libmaum::Config::Instance();
  auto logger = LOGGER();
  string post_path(c.Get("brain-ta.resource.3.kor.path"));
  string file_path("/ner/at_post_pattern.txt");
  post_path += file_path;

  BackUpAndSaveFile(nerDict, post_path);

  // ner global reload 과정
  glob_lmi->close_NER_global();
  logger->debug("[NLP3] close_NER_global is called");
  glob_lmi->init_NER_global(path.get());
  logger->debug("[NLP3] init_NER_global is called");

  Callback(nerDict);
}

/**
 @brief 전처리 패턴 개체명 사전 적용
 @param NerDict 적용할 전처리 개체명 사전 정보
 @return None
 */
void Nlp3Kor::UpdatePreNerDict(const NerDict *nerDict) {

  LMInterface *glob_lmi =
      reinterpret_cast<LMInterface *>(this->global_lmi_ptr_);
  unique_ptr<char[]> path(new char[res_path_.size() + 1]);
  strcpy(path.get(), res_path_.c_str());

  auto &c = libmaum::Config::Instance();
  auto logger = LOGGER();
  string pre_path(c.Get("brain-ta.resource.3.kor.path"));
  string file_path("/ner/at_pre_pattern.txt");
  pre_path += file_path;

  BackUpAndSaveFile(nerDict, pre_path);

  // ner global reload 과정
  glob_lmi->close_NER_global();
  logger->debug("[NLP3] close_NER_global is called");
  glob_lmi->init_NER_global(path.get());
  logger->debug("[NLP3] init_NER_global is called");

  Callback(nerDict);
}

/**
 @brief 오류 태그 변경 개체명 사전 적용
 @param ch_ner_dict 적용할 개체명 태그 변경 사전 정보
 @return None
 */
void Nlp3Kor::UpdatePostChangeNerDict(const ChangedNerDict *ch_ner_dict) {
  LMInterface *glob_lmi =
      reinterpret_cast<LMInterface *>(this->global_lmi_ptr_);
  unique_ptr<char[]> path(new char[res_path_.size() + 1]);
  strcpy(path.get(), res_path_.c_str());

  auto &c = libmaum::Config::Instance();
  auto logger = LOGGER();
  string change_path(c.Get("brain-ta.resource.3.kor.path"));
  string file_path("/ner/at_post_change_dic.txt");
  change_path += file_path;

  BackUpAndChangeFile(ch_ner_dict, change_path);

  // ner global reload 과정
  glob_lmi->close_NER_global();
  logger->debug("[NLP3] close_NER_global is called");
  glob_lmi->init_NER_global(path.get());
  logger->debug("[NLP3] init_NER_global is called");

  CallbackCh(ch_ner_dict);
}

/**
 @brief 기존 파일을 백업하고, 전달받은 내용을 Morp 사용자 사전으로 저장
 @param custom_dict_list 적용할 할 사전 정보
 @param path 복사 할 파일 full 경로
 @return None
 */
void Nlp3Kor::ReplaceMorpCustomDict(MorpDictList::MorpCustomDictList *custom_dict_list,
                                    const string &path) {
  auto logger = LOGGER();

  // 기존 파일 백업
  std::ifstream src_path(path, std::ios::binary);
  std::ofstream dst_path(path + ".ori", std::ios::binary);
  dst_path << src_path.rdbuf();
  dst_path.close();
  src_path.close();

  // 새로운 사전 파일 작성
  ofstream w_file(path, std::ofstream::out);

  for (auto &entry : custom_dict_list->entries()) {
    string line = entry.word() + "\t" + entry.pattern();
    if (entry.description().length() > 0) {
      line += "\t[" + entry.description() + "]\n";
    } else {
      line += "\n";
    }
    w_file << line;
  }
  w_file.close();
}

/**
 @brief 기존 파일을 백업하고, 전달받은 내용을 Morp 복합 사전으로 저장
 @param compound_noun_dict_list 적용할 할 사전 정보
 @param path 복사 할 파일 full 경로
 @return None
 */
void Nlp3Kor::ReplaceMorpCompoundDict(MorpDictList::MorpCompoundNounDictList *compound_noun_dict_list,
                                      const string &path) {
  auto logger = LOGGER();

  // 기존 파일 백업
  std::ifstream src_path(path, std::ios::binary);
  std::ofstream dst_path(path + ".ori", std::ios::binary);
  dst_path << src_path.rdbuf();
  dst_path.close();
  src_path.close();

  // 새로운 사전 파일 작성
  ofstream w_file(path, std::ofstream::out);

  for (auto &entry : compound_noun_dict_list->entries()) {
    if (entry.description().length() > 0) {
      string desc_line = ";; " + entry.description() + "\n";
      w_file << desc_line;
    }
    string line = entry.src_pos() + "\t" + entry.dest_pos();
    if (entry.type().length() > 0) {
      line += "\t" + entry.type() + "\n";
    } else {
      line += "\n";
    }
    w_file << line;
  }
  w_file.close();
}

/**
 @brief 기존 파일을 백업하고, 전달받은 내용을 Morp 패턴 사전으로 저장
 @param pattern_dict_list 적용할 할 사전 정보
 @param path 복사 할 파일 full 경로
 @return None
 */
void Nlp3Kor::ReplaceMorpPatternDict(MorpDictList::MorpPatternDictList *pattern_dict_list,
                                     const string &path) {
  auto logger = LOGGER();

  // 기존 파일 백업
  std::ifstream src_path(path, std::ios::binary);
  std::ofstream dst_path(path + ".ori", std::ios::binary);
  dst_path << src_path.rdbuf();
  dst_path.close();
  src_path.close();

  // 새로운 사전 파일 작성
  ofstream w_file(path, std::ofstream::out);

  for (auto &entry : pattern_dict_list->entries()) {
    if (entry.description().length() > 0) {
      string desc_line = ";; " + entry.description() + "\n";
      w_file << desc_line;
    }
    string line =
        entry.index() + "\t" + entry.src_pos() + "\t" + entry.dest_pos() + "\t"
            + entry.pattern() + "\n";
    w_file << line;
  }
  w_file.close();
}

/**
 @brief 형태소 사전 변경 함수
 @param  morp_dict_list 적용할 형태소 사전 관련 정보
 @return 0 or -1 성공여부
 */
int Nlp3Kor::UpdateMorpDictList(const MorpDictList *morp_dict_list) {
  LMInterface *glob_lmi =
      reinterpret_cast<LMInterface *>(this->global_lmi_ptr_);
  unique_ptr<char[]> path(new char[res_path_.size() + 1]);
  strcpy(path.get(), res_path_.c_str());

  auto &c = libmaum::Config::Instance();
  auto logger = LOGGER();
  string mlt_path(c.Get("brain-ta.resource.3.kor.path"));

  // morph global reload 과정
  for (auto &morp_dict : morp_dict_list->morp_dicts()) {
    switch (morp_dict.type()) {
      // MORP_CUSTOM
      case MorpDictList_MorpDictType::MorpDictList_MorpDictType_MORP_CUSTOM : {
        MorpDictList::MorpDictEntry dict_entry = morp_dict.entries();
        MorpDictList::MorpCustomDictList
            *custom_dict_list = dict_entry.mutable_custom_dict();
        string file_full_path(mlt_path
                                  + "/MLT/morph_dic_custom.txt"); // .../MLT/morph_dic_custom.txt

        // Backup and Replace Morp custom dict
        ReplaceMorpCustomDict(custom_dict_list, file_full_path);

        // atomic lock
        WriterLock wlock(*this->lock_);
        if (wlock.state == -1) {
          return -1;
        }
        // Reload custom dict
//        glob_lmi->load_morp_custom_dic(file_full_path, custom_dict_list->append());
        glob_lmi->load_morp_custom_dic(file_full_path, false);
        logger->debug("[NLP3] load_morp_custom_dic is called");
        break;
      }
        // MORP_EOJEOL
      case MorpDictList_MorpDictType::MorpDictList_MorpDictType_MORP_EOJEOL : {
        MorpDictList::MorpDictEntry dict_entry = morp_dict.entries();

        // eojeol dict is consist of RocksDB

        // atomic lock
        WriterLock wlock(*this->lock_);
        if (wlock.state == -1) {
          return -1;
        }
        // Reload eojeol dict
        glob_lmi->m_scan->eojeol_dic_1e->clear();
        glob_lmi->m_scan->eojeol_dic_2e->clear();
        glob_lmi->m_scan->load_eojeol_dic(mlt_path);
        logger->debug("[NLP3] load_eojeol_dic is called");
        break;
      }
        // MORP_COMP_WORD
      case MorpDictList_MorpDictType::MorpDictList_MorpDictType_MORP_COMPOUND_WORD : {
        MorpDictList::MorpDictEntry dict_entry = morp_dict.entries();
        MorpDictList::MorpCompoundNounDictList
            *compound_noun_dict_list = dict_entry.mutable_compound_noun_dict();
        string file_full_path
            (mlt_path + "/MLT/compnoun_dic.txt"); // .../MLT/compnoun_dic.txt

        // Backup and Replace Morp compound noun dict
        ReplaceMorpCompoundDict(compound_noun_dict_list, file_full_path);

        // atomic lock
        WriterLock wlock(*this->lock_);
        if (wlock.state == -1) {
          return -1;
        }

        // Reload compound dict
//        glob_lmi->m_scan->COMP_WORD_DIC_VERB->clear();
        glob_lmi->m_scan->COMP_WORD_DIC_COMPNOUN->clear();
        glob_lmi->m_scan->load_comp_word_compnoun_dic(mlt_path);
        logger->debug("[NLP3] load_comp_word_compnoun_dic is called");
        break;
      }
        // MORP_POST_PATTERN_STR
      case MorpDictList_MorpDictType::MorpDictList_MorpDictType_MORP_POST_PATTERN_STR : {
        MorpDictList::MorpDictEntry dict_entry = morp_dict.entries();
        MorpDictList::MorpPatternDictList
            *pattern_dict_list = dict_entry.mutable_pattern_dict();
        string file_full_path(mlt_path
                                  + "/MLT/post_pattern.regex.string.txt"); // .../MLT/post_pattern.regex.string.txt

        // Backup and Replace Morp pattern string dict
        ReplaceMorpPatternDict(pattern_dict_list, file_full_path);

        // atomic lock
        WriterLock wlock(*this->lock_);
        if (wlock.state == -1) {
          return -1;
        }

        // Reload post pattern string dict
        glob_lmi->m_scan->PostPatternVec_string.clear();
        glob_lmi->m_scan->load_post_regex_pattern(mlt_path);
        logger->debug("[NLP3] load_post_regex_pattern is called");
        break;
      }
        // MORP_POST_PATTERN_MORP
      case MorpDictList_MorpDictType::MorpDictList_MorpDictType_MORP_POST_PATTERN_MORP : {
        MorpDictList::MorpDictEntry dict_entry = morp_dict.entries();
        MorpDictList::MorpPatternDictList
            *pattern_dict_list = dict_entry.mutable_pattern_dict();
        string file_full_path(mlt_path
                                  + "/MLT/post_pattern.regex.morp.txt"); // .../MLT/post_pattern.regex.morp.txt

        // Backup and Replace Morp pattern morp dict
        ReplaceMorpPatternDict(pattern_dict_list, file_full_path);

        // atomic lock
        WriterLock wlock(*this->lock_);
        if (wlock.state == -1) {
          return -1;
        }

        // Reload post pattern morp dict
        glob_lmi->m_scan->PostPatternVec_morp.clear();
        glob_lmi->m_scan->load_post_pattern_by_tagging(mlt_path);
        logger->debug("[NLP3] load_post_pattern_by_tagging is called");
        break;
      }
      default:logger->warn("[NLP3] type is not exist");
        break;
    }
  }
  return 0;
}

/**
 @brief 기존 파일을 백업하고, 전달받은 내용을 개체명 패턴 사전으로 저장
 @param pattern_list 적용할 할 사전 정보
 @param path 복사 할 파일 full 경로
 @param append  덮어쓰기 여부
 @return None
 */
void Nlp3Kor::ReplaceNePatternDict(NamedEntityDictList::NePatternDictList *pattern_list,
                                   const string &path) {
  auto logger = LOGGER();

  // 기존 파일 백업
  std::ifstream src_path(path, std::ios::binary);
  std::ofstream dst_path(path + ".ori", std::ios::binary);
  dst_path << src_path.rdbuf();
  dst_path.close();
  src_path.close();

  // 새로운 사전 파일 작성
  ofstream w_file(path, std::ofstream::out);

  for (auto &entry : pattern_list->entries()) {
    string line = entry.ne_tag() + "=1=" + entry.pattern() + "\n";
    w_file << line;
  }
  w_file.close();
}

/**
 @brief 기존 파일을 백업하고, 전달받은 내용을 후처리 NE 변환 사전으로 저장
 @param change_dict_list 적용할 할 사전 정보
 @param path 복사 할 파일 full 경로
 @param append  덮어쓰기 여부
 @return None
 */
void Nlp3Kor::ReplaceNeChangeDict(NamedEntityDictList::NeChangeDictList *change_dict_list,
                                  const string &path) {
  auto logger = LOGGER();

  // 기존 파일 백업
  std::ifstream src_path(path, std::ios::binary);
  std::ofstream dst_path(path + ".ori", std::ios::binary);
  dst_path << src_path.rdbuf();
  dst_path.close();
  src_path.close();

  // 새로운 사전 파일 작성
  ofstream w_file(path, std::ofstream::out);

  for (auto &entry : change_dict_list->entries()) {
    if (entry.description().length() > 0) {
      string desc_line = ";; " + entry.description() + "\n";
      w_file << desc_line;
    }
    string line =
        entry.word() + "^^" + entry.src_ne_tag() + "\t->\t" + entry.word()
            + "^^" + entry.dest_ne_tag() + "\n";
    w_file << line;
  }
  w_file.close();
}

/**
 @brief 기존 파일을 백업하고, 전달받은 내용을 예외처리 사전으로 저장
 @param filter_list 적용할 할 사전 정보
 @param path 복사 할 파일 full 경로
 @param append  덮어쓰기 여부
 @return None
 */
void Nlp3Kor::ReplaceNeFilterDict(NamedEntityDictList::NeFilterDictList *filter_list,
                                  const string &path) {
  auto logger = LOGGER();

  // 기존 파일 백업
  std::ifstream src_path(path, std::ios::binary);
  std::ofstream dst_path(path + ".ori", std::ios::binary);
  dst_path << src_path.rdbuf();
  dst_path.close();
  src_path.close();

  // 새로운 사전 파일 작성
  ofstream w_file(path, std::ofstream::out);

  for (auto &entry : filter_list->entries()) {
    if (entry.description().length() > 0) {
      string desc_line = ";; " + entry.description() + "\n";
      w_file << desc_line;
    }
    string line = entry.word() + "\n";
    w_file << line;
  }
  w_file.close();
}

/**
 @brief 기존 파일을 백업하고, 전달받은 내용을 예외처리 사전으로 저장
 @param filter_list 적용할 할 사전 정보
 @param path 복사 할 파일 full 경로
 @param append  덮어쓰기 여부
 @return None
 */
void Nlp3Kor::ReplaceNeEtcDict(NamedEntityDictList::NeEtcDictList *etc_dict_list,
                               const string &path) {
  auto logger = LOGGER();

  // 기존 파일 백업
  std::ifstream src_path(path, std::ios::binary);
  std::ofstream dst_path(path + ".ori", std::ios::binary);
  dst_path << src_path.rdbuf();
  dst_path.close();
  src_path.close();

  // 새로운 사전 파일 작성
  ofstream w_file(path, std::ofstream::out);

  for (auto &entry : etc_dict_list->entries()) {
    if (entry.description().length() > 0) {
      string desc_line = ";; " + entry.description() + "\n";
      w_file << desc_line;
    }
    string line = entry.word() + "=" + entry.ne_tag() + "\n";
    w_file << line;
  }
  w_file.close();
}

/**
 @brief 개체명 사전 변경 함수
 @param ne_dict_list 적용할 개체명 사전 정보
 @return 0 or -1 성공여부
 */
int Nlp3Kor::UpdateNamedEntityDictList(const NamedEntityDictList *ne_dict_list) {
  LMInterface *glob_lmi =
      reinterpret_cast<LMInterface *>(this->global_lmi_ptr_);
  unique_ptr<char[]> path(new char[res_path_.size() + 1]);
  strcpy(path.get(), res_path_.c_str());

  auto &c = libmaum::Config::Instance();
  auto logger = LOGGER();
  string ner_path(c.Get("brain-ta.resource.3.kor.path"));
  string file_path("/ner/");
  ner_path += file_path;

  for (auto &ne_dict : ne_dict_list->ne_dicts()) {
    switch (ne_dict.type()) {
      // NE_PRE_PATTERN
      case NamedEntityDictList_NamedEntityDictType::NamedEntityDictList_NamedEntityDictType_NE_PRE_PATTERN : {
        NamedEntityDictList::NamedEntityDictEntry dict_entry = ne_dict.dict();
        NamedEntityDictList::NePatternDictList
            *pre_pattern_list = dict_entry.mutable_pattern_dict();
        string file_full_path(ner_path
                                  + glob_lmi->m_NER->pre_pattern_name); // .../at_pre_pattern.txt

        // Backup and Replace NE pattern dict
        ReplaceNePatternDict(pre_pattern_list, file_full_path);

        // atomic lock
        WriterLock wlock(*this->lock_);
        if (wlock.state == -1) {
          return -1;
        }

        // Reload pre pattern
        glob_lmi->m_NER->prePatternVector->clear();
        glob_lmi->m_NER->loadPrePattern(ner_path);
        logger->debug("[NLP3] NER loadPrePattern is called");
        break;
      }
        // NE_POST_PATTERN
      case NamedEntityDictList_NamedEntityDictType::NamedEntityDictList_NamedEntityDictType_NE_POST_PATTERN : {
        NamedEntityDictList::NamedEntityDictEntry dict_entry = ne_dict.dict();
        NamedEntityDictList::NePatternDictList
            *post_pattern_list = dict_entry.mutable_pattern_dict();
        string file_full_path(ner_path
                                  + glob_lmi->m_NER->post_pattern_name); // .../at_post_pattern.txt

        // Backup and Replace NE pattern dict
        ReplaceNePatternDict(post_pattern_list, file_full_path);

        // atomic lock
        WriterLock wlock(*this->lock_);
        if (wlock.state == -1) {
          return -1;
        }

        // Reload post pattern
        glob_lmi->m_NER->postPatternVector->clear();
        glob_lmi->m_NER->loadPostPattern(ner_path);
        logger->debug("[NLP3] NER loadPostPattern is called");
        break;
      }
        // NE_POST_CHANGE
      case NamedEntityDictList_NamedEntityDictType::NamedEntityDictList_NamedEntityDictType_NE_POST_CHANGE : {
        NamedEntityDictList::NamedEntityDictEntry dict_entry = ne_dict.dict();
        NamedEntityDictList::NeChangeDictList
            *change_dict_list = dict_entry.mutable_change_dict();
        string file_full_path
            (ner_path + "at_post_change_dic.txt"); // .../at_post_change_dic.txt

        // Backup and Replace NE change dict
        ReplaceNeChangeDict(change_dict_list, file_full_path);

        // atomic lock
        WriterLock wlock(*this->lock_);
        if (wlock.state == -1) {
          return -1;
        }

        // ner post change dict
        glob_lmi->m_NER->postChangeDictionary->clear();
        glob_lmi->m_NER->loadPostChangeDictionary(ner_path);
        logger->debug("[NLP3] NER loadPostChangeDictionary is called");
        break;
      }
        // NE_FILTER
      case NamedEntityDictList_NamedEntityDictType::NamedEntityDictList_NamedEntityDictType_NE_FILTER : {
        NamedEntityDictList::NamedEntityDictEntry dict_entry = ne_dict.dict();
        NamedEntityDictList::NeFilterDictList
            *filter_list = dict_entry.mutable_filter_dict();
        string file_full_path
            (ner_path + glob_lmi->m_NER->filter_name); // .../at_filter.txt

        // Backup and Replace filter dict
        ReplaceNeFilterDict(filter_list, file_full_path);

        // atomic lock
        WriterLock wlock(*this->lock_);
        if (wlock.state == -1) {
          return -1;
        }

        // ner filter set
        glob_lmi->m_NER->filterSet->clear();
        glob_lmi->m_NER->loadFilter(ner_path);
        logger->debug("[NLP3] NER loadFilter is called");
        break;
      }
        // NE_ADD_PRE_DIC
      case NamedEntityDictList_NamedEntityDictType::NamedEntityDictList_NamedEntityDictType_NE_ADD_PRE_DIC : {
        NamedEntityDictList::NamedEntityDictEntry dict_entry = ne_dict.dict();
        NamedEntityDictList::NeEtcDictList
            *pre_add_dict_list = dict_entry.mutable_etc_dict();
        string file_full_path(ner_path
                                  + glob_lmi->m_NER->pre_add_dictionary_name); // .../at_pre_add_dic.txt

        // Backup and Replace filter dict
        ReplaceNeEtcDict(pre_add_dict_list, file_full_path);

        // atomic lock
        WriterLock wlock(*this->lock_);
        if (wlock.state == -1) {
          return -1;
        }

        // ner add pre dict
        // 추가 사전의 경우, 기존 사전 MAP 변수에 추가하는 것이므로 초기화 하면 기존 사전 파일도 로딩
        glob_lmi->m_NER->preDictionary->clear();
        glob_lmi->m_NER->loadPreDictionary(ner_path);
        glob_lmi->m_NER->loadPreAddDictionary(ner_path);
        logger->debug(
            "[NLP3] NER loadPreDictionary & loadPreAddDictionary is called");
        break;
      }
        // NE_ADD_POST_DIC
      case NamedEntityDictList_NamedEntityDictType::NamedEntityDictList_NamedEntityDictType_NE_ADD_POST_DIC : {
        NamedEntityDictList::NamedEntityDictEntry dict_entry = ne_dict.dict();
        NamedEntityDictList::NeEtcDictList
            *post_add_dict_list = dict_entry.mutable_etc_dict();
        string file_full_path(ner_path
                                  + glob_lmi->m_NER->post_add_dictionary_name); // .../at_post_add_dic.txt

        // Backup and Replace filter dict
        ReplaceNeEtcDict(post_add_dict_list, file_full_path);

        // atomic lock
        WriterLock wlock(*this->lock_);
        if (wlock.state == -1) {
          return -1;
        }

        // ner add post dict
        // 추가 사전의 경우, 기존 사전 MAP 변수에 추가하는 것이므로 초기화 하면 기존 사전 파일도 로딩
        glob_lmi->m_NER->postDictionary->clear();
        glob_lmi->m_NER->loadPostDictionary(ner_path);
        glob_lmi->m_NER->loadPostAddDictionary(ner_path);
        logger->debug(
            "[NLP3] NER loadPreDictionary & loadPreAddDictionary is called");
        break;
      }
        // NE_ETC_DIC
      case NamedEntityDictList_NamedEntityDictType::NamedEntityDictList_NamedEntityDictType_NE_ETC_DIC : {
        break;
      }
      default:logger->warn("[NLP3] type is not exist");
        break;
    }
  }
  return 0;
}

/**
 @brief 요청한 설정 값이 기존 설정 값과 차이가 있는지 검사하는 함수
 @param neConfig 개체명 설정 정보
 @return true|false 설정 값 변경 여부
 */
bool Nlp3Kor::CheckConfigFile(const NamedEntityConfig *neConfig) {
  LMInterface *glob_lmi =
      reinterpret_cast<LMInterface *>(this->global_lmi_ptr_);

  if (neConfig->use_pre_dictionary() != glob_lmi->m_NER->use_pre_dictionary) {
    return true;
  } else if (neConfig->use_pre_add_dictionary()
      != glob_lmi->m_NER->use_pre_add_dictionary) {
    return true;
  } else if (neConfig->use_pre_pattern() != glob_lmi->m_NER->use_pre_pattern) {
    return true;
  } else if (neConfig->use_post_dictionary()
      != glob_lmi->m_NER->use_post_dictionary) {
    return true;
  } else if (neConfig->use_post_add_dictionary()
      != glob_lmi->m_NER->use_post_add_dictionary) {
    return true;
  } else if (neConfig->use_post_pattern()
      != glob_lmi->m_NER->use_post_pattern) {
    return true;
  } else if (neConfig->use_post_change_dictionary()
      != glob_lmi->m_NER->use_post_change_dictionary) {
    return true;
  } else if (neConfig->use_filter() != glob_lmi->m_NER->use_filter) {
    return true;
  } else if (neConfig->use_svm_model_all()
      != glob_lmi->m_NER->use_svm_model_all) {
    return true;
  }
  return false;
}

/**
 @brief 문장에 특정 부분이 있는지 찾아서 있으면 문장을 특정 값으로 바꾸는 함수
 @param sent 적용 문장
 @param find_str 찾을 문자열
 @param value 변경될 문자열에 넣을 값
 @return sent 변경된 문장
 */
static string FindStringAndReplace(string &sent, const string &find_str,
                                   bool value) {
  size_t pos = 0;
  string replace_str;
  if (value)
    replace_str = find_str + "=1\n";
  else
    replace_str = find_str + "=0\n";

  sent.replace(pos, sent.length(), replace_str);
  return sent;
}

/**
 @brief 개체명 인식 관련 config 적용 함수
 @param ne_config 적용할 개체명 설정 정보
 @return None
 */
void Nlp3Kor::UpdateNamedEntityConfig(const NamedEntityConfig *ne_config) {
  LMInterface *glob_lmi =
      reinterpret_cast<LMInterface *>(this->global_lmi_ptr_);

  auto &c = libmaum::Config::Instance();
  auto logger = LOGGER();

  FILE *file, *file_w;
  char *line = nullptr;
  size_t len = 0;
  ssize_t read;
  size_t pos = 0;

  string ner_path(c.Get("brain-ta.resource.3.kor.path"));
  string file_path("/ner/");
  ner_path += file_path;

  string path(ner_path + "config.ini");
  string dump_file_path(path + ".dump");

  // 변경사항 체크
  bool is_changed = CheckConfigFile(ne_config);
  if (!is_changed) {
    logger->info("not changed config file");
    return;
  }

  // Backup config.ini
  // 기존 파일 백업
  std::ifstream src_path(path, std::ios::binary);
  std::ofstream dst_path(path + ".ori", std::ios::binary);
  dst_path << src_path.rdbuf();
  dst_path.close();
  src_path.close();

  // 리소스 파일 열기
  file = fopen(path.c_str(), "r");
  file_w = fopen(dump_file_path.c_str(), "w");

  if (file == nullptr) {
    logger->error("[NLP3] UpdateNamedEntityConfig is failed. (fail file read)");
  } else {

    while ((read = getline(&line, &len, file)) != -1) {
      // 공백 생략
      if (len == 0) {
        fprintf(file_w, "%s", line);
        continue;
      }

      // 주석 생략
      if (line[0] == ';' || line[0] == '\n') {
        fprintf(file_w, "%s", line);
        continue;
      }

      string conf_str(line);

      if ((pos = conf_str.find("use_pre_dictionary", 0)) != std::string::npos) {
        conf_str = FindStringAndReplace(conf_str,
                                        "use_pre_dictionary",
                                        ne_config->use_pre_dictionary());
      } else if ((pos = conf_str.find("use_pre_add_dictionary", 0))
          != std::string::npos) {
        conf_str = FindStringAndReplace(conf_str,
                                        "use_pre_add_dictionary",
                                        ne_config->use_pre_add_dictionary());
      } else if ((pos = conf_str.find("use_pre_pattern", 0))
          != std::string::npos) {
        conf_str = FindStringAndReplace(conf_str,
                                        "use_pre_pattern",
                                        ne_config->use_pre_pattern());
      } else if ((pos = conf_str.find("use_post_dictionary", 0))
          != std::string::npos) {
        conf_str = FindStringAndReplace(conf_str,
                                        "use_post_dictionary",
                                        ne_config->use_post_dictionary());
      } else if ((pos = conf_str.find("use_post_add_dictionary", 0))
          != std::string::npos) {
        conf_str = FindStringAndReplace(conf_str,
                                        "use_post_add_dictionary",
                                        ne_config->use_post_add_dictionary());
      } else if ((pos = conf_str.find("use_post_pattern", 0))
          != std::string::npos) {
        conf_str = FindStringAndReplace(conf_str,
                                        "use_post_pattern",
                                        ne_config->use_post_pattern());
      } else if ((pos = conf_str.find("use_post_change_dictionary", 0))
          != std::string::npos) {
        conf_str = FindStringAndReplace(conf_str,
                                        "use_post_change_dictionary",
                                        ne_config->use_post_change_dictionary());
      } else if ((pos = conf_str.find("use_filter", 0)) != std::string::npos) {
        conf_str = FindStringAndReplace(conf_str,
                                        "use_filter",
                                        ne_config->use_filter());
      } else if ((pos = conf_str.find("use_svm_model_all", 0))
          != std::string::npos) {
        conf_str = FindStringAndReplace(conf_str,
                                        "use_svm_model_all",
                                        ne_config->use_svm_model_all());
      }

      // Write config dump file
      fprintf(file_w, "%s", conf_str.c_str());
    }
  }
  // File close
  fclose(file_w);
  fclose(file);

  // Replace config.ini
  string dump_path(path + ".dump");
  int ret = rename(dump_path.c_str(), path.c_str());
  if (ret == 0)
    logger->debug("File successfully renamed");
  else
    logger->debug("Error renaming file");

  // atomic lock
  WriterLock wlock(*this->lock_);

  // Read config
  bool pre_model_flag = glob_lmi->m_NER->use_svm_model_all;
  glob_lmi->m_NER->readConfig(ner_path);
  logger->debug("[NLP3] NER readConfig is called");
  bool post_model_flag = glob_lmi->m_NER->use_svm_model_all;

  // 초기에 기계학습 모델이 로딩이 되어 있지 않는 경우, 따로 기계학습 모델을 로딩해야 함
  // Load model
  logger->debug("use_svm_model_all : {} , {}", pre_model_flag, post_model_flag);
  if (pre_model_flag == false && pre_model_flag != post_model_flag) {
    glob_lmi->m_NER->loadModel(ner_path);
    logger->debug("[NLP3] NER loadModel is called");
  }
}

/**
 @brief 개체명 태그 목록 반환
 @param  ret_map 클라이언트에게 넘길 개체명 태그 목록
 @return 개체명 태그 목록
 */
void Nlp3Kor::GetNamedEntityTagMap(NamedEntityTagMap *ret_map) {
  auto logger = LOGGER();

  map<string, NeTag>::iterator iter;

  for (iter = ne_tag_map.begin(); iter != ne_tag_map.end(); iter++) {
    NamedEntityTagMap::NeTag *temp = ret_map->add_tags();
    temp->set_tag(iter->first);
    temp->set_code(iter->second.code);
    temp->set_name(iter->second.name);
  }
}

/**
 @brief 개체명 태그 추가 함수
 @param tag_map 개체명 태그 정보
 @return None
 */
void Nlp3Kor::UpdateNamedEntityTagMap(const NamedEntityTagMap *tag_map) {
  LMInterface *glob_lmi =
      reinterpret_cast<LMInterface *>(this->global_lmi_ptr_);
  auto &c = libmaum::Config::Instance();
  auto logger = LOGGER();
  string path(c.Get("brain-ta.resource.3.kor.path"));
  string file_path("/ner/ne_tag_map.txt.146");
  path += file_path;

  // 기존 파일 백업
  std::ifstream src_path(path, std::ios::binary);
  std::ofstream dst_path(path + ".ori", std::ios::binary);
  dst_path << src_path.rdbuf();
  dst_path.close();
  src_path.close();

  // 맵 사전 파일 작성
  ofstream w_file(path, std::ofstream::out);

  for (auto &entry : tag_map->tags()) {
    logger->debug("tag : {0}, code : {1}, name : {2}",
                  entry.tag(),
                  entry.code(),
                  entry.name());

    // code 형변환
    std::stringstream code_str;
    code_str << entry.code();

    // 파일 쓰기
    string info_line =
        ";% " + entry.tag() + "," + code_str.str() + "," + entry.name() + "\n";
    w_file << info_line;
    string line = entry.tag() + "=" + code_str.str() + "\n";
    w_file << line;
  }
  w_file.close();

  // atomic lock
  WriterLock wlock(*this->lock_);

  // Reload ne tag map
  ne_tag_map.clear();
  LoadNeTagMap();

  // ner tag set
  string ner_path(file_path + "/ner/");
  glob_lmi->m_NER->neTagMap->clear();
  glob_lmi->m_NER->reverseNeTagMap->clear();
  glob_lmi->m_NER->initNeTagMap(ner_path);
  logger->debug("[NLP3] NER initNeTagMap is called");
}

/**
 @brief 개체명 태그 읽는 함수
 @param None
 @return None
 */
void Nlp3Kor::LoadNeTagMap() {

  auto logger = LOGGER();

  FILE *file;
  char *line = nullptr;
  size_t len = 0;
  ssize_t read;

  string file_path(res_path_ + "/ner/ne_tag_map.txt.146");

  logger->info("start loadNeTagMap()");

  // 리소스 파일 열기
  file = fopen(file_path.c_str(), "r");

  if (file == nullptr) {
    logger->error("[NLP3] loadNeTagMap is failed. (fail file read)");
  }

  while ((read = getline(&line, &len, file)) != -1) {
    if (len > 2 && line[0] == ';' && line[1] == '%') {

      // ;% 문자 제거
      std::string tag_str(line);
      tag_str.replace(0, 2, "");

      // 줄바꿈 제거
      tag_str.replace(tag_str.length() - 1, tag_str.length(), "");

      // 앞에 공백 제거
      for (int i = 0; i < tag_str.length(); i++) {
        if (tag_str[i] == ' ' || tag_str[i] == '\t') {
          tag_str.replace(i, i + 1, "");
          i--;
        }
        if (tag_str[i] >= 'A')
          break;
      }

      // string -> char 변환
      std::vector<char> vector(tag_str.length() + 1);
      std::strcpy(&vector[0], tag_str.c_str());
      char *tag_line = &vector[0];

      // , 로 문자열 파싱
      char *token = strtok(&tag_line[0], ",");
      int idx = 0;
      NeTag tag_info;
      string key;
      while (token != nullptr) {
        if (idx == 0) {
          std::string tag(token);
          key = tag;
          tag_info.tag = tag;
        } else if (idx == 1) {
          tag_info.code = atoi(token);
        } else if (idx == 2) {
          std::string tag_name(token);
          tag_info.name = tag_name;
        }
        token = strtok(nullptr, ",");
        idx++;
      }

      // tag map 등록
      pair<string, NeTag> tag_pair(key, tag_info);
      ne_tag_map.insert(tag_pair);

      pair<int, NeTag> tag_pair_by_code(tag_info.code, tag_info);
      ne_tag_map_by_code.insert(tag_pair_by_code);
    }
  }

  fclose(file);
  logger->info("end loadNeTagMap()");

}

void split(const string &s, char c, vector<string> &v) {
  string::size_type i = 0;
  string::size_type j = s.find(c);

  while (j != string::npos) {
    v.push_back(s.substr(i, j - i));
    i = ++j;
    j = s.find(c, j);

    if (j == string::npos)
      v.push_back(s.substr(i, s.length()));
  }
}

/**
 @brief RocksDB 사전 검색
 @param req_info 찾을 사전 정보
 @param res_dict 반환할 사전 정보
 @return None
 */
void Nlp3Kor::FindWord(const FindWordRequest *req_info,
                       FindWordResponse *res_dict) {
  LMInterface *glob_lmi =
      reinterpret_cast<LMInterface *>(this->global_lmi_ptr_);
  auto logger = LOGGER();
  logger->debug("SearchRocksDB is called");

  string key = req_info->word();
  string value;

  RocksDB *base_db = glob_lmi->m_scan->db_pre_dic_base;
  RocksDB *cn_db = glob_lmi->m_scan->db_pre_dic_cn;
  RocksDB *eojeol_1e_db = glob_lmi->m_scan->db_eojeol_dic_1e;
  RocksDB *eojeol_2e_db = glob_lmi->m_scan->db_eojeol_dic_2e;
  RocksDB *verb_db = glob_lmi->m_scan->db_verb_dic_surface;
  // 개체명 사전
  RocksDB *ne_db = glob_lmi->m_NER->db_at_dic;

  res_dict->set_word(key);

  if (base_db->contains(key)) {
    value = base_db->get(key);
    FindWordResponse::BuiltinDict *entry = res_dict->add_entries();
    entry->set_dict_name("db_pre_dic_base");
    entry->set_value(value);
    entry->set_key(key);

    logger->debug("db_pre_dic_base : {0} : {1}", key, value);
  }
  if (cn_db->contains(key)) {
    value = cn_db->get(key);
    FindWordResponse::BuiltinDict *entry = res_dict->add_entries();
    entry->set_dict_name("db_pre_dic_cn");
    entry->set_value(value);
    entry->set_key(key);

    logger->debug("db_pre_dic_cn : {0} : {1}", key, value);
  }
  if (eojeol_1e_db->contains(key)) {
    value = eojeol_1e_db->get(key);
    FindWordResponse::BuiltinDict *entry = res_dict->add_entries();
    entry->set_dict_name("db_eojeol_dic_1e");
    entry->set_value(value);
    entry->set_key(key);

    logger->debug("db_eojeol_dic_1e : {0} : {1}", key, value);
  }
  if (eojeol_2e_db->contains(key)) {
    value = eojeol_2e_db->get(key);
    FindWordResponse::BuiltinDict *entry = res_dict->add_entries();
    entry->set_dict_name("db_eojeol_dic_2e");
    entry->set_value(value);
    entry->set_key(key);

    logger->debug("db_eojeol_dic_2e : {0} : {1}", key, value);
  }
  if (verb_db->contains(key)) {
    value = verb_db->get(key);
    FindWordResponse::BuiltinDict *entry = res_dict->add_entries();
    entry->set_dict_name("db_verb_dic_surface");
    entry->set_value(value);
    entry->set_key(key);

    logger->debug("db_verb_dic_surface : {0} : {1}", key, value);
  }
  if (ne_db->contains(key)) {
    value = ne_db->get(key);
    logger->info("{}", value);
    vector<string> v;
    split(value, '|', v);
    if (v.size() == 0) {
      std::map<int, NeTag>::iterator
          ne_find_it = ne_tag_map_by_code.find(atoi(value.c_str()));
      if (ne_find_it != ne_tag_map_by_code.end()) {
        FindWordResponse::BuiltinDict *entry = res_dict->add_entries();
        entry->set_dict_name("db_at_dic");
        entry->set_value(ne_find_it->second.tag);
        entry->set_key(key);

        logger->debug("db_at_dic : {0} : {1} {2} {3}",
                      key,
                      value,
                      ne_find_it->second.tag,
                      ne_find_it->second.name);
      } else {
        FindWordResponse::BuiltinDict *entry = res_dict->add_entries();
        entry->set_dict_name("db_at_dic");
        entry->set_value(value);
        entry->set_key(key);

        logger->debug("db_at_dic : {0} : {1}", key, value);
      }
    } else {
      for (int i = 0; i < v.size(); ++i) {
        std::map<int, NeTag>::iterator
            ne_find_it = ne_tag_map_by_code.find(atoi(v[i].c_str()));
        if (ne_find_it != ne_tag_map_by_code.end()) {
          FindWordResponse::BuiltinDict *entry = res_dict->add_entries();
          entry->set_dict_name("db_at_dic");
          entry->set_value(ne_find_it->second.tag);
          entry->set_key(key);

          logger->debug("db_at_dic : {0} : {1} {2} {3}",
                        key,
                        v[i],
                        ne_find_it->second.tag,
                        ne_find_it->second.name);
        } else {
          FindWordResponse::BuiltinDict *entry = res_dict->add_entries();
          entry->set_dict_name("db_at_dic");
          entry->set_value(v[i]);
          entry->set_key(key);

          logger->debug("db_at_dic : {0} : {1}", key, v[i]);
        }
      }
    }
  }
}

/**
 @brief 띄어쓰기 사전 로드 함수
 @param None
 @return None
 */
void Nlp3Kor::LoadSpaceDict() {

  auto logger = LOGGER();

  FILE *file;
  char *line = nullptr;
  size_t len = 0;
  ssize_t read;

  string file_path(res_path_ + "/space/space_dic.txt");

  logger->info("start LoadSpaceDict()");

  // 리소스 파일 열기
  file = fopen(file_path.c_str(), "r");

  if (file == nullptr) {
    logger->error("[NLP3] LoadSpaceDict is failed. (fail file read)");
    return;
  }

  while ((read = getline(&line, &len, file)) != -1) {
    if (len > 2) {

      // string
      std::string dic_str(line);

      // 줄바꿈 제거
      dic_str.replace(dic_str.length() - 1, dic_str.length(), "");

      // string -> char 변환
      std::vector<char> vector(dic_str.length() + 1);
      std::strcpy(&vector[0], dic_str.c_str());
      char *dic_line = &vector[0];

      // , 로 문자열 파싱
      char *token = strtok(&dic_line[0], "\t");
      int idx = 0;
      string key;
      string value;
      string is_use;
      string type;
      while (token != nullptr) {
        if (idx == 0) {
          std::string data(token);
          key = data;
        } else if (idx == 1) {
          std::string data(token);
          value = data;
        } else if (idx == 2) {
          std::string data(token);
          is_use = data;
        } else if (idx == 3) {
          std::string data(token);
          type = data;
        }

        token = strtok(nullptr, "\t");
        idx++;
      }

      // space map 등록
      if (strcmp(is_use.c_str(), "Y") == 0) {
        pair<string, string> space_pair(key, value);
        space_map.insert(space_pair);
      }
    }
  }

  fclose(file);
  logger->info("end LoadSpaceDict()");

}

void Nlp3Kor::SpaceAndAnalyzeOne(const InputText *text, Document *document) {
  auto logger = LOGGER();
//  logger->info("start SpaceAndAnalyzeOne()");
  LMInterface *glob_lmi =
      reinterpret_cast<LMInterface *>(this->global_lmi_ptr_);

  const string &otext = text->text();
  string input_text = otext;
  auto nlp_level = text->level();
  auto nlp_mask = text->feature_mask();
  int32_t local_mask = LevelToFeature(nlp_level, nlp_mask);

  auto split_sentence = text->split_sentence();
  std::vector<string> sentVec;

  // substitute space

  if (!space_map.empty()) {
    // Case 1 ( TODO: add n-gram)
//    map<string, string>::iterator sent_find_it = space_map.find(input_text);
//    if (sent_find_it != space_map.end()) {
//      logger->info("case 1 : {0} : {1} ", sent_find_it->first, sent_find_it->second);
//      input_text = sent_find_it->second;
//    }

    // Case 2
    map<string, string>::iterator iter;
    for (iter = space_map.begin(); iter != space_map.end(); iter++) {
      std::size_t found = input_text.find(iter->first);
      if (found != std::string::npos) {
        input_text.erase(found, iter->first.length());
        input_text.insert(found, iter->second);
        logger->info("change : {0} -> {1} ", iter->first, iter->second);
        break;
      }
    }
  }

  // NLP Analyze

  unique_ptr<char[]> path(new char[res_path_.size() + 1]);
  strcpy(path.get(), res_path_.c_str());

  LMInterface lmThread(*glob_lmi);

  // sentence space 처리
  bool use_space = text->use_space();

  if (use_space) {
    string sent;
    Utf8ToEuckr(input_text.c_str(), sent);
    sent = glob_lmi->do_space(sent);
    EuckrToUtf8(sent, input_text);
  }

  ReaderLock rlock(*this->lock_);

  //
  // THREAD INIT BLOCK
  //
  {
    lmThread.init_SR_thread(path.get(), *glob_lmi);
    lmThread.init_morp_thread(path.get(), *glob_lmi);

    if (CanFeature(local_mask, NlpFeature::NLPF_NAMED_ENTITY))
      lmThread.init_NER_thread(path.get(), *glob_lmi);
    if (CanFeature(local_mask, NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION))
      lmThread.init_WSD_thread(path.get(), *glob_lmi);
    if (CanFeature(local_mask, NlpFeature::NLPF_DEPENDENCY_PARSER))
      lmThread.init_DParse_thread(path.get(), *glob_lmi);
    if (CanFeature(local_mask, NlpFeature::NLPF_SEMANTIC_ROLE_LABELING))
      lmThread.init_srl_thread(path.get(), *glob_lmi);
    if (CanFeature(local_mask, NlpFeature::NLPF_ZERO_ANAPHORA))
      lmThread.init_ZA_thread(path.get(), *glob_lmi);
  }

  //
  // 처리 블럭
  //
  {
    N_Doc *ndoc(new N_Doc());

    // 문장 쪼개기
    SplitSentence(lmThread, input_text, split_sentence, sentVec);

    // 기본작업
    lmThread.make_NDoc(sentVec, *ndoc);

    // 형태소 분석
    lmThread.do_analyze_morph(*ndoc);

    if (CanFeature(local_mask, NlpFeature::NLPF_NAMED_ENTITY))
      lmThread.do_NER(*ndoc);
    if (CanFeature(local_mask, NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION))
      lmThread.do_WSD(*ndoc);
    if (CanFeature(local_mask, NlpFeature::NLPF_DEPENDENCY_PARSER))
      lmThread.do_DParse(*ndoc);
    if (CanFeature(local_mask, NlpFeature::NLPF_SEMANTIC_ROLE_LABELING))
      lmThread.do_srl(*ndoc);
    if (CanFeature(local_mask, NlpFeature::NLPF_ZERO_ANAPHORA))
      lmThread.do_ZA(*ndoc, false);

    ToMessage(*ndoc, document, local_mask);
    delete ndoc;
  }

  //
  // 정리 블럭
  //
  {
    if (CanFeature(local_mask, NlpFeature::NLPF_ZERO_ANAPHORA))
      lmThread.close_ZA_thread();
    if (CanFeature(local_mask, NlpFeature::NLPF_SEMANTIC_ROLE_LABELING))
      lmThread.close_srl_thread();
    if (CanFeature(local_mask, NlpFeature::NLPF_DEPENDENCY_PARSER))
      lmThread.close_DParse_thread();
    if (CanFeature(local_mask, NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION))
      lmThread.close_WSD_thread();
    if (CanFeature(local_mask, NlpFeature::NLPF_NAMED_ENTITY))
      lmThread.close_NER_thread();
    lmThread.close_morp_thread();
    lmThread.close_SR_thread();
  }
}

/**
 @brief 치환 사전 목록 반환
 @param  ret_dict 클라이언트에게 넘길 치환 사전 목록
 @return 치환 사전 목록
 */
void Nlp3Kor::GetReplaceDict(ReplacementDictList *ret_dict) {
  auto logger = LOGGER();

  map<string, string>::iterator iter;

  for (iter = space_map.begin(); iter != space_map.end(); iter++) {
    ReplacementDictList::ReplacementDict *temp = ret_dict->add_entries();
    temp->set_src_str(iter->first);
    temp->set_dest_str(iter->second);
  }
}

/**
 @brief 치환 사전 추가 함수
 @param replace_dict_list 치환 사전 정보
 @return None
 */
bool Nlp3Kor::UpdateReplaceDict(const ReplacementDictList *replace_dict_list) {
  auto &c = libmaum::Config::Instance();
  auto logger = LOGGER();

  string path(c.Get("brain-ta.resource.3.kor.path"));
  string file_path("/space/space_dic.txt");
  path += file_path;

  if (replace_dict_list->auth_key() != kMltCallbackApiKey) {
    logger->error("auth error!");
    return false;
  }

  struct tm *t;
  time_t timer;

  timer = time(NULL);    // 현재 시각을 초 단위로 얻기
  t = localtime(&timer); // 초 단위의 시간을 분리하여 구조체에 넣기

  char time_char[20];

  sprintf(time_char, "%04d%02d%02d%02d%02d%02d",
          t->tm_year + 1900, t->tm_mon + 1, t->tm_mday,
          t->tm_hour, t->tm_min, t->tm_sec
  );

  string backup_time(time_char);

  // 기존 파일 백업
  std::ifstream src_path(path, std::ios::binary);
  std::ofstream dst_path(path + "." + backup_time, std::ios::binary);
  dst_path << src_path.rdbuf();
  dst_path.close();
  src_path.close();

  // 맵 사전 파일 작성
  ofstream w_file(path, std::ofstream::out);

  for (auto &entry : replace_dict_list->entries()) {
    logger->debug("tag : {0}, code : {1}, name : {2}",
                  entry.src_str(),
                  entry.dest_str(),
                  entry.used());

    // 파일 쓰기
    string line;
    if (entry.type() != "") {
      line = entry.src_str() + "\t" + entry.dest_str() + "\t" + entry.used()
          + "\t" + entry.type() + "\n";
    } else {
      line = entry.src_str() + "\t" + entry.dest_str() + "\t" + entry.used()
          + "\n";
    }

    w_file << line;
  }
  w_file.close();

  // Reload space_map
  space_map.clear();
  LoadSpaceDict();

  return true;
}

