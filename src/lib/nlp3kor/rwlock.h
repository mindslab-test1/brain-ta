//
// Created by Austin.lee on 2019-01-03.
// refs. https://gist.github.com/yizhang82/500da684837161055978011c5850d296#file-rw_spin_lock-h-L147
//

#ifndef PROJECT_RWLOCK_H
#define PROJECT_RWLOCK_H

#include <string>
#include <atomic>
#include<iostream>
#include <unistd.h>

using namespace std;

class AtomicRWlock {
 public:
  AtomicRWlock() {
    readers_ = 0;
  }

 public:
  int AcquireReader();
  int ReleaseReader();

  int AcquireWriter();
  int ReleaseWriter();

 private:
  const uint32_t kHasWriter = 0xffffffff;
  const int kRetryThreshold = 10000;
  const int kRetryCount = 3;
  const int kSleepTime = 100;
  std::atomic <uint32_t> readers_;

};

class ReaderLock {
 public:
  ReaderLock(AtomicRWlock &lock) : lock_(lock) {
    state = lock_.AcquireReader();
  }

  ~ReaderLock() {
    lock_.ReleaseReader();
  }

  int state = 0;

 private:
  AtomicRWlock &lock_;
};

class WriterLock {
 public:
  WriterLock(AtomicRWlock &lock) : lock_(lock) {
    state = lock_.AcquireWriter();
  }

  ~WriterLock() {
    lock_.ReleaseWriter();
  }

  int state = 0;

 private:
  AtomicRWlock &lock_;
};

#endif //PROJECT_RWLOCK_H
