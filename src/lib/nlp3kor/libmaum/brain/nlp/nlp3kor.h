#ifndef NLP3KOR_H
#define NLP3KOR_H

#include "libmaum/brain/nlp/nlp.h"
#include "maum/brain/nlp/nlp3_custom.grpc.pb.h"
#include <map>
#include <time.h>

using maum::brain::nlp::NlpAnalysisLevel;
using maum::brain::nlp::NerDict;
using maum::brain::nlp::ChangedNerDict;
using maum::brain::nlp::MorpDictList;
using maum::brain::nlp::MorpDictList_MorpDictType;
using maum::brain::nlp::NamedEntityDictList;
using maum::brain::nlp::NamedEntityDictList_NamedEntityDictType;
using maum::brain::nlp::NamedEntityConfig;
using maum::brain::nlp::NamedEntityTagMap;
using maum::brain::nlp::FindWordRequest;
using maum::brain::nlp::FindWordResponse;
using maum::brain::nlp::ReplacementDictList;
using google::protobuf::Empty;

class LMInterface;
struct N_Doc;
class AtomicRWlock;

class Nlp3Kor : public Nlp {
 public:
  Nlp3Kor(const string &res_path, int nlp_feature_mask)
      : Nlp(res_path, nlp_feature_mask) {
  }

  virtual ~Nlp3Kor() {
  }
  static void GetPosTaggedString(const Document *document,
                                 vector<string> *pos_tagged_str);
  static void GetNerTaggedString(const Document *document,
                                 vector<string> *ner_tagged_str);
  static void GetPosNerTaggedString(const Document *document,
                                    vector<string> *pos_ner_tagged_str);

  void ToMessage(const N_Doc &ndoc, Document *document, int32_t mask);
  void UpdateFeatures() override;
  void AnalyzeOne(const InputText *text, Document *document) override;
  void Uninit() override;

  void SetClassifierModel(const string &classifier_model_path) {};
  void Classify(const Document &document, ClassifiedSummary *sum) {};
  void GetPosTaggedStr(const Document *document,
                       vector<string> *pos_tagged_str) override {
    GetPosTaggedString(document, pos_tagged_str);
  }
  void GetNerTaggedStr(const Document *document,
                       vector<string> *ner_tagged_str) override {
    GetNerTaggedString(document, ner_tagged_str);
  }
  void GetPosNerTaggedStr(const Document *document,
                          vector<string> *pos_ner_tagged_str) override {
    GetPosNerTaggedString(document, pos_ner_tagged_str);
  }
  void UpdateNerDict(const NerDict *nerDict);
  void UpdatePreNerDict(const NerDict *nerDict);
  void UpdatePostNerDict(const NerDict *nerDict);
  void UpdatePostChangeNerDict(const ChangedNerDict *ch_ner_dict);

  int UpdateMorpDictList(const MorpDictList *morp_dict_list);
  int UpdateNamedEntityDictList(const NamedEntityDictList *ne_dict_list);
  void UpdateNamedEntityConfig(const NamedEntityConfig *ne_config);

  void GetNamedEntityTagMap(NamedEntityTagMap *ret_map);
  void UpdateNamedEntityTagMap(const NamedEntityTagMap *tag_map);

  // NE Replace dict
  void ReplaceNePatternDict(NamedEntityDictList::NePatternDictList *pattern_list,
                            const string &path);
  void ReplaceNeChangeDict(NamedEntityDictList::NeChangeDictList *change_dict_list,
                           const string &path);
  void ReplaceNeFilterDict(NamedEntityDictList::NeFilterDictList *filter_list,
                           const string &path);
  void ReplaceNeEtcDict(NamedEntityDictList::NeEtcDictList *etc_dict_list,
                        const string &path);

  // Morp Replace dict
  void ReplaceMorpCustomDict(MorpDictList::MorpCustomDictList *custom_dict_list,
                             const string &path);
  void ReplaceMorpCompoundDict(MorpDictList::MorpCompoundNounDictList *compound_noun_dict_list,
                               const string &path);
  void ReplaceMorpPatternDict(MorpDictList::MorpPatternDictList *pattern_dict_list,
                              const string &path);

  void FindWord(const FindWordRequest *req_info, FindWordResponse *res_dict);

  void LoadNeTagMap();

  // 띄어쓰기 관련 함수
  void LoadSpaceDict();
  void SpaceAndAnalyzeOne(const InputText *text, Document *document);
  void GetReplaceDict(ReplacementDictList *ret_dict);
  bool UpdateReplaceDict(const ReplacementDictList *replace_dict_list);

  bool CheckConfigFile(const NamedEntityConfig *neConfig);

 private:
  enum DictType {
    POS_DICT = 0,
    PRE_DICT = 1
  };

  struct NeTag {
    string tag;
    string name; // 개체명 태그에 대한 이름
    int code; // 코드 번호
  };

  int32_t LevelToFeature(NlpAnalysisLevel level,
                         int32_t feature_mask);
  // 개체명 태그 정보
  std::map<string, NeTag> ne_tag_map;
  std::map<int, NeTag> ne_tag_map_by_code;

  // 띄어쓰기 사전 정보
  std::map<string, string> space_map;

  // 멀티쓰레드를 사용하기 위해 사용하는 변수
  // 헤더에서 `LMInterface`를 사용하기 위해서 다른 헤더를 참조하면
  // 컴파일 과정의 오류가 발생하므로 이를 피하기 위해서
  // void *로 사용한다.
  void *global_lmi_ptr_ = nullptr;
  int Callback(const NerDict *nerDict, DictType type = DictType::POS_DICT);

  AtomicRWlock *lock_;
  const char *kMltCallbackApiKey = "7c3a778e29ac522de62c4e22aa90c9";
};

/**
 * 생성된 feature 가 특정한 기능을 지원하는지 여부를 반환한다.
 *
 * @param mask 계산된 feature mask
 * @param feature 요청할 feature
 * @return 해당 특징을 가지고 있는지 여부
 */
inline bool CanFeature(int32_t mask, NlpFeature feature) {
  return (mask & (1 << int(feature))) > 0;
}

#endif //NLP3KOR_H
