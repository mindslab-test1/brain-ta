//
// Created by Austin.lee on 2019-01-03.
//

#include "rwlock.h"

int AtomicRWlock::AcquireReader() {
  int retry = 0;
  int retry_count = 0;
  while (true) {
    uint32_t prev_readers = readers_;
    if (prev_readers != kHasWriter) {
      uint32_t new_readers = prev_readers + 1;
      if (readers_.compare_exchange_weak(prev_readers, new_readers)) {
        // we've won the race
        return 0;
      }
    }

    retry++;
    if (retry > kRetryThreshold) {
      // init retry
      retry = 0;
      retry_count++;

      if (retry_count > kRetryCount) {
        // kRetryCount 이상인 경우에 대한 처리
        return -1;
      }

      // kRetryThreshold 이상인 경우에 대한 처리
      // 슬립이후 재시도
      usleep(kSleepTime);
    }
  }
}

int AtomicRWlock::ReleaseReader() {
  int retry = 0;
  int retry_count = 0;
  while (true) {
    uint32_t prev_readers = readers_;
    if (prev_readers != kHasWriter && prev_readers > 0) {
      uint32_t new_readers = prev_readers - 1;
      if (readers_.compare_exchange_weak(prev_readers, new_readers)) {
        // we've won the race
        return 0;
      }
    }

    if (retry > kRetryThreshold) {
      // init retry
      retry = 0;
      retry_count++;

      if (retry_count > kRetryCount) {
        // kRetryCount 이상인 경우에 대한 처리
        return -1;
      }

      // kRetryThreshold 이상인 경우에 대한 처리
      // 슬립이후 재시도
      usleep(kSleepTime);
    }
  }
}

int AtomicRWlock::AcquireWriter() {
  int retry = 0;
  int retry_count = 0;
  while (true) {
    uint32_t prev_readers = readers_;
    if (prev_readers == 0) {
      if (readers_.compare_exchange_weak(prev_readers, kHasWriter)) {
        // we've won the race
        return 0;
      }
    }

    retry++;
    if (retry > kRetryThreshold) {
      // init retry
      retry = 0;
      retry_count++;

      if (retry_count > kRetryCount) {
        // kRetryCount 이상인 경우에 대한 처리
        return -1;
      }

      // kRetryThreshold 이상인 경우에 대한 처리
      // 슬립이후 재시도
      usleep(kSleepTime);
    }
  }
}

int AtomicRWlock::ReleaseWriter() {
  int retry = 0;
  int retry_count = 0;
  while (true) {
    uint32_t prev_readers = readers_;
    if (prev_readers == kHasWriter) {
      if (readers_.compare_exchange_weak(prev_readers, 0)) {
        // we've won the race
        return 0;
      }
    }

    retry++;
    if (retry > kRetryThreshold) {
      // init retry
      retry = 0;
      retry_count++;

      if (retry_count > kRetryCount) {
        // kRetryCount 이상인 경우에 대한 처리
        return -1;
      }

      // kRetryThreshold 이상인 경우에 대한 처리
      // 슬립이후 재시도
      usleep(kSleepTime);
    }
  }
}