#if !defined(ENG_UTIL_INCLUDED_)
#define ENG_UTIL_INCLUDED_

#include <string.h> // strchr

#include <vector>
using std::vector;

#define is_digit(c)     ('0'<=(char)c && ((char)c)<='9')
#define is_capital(c)    ('A'<=(char)c && ((char)c)<='Z')
#define is_alpha(c)      (('a'<=(char)c && ((char)c)<='z')||('A'<=(char)c && ((char)c)<='Z'))
#define is_alnum(c)      (is_alpha(c) || is_digit(c))

/** Tokenize string to words.
 Tokenization of string and assignment to word vector.
 Delimiters are set of char.
 @param str string
 @param tokens token vector
 @param delimiters delimiters to divide string
 @return none
 */
inline void tokenize(const string &str,
                     vector<string> &tokens,
                     const string &delimiters) {
  tokens.clear();
  string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  string::size_type pos = str.find_first_of(delimiters, lastPos);
  while (string::npos != pos || string::npos != lastPos) {
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    lastPos = str.find_first_not_of(delimiters, pos);
    pos = str.find_first_of(delimiters, lastPos);
  }
}

///< String 변환: 처음 한번만 바꿈
inline string my_replace(const string org, const string &from, const string &to) {
  size_t idx = org.find(from);

  if (idx != string::npos) {
    string ret = org;
    ret.replace(idx, from.size(), to);
    return ret;
  } else {
    return org;
  }
}

///< String 변환 (모두 변환) - by leeck
inline string my_replace_all(const string &org, const string &from, const string &to) {
  string result = org;
  string::size_type pos = 0;
  string::size_type offset = 0;

  while ((pos = result.find(from, offset)) != string::npos) {
    result.replace(result.begin() + pos,
                   result.begin() + pos + from.size(),
                   to);
    offset = pos + to.size();
  }

  return result;
}

///< get_lower
inline string get_lower(const string &word) {
  char result[1000];
  result[0] = 0;
  for (string::size_type i = 0; i < word.length() && i < 999; i++) {
    int c = word.at(i);
    if (isupper(c)) result[i] = tolower(c);
    else result[i] = c;
  }
  if (word.length() < 999) {
    result[word.length()] = 0;
  } else {
    result[999] = 0;
  }
  return result;
}

///< get_prefix
inline string get_prefix(const string &word, string::size_type n) {
  return word.substr(0, n);
}

///< get_suffix
inline string get_suffix(const string &word, string::size_type n) {
  return word.substr(word.length() - n, n);
}

///< is_all_upper
inline int is_all_upper(const string &word) {
  for (string::size_type i = 0; i < word.size(); i++) {
    int c = word.at(i);
    if (isupper(c)) continue;
    else return 0;
  }
  return 1;
}

///< is_all_digit
inline int is_all_digit(const string &word) {
  for (string::size_type i = 0; i < word.size(); i++) {
    int c = word.at(i);
    if (isdigit(c)) continue;
    else return 0;
  }
  return 1;
}

///< str2가 str1의 뒤에 있는지?
inline int strend(const string &str1, const string &str2) {
  const char *pt1;

  if (str2.size() >= str1.size()) {
    return 0;
  } else {
    pt1 = str1.c_str();
    pt1 = strchr(str1.c_str(), 0);
    pt1 = pt1 - str2.size();
    return (!strcmp(pt1, str2.c_str()));
  }
}

///< nomalize digit and alphabet: 0~9 --> 0 and lower
inline string nomalize(const string &word) {
  char result[1000];
  result[0] = 0;
  for (string::size_type i = 0; i < word.length() && i < 999; i++) {
    int c = word.at(i);
    if (c >= '0' && c <= '9')
      result[i] = '0';
    else if (isupper(c))
      result[i] = tolower(c);
    else
      result[i] = c;
  }
  if (word.length() < 999) {
    result[word.length()] = 0;
  } else {
    result[999] = 0;
  }
  return result;
}

inline string ReplacePunctuation(const string &strLine) {
  string input;
  int len_str = (int) strLine.size();
  int b_punc = 0;
  for (int li = 0; li < len_str; li++) {
    if (strLine[li] == '.' || strLine[li] == '?' || strLine[li] == '!') {
      b_punc = 1;
      int len_input = (int) input.size() - 1;
      for (int in_i = len_input; in_i >= 0; in_i--) {
        if (input[in_i] != ' ') {
          if (len_input != in_i)
            input.erase(in_i + 1, len_input - in_i);
          break;
        }
      }
    }
    input += strLine[li];
    if (b_punc) {
      input += ' ';
      b_punc = 0;
    }
  }
  return input;
}


#endif // ENG_UTIL_INCLUDED_
