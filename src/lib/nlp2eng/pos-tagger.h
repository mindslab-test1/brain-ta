#ifndef POS_TAGGER_INCLUDED_
#define POS_TAGGER_INCLUDED_

#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <set>
#include <map>
#include "ssvm.h"

using namespace std;

#ifndef ENG_WORD_T_DEFINED
#define ENG_WORD_T_DEFINED
// word type
typedef struct word_struct {
  // id: 1부터 시작
  int id;
  // word string
  string str;
  // lemma
  string lemma;
  // POS tag
  string pos_tag;
  // gold POS tag: for conll2008
  string gpostag;
  // split form, lemma, postag : for conll2008
  string split_form, split_lemma, split_postag;
  // NE tag
  string ne_tag;
  // Chunk tag
  string chunk_tag;
  // head (dependency parsing): root = 0, (1부터 시작)
  int head;
  // dependency label
  string dep_label;
  // SRL infomation: predicate sense, argument, arg_info vector(id, string)
  string pred;
  vector<string> arg; // for conll format
  vector<pair<int, string> > arg_info; // 현재 pred에 할당된 arg의 id, string
} word_t;
#endif

class POS_tagger {
 public:
  POS_tagger();
  virtual ~POS_tagger();

  void init(const string &dir);

  void close();

  // POS tagging
  vector<word_t>
  do_pos_tagging(const string &input, double &prob, int use_tokenize = 1);

  // N-best POS tagging
  vector<vector<word_t> > do_pos_tagging_nbest(const string &input,
                                               vector<double> &prob,
                                               int n = 5,
                                               int use_tokenize = 1);

  // print POS tagging result
  string print_pos_tagging(vector<word_t> input);

  // print token (the result of tokenizer)
  string print_token(vector<word_t> input);

  // tokenize input sentence
  vector<string> tokenize(const string &input);

  // feature generation for POS tagging
  vector<vector<string> > make_feature(const vector<string> &tokens);

  // stemming
  string do_stemming(const string &word, const string &pos_tag);

  // load_train_data: word pos_tag 포맷
  vector<vector<word_t> > load_train_data(const string &file);

 private:
  SSVM ssvm;
  // cluster
  map<string, string> cluster;
  // word embedding
  map<string, vector<float> > embedding;

  // exception dic: (word, POS tag) --> lemma
  map<pair<string, char>, string> exception_dic;
  // lemma set: (word, POS tag)
  set<pair<string, char>> lemma_set;
  // abbrev set: word
  set<string> abbrev_set;
  // detachment rule
  vector<pair<string, string> > detachment_rule;
};

#endif // POS_TAGGER_INCLUDED_
