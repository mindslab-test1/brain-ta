#include <stdio.h>
#include <deque>
#include <algorithm>
#include "sa-nn.h"

#ifdef _OPENMP
#include <omp.h>
#endif

// select GRU_search (attention) or LSTM_encoder
const int USE_GRU_SEARCH = 0;

/**
  SA_NN 인스턴스 생성
*/
SA_NN::SA_NN() {
  verbose = 0;
  model_loaded = 0;
  binary_model = 0;

  if (USE_GRU_SEARCH) {
    // nn_search
    //nn_search.activation = RELU;
    nn_search.activation = TANH;
    nn_search.x_drop_out_rate = 0.8;
    nn_search.h_drop_out_rate = 0.5;
  } else {
    //nn_encoder.activation = RELU;
    nn_encoder.activation = TANH;
    nn_encoder.x_drop_out_rate = 0.8;
    nn_encoder.h_drop_out_rate = 0.5;
  }
}

/// Destruction
SA_NN::~SA_NN() {
  outcome_vec.clear();
  word_map.clear();
}

/**
  SA_NN 초기화
  @param dir  리소스 디렉토리
  @return 초기화 성공 (0) 여부
*/
int SA_NN::init(const string &dir, const string &res) {
  // load model
  string file;
  if (USE_GRU_SEARCH) {
    // nn_search
    file = dir + "/" + "sa_model.gru_search.txt";
  } else {
    // nn_encoder
    file = dir + "/sa_model.lstm_encoder.txt";
  }
  load_model(file);

  // outcome vocab.
  file = dir + "/vocab_sa.txt";
  load_outcome_vec(file);

  // word vocab.
  file = res + "/vocab_sa_word.txt";
  load_word_map(file);
  return 0;
}

#if 0
int SA_NN::init(const string &dir, const string &proj) {
  // load model
  string file;
  if (USE_GRU_SEARCH) {
    // nn_search
    file = dir + "/" + proj + "_sa_model.gru_search.txt";
  } else {
    // nn_encoder
    file = dir + "/" + proj + "_sa_model.lstm_encoder.txt";
  }
  load_model(file);

  // outcome vocab.
  file = dir + "/" + proj + "_vocab_sa.txt";
  load_outcome_vec(file);

  // word vocab.
  file = dir + "/" + proj + "_vocab_sa_word.txt";
  load_word_map(file);
  return 0;
}
#endif

/**
  Neural Network model loading
  @param file Neural Network model file
*/
void SA_NN::load_model(const string &file) {
  try {
    if (USE_GRU_SEARCH) {
      nn_search.load_model(file);
    } else {
      nn_encoder.load_model(file);
    }
  } catch (exception e) {
    cerr << "Error: " << e.what() << endl;
    exit(1);
  }
  model_loaded = 1;
}

/**
  Outcome dictionary loading
  @param file Outcome dictionary file
*/
void SA_NN::load_outcome_vec(const string &file) {
  FILE *in;
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    exit(1);
  }

  cerr << "loading " << file << " ... ";
  char temp[1000];
  while (!feof(in)) {
    fgets(temp, 1000, in);
    if (temp[0] == ';') continue;
    vector<string> token;
    tokenize(temp, token, " \t\n\r");
    if (token.size() == 1) {
      outcome_vec.push_back(token[0]);
    } else if (token.size() > 1) {
      cerr << "Error(load_outcome_vec): " << temp << endl;
    }
    temp[0] = '\0';
  }
  fclose(in);
  cerr << "done: " << outcome_vec.size() << endl;
}

/**
  Word dictionary loading
  @param file word dictionary file
*/
void SA_NN::load_word_map(const string &file) {
  FILE *in;
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    exit(1);
  }

  cerr << "loading " << file << " ... ";
  char temp[1000];
  int idx = 0;
  while (!feof(in)) {
    fgets(temp, 1000, in);
    vector<string> token;
    tokenize(temp, token, " \t\n\r");
    if (token.size() == 1) {
      word_map[token[0]] = idx++;
    } else if (token.size() > 1) {
      cerr << "Error(load_word_map):" << idx << " [" << temp << "]" << endl;
    }
    temp[0] = '\0';
  }
  fclose(in);
  cerr << "done: " << idx << endl;
}

/// close parser
void SA_NN::close() {
}

/**
  SA 수행
  @param input  POS 태깅된 문장
  @param prob   SA score가 저장됨
  @return string  SA 결과 (NEG or POS)
*/
string SA_NN::do_sa(vector<word_t> &input, float &prob) {
  vector<int> word_input;
  for (int i = 0; i < input.size(); i++) {
    //string w = input[i].lemma + "/" + input[i].type;
    string w = input[i].str;
    w = normalize(w);
    if (word_map.find(w) != word_map.end()) {
      //cerr << "word_map: " << w << " " << word_map[w] << endl;
      word_input.push_back(word_map[w]);
    } else {
      cerr << "word_map(UNK): " << w << endl;
      word_input.push_back(word_map["UNKNOWN"]);
    }
  }

  int result;
  if (USE_GRU_SEARCH) {
    // nn_search
    vector<float> alignment;
    result = nn_search.classify(word_input, prob, alignment);
    cout << "<attention>" << endl;
    for (int i = 0; i < alignment.size(); i++) {
      cout << input[i].str << "\t: " << alignment[i] << endl;
    }
  } else {
    // nn_encoder
    result = nn_encoder.classify(word_input, prob);
  }

  return outcome_vec[result];
}

string SA_NN::do_sa(E_Sentence &e_sent, float &prob) {
  vector<int> word_input;
  for (int i = 0; i < (int) e_sent.word.size(); i++) {
    //string w = input[i].lemma + "/" + input[i].type;
    //string w = e_sent.morp[i].lemma+"/"+e_sent.morp[i].type;
    string w = e_sent.word[i].str;
    w = normalize(w);
    if (word_map.find(w) != word_map.end()) {
      //cerr << "word_map: " << w << " " << word_map[w] << endl;
      word_input.push_back(word_map[w]);
    } else {
      //cerr << "word_map(UNK): " << w << endl;
      word_input.push_back(word_map["UNKNOWN"]);
    }
  }

  int result;
  if (USE_GRU_SEARCH) {
    // nn_search
    vector<float> alignment;
    result = nn_search.classify(word_input, prob, alignment);
    cout << "<attention>" << endl;
    for (int i = 0; i < alignment.size(); i++) {
      cout << e_sent.morp[i].lemma << "\t: " << alignment[i] << endl;
    }
  } else {
    // nn_encoder
    result = nn_encoder.classify(word_input, prob);
  }

  return outcome_vec[result];
}

vector<string> SA_NN::do_multy_SA(E_Sentence &e_sent, vector<float> &vec_prob) {
  // word_t 방식으로 E_Sentence 전송
  vector<int> word_input;
  for (int i = 0; i < e_sent.word.size(); i++) {
    //string w = e_sent.morp[i].lemma + "/" + e_sent.morp[i].type;
    string w = e_sent.word[i].str;
    w = normalize(w);
    if (word_map.find(w) != word_map.end()) {
      word_input.push_back(word_map[w]);
    } else {
      //cerr << "word_map(UNK): " << w << endl;
      word_input.push_back(word_map["UNKNOWN"]);
    }
  }

  vector<int> vecResult = nn_encoder.multy_classify(word_input, vec_prob);
  vector<string> vecSA;
  for (int i = 0; i < (int) vecResult.size(); i++) {
    if (vecResult[i] == -1)
      break;
    else
      vecSA.push_back(outcome_vec[vecResult[i]]);
  }
  if ((int) vecSA.size() == 0)
    vecSA.push_back("NA");

  e_sent.SA.type = vecSA[0];
  e_sent.SA.prob = vec_prob[0];

  return vecSA;
}
