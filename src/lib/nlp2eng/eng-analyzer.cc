#include <string.h>
#include <stdio.h>
#include "libmaum/brain/nlp/eng-analyzer.h"
#include "util.h"
#include "pos-tagger.h"
#include "ner.h"
#include "sa-nn.h"

#ifdef _OPENMP
#include <omp.h>
#endif

Eng_analyzer::Eng_analyzer() {
  pos_tagger = NULL;
  ner = NULL;
  sa = NULL;
#ifdef _OPENMP
  cerr << "[OpenMP] Number of threads: " << omp_get_max_threads() << endl;
#endif
}

Eng_analyzer::~Eng_analyzer() {
  if (pos_tagger != NULL) delete pos_tagger;
  if (ner != NULL) delete ner;
  if (sa != NULL) delete sa;
}

// pos tagging
void Eng_analyzer::init_pos_tagger(const string &rsc) {
  if (pos_tagger == NULL) pos_tagger = new POS_tagger;
  pos_tagger->init(rsc);
}

void Eng_analyzer::close_pos_tagger() {
  if (pos_tagger != NULL) {
    pos_tagger->close();
    delete pos_tagger;
    pos_tagger = NULL;
  }
}

void Eng_analyzer::do_pos_tagging(const string &input,
                                  EDoc &edoc,
                                  int split_sent,
                                  int use_tokenize) {
  edoc.text = input;
  edoc.sentence.clear();
  // sentence 분리: tokenization 이용
  vector<string> sent, token;
  token = pos_tagger->tokenize(input);

  string cur_sent;
  // sentence 분리
  if (split_sent) {
    for (int i = 0; i < token.size(); i++) {
      if (token[i] == "." || token[i] == "!" || token[i] == "?") {
        // .'' 처리
        if (i + 1 < token.size()
            && (token[i + 1] == "''" || token[i + 1] == "'")) {
          cur_sent += token[i++] + " ";
        }
        cur_sent += token[i];
        sent.push_back(cur_sent);
        cur_sent = "";
      } else {
        cur_sent += token[i] + " ";
      }
    }
  } else {
    cur_sent = input;
  }
  if (cur_sent != "") {
    sent.push_back(cur_sent);
  }
  // EDoc
  for (decltype(sent.size()) i = 0; i < sent.size(); i++) {
    ESent esent;
    edoc.sentence.push_back(esent);
  }
  // pos tagging
#pragma omp parallel for
  for (decltype(sent.size()) i = 0; i < sent.size(); i++) {
    ESent &esent = edoc.sentence[i];
    double prob;
    esent.text = sent[i];
    esent.word = pos_tagger->do_pos_tagging(sent[i], prob, use_tokenize);
  }
}

string Eng_analyzer::sprint_pos_tagging(EDoc &edoc) {
  string result;
  for (auto & sent : edoc.sentence) {
    result += pos_tagger->print_pos_tagging(sent.word);
    result += "\n";
  }
  return result;
}

// NER
void Eng_analyzer::init_ner(const string &rsc) {
  if (ner == NULL)
    ner = new NER;
  ner->init(rsc);
}

void Eng_analyzer::close_ner() {
  if (ner != NULL) {
    ner->close();
    delete ner;
    ner = NULL;
  }
}

void Eng_analyzer::do_ner(EDoc &edoc) {
#pragma omp parallel for
  for (int i = 0; i < (int) edoc.sentence.size(); i++) {
    double prob;
    edoc.sentence[i].word = ner->do_ner(edoc.sentence[i].word, prob);
  }
}

string Eng_analyzer::sprint_ner(EDoc &edoc) {
  string result;
  for (size_t i = 0; i < edoc.sentence.size(); i++) {
    result += ner->print_ner(edoc.sentence[i].word);
    result += "\n";
  }
  return result;
}

// sentiment analysis (SA)
void Eng_analyzer::init_sa(const string &rsc, const string &res) {
  if (sa == NULL) sa = new SA_NN;
  sa->init(rsc, res);
}

#if 0
void Eng_analyzer::init_sa(const string &rsc, const string &proj) {
  if (sa == NULL) sa = new SA_NN;
  sa->init(rsc, proj);
}
#endif

void Eng_analyzer::close_sa() {
  if (sa != NULL) {
    sa->close();
    delete sa;
    sa = NULL;
  }
}

void Eng_analyzer::do_sa(EDoc &edoc) {
  for (int i = 0; i < (int) edoc.sentence.size(); i++) {
    vector<word_t> input;
    for (int j = 0; j < (int) edoc.sentence[i].word.size(); j++) {
      input.push_back(edoc.sentence[i].word[j]);
    }
    edoc.sentence[i].SA = sa->do_sa(input, edoc.sentence[i].sentiment_prob);
  }
// edoc.sentiment = sa->do_sa(input, edoc.sentiment_prob);
}

void Eng_analyzer::do_sa_doc(EDoc &edoc) {
  vector<word_t> input;
  for (int i = 0; i < (int) edoc.sentence.size(); i++) {
    for (int j = 0; j < (int) edoc.sentence[i].word.size(); j++) {
      input.push_back(edoc.sentence[i].word[j]);
    }
  }
  edoc.sentiment = sa->do_sa(input, edoc.sentiment_prob);
}
vector<string> Eng_analyzer::do_single_sa(E_Doc &edoc, vector<float> &vec_float) {
  vector<string> vec_sa;
  for (int i = 0; i < (int) edoc.sentence.size(); i++) {
    E_Sentence &sent = edoc.sentence[i];
    sent.SA.type = sa->do_sa(sent, sent.SA.prob);
    vec_sa.push_back(sent.SA.type);
    vec_float.push_back(sent.SA.prob);
  }
  return vec_sa;
}

vector<vector<string> >
Eng_analyzer::do_multy_sa(E_Doc &edoc, vector<vector<float> > &vec_prob) {
  vector<vector<string> > vec_sa;
  for (int i = 0; i < (int) edoc.sentence.size(); i++) {
    E_Sentence &sent = edoc.sentence[i];
    vector<float> tmp_float;
    vector<string> tmp_sa = sa->do_multy_SA(sent, tmp_float);
    vec_sa.push_back(tmp_sa);
    vec_prob.push_back(tmp_float);
  }
  return vec_sa;
}

E_Doc Eng_analyzer::Make_JSON(EDoc &edoc) {
  // char temp[4096];
  E_Doc e_doc;
  e_doc.text = edoc.text;

  // 20160809
  e_doc.channel = edoc.channel;
  e_doc.con_id = edoc.con_id;
  e_doc.side = edoc.side;

  for (int sid = 0; sid < (int) edoc.sentence.size(); sid++) {
    ESent &sent = edoc.sentence[sid];
    E_Sentence e_sent;
    e_sent.id = sid;

    // 20160809
    e_sent.ts = sent.ts;
    e_sent.te = sent.te;

    string sent_text = my_replace_all(sent.text, "\\", "\\\\");
    e_sent.text = my_replace_all(sent_text, "\"", "\\\"");

    // morp, original
    for (int wid = 0; wid < (int) sent.word.size(); wid++) {
      word_t &word = sent.word[wid];
      E_Morp emorp;
      emorp.id = wid;
      string morp_str = my_replace_all(word.lemma, "\\", "\\\\");
      emorp.lemma = my_replace_all(morp_str, "\"", "\\\"");
      emorp.type = word.pos_tag;
      emorp.position = wid;
      //emorp.position
      e_sent.morp.push_back(emorp);

      E_Word eword;
      eword.id = wid;
      string word_lemma = my_replace_all(word.str, "\\", "\\\\");
      eword.str = my_replace_all(word_lemma, "\"", "\\\"");
      eword.type = word.pos_tag;
      eword.position = wid;
      //eword.position
      e_sent.word.push_back(eword);
    }
    int bracket_is_oppend = 0;
    // int ne_id = 0;
    vector<string> ne_word;
    string tmp_ne = "";
    vector<string> tag;
    vector<int> vec_begin;
    vector<int> vec_end;
    // NE
    if ((int) sent.word[0].ne_tag.size() != 0) {
      for (int nid = 0; nid < (int) sent.word.size(); nid++) {
        word_t &input = sent.word[nid];
        if (bracket_is_oppend && (input.ne_tag.at(0) == 'B' || input.ne_tag.at(0) == 'O')) {
          vec_end.push_back(nid - 1);

          ne_word.push_back(tmp_ne);
          bracket_is_oppend = 0;
          tmp_ne.clear();
        } else if (bracket_is_oppend) {
          tmp_ne += ' ';
          tmp_ne += input.lemma;
        }
        if (input.ne_tag.at(0) == 'B') {
          vec_begin.push_back(nid);

          string tmp_tag;
          tmp_tag = input.ne_tag;
          tmp_tag.replace(0, 2, "");
          tag.push_back(tmp_tag);
          bracket_is_oppend = 1;
          tmp_ne += input.lemma;
        }
        if (!bracket_is_oppend && input.ne_tag.at(0) != 'O') {
          vec_begin.push_back(nid);
          vec_end.push_back(nid);
          ne_word.push_back(input.lemma);
          tag.push_back(input.ne_tag);
        }
      }
      if (bracket_is_oppend) {
        vec_end.push_back(sent.word.size() - 1);
        ne_word.push_back(tmp_ne);
      }
      for (int nid = 0; nid < (int) ne_word.size(); nid++) {
        E_NE ene;
        ene.id = nid;
        string ne_text = my_replace_all(ne_word[nid], "\\", "\\\\");
        ene.text = my_replace_all(ne_text, "\"", "\\\"");
        ene.type = tag[nid];
        ene.begin = vec_begin[nid];
        ene.end = vec_end[nid];
        e_sent.NE.push_back(ene);
      }
    }
    // SA
    e_sent.SA.type = sent.SA;
    e_sent.SA.prob = sent.sentiment_prob;
    e_doc.sentence.push_back(e_sent);
  } // for sentence

  return e_doc;
}

string Eng_analyzer::sprint_EJSON(E_Doc &edoc, int human_readable = 1) {
  char temp[1000];
  temp[999] = '\0';
  string doc_text = my_replace_all(edoc.text, "\\", "\\\\");
  string json_str = "{\"text\" : \"" + my_replace_all(doc_text, "\"", "\\\"") + "\",";
  if (human_readable) json_str += "\n"; else json_str += " ";
  // 20160809
  json_str += " \"channel\" : \"" + edoc.channel + "\",";
  if (human_readable) json_str += "\n"; else json_str += ' ';
  json_str += " \"con_id\" : \"" + edoc.con_id + "\",";
  if (human_readable) json_str += "\n"; else json_str += ' ';
  json_str += " \"side\" : \"" + edoc.side + "\",";
  if (human_readable) json_str += "\n"; else json_str += ' ';

  json_str += " \"sentence\" : [";
  for (int sid = 0; sid < (int) edoc.sentence.size(); sid++) {
    E_Sentence &sent = edoc.sentence[sid];
    if (human_readable) json_str += "\t";
    json_str += '{';
    if (human_readable) json_str += "\n\t";
    // id
    json_str += "\"id\" : ";
    sprintf(temp, "%d", sent.id);
    json_str += temp;
    json_str += ',';
    if (human_readable) json_str += "\n\t";

    // 20160809
    // ts
    json_str += "\"ts\" : ";
    sprintf(temp, "%d", sent.ts);
    json_str += temp;
    json_str += ',';
    if (human_readable) json_str += "\n\t";
    // te
    json_str += "\"te\" : ";
    sprintf(temp, "%d", sent.te);
    json_str += temp;
    json_str += ',';
    if (human_readable) json_str += "\n\t";

    // txt
    json_str += "\"text\" : \"";
    string sent_text = my_replace_all(sent.text, "\\", "\\\\");
    json_str += my_replace_all(sent_text, "\"", "\\\"");
    json_str += "\",";
    if (human_readable) json_str += "\n"; else json_str += ' ';
    // morp
    if (human_readable) json_str += "\t";
    json_str += "\"morp\" : [";
    if (human_readable) json_str += "\n";
    int len_morp = (int) sent.morp.size();
    for (int mid = 0; mid < len_morp; mid++) {
      E_Morp &morp = sent.morp[mid];
      if (human_readable) json_str += "\t\t";
      json_str += '{';
      // id
      json_str += "\"id\" : ";
      sprintf(temp, "%d", morp.id);
      json_str += temp;
      json_str += ", ";
      // str
      json_str += "\"lemma\" : \"";
      string morp_lemma = my_replace_all(morp.lemma, "\\", "\\\\");
      json_str += my_replace_all(morp_lemma, "\"", "\\\"");
      json_str += "\", ";
      // type
      json_str += "\"type\" : \"";
      json_str += morp.type;
      json_str += "\", ";
      // position
      json_str += "\"position\" : ";
      sprintf(temp, "%d", morp.position);
      json_str += temp;
      if (mid + 1 == len_morp) {
        json_str += '}';
      } else {
        json_str += "},";
      }
      if (human_readable) json_str += "\n"; else json_str += ' ';
    }
    if (human_readable) json_str += "\t";
    json_str += "],";
    if (human_readable) json_str += "\n"; else json_str += ' ';

    // original
    if (human_readable) json_str += "\t";
    json_str += "\"word\" : [";
    if (human_readable) json_str += "\n";
    int len_word = (int) sent.word.size();
    for (int wid = 0; wid < len_word; wid++) {
      E_Word &word = sent.word[wid];
      if (human_readable) json_str += "\t\t";
      json_str += '{';
      // id
      json_str += "\"id\" : ";
      sprintf(temp, "%d", word.id);
      json_str += temp;
      json_str += ", ";
      // str
      json_str += "\"str\" : \"";
      string word_str = my_replace_all(word.str, "\\", "\\\\");
      json_str += my_replace_all(word_str, "\"", "\\\"");
      json_str += "\", ";
      // type
      json_str += "\"type\" : \"";
      json_str += word.type;
      json_str += "\", ";
      // position
      json_str += "\"position\" : ";
      sprintf(temp, "%d", word.position);
      json_str += temp;
      if (wid + 1 == len_word) {
        json_str += '}';
      } else {
        json_str += "},";
      }
      if (human_readable) json_str += "\n"; else json_str += ' ';
    }
    if (human_readable) json_str += "\t";
    json_str += "],";
    if (human_readable) json_str += "\n"; else json_str += ' ';

    // ne
    if (human_readable) json_str += "\t";
    json_str += "\"NE\" : [";
    if (human_readable) json_str += "\n";
    int len_ne = sent.NE.size();
    for (int nid = 0; nid < len_ne; nid++) {
      E_NE &ene = sent.NE[nid];
      if (human_readable) json_str += "\t\t";
      json_str += '{';
      // id
      json_str += "\"id\" : ";
      sprintf(temp, "%d", ene.id);
      json_str += temp;
      json_str += ", ";
      // text
      json_str += "\"text\" : \"";
      string ne_text = my_replace_all(ene.text, "\\", "\\\\");
      json_str += my_replace_all(ne_text, "\"", "\\\"");
      json_str += "\", ";
      // type
      json_str += "\"type\" : \"";
      json_str += ene.type;
      json_str += "\", ";
      // begin
      json_str += "\"begin\" : ";
      sprintf(temp, "%d", ene.begin);
      json_str += temp;
      json_str += ", ";
      // end
      json_str += "\"end\" : ";
      sprintf(temp, "%d", ene.end);
      json_str += temp;
      if (nid + 1 == len_ne) {
        json_str += '}';
      } else {
        json_str += "},";
      }
      if (human_readable) json_str += "\n"; else json_str += ' ';
    }
    if (human_readable) json_str += "\t";
    json_str += "],";
    if (human_readable) json_str += "\n"; else json_str += ' ';

    // SA for sentence
    if (human_readable) json_str += "\t";
    json_str += "\"SA\" : ";
    // if (human_readable) json_str += "\n";
    // if(human_readable) json_str += "\t\t" ;
    json_str += "{";
    json_str += "\"type\" : \"";
    json_str += sent.SA.type;
    json_str += "\", ";
    json_str += "\"prob\" : ";
    sprintf(temp, "%.3f", sent.SA.prob);
    json_str += temp;
    json_str += '}';
    if (human_readable) json_str += "\n"; else json_str += ' ';
    if (sid + 1 == edoc.sentence.size()) {
      if (human_readable) json_str += "\t";
      json_str += '}';
      if (human_readable) json_str += "\n";
    } else {
      if (human_readable) json_str += "\t";
      json_str += "},";
      if (human_readable) json_str += "\n";
    }
  }
  json_str += "] }";
  if (human_readable) json_str += "\n";

  return json_str;
}

string Eng_analyzer::sprint_JSON(EDoc &edoc, int human_readable = 1) {
  char temp[1000];
  temp[999] = '\0';
  string json_str = "{\"text\" : \"" + edoc.text + "\",";
  if (human_readable) json_str += "\n"; else json_str += ' ';

  // 20160809
  json_str += " \"channel\" : \"" + edoc.channel + "\",";
  if (human_readable) json_str += "\n"; else json_str += ' ';
  json_str += " \"con_id\" : \"" + edoc.con_id + "\",";
  if (human_readable) json_str += "\n"; else json_str += ' ';
  json_str += " \"side\" : \"" + edoc.side + "\",";
  if (human_readable) json_str += "\n"; else json_str += ' ';

  json_str += " \"sentence\" : [";
  if (human_readable) json_str += "\n";
  for (int sid = 0; sid < (int) edoc.sentence.size(); sid++) {
    ESent &sent = edoc.sentence[sid];
    if (human_readable) json_str += "\t";
    json_str += '{';
    if (human_readable) json_str += "\n\t";
    // id
    json_str += "\"id\" : ";
    sprintf(temp, "%d", sid);
    json_str += temp;
    json_str += ',';
    if (human_readable) json_str += "\n\t";

    // 20160809
    // ts
    json_str += "\"ts\" : ";
    sprintf(temp, "%d", sent.ts);
    json_str += temp;
    json_str += ',';
    if (human_readable) json_str += "\n\t";
    // te
    json_str += "\"te\" : ";
    sprintf(temp, "%d", sent.te);
    json_str += temp;
    json_str += ',';
    if (human_readable) json_str += "\n\t";

    // txt
    json_str += "\"text\" : \"";
    string sent_text = my_replace_all(sent.text, "\\", "\\\\");
    json_str += my_replace_all(sent_text, "\"", "\\\"");
    json_str += "\",";
    if (human_readable) json_str += "\n"; else json_str += ' ';

    // morp
    if (human_readable) json_str += "\t";
    json_str += "\"morp\" : [";
    if (human_readable) json_str += "\n";
    for (int wid = 0; wid < (int) sent.word.size(); wid++) {
      word_t &word = sent.word[wid];
      if (human_readable) json_str += "\t\t";
      json_str += '{';
      // id
      json_str += "\"id\" : ";
      sprintf(temp, "%d", word.id - 1);
      json_str += temp;
      json_str += ", ";
      // str
      json_str += "\"lemma\" : \"";
      string morp_lemma = my_replace_all(word.lemma, "\\", "\\\\");
      json_str += my_replace_all(morp_lemma, "\"", "\\\"");
      json_str += "\", ";
      // type
      json_str += "\"type\" : \"";
      json_str += word.pos_tag;
      json_str += "\"";
      if (wid + 1 == (int) sent.word.size()) {
        json_str += '}';
      } else {
        json_str += "},";
      }
      if (human_readable) json_str += "\n"; else json_str += ' ';
    }
    if (human_readable) json_str += "\t";
    json_str += "],";
    if (human_readable) json_str += "\n"; else json_str += ' ';

    // original
    if (human_readable) json_str += "\t";
    json_str += "\"word\" : [";
    if (human_readable) json_str += "\n";
    for (int wid = 0; wid < (int) sent.word.size(); wid++) {
      word_t &word = sent.word[wid];
      if (human_readable) json_str += "\t\t";
      json_str += '{';
      // id
      json_str += "\"id\" : ";
      sprintf(temp, "%d", word.id - 1);
      json_str += temp;
      json_str += ", ";
      // str
      json_str += "\"str\" : \"";
      string morp_lemma = my_replace_all(word.str, "\\", "\\\\");
      json_str += my_replace_all(morp_lemma, "\"", "\\\"");
      json_str += "\", ";
      // type
      json_str += "\"type\" : \"";
      json_str += word.pos_tag;
      json_str += "\"";
      if (wid + 1 == (int) sent.word.size()) {
        json_str += '}';
      } else {
        json_str += "},";
      }
      if (human_readable) json_str += "\n"; else json_str += ' ';
    }
    if (human_readable) json_str += "\t";
    json_str += "],";
    if (human_readable) json_str += "\n"; else json_str += ' ';

    // ne
    if (human_readable) json_str += "\t";
    json_str += "\"NE\" : [";
    if (human_readable) json_str += "\n";
    int bracket_is_oppend = 0;
    // int ne_id = 0;
    vector<string> ne_word;
    string tmp_ne = "";
    vector<string> tag;
    vector<int> vec_begin;
    vector<int> vec_end;
    for (int nid = 0; nid < (int) sent.word.size(); nid++) {
      word_t &input = sent.word[nid];
      if (bracket_is_oppend && (input.ne_tag.at(0) == 'B' || input.ne_tag.at(0) == 'O')) {
        vec_end.push_back(nid - 1);

        ne_word.push_back(tmp_ne);
        bracket_is_oppend = 0;
        tmp_ne.clear();
      } else if (bracket_is_oppend) {
        tmp_ne += ' ';
        tmp_ne += input.lemma;
      }
      if (input.ne_tag.at(0) == 'B') {
        vec_begin.push_back(nid);

        string tmp_tag;
        tmp_tag = input.ne_tag;
        tmp_tag.replace(0, 2, "");
        tag.push_back(tmp_tag);
        bracket_is_oppend = 1;
        tmp_ne += input.lemma;
      }
      if (!bracket_is_oppend && input.ne_tag.at(0) != 'O') {
        vec_begin.push_back(nid);
        vec_end.push_back(nid);
        ne_word.push_back(input.lemma);
        tag.push_back(input.ne_tag);
      }
    }
    if (bracket_is_oppend) {
      vec_end.push_back(sent.word.size() - 1);
      ne_word.push_back(tmp_ne);
    }
    for (int nid = 0; nid < (int) ne_word.size(); nid++) {
      if (human_readable) json_str += "\t\t";
      json_str += '{';
      // id
      json_str += "\"id\" : ";
      sprintf(temp, "%d", nid);
      json_str += temp;
      json_str += ", ";
      // text
      json_str += "\"text\" : \"";
      string ne_text = my_replace_all(ne_word[nid], "\\", "\\\\");
      json_str += my_replace_all(ne_text, "\"", "\\\"");
      json_str += "\", ";
      // type
      json_str += "\"type\" : \"";
      json_str += tag[nid];
      json_str += "\", ";
      // begin
      json_str += "\"begin\" : ";
      sprintf(temp, "%d", vec_begin[nid]);
      json_str += temp;
      json_str += "\", ";
      // end
      json_str += "\"end\" : ";
      sprintf(temp, "%d", vec_end[nid]);
      json_str += temp;
      if (nid + 1 == (int) ne_word.size()) {
        json_str += '}';
      } else {
        json_str += "},";
      }
      if (human_readable) json_str += "\n"; else json_str += ' ';
    }
    if (human_readable) json_str += "\t";
    json_str += "],";
    if (human_readable) json_str += "\n"; else json_str += ' ';

    // SA for sentence
    if (human_readable) json_str += "\t";
    json_str += "\"SA\" : ";
    if (human_readable) json_str += "\n";
    if (human_readable) json_str += "\t\t";
    json_str += '{';
    json_str += "\"type\" : \"";
    json_str += sent.SA;
    json_str += "\", ";
    json_str += "\"prob\" : ";
    sprintf(temp, "%.3f", sent.sentiment_prob);
    json_str += temp;
    json_str += '}';
    if (human_readable) json_str += "\n"; else json_str += ' ';
    if (sid + 1 == edoc.sentence.size()) {
      if (human_readable) json_str += "\t";
      json_str += '}';
      if (human_readable) json_str += "\n";
    } else {
      if (human_readable) json_str += "\t";
      json_str += "},";
      if (human_readable) json_str += "\n";
    }
  }
  json_str += "] }";
  if (human_readable) json_str += "\n";
  return json_str;
}

// yghwang 20171130
vector<string> Eng_analyzer::get_postagged_str(EDoc &edoc) {
  vector<string> ret;
  for (auto & sent : edoc.sentence) {
    string result = pos_tagger->print_pos_tagging(sent.word);
    ret.push_back(result) ;
  }
  return ret ;
}
