#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

// activation function
#define SIGM  1
#define TANH  2
#define RELU  3


/**
 *
 * @author hhs, modified by leeck
 *
 *
 */


//////////////////////////////////////////////////////////////////
class Seq2label_GRU_search {
 public:
  Seq2label_GRU_search();
  void load_model(const string &file_name);
  int classify(vector<int> input_vec, float &prob, vector<float> &alignment);

  int activation;
  float x_drop_out_rate;
  float h_drop_out_rate;

 private:
  int de;
  int ne;
  int nh;
  int nc;
  //int nall;
  //int nx;
  //int xh;
  vector<vector<float> > emb;
  vector<vector<float> > Wgx;
  vector<vector<float> > Wgxb;
  vector<vector<float> > Whx;
  vector<vector<float> > Whxb;
  vector<vector<float> > Ugh;
  vector<vector<float> > Ughb;
  vector<vector<float> > Uhh;
  vector<vector<float> > Uhhb;
  vector<float> bg;
  vector<float> bgb;
  vector<float> bh;
  vector<float> bhb;
  vector<vector<float> > Wa;
  vector<float> ba;
  vector<float> va;
  vector<vector<float> > Wyc;
  vector<float> by;
  vector<float> h0;
  vector<float> h0b;
};

