#if !defined(NN_UTIL_INCLUDED_)
#define NN_UTIL_INCLUDED_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <vector>
#include <stdexcept>

using namespace std;

/** Tokenize string to words.
 Tokenization of string and assignment to word vector.
 Delimiters are set of char.
 @param str string
 @param tokens token vector
 @param delimiters delimiters to divide string
 @return none
 */
inline void tokenize(const string &str,
                     vector<string> &tokens,
                     const string &delimiters) {
  tokens.clear();
  string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  string::size_type pos = str.find_first_of(delimiters, lastPos);
  while (string::npos != pos || string::npos != lastPos) {
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    lastPos = str.find_first_not_of(delimiters, pos);
    pos = str.find_first_of(delimiters, lastPos);
  }
}

/// activation function: sigmoid
inline float sigm(float a) {
  return (1.0F / (1.0F + expf(-a)));
}

/// activation function: tanh

inline float mytanh(float a) {
  return ((expf(a) - expf(-a)) / (expf(a) + expf(-a)));
}


/// activation function: ReLU
inline float relu(float x) {
  return x < 0 ? 0 : x;
}

/// element wise
inline void vector_sigm(vector<float> z, vector<float> &result_z) {
  result_z.clear();
  for (int i = 0; i < (int) z.size(); i++) {
    result_z.push_back(sigm(z[i]));
  }
}

inline void vector_tanh(vector<float> z, vector<float> &result_z) {
  result_z.clear();
  for (int i = 0; i < (int) z.size(); i++) {
    result_z.push_back(mytanh(z[i]));
  }
}

inline void vector_relu(vector<float> z, vector<float> &result_z) {
  result_z.clear();
  for (int i = 0; i < (int) z.size(); i++) {
    result_z.push_back(relu(z[i]));
  }
}

inline void vector_softmax(vector<float> z, vector<float> &result_z) {
  float sum_temp = 0.0;
  result_z.clear();
  for (int i = 0; i < (int) z.size(); i++) {
    sum_temp = sum_temp + expf(z[i]);
  }
  for (int i = 0; i < (int) z.size(); i++) {
    result_z.push_back(expf(z[i]) / sum_temp);
  }
}

inline void vector_softmax2(vector<float> z, vector<float> &result_z) {
  float sum_temp = 0.0;
  float max_e = -INFINITY;

  for (int i = 0; i < (int) z.size(); i++) {
    if (z[i] > max_e) {
      max_e = z[i];
    }
  }

  result_z.clear();
  for (int i = 0; i < (int) z.size(); i++) {
    sum_temp = sum_temp + expf(z[i] - max_e);
  }
  for (int i = 0; i < (int) z.size(); i++) {
    result_z.push_back(expf(z[i] - max_e) / sum_temp);
  }
}

inline void vector_log(vector<float> z, vector<float> &result_z) {
  result_z.clear();
  for (int i = 0; i < (int) z.size(); i++) {
    result_z.push_back(logf(z[i]));
  }
}

inline void matrix_vector_mul(const vector<vector<float> > &weghit11,
                              const vector<float> &input_x,
                              vector<float> &result_z) {
  if ((int) weghit11[0].size() != (int) input_x.size()) {
    cout << "matrix_vector_mul error 2" << endl;
    throw std::invalid_argument( "matrix_vector_mul error 2" );
  }

  float temp = 0;

  result_z.clear();
  for (int i = 0; i < (int) weghit11.size(); i++) {
    temp = 0;
    for (int j = 0; j < (int) input_x.size(); j++) {
      temp = input_x[j] * weghit11[i][j] + temp;
    }
    result_z.push_back(temp);
  }
}

inline void matrix_matrix_mul(const vector<vector<float> > &a,
                              const vector<vector<float> > &b,
                              vector<vector<float> > &result_z) {
  if ((int) a[0].size() != (int) b.size()) {
    cout << "matrix_matrix_mul error" << endl;
    throw std::invalid_argument( "matrix_matrix_mul error" );
  }

  vector<float> temp2;
  float temp = 0.0;

  temp2.clear();
  result_z.clear();

  for (int i = 0; i < (int) a.size(); i++) {
    temp2.clear();
    for (int j = 0; j < (int) b[0].size(); j++) {
      temp = 0.0;
      for (int k = 0; k < (int) b.size(); k++) {
        temp = temp + a[i][k] * b[k][j];
      }
      temp2.push_back(temp);
    }
    result_z.push_back(temp2);
  }
}

inline void vector_vector_mul(const vector<float> &weghit111,
                              const vector<float> &input_xx,
                              vector<float> &result_zz) {
  if ((int) weghit111.size() != (int) input_xx.size()) {
    cout << "vector_vector_mul error: " << weghit111.size() << " != "
         << input_xx.size() << endl;
    throw std::invalid_argument( "vector_vector_mul error" );
  }

  float temp = 0;
  result_zz.clear();
  for (int i = 0; i < (int) weghit111.size(); i++) {
    temp = weghit111[i] * input_xx[i];
    result_zz.push_back(temp);
  }
}

inline void vector_vector_mul2(vector<float> weghit111,
                               vector<float> input_xx,
                               float &result_z) {
  if ((int) weghit111.size() != (int) input_xx.size()) {
    cout << "vector_vector_mul2 error" << endl;
    throw std::invalid_argument( "vector_vector_mul2 error" );
  }

  float temp = 0;

  for (int i = 0; i < (int) weghit111.size(); i++) {
    temp = temp + weghit111[i] * input_xx[i];
  }
  result_z = temp;
}

inline void hidden_alignment(const vector<float> &a,
                             const vector<vector<float> > &hidden_layer,
                             vector<float> &result_z) {
  if ((int) a.size() != (int) hidden_layer.size()) {
    cout << "hidden_alignment error" << endl;
    throw std::invalid_argument( "hidden_alignment error" );
  }

  result_z.clear();
  for (int i = 0; i < (int) hidden_layer[0].size(); i++) {
    float temp = 0;
    for (int j = 0; j < (int) hidden_layer.size(); j++) {
      temp = temp + a[j] * hidden_layer[j][i];
    }
    result_z.push_back(temp);
  }
}

inline void vector_add(vector<float> a, vector<float> &b) {
  if ((int) a.size() != (int) b.size()) {
    cout << "vector_add error" << endl;
  }

  for (int i = 0; i < (int) a.size(); i++) {
    b[i] = a[i] + b[i];
  }
}

inline void vector_copy(vector<float> a, vector<float> &b) {
  b.clear();
  for (int i = 0; i < (int) a.size(); i++) {
    b.push_back(a[i]);
  }
}

inline void vector_copy2(vector<int> a, vector<int> &b) {
  b.clear();
  for (int i = 0; i < (int) a.size(); i++) {
    b.push_back(a[i]);
  }
}

inline void vector_copy3(vector<vector<float> > a, vector<vector<float> > &b) {
  b.clear();
  for (int i = 0; i < (int) a.size(); i++) {
    b.push_back(a[i]);
  }
}

inline void trans_matrix(vector<vector<float> > a, vector<vector<float> > &b) {
  b.clear();
  vector<float> temp;
  for (int i = 0; i < (int) a[0].size(); i++) {
    temp.clear();
    for (int j = 0; j < (int) a.size(); j++) {
      temp.push_back(a[j][i]);
    }
    b.push_back(temp);
  }
}

#endif // NN_UTIL_INCLUDED_
