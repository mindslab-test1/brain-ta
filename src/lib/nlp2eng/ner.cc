#include <stdio.h>
#include "ner.h"
#include "util.h"

#pragma warning(disable: 4786)
#pragma warning(disable: 4996)
#pragma warning(disable: 4267)
#pragma warning(disable: 4244)
#pragma warning(disable: 4018)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

NER::NER() {
}

NER::~NER() {
}


void NER::init(const string &dir) {
  string file;
  FILE *in;

  // crf
  //file = dir + "/" + "ner_model.txt";
  file = dir + "/" + "ner_model.bin";

  try {
    //crf.load(file);
    crf.load_bin(file);
  } catch (exception e) {
    cerr << "Error: " << e.what() << endl;
    exit(1);
  }

  // NE dic
  file = dir + "/" + "ner_dic.txt";
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    exit(-1);
  }
  cerr << "loading " << file << " ... ";
  char strtmp[1000];
  while (!feof(in)) {
    fgets(strtmp, 1000, in);
    if (strtmp[0] == ';') continue;

    vector<string> token;
    crf.tokenize(strtmp, token, " \t\n\r");
    if (token.size() == 2) {
      dic[get_lower(token[1])] = token[0];
    } else if (token.size() > 2) {
      string ne;
      for (int i = 1; i < token.size(); i++) {
        ne += token[i];
      }
      dic[get_lower(ne)] = token[0];
    } else {
      printf("Error(init): %s", strtmp);
    }
  }
  fclose(in);
  cerr << "done." << endl;

  // cluster dic
  file = dir + "/" + "cluster.txt";
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    exit(-1);
  }
  cerr << "loading " << file << " ... ";
  while (!feof(in)) {
    fgets(strtmp, 1000, in);
    if (strtmp[0] == ';') continue;
    vector<string> token;
    crf.tokenize(strtmp, token, " \t\n\r");
    if (token.size() == 3) {
      // cluster prob word
      cluster[get_lower(token[2])] = token[0];
    } else {
      cerr << "Warning(cluster.txt): " << strtmp << endl;
    }
  }
  fclose(in);
  cerr << "done." << endl;

  // word embedding
  file = dir + "/" + "embedding.txt";
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    return;
  }
  cerr << "loading " << file << " ... ";
  while (!feof(in)) {
    fgets(strtmp, 1000, in);
    if (strtmp[0] == ';') continue;
    vector<string> token;
    crf.tokenize(strtmp, token, " \t\n\r");
    if (token.size() >= 3) {
      // word
      string word = token[0];
      vector<float> word_embedding;
      for (int i = 1; i < token.size(); i++) {
        word_embedding.push_back((float)atof(token[i].c_str()));
      }
      embedding[word] = word_embedding;
    } else {
      printf("Error(word embedding): %s", strtmp);
    }
  }
  fclose(in);
  cerr << "done." << endl;
}


void NER::close() {
  dic.clear();
  crf.clear();
}


///< load_train_data: word NE-tag 포맷
vector<vector<word_t> > NER::load_train_data(const string &file) {
  FILE *in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    exit(-1);
  }

  vector<vector<word_t> > result;
  vector<word_t> sent;
  char strtmp[10000];
  while (!feof(in)) {
    fgets(strtmp, 10000, in);
    if (strtmp[0] == '#') continue;

    vector<string> token;
    crf.tokenize(strtmp, token, " \t\n\r");
    if (token.size() == 0) {
      if (sent.size() > 0) {
        result.push_back(sent);
        sent.clear();
      }
    } else if (token.size() == 2) {
      word_t w;
      w.str = token[0];
      w.ne_tag = token[1];
      sent.push_back(w);
    } else {
      cerr << "Error: " << strtmp << endl;
    }
  }
  if (sent.size() > 0) {
    result.push_back(sent);
    sent.clear();
  }
  fclose(in);

  return result;
}


///< NE tagging
vector<word_t> NER::do_ner(vector<word_t> input, double &prob) {
  vector<word_t> result;
  vector<vector<string> > feature = make_feature(input);

  // CRF 실행
  vector<string> outcome;
  prob = crf.eval(feature, outcome);

  for (int i = 0; i < outcome.size(); i++) {
    word_t word;
    word.id = input[i].id;
    word.str = input[i].str;
    word.lemma = input[i].lemma;
    word.pos_tag = input[i].pos_tag;
    word.chunk_tag = input[i].chunk_tag;
    word.head = input[i].head;
    word.dep_label = input[i].dep_label;
    word.ne_tag = outcome[i];
    result.push_back(word);
  }

  return result;
}


///< N-best NE tagging
vector<vector<word_t> >
NER::do_ner_nbest(vector<word_t> input, vector<double> &prob, int n) {
  vector<vector<word_t> > result;
  vector<vector<string> > feature = make_feature(input);

  // CRF 실행
  vector<vector<string> > outcome;
  prob.clear();
  prob = crf.eval_nbest(feature, outcome, n);

  for (int i = 0; i < outcome.size(); i++) {
    vector<word_t> cur_result;
    for (int j = 0; j < outcome[i].size(); j++) {
      word_t word;
      word.str = input[j].str;
      word.lemma = input[j].lemma;
      word.pos_tag = input[j].pos_tag;
      word.chunk_tag = input[j].chunk_tag;
      word.ne_tag = outcome[i][j];
      cur_result.push_back(word);
    }
    result.push_back(cur_result);
  }

  return result;
}


////< print
string NER::print_ner(vector<word_t> input) {
  string result;
  int bracket_is_opened = 0;
  for (int i = 0; i < input.size(); i++) {
    if (bracket_is_opened
        && (input[i].ne_tag.at(0) == 'B' || input[i].ne_tag.at(0) == 'O')) {
      result += ']';
      bracket_is_opened = 0;
    }
    if (result != "") result += ' ';
    if (input[i].ne_tag.at(0) == 'B') {
      string tag = input[i].ne_tag;
      tag.replace(0, 2, "");
      result += '[' + tag + ' ';
      bracket_is_opened = 1;
    }
    result += input[i].str;
    if (!bracket_is_opened && input[i].ne_tag.at(0) != 'O') {
      result += '/';
      result += input[i].ne_tag;
    }
  }
  if (bracket_is_opened) result += ']';
  return result;
}


///< feature generation
vector<vector<string> > NER::make_feature(vector<word_t> input) {
  vector<vector<string> > result;
  char temp[1000];
  temp[999] = 0;
  int window_size = 2;

  for (int i = 0; i < input.size(); i++) {
    vector<string> feature;

    // lexical feature
    for (int j = -window_size; j <= window_size; j++) {
      // unigram
      if (i + j >= 0 && i + j < input.size()) {
        sprintf(temp, "L%d=", j);
        feature.emplace_back(temp + get_lower(input[i + j].str));
        // bigram
        if (j + 1 <= window_size && i + j + 1 < input.size()) {
          sprintf(temp, "L%d%d=", j, j + 1);
          feature.emplace_back(
              temp + get_lower(input[i + j].str + "_" + input[i + j + 1].str));
        }
      }
    }

    // pos feature
    for (int j = -window_size; j <= window_size; j++) {
      if (i + j >= 0 && i + j < input.size()) {
        // unigram
        sprintf(temp, "P%d=", j);
        feature.push_back(temp + input[i + j].pos_tag);
        // pos + lexical
        sprintf(temp, "PL%d=", j);
        feature.emplace_back(
            temp + input[i + j].pos_tag + "_" + input[i + j].str);
        // bigram
        if (j + 1 <= window_size && i + j + 1 < input.size()) {
          sprintf(temp, "P%d%d=", j, j + 1);
          feature.emplace_back(
              temp + input[i + j].pos_tag + "_" + input[i + j + 1].pos_tag);
        }
        // trigram
        if (j + 2 <= window_size && i + j + 2 < input.size()) {
          sprintf(temp, "P%d%d%d=", j, j + 1, j + 2);
          feature.emplace_back(
              temp + input[i + j].pos_tag + "_" + input[i + j + 1].pos_tag + "_"
                  + input[i + j + 2].pos_tag);
        }
      }
    }

    // prefix feature
    for (int j = -window_size; j <= window_size; j++) {
      if (i + j >= 0 && i + j < input.size() && input[i + j].str.length() > 4) {
        // unigram
        sprintf(temp, "P%d=", j);
        string cur_token = get_lower(input[i + j].str);
        feature.emplace_back(temp + get_prefix(cur_token, 4));
        feature.emplace_back(temp + get_prefix(cur_token, 3));
        feature.emplace_back(temp + get_prefix(cur_token, 2));
        // bigram
        if (j + 1 <= window_size && i + j + 1 < input.size()) {
          if (input[i + j + 1].str.length() > 4) {
            string n_token = get_lower(input[i + j + 1].str);
            sprintf(temp, "Pref%d%d=", j, j + 1);
            feature.emplace_back(
                temp + get_prefix(cur_token, 4) + "_" + get_prefix(n_token, 4));
            feature.emplace_back(
                temp + get_prefix(cur_token, 3) + "_" + get_prefix(n_token, 3));
            feature.emplace_back(
                temp + get_prefix(cur_token, 2) + "_" + get_prefix(n_token, 2));
          }
        }
      }
    }

    // suffix feature
    for (int j = -window_size; j <= window_size; j++) {
      if (i + j >= 0 && i + j < input.size() && input[i + j].str.length() > 4) {
        // unigram
        sprintf(temp, "Suf%d=", j);
        string cur_token = get_lower(input[i + j].str);
        feature.emplace_back(temp + get_suffix(cur_token, 4));
        feature.emplace_back(temp + get_suffix(cur_token, 3));
        feature.emplace_back(temp + get_suffix(cur_token, 2));
        // bigram
        if (j + 1 <= window_size && i + j + 1 < input.size()) {
          if (input[i + j + 1].str.length() > 4) {
            string n_token = get_lower(input[i + j + 1].str);
            sprintf(temp, "Suf%d%d=", j, j + 1);
            feature.emplace_back(
                temp + get_suffix(cur_token, 4) + "_" + get_suffix(n_token, 4));
            feature.emplace_back(
                temp + get_suffix(cur_token, 3) + "_" + get_suffix(n_token, 3));
            feature.emplace_back(
                temp + get_suffix(cur_token, 2) + "_" + get_suffix(n_token, 2));
          }
        }
      }
    }

    // is_capital feature
    for (int j = -window_size; j <= window_size; j++) {
      if (i + j >= 0 && i + j < input.size() && input[i + j].str.length() > 0) {
        if (isupper(input[i + j].str.at(0))) {
          sprintf(temp, "Cap%d", j);
          feature.emplace_back(temp);
          if (j + 1 <= window_size && i + j + 1 < input.size()) {
            if (isupper(input[i + j + 1].str.at(0))) {
              sprintf(temp, "Cap%d%d", j, j + 1);
              feature.emplace_back(temp);
            }
          }
        }
        if (is_all_upper(input[i + j].str)) {
          sprintf(temp, "AllCap%d", j);
          feature.emplace_back(temp);
          if (j + 1 <= window_size && i + j + 1 < input.size()) {
            if (is_all_upper(input[i + j + 1].str)) {
              sprintf(temp, "AllCap%d%d", j, j + 1);
              feature.emplace_back(temp);
            }
          }
        }
      }
    }

    // is_num feature
    for (int j = -window_size; j <= window_size; j++) {
      if (i + j >= 0 && i + j < input.size() && input[i + j].str.length() > 0) {
        if (is_digit(input[i + j].str.at(0))) {
          sprintf(temp, "Num%d", j);
          feature.emplace_back(temp);
          if (j + 1 <= window_size && i + j + 1 < input.size()) {
            if (is_digit(input[i + j + 1].str.at(0))) {
              sprintf(temp, "Num%d%d", j, j + 1);
              feature.emplace_back(temp);
            }
          }
        }
        if (is_all_digit(input[i + j].str)) {
          sprintf(temp, "AllNum%d", j);
          feature.emplace_back(temp);
          if (j + 1 <= window_size && i + j + 1 < input.size()) {
            if (is_all_digit(input[i + j + 1].str)) {
              sprintf(temp, "AllNum%d%d", j, j + 1);
              feature.emplace_back(temp);
            }
          }
        }
      }
    }

    // NE dic feature
    for (int j = -window_size; j <= window_size; j++) {
      if (i + j >= 0 && i + j < input.size()) {
        string key = get_lower(input[i + j].str);
        // 한 단어
        if (dic.find(key) != dic.end()) {
          sprintf(temp, "F%d=", j);
          feature.emplace_back(temp + dic[key]);
          if (j == 0)
            feature.emplace_back("B-" + dic[key]);
        }
        // 두 단어 이상
        for (int k = 1; j + k <= window_size && i + j + k < input.size(); k++) {
          key += get_lower(input[i + j + k].str);
          if (dic.find(key) != dic.end()) {
            sprintf(temp, "F%d%d=", j, j + k);
            feature.emplace_back(temp + dic[key]);
            if (j == 0)
              feature.emplace_back("B-" + dic[key]);
            else if (j < 0 && j + k >= 0)
              feature.emplace_back("I-" + dic[key]);
          }
        }
      }
    }

    // cluster feature
    for (int j = -window_size; j <= window_size; j++) {
      if (i + j >= 0 && i + j < input.size()) {
        string cur_token = get_lower(input[i + j].str);
        if (cluster.find(cur_token) != cluster.end()) {
          sprintf(temp, "C%d=", j);
          feature.emplace_back(temp + cluster[cur_token]);
          // bigram
          if (j + 1 <= window_size && i + j + 1 < input.size()) {
            string next_token = get_lower(input[i + j + 1].str);
            if (cluster.find(next_token) != cluster.end()) {
              sprintf(temp, "C%d%d=", j, j + 1);
              feature.emplace_back(
                  temp + cluster[cur_token] + "_" + cluster[next_token]);
            }
          }
        }
      }
    }

    // word embedding
    if (!embedding.empty()) {
      string cur_token = get_lower(input[i].str);
      string key = nomalize(cur_token);
      if (embedding.find(key) == embedding.end()) key = "UNKNOWN";
      // word embedding
      vector<float> &embedding_vec = embedding[key];
      for (int k = 0; k < embedding_vec.size(); k++) {
        sprintf(temp, "WE=%d_%d", k, int(2 * embedding_vec[k]));
        feature.emplace_back(temp);
        sprintf(temp, "WE2=%d_%d", k, int(50 * embedding_vec[k]));
        feature.emplace_back(temp);
      }
    }

    result.push_back(feature);
  }

  return result;
}


