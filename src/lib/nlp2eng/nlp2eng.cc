#include "libmaum/brain/nlp/nlp2eng.h"
#include <libmaum/brain/nlp/eng-analyzer.h>
#include <libmaum/common/config.h>

void Nlp2Eng::UpdateFeatures() {
  auto logger = LOGGER();
  anal_ = std::make_shared<Eng_analyzer>();

  if (HasFeature(NlpFeature::NLPF_MORPHEME)) {
    anal_->init_pos_tagger(res_path_);
    logger->debug("[NLP2] loaded init_pos_tagger");
  }

  if (HasFeature(NlpFeature::NLPF_NAMED_ENTITY)) {
    anal_->init_ner(res_path_);
    logger->debug("[NLP2] loaded init_ner");
  }
}

void Nlp2Eng::Uninit() {
  anal_.reset();
}

/**
  @breif 분석된 결과를 Document에 저장시키기 위한 함수
  @param E_Doc &doc : 언어분석 결과를 담고 있는
  @param Document *anal : 언어분석 결과를 클라이언트에 보낼 message
  @param NlpAnalysisLevel level : 언어분석 레벨
*/
void ToMessage(const E_Doc &doc, Document *anal, NlpAnalysisLevel level) {
  string result;
  // sentence
  for (const E_Sentence &sent: doc.sentence) {
    // 각 문장에 대한 정보
    maum::brain::nlp::Sentence *sentence = anal->add_sentences();
    sentence->set_seq(sent.id);
    sentence->set_text(sent.text);
    // morp, 형태소 분석 결과
    for (const E_Morp &morp: sent.morp) {
      maum::brain::nlp::Morpheme *morpheme = sentence->add_morps();
      morpheme->set_seq(morp.id);
      morpheme->set_lemma(morp.lemma);
      morpheme->set_type(morp.type);
      morpheme->set_position(morp.position);
    }
    // word, word에 대한 정보 및 분석결과
    // morp와 결과는 같지만 구조체 내 멤버변수가 다름
    for (const E_Word &word: sent.word) {
      maum::brain::nlp::Word *w = sentence->add_words();
      w->set_seq(word.id);
      w->set_text(word.str);
      w->set_type(word.type);
      w->set_position(word.position);
    }
    if (level == NlpAnalysisLevel::NLP_ANALYSIS_MORPHEME)
      continue;
    // NER, 개체명 인식 결과
    for (const E_NE &ne: sent.NE) {
      maum::brain::nlp::NamedEntity *entity = sentence->add_nes();
      entity->set_seq(ne.id);
      entity->set_text(ne.text);
      entity->set_type(ne.type);
      entity->set_begin(ne.begin);
      entity->set_end(ne.end);
    }
    if (level == NlpAnalysisLevel::NLP_ANALYSIS_NAMED_ENTITY)
      continue;
  }
}

void ToEDoc(const Document &document, E_Doc &edoc) {
  string result;

  for (const auto &sent : document.sentences()) {
    E_Sentence sentence;
    sentence.id = sent.seq();
    sentence.text = result;

    // morp 형태소 분석 결과
    for (const auto &morp : sent.morps()) {
      E_Morp morpheme;
      morpheme.id = morp.seq();
      morpheme.lemma = morp.lemma();
      morpheme.type = morp.type();
      morpheme.position = morp.position();
      sentence.morp.push_back(morpheme);
    }

    // word 분석결과
    for (const auto &word : sent.words()) {
      E_Word w;
      w.id = word.seq();
      w.str = result;
      w.type = word.type();
      w.position = word.position();
      sentence.word.push_back(w);
    }
    edoc.sentence.push_back(sentence);
  }
}

void Nlp2Eng::AnalyzeOne(const InputText *text, Document *document) {
  const string &otext = text->text();
  auto nlp_level = text->level();
  EDoc doc;
  switch (nlp_level) {
    // 모든 모듈을 분석
    case NlpAnalysisLevel::NLP_ANALYSIS_ALL: {
      anal_->do_pos_tagging(otext, doc);
      anal_->do_ner(doc);
      E_Doc doc2 = anal_->Make_JSON(doc);
      ToMessage(doc2, document, NlpAnalysisLevel::NLP_ANALYSIS_ALL);
      break;
    }
      // word, morp 분석
    case NlpAnalysisLevel::NLP_ANALYSIS_MORPHEME: {
      anal_->do_pos_tagging(otext, doc);
      E_Doc doc2 = anal_->Make_JSON(doc);
      ToMessage(doc2, document, NlpAnalysisLevel::NLP_ANALYSIS_MORPHEME);
      break;
    }
      // word, morp, ner 분석
    case NlpAnalysisLevel::NLP_ANALYSIS_NAMED_ENTITY: {
      anal_->do_pos_tagging(otext, doc);
      anal_->do_ner(doc);
      E_Doc doc2 = anal_->Make_JSON(doc);
      ToMessage(doc2, document, NlpAnalysisLevel::NLP_ANALYSIS_NAMED_ENTITY);
      break;
    }
    default: {
      break;
    }
  }
  document->set_lang(maum::common::LangCode::eng);
}

void Nlp2Eng::SetClassifierModel(const string &classifier_model_path) {
  auto logger = LOGGER();
  auto &c = libmaum::Config::Instance();
  string sa_model_root = c.Get("brain-ta.cl.model.dir");
  string full_path = sa_model_root;

  full_path += '/';
  full_path += classifier_model_path;
  logger->debug("[NLP2] model full path : {}", full_path);
  anal_->init_sa(full_path, res_path_);
}

void Nlp2Eng::Classify(const Document &document, ClassifiedSummary *sum) {

  auto logger = LOGGER();

  E_Doc edoc;

  // NlpDocument to e_Doc
  ToEDoc(document, edoc);

  vector<vector<float>> vec_prob;
  vector<vector<string>> vec_sa;
  vec_sa = anal_->do_multy_sa(edoc, vec_prob);

  for (vector<E_Sentence>::size_type i = 0; i < edoc.sentence.size(); i++) {
    auto sent = sum->add_sentences();
    sent->set_text(edoc.sentence[i].text);
    for (vector<vector<string>>::size_type j = 0; j < vec_sa[i].size(); j++) {
      // 이를 소팅할 수 있는 map 에 추가하여 가장 높은 값이 나오도록
      // 순서는 float, string map 으로 처리한다.
      auto cl = sent->add_cls();
      cl->set_sa(vec_sa[i][j]);
      cl->set_probability(vec_prob[i][j]);
    }
  }
  // float prob
  // prob = vec_prob[0][0];
  for (auto sent : edoc.sentence) {
    logger->debug("sent: {} , found SA: {} {} ", sent.text,
                  sent.SA.type, sent.SA.prob);
  }

  // 출력
  // 문장이 여러개면 여러개의 결과물이 나올 수 있겠으나, 하나만 취한다.
  if (edoc.sentence.empty()) {
    logger->debug<const char *>("Doc has no sentences, return []");
    sum->set_c1_top("");
    sum->set_cl_top("");
  } else if (vec_prob[0][0] > 0) {
    // confidence level modify
    sum->set_c1_top(edoc.sentence[0].SA.type);
    sum->set_cl_top(edoc.sentence[0].SA.type);
    sum->set_probability_top(edoc.sentence[0].SA.prob);
  } else {
    logger->debug("found result is not proper! {}", vec_prob[0][0]);
    sum->set_c1_top("");
    sum->set_cl_top("");
  }
}

void Nlp2Eng::GetPosTaggedStr(const Document *document,
                              vector<string> *pos_tagged_str) {
  for (auto &sent : document->sentences()) {
    string result;
    for (int i = 0; i < sent.morps().size(); i++) {
      if (result != "") result += " ";
      result += sent.words(i).text();
      result += "/";
      result += sent.morps(i).lemma();
      result += "/";
      result += sent.morps(i).type();
    }
    pos_tagged_str->push_back(result);
  }
};

void Nlp2Eng::GetNerTaggedStr(const Document *document,
                              vector<string> *ner_tagged_str) {
  // TODO
  // eng-analyzer.cc 에서 EDoc ->E_Doc ner 정보가 저장이 안되고 있음
  // https://github.com/mindslab-ai/brain-ta/issues/298
  for (auto &sent: document->sentences()) {
    string result;
    for (auto &ner: sent.nes()) {
      if (result != "") result += " ";
      result += ner.text();
      result += "/";
      result += ner.type();
    }
    ner_tagged_str->push_back(result);
  }
}
