#include <stdio.h>
#include "pos-tagger.h"
#include "util.h"

// Construction
POS_tagger::POS_tagger() {
}

// Destruction
POS_tagger::~POS_tagger() {
}

// initialize pos tagger
void POS_tagger::init(const string &dir) {
  string file;
  FILE *in;

  // ssvm
  file = dir + "/" + "pos_model.bin";

  try {
    ssvm.load_bin(file);
  } catch (exception e) {
    cerr << "Error: " << e.what() << endl;
    exit(1);
  }

  // cluster dic
  file = dir + "/" + "cluster.txt";
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    exit(-1);
  }
  char temp[1000];
  while (!feof(in)) {
    fgets(temp, 1000, in);
    if (temp[0] == ';') continue;
    vector<string> token;
    ssvm.tokenize(temp, token, " \t\n\r");
    if (token.size() == 3) {
      // cluster prob word
      cluster[get_lower(token[2])] = token[0];
    } else {
      cerr << "Warning(cluster.txt): " << temp << endl;
    }
  }
  fclose(in);

  // word embedding
  file = dir + "/" + "embedding.txt";
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    return;
  }
  cerr << "loading " << file << " ... ";
  while (!feof(in)) {
    fgets(temp, 1000, in);
    if (temp[0] == ';') continue;
    vector<string> token;
    ssvm.tokenize(temp, token, " \t\n\r");
    if (token.size() >= 3) {
      // word
      string word = token[0];
      vector<float> word_embedding;
      for (int i = 1; i < token.size(); i++) {
        word_embedding.push_back((float)atof(token[i].c_str()));
      }
      embedding[word] = word_embedding;
    } else {
      printf("Error(word embedding): %s", temp);
    }
  }
  fclose(in);
  cerr << "done." << endl;

  // exception dic
  file = dir + "/" + "pos_exc.txt";
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    exit(-1);
  }
  while (!feof(in)) {
    fgets(temp, 1000, in);
    if (temp[0] == ';') continue;
    vector<string> token;
    ssvm.tokenize(temp, token, " \t\n\r");
    if (token.size() == 3) {
      exception_dic[make_pair(token[0], token[1].at(0))] = token[2];
    } else if (token.size() > 0) {
      cerr << "Warning(pos_exc.txt): " << temp << endl;
    }
  }
  fclose(in);

  // lemma dic
  file = dir + "/" + "pos_lemma.txt";
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    exit(-1);
  }
  while (!feof(in)) {
    fgets(temp, 1000, in);
    if (temp[0] == ';') continue;
    vector<string> token;
    ssvm.tokenize(temp, token, " \t\n\r");
    if (token.size() == 2) {
      lemma_set.insert(make_pair(token[0], token[1].at(0)));
    } else if (token.size() > 0) {
      cerr << "Warning(pos_lemma.txt): " << temp << endl;
    }
  }
  fclose(in);

  // abbrev dic
  file = dir + "/" + "pos_abbrev.txt";
  in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    exit(-1);
  }
  while (!feof(in)) {
    fgets(temp, 1000, in);
    if (temp[0] == ';') continue;
    vector<string> token;
    ssvm.tokenize(temp, token, " \t\n\r");
    if (token.size() == 1) {
      abbrev_set.insert(get_lower(token[0]));
    } else if (token.size() > 0) {
      cerr << "Warning(pos_abbrev.txt): " << temp << endl;
    }
  }
  fclose(in);

  // detachment rule
  // noun
  detachment_rule.push_back(make_pair("s", ""));
  detachment_rule.push_back(make_pair("ses", "s"));
  detachment_rule.push_back(make_pair("xes", "x"));
  detachment_rule.push_back(make_pair("zes", "z"));
  detachment_rule.push_back(make_pair("ches", "ch"));
  detachment_rule.push_back(make_pair("shes", "sh"));
  detachment_rule.push_back(make_pair("men", "man"));
  detachment_rule.push_back(make_pair("ies", "y"));
  // verb
  detachment_rule.push_back(make_pair("es", "e"));
  detachment_rule.push_back(make_pair("es", ""));
  detachment_rule.push_back(make_pair("ed", "e"));
  detachment_rule.push_back(make_pair("ed", ""));
  detachment_rule.push_back(make_pair("ing", "e"));
  detachment_rule.push_back(make_pair("ing", ""));
  // adj
  detachment_rule.push_back(make_pair("er", ""));
  detachment_rule.push_back(make_pair("est", ""));
  detachment_rule.push_back(make_pair("er", "e"));
  detachment_rule.push_back(make_pair("est", "e"));
}

void POS_tagger::close() {
  exception_dic.clear();
  lemma_set.clear();
  ssvm.clear();
}

///< POS tagging
vector<word_t>
POS_tagger::do_pos_tagging(const string &input, double &prob, int use_tokenize) {
  vector<word_t> result;
  vector<string> tokens;
  if (use_tokenize) {
    tokens = tokenize(input);
  } else {
    // input is tokenized word
    ssvm.tokenize(input, tokens, " \t\n\r");
  }
  vector<vector<string> > feature = make_feature(tokens);

  // SSVM 실행
  vector<string> outcome;
  prob = ssvm.eval(feature, outcome);

  for (int i = 0; i < outcome.size(); i++) {
    word_t word;
    word.id = i + 1;
    word.str = tokens[i];
    word.pos_tag = outcome[i];
    word.lemma = do_stemming(get_lower(tokens[i]), outcome[i]);
    word.head = 0;
    result.push_back(word);
  }

  return result;
}

///< N-best pos tagging
vector<vector<word_t> > POS_tagger::do_pos_tagging_nbest(
    const string &input, vector<double> &prob, int n, int use_tokenize) {
  vector<vector<word_t> > result;
  vector<string> tokens;
  if (use_tokenize) {
    tokens = tokenize(input);
  } else {
    // input is tokenized word
    ssvm.tokenize(input, tokens, " \t\n\r");
  }
  vector<vector<string> > feature = make_feature(tokens);

  // SSVM 실행
  vector<vector<string> > outcome;
  prob.clear();
  prob = ssvm.eval_nbest(feature, outcome, n);

  for (int i = 0; i < outcome.size(); i++) {
    vector<word_t> cur_result;
    for (int j = 0; j < outcome[i].size(); j++) {
      word_t word;
      word.id = j + 1;
      word.str = tokens[j];
      word.pos_tag = outcome[i][j];
      word.lemma = do_stemming(get_lower(tokens[j]), outcome[i][j]);
      word.head = 0;
      cur_result.push_back(word);
    }
    result.push_back(cur_result);
  }

  return result;
}

////< print
string POS_tagger::print_pos_tagging(vector<word_t> input) {
  string result;
  for (int i = 0; i < input.size(); i++) {
    if (result != "") result += ' ';
    result += input[i].str;
    result += '/';
    result += input[i].lemma;
    result += '/';
    result += input[i].pos_tag;
  }
  return result;
}

////< print
#if 0
string POS_tagger::print_token(vector<word_t> input) {
  string result;
  for (int i = 0; i < input.size(); i++) {
    if (result != "") result += " ";
    result += input[i].str;
  }
  return result;
}
#endif

///< tokenize
vector<string> POS_tagger::tokenize(const string &input) {
  string result = " ";
  result += input;
  result += ' ';

  // 2-byte symbol
  // new
  result = my_replace_all(result, " ", " ");   // c2 a0
  result = my_replace_all(result, "―", " - "); // e2 80 95
  result = my_replace_all(result, "‘", "'");   // e2 80 98
  result = my_replace_all(result, "’", "'");   // e2 80 99
  result = my_replace_all(result, "’’", "\""); // e2 80 99 e2 80 99
  result = my_replace_all(result, "“", "\"");  // e2 80 9c
  result = my_replace_all(result, "”", "\"");  // e2 80 9d
  // old
  result = my_replace_all(result, "‘", "' ");  // euckr a1 ae
  result = my_replace_all(result, "’", "'");   // euckr a1 af
  result = my_replace_all(result, "“", "\" "); // euckr a1 b0
  result = my_replace_all(result, "”", "\"");  // euckr a1 b1
  // 예외 처리
  result = my_replace_all(result, "`s ", "'s ");
  result = my_replace_all(result, "s` ", "s' ");

  // ``, '' -> 앞 뒤로 빈칸 삽입
  result = my_replace_all(result, "``", " `` ");
  result = my_replace_all(result, "''", " '' ");
  // " --> ``
  result = my_replace_all(result, " \"", " `` ");
  result = my_replace_all(result, "[\"", "[ `` ");
  result = my_replace_all(result, "(\"", "( `` ");
  result = my_replace_all(result, "{\"", "{ `` ");
  result = my_replace_all(result, "<\"", "< `` ");

  // symbol
  result = my_replace_all(result, "...", " ... ");
  // 숫자 처리 고려
  result = my_replace_all(result, ", ", " , ");
  result = my_replace_all(result, ";", " ; ");
  result = my_replace_all(result, ":", " : ");
  result = my_replace_all(result, "@", " @ ");
  result = my_replace_all(result, "#", " # ");
  result = my_replace_all(result, "$", "$ ");     // US$, HK$
  result = my_replace_all(result, "%", " % ");
  result = my_replace_all(result, "?", " ? ");
  result = my_replace_all(result, "!", " ! ");
  result = my_replace_all(result, "[", " [ ");
  result = my_replace_all(result, "]", " ] ");
  result = my_replace_all(result, "(", " ( ");
  result = my_replace_all(result, ")", " ) ");
  result = my_replace_all(result, "{", " { ");
  result = my_replace_all(result, "}", " } ");
  result = my_replace_all(result, "<", " < ");
  result = my_replace_all(result, ">", " > ");
  result = my_replace_all(result, "--", " @@--@@ ");
  result = my_replace_all(result, " -", " - ");   // new: -Lady
  result = my_replace_all(result, " @@--@@ ", " -- "); // new


  // possessive or close-single-quote
  result = my_replace_all(result, "''", " @@quote@@ ");
  result = my_replace_all(result, "' ", " ' ");
  // 'How, 'New - 새로 추가
  result = my_replace_all(result, " '", " ' ");
  result = my_replace_all(result, " @@quote@@ ", " '' ");

  // as in it's, I'm, we'd
  result = my_replace_all(result, "'s ", " 's ");
  result = my_replace_all(result, "'S ", " 'S ");
  result = my_replace_all(result, "'m ", " 'm ");
  result = my_replace_all(result, "'M ", " 'M ");
  result = my_replace_all(result, "'d ", " 'd ");
  result = my_replace_all(result, "'D ", " 'D ");

  // 'll, 're, 've, n't
  result = my_replace_all(result, "'ll ", " 'll ");
  result = my_replace_all(result, "'LL ", " 'LL ");
  result = my_replace_all(result, "'re ", " 're ");
  result = my_replace_all(result, "'RE ", " 'RE ");
  result = my_replace_all(result, "'ve ", " 've ");
  result = my_replace_all(result, "'VE ", " 'VE ");
  result = my_replace_all(result, "n't ", " n't ");
  result = my_replace_all(result, "N'T ", " N'T ");
  result = my_replace_all(result, "n'T ", " n'T ");
  // 'em, 'til (내가 추가 한 것)
  result = my_replace_all(result, "'em ", " 'em ");
  result = my_replace_all(result, "'Em ", " 'Em ");
  result = my_replace_all(result, "'til ", " 'til ");

  // cannot, d'ye, gimme, gonna, gotta, lemme, more'n, 'tis, 'twas, wanna
  result = my_replace_all(result, " cannot ", " can not ");
  result = my_replace_all(result, " Cannot ", " Can not ");
  result = my_replace_all(result, " d'ye ", " d' ye ");
  result = my_replace_all(result, " D'ye ", " D' ye ");
  result = my_replace_all(result, " gimme ", " gim me ");
  result = my_replace_all(result, " Gimme ", " Gim me ");
  result = my_replace_all(result, " gonna ", " gon na ");
  result = my_replace_all(result, " Gonna ", " Gon na ");
  result = my_replace_all(result, " gotta ", " got nlp ");
  result = my_replace_all(result, " Gotta ", " Got nlp ");
  result = my_replace_all(result, " lemme ", " lem me ");
  result = my_replace_all(result, " Lemme ", " Lem me ");
  result = my_replace_all(result, " more'n ", " more 'n ");
  //result = my_replace_all(result, " 'tis ", " 't is ");
  //result = my_replace_all(result, " 'Tis ", " 'T is ");
  //result = my_replace_all(result, " 'twas ", " 't was ");
  //result = my_replace_all(result, " 'Twas ", " 'T was ");
  result = my_replace_all(result, " wanna ", " wan na ");
  result = my_replace_all(result, " Wanna ", " Wan na ");
  // 'How, 'New ('tis, 'Tis, 'twas, 'Twas 고려) - 새로 추가
  result = my_replace_all(result, " ' tis ", " 't is ");
  result = my_replace_all(result, " ' Tis ", " 'T is ");
  result = my_replace_all(result, " ' twas ", " 't was ");
  result = my_replace_all(result, " ' Twas ", " 'T was ");
  // 잘못된 것 수정 (' 관련)
  result = my_replace_all(result, " ' s ", " 's ");
  result = my_replace_all(result, " ' S ", " 'S ");
  result = my_replace_all(result, " ' m ", " 'm ");
  result = my_replace_all(result, " ' M ", " 'M ");
  result = my_replace_all(result, " ' d ", " 'd ");
  result = my_replace_all(result, " ' D ", " 'D ");
  result = my_replace_all(result, " ' ll ", " 'll ");
  result = my_replace_all(result, " ' LL ", " 'LL ");
  result = my_replace_all(result, " ' re ", " 're ");
  result = my_replace_all(result, " ' RE ", " 'RE ");
  result = my_replace_all(result, " ' ve ", " 've ");
  result = my_replace_all(result, " ' VE ", " 'VE ");

  // " --> ''
  result = my_replace_all(result, "\"", " '' ");

  vector<string> token, token2;
  ssvm.tokenize(result, token, " \t\n\r");

  // 분리된 것 중에서 다시 합칠 것은 합침
  for (vector<string>::size_type i = 0; i < token.size(); i++) {
    // 끝이 .으로 끝나는 경우 처리
    if (token[i].size() >= 2 && strend(token[i], ".")) {
      // 시작이 alphabet
      if (isalpha(token[i].at(0))) {
        if (abbrev_set.find(get_lower(token[i])) != abbrev_set.end()) {
          token2.push_back(token[i]);
        } else {
          // . 분리
          string word = get_prefix(token[i], token[i].size() - 1);
          token2.push_back(word);
          token2.push_back(".");
        }
      }
        // 시작이 숫자이거나 . 바로 앞이 숫자
      else if (isdigit(token[i].at(0))
          || isdigit(token[i].at(token[i].size() - 2))) {
        // . 분리하지 않음: Oct. 9.
        //token2.push_back(token[i]);
        // . 분리
        string word = get_prefix(token[i], token[i].size() - 1);
        token2.push_back(word);
        token2.push_back(".");
        // abbrev_set에 포함되어 있으면 분리하지 않음: ... 등
      } else if (abbrev_set.find(get_lower(token[i])) != abbrev_set.end()) {
        token2.push_back(token[i]);
      } else {
        // . 분리
        string word = get_prefix(token[i], token[i].size() - 1);
        token2.push_back(word);
        token2.push_back(".");
      }
    } else if (i + 2 < token.size() && token[i].size() <= 2
        && token[i + 2].size() <= 2 && isdigit(token[i].at(0))
        && token[i + 1] == ":" && isdigit(token[i + 2].at(0))) {
      // 시간 표현 처리: "2 : 43" --> 2:43
      token2.push_back(token[i] + token[i + 1] + token[i + 2]);
      i += 2;
    } else if (i + 2 < token.size() && isdigit(token[i].at(0))
        && token[i + 1] == "%" && token[i + 2].size() >= 2
        && token[i + 2].at(0) == '-' && isalpha(token[i + 2].at(1))) {
      // 81%-owned 류 처리: "[0-9]* % -[a-z]*" --> [0-9]*%-[a-z]*
      token2.push_back(token[i] + token[i + 1] + token[i + 2]);
      i += 2;
    } else {
      // 모두 분리
      token2.push_back(token[i]);
    }
  }

  return token2;
}

///< feature generation
vector<vector<string>> POS_tagger::make_feature(const vector<string> &tokens) {
  vector<vector<string>> result;
  char temp[1000];
  temp[999] = 0;

  for (vector<string>::size_type i = 0; i < tokens.size(); i++) {
    vector<string> feature;

    // lexical feature
    for (int j = -2; j <= 2; j++) {
      // unigram
      if (i + j >= 0 && i + j < tokens.size()) {
        sprintf(temp, "L%d=", j);
        feature.push_back(temp + get_lower(tokens[i + j]));
        // bigram
        if (j <= 1 && i + j + 1 < tokens.size()) {
          sprintf(temp, "L%d%d=", j, j + 1);
          feature.emplace_back(
              temp + get_lower(tokens[i + j] + "_" + tokens[i + j + 1]));
        }
      }
    }

    // prefix & suffix feature
    for (int j = -2; j <= 2; j++) {
      if (i + j >= 0 && i + j < tokens.size() && tokens[i + j].size() > 4) {
        // prefix feature
        // unigram
        sprintf(temp, "P%d=", j);
        string cur_token = get_lower(tokens[i + j]);
        feature.emplace_back(temp + get_prefix(cur_token, 4));
        feature.emplace_back(temp + get_prefix(cur_token, 3));
        feature.emplace_back(temp + get_prefix(cur_token, 2));
        // bigram
        if (j <= 1 && i + j + 1 < tokens.size() &&
            tokens[i + j + 1].size() > 4) {
          sprintf(temp, "P%d%d=", j, j + 1);
          string next_token = get_lower(tokens[i + j + 1]);
          feature.emplace_back(temp + get_prefix(cur_token, 4) + "_"
                                   + get_prefix(next_token, 4));
          feature.emplace_back(temp + get_prefix(cur_token, 3) + "_"
                                   + get_prefix(next_token, 3));
          feature.emplace_back(temp + get_prefix(cur_token, 2) + "_"
                                   + get_prefix(next_token, 2));
        }
        // suffix feature
        // unigram
        sprintf(temp, "S%d=", j);
        feature.emplace_back(temp + get_suffix(cur_token, 4));
        feature.emplace_back(temp + get_suffix(cur_token, 3));
        feature.emplace_back(temp + get_suffix(cur_token, 2));
        // bigram
        if (j <= 1 && i + j + 1 < tokens.size()
            && tokens[i + j + 1].size() > 4) {
          sprintf(temp, "S%d%d=", j, j + 1);
          string next_token = get_lower(tokens[i + j + 1]);
          feature.emplace_back(temp + get_suffix(cur_token, 4) + "_"
                                   + get_suffix(next_token, 4));
          feature.emplace_back(temp + get_suffix(cur_token, 3) + "_"
                                   + get_suffix(next_token, 3));
          feature.emplace_back(temp + get_suffix(cur_token, 2) + "_"
                                   + get_suffix(next_token, 2));
        }
      }
    }

    // is_capital feature
    for (int j = -2; j <= 2; j++) {
      if (i + j >= 0 && i + j < tokens.size()) {
        if (isupper(tokens[i + j].at(0))) {
          sprintf(temp, "Cap%d", j);
          feature.emplace_back(temp);
          if (j <= 1 && i + j + 1 < tokens.size()
              && isupper(tokens[i + j + 1].at(0))) {
            sprintf(temp, "Cap%d%d", j, j + 1);
            feature.emplace_back(temp);
          }
        }
        if (is_all_upper(tokens[i + j])) {
          sprintf(temp, "AllCap%d", j);
          feature.emplace_back(temp);
          if (j <= 1 && i + j + 1 < tokens.size()
              && is_all_upper(tokens[i + j + 1])) {
            sprintf(temp, "AllCap%d%d", j, j + 1);
            feature.emplace_back(temp);
          }
        }
      }
    }

    // is_num, allNum feature
    for (int j = -2; j <= 2; j++) {
      if (i + j >= 0 && i + j < tokens.size()) {
        if (isdigit(tokens[i + j].at(0))) {
          sprintf(temp, "Num%d", j);
          feature.emplace_back(temp);
          if (j <= 1 && i + j + 1 < tokens.size()
              && isdigit(tokens[i + j + 1].at(0))) {
            sprintf(temp, "Num%d%d", j, j + 1);
            feature.emplace_back(temp);
          }
        }
        if (tokens[i + j].size() > 1 && is_all_digit(tokens[i + j])) {
          sprintf(temp, "AllNum%d", j);
          feature.emplace_back(temp);
          if (j <= 1 && i + j + 1 < tokens.size()
              && is_all_digit(tokens[i + j + 1])) {
            sprintf(temp, "AllNum%d%d", j, j + 1);
            feature.emplace_back(temp);
          }
        }
      }
    }

    // cluster feature
    for (int j = -2; j <= 2; j++) {
      if (i + j >= 0 && i + j < tokens.size()) {
        string cur_token = get_lower(tokens[i + j]);
        if (cluster.find(cur_token) != cluster.end()) {
          sprintf(temp, "C%d=", j);
          feature.emplace_back(temp + cluster[cur_token]);
          // bigram
          if (j <= 1 && i + j + 1 < tokens.size()) {
            string next_token = get_lower(tokens[i + j + 1]);
            if (cluster.find(next_token) != cluster.end()) {
              sprintf(temp, "C%d%d=", j, j + 1);
              feature.emplace_back(
                  temp + cluster[cur_token] + "_" + cluster[next_token]);
            }
          }
        }
      }
    }

    // word embedding
    if (!embedding.empty()) {
      string cur_token = get_lower(tokens[i]);
      string key = nomalize(cur_token);
      if (embedding.find(key) == embedding.end()) key = "UNKNOWN";
      // word embedding
      vector<float> &embedding_vec = embedding[key];
      for (int k = 0; k < embedding_vec.size(); k++) {
        sprintf(temp, "WE=%d_%d", k, int(5 * embedding_vec[k]));
        feature.emplace_back(temp);
      }
    }

    result.push_back(feature);
  }

  return result;
}

///< stemming: WordNet morph 참고
string POS_tagger::do_stemming(const string &word, const string &pos_tag) {
  string return_val, temp_buf, end;

  char pos = pos_tag.at(0);

  // first look for word on exception list
  pair<string, char> key = make_pair(word, pos);
  if (exception_dic.find(key) != exception_dic.end()) return exception_dic[key];

  // only use exception list for adverbs
  if (pos == 'R') return word;

  if (pos == 'N') {
    if (strend(word, "ful")) {
      temp_buf = get_prefix(word, word.size() - 3);
      end = "ful";
    } else {
      // check for noun ending with 'ss' or short words
      if (strend(word, "ss") || (word.size() <= 2)) return word;
    }
  }

  // If not in exception list, try applying rules from tables
  if (temp_buf == "") temp_buf = word;

  if (lemma_set.find(make_pair(word, pos)) != lemma_set.end()) {
    return word;
  }
  for (int i = 0; i < detachment_rule.size(); i++) {
    if (strend(temp_buf, detachment_rule[i].first)) {
      return_val = get_prefix(temp_buf,
                              temp_buf.size()
                                  - detachment_rule[i].first.size());
      return_val += detachment_rule[i].second;
      if (lemma_set.find(make_pair(return_val, pos)) != lemma_set.end()) {
        return return_val + end;
      }
    }
  }

  return word;
}

///< load_train_data: word pos_tag 포맷
vector<vector<word_t>> POS_tagger::load_train_data(const string &file) {
  FILE *in = fopen(file.c_str(), "r+");
  if (in == NULL) {
    cerr << "File open error: " << file << endl;
    exit(-1);
  }

  vector<vector<word_t> > result;
  vector<word_t> sent;
  char strtmp[10000];
  while (!feof(in)) {
    fgets(strtmp, 10000, in);
    if (strtmp[0] == '#') continue;

    vector<string> token;
    ssvm.tokenize(strtmp, token, " \t\n\r");
    if (token.size() == 0) {
      if (sent.size() > 0) {
        result.push_back(sent);
        sent.clear();
      }
    } else if (token.size() == 2) {
      word_t w;
      w.str = token[0];
      w.pos_tag = token[1];
      sent.push_back(w);
    } else {
      cerr << "Error: " << strtmp << endl;
    }
  }
  if (sent.size() > 0) {
    result.push_back(sent);
    sent.clear();
  }
  fclose(in);

  return result;
}
