#if !defined(NER_INCLUDED_)
#define NER_INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma warning( disable : 4786 )

#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <set>
#include <map>
#include "pos-tagger.h"

using namespace std;

class NER {
 public:
  NER();
  virtual ~NER();

  void init(const string &dir);

  void close();

  ///< load_train_data: word NE-tag 포맷
  vector<vector<word_t> > load_train_data(const string &file);

  // NE tagging
  vector<word_t> do_ner(vector<word_t> input, double &prob);

  // N-best NE tagging
  vector<vector<word_t> >
  do_ner_nbest(vector<word_t> input, vector<double> &prob, int n = 5);

  string print_ner(vector<word_t> input);

  vector<vector<string> > make_feature(vector<word_t> input);

 private:
  SSVM crf;
  map<string, string> dic;
  map<string, string> cluster;
  map<string, vector<float>> embedding;  // word embedding
};


#endif // NER_INCLUDED_
