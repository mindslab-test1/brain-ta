#include <stdlib.h>
#include <math.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <stdexcept>

#include "nn-util.h"
#include "seq2label-gru-search.h"

using namespace std;

/**
 *
 * @author hhs, modified by leeck
 *
 *
 */
void Seq2label_GRU_search::load_model(const string &file_name) {
  ifstream is1;
  is1.open(file_name.c_str(), ios::in);
  if (is1.fail()) {
    cerr << file_name << " open fail" << endl;
    exit(1);
  }

  cerr << "load " << file_name << "... ";

  this->emb.clear();
  this->Wgx.clear();
  this->Wgxb.clear();
  this->Whx.clear();
  this->Whxb.clear();
  this->Ugh.clear();
  this->Ughb.clear();
  this->Uhh.clear();
  this->Uhhb.clear();
  this->bg.clear();
  this->bgb.clear();
  this->bh.clear();
  this->bhb.clear();
  this->Wa.clear();
  this->ba.clear();
  this->va.clear();
  this->Wyc.clear();
  this->by.clear();
  this->h0.clear();
  this->h0b.clear();

  string s1;
  vector<string> s_temp1;
  vector<float> s_temp2;

  s_temp1.clear();
  s_temp2.clear();

  getline(is1, s1);
  if (s1.find("emb") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        s_temp1.clear();
        s_temp2.clear();
        tokenize(s1, s_temp1, " \t\r\n");
        for (int i = 0; i < (int) s_temp1.size(); i++) {
          s_temp2.push_back(this->x_drop_out_rate * stof(s_temp1[i]));
        }
        this->emb.push_back(s_temp2);
      } else {
        break;
      }
    }
  } else {
    cerr << "emb error!" << endl;
    throw std::invalid_argument( "model error: emb error" );
  }

  getline(is1, s1);
  if (s1.find("Wgx") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        s_temp1.clear();
        s_temp2.clear();
        tokenize(s1, s_temp1, " \t\r\n");
        for (int i = 0; i < (int) s_temp1.size(); i++) {
          s_temp2.push_back(stof(s_temp1[i]));
        }
        this->Wgx.push_back(s_temp2);
      } else {
        break;
      }

    }
  } else {
    cerr << "Wgx error!" << endl;
    throw std::invalid_argument( "model error: Wgx error" );
  }

  getline(is1, s1);
  if (s1.find("Wgxb") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        s_temp1.clear();
        s_temp2.clear();
        tokenize(s1, s_temp1, " \t\r\n");
        for (int i = 0; i < (int) s_temp1.size(); i++) {
          s_temp2.push_back(stof(s_temp1[i]));
        }
        this->Wgxb.push_back(s_temp2);
      } else {
        break;
      }

    }
  } else {
    cerr << "Wgxb error!" << endl;
    throw std::invalid_argument( "model error: Wgxb error" );
  }

  getline(is1, s1);
  if (s1.find("Whx") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        s_temp1.clear();
        s_temp2.clear();
        tokenize(s1, s_temp1, " \t\r\n");
        for (int i = 0; i < (int) s_temp1.size(); i++) {
          s_temp2.push_back(stof(s_temp1[i]));
        }
        this->Whx.push_back(s_temp2);
      } else {
        break;
      }

    }
  } else {
    cerr << "Whx error!" << endl;
    throw std::invalid_argument( "model error: Whx error" );
  }

  getline(is1, s1);
  if (s1.find("Whxb") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        s_temp1.clear();
        s_temp2.clear();
        tokenize(s1, s_temp1, " \t\r\n");
        for (int i = 0; i < (int) s_temp1.size(); i++) {
          s_temp2.push_back(stof(s_temp1[i]));
        }
        this->Whxb.push_back(s_temp2);
      } else {
        break;
      }

    }
  } else {
    cerr << "Whxb error!" << endl;
    throw std::invalid_argument( "model error: Whxb error" );
  }

  getline(is1, s1);
  if (s1.find("Ugh") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        s_temp1.clear();
        s_temp2.clear();
        tokenize(s1, s_temp1, " \t\r\n");
        for (int i = 0; i < (int) s_temp1.size(); i++) {
          s_temp2.push_back(stof(s_temp1[i]));
        }
        this->Ugh.push_back(s_temp2);
      } else {
        break;
      }

    }
  } else {
    cerr << "Ugh error!" << endl;
    throw std::invalid_argument( "model error: Ugh error" );
  }

  getline(is1, s1);
  if (s1.find("Ughb") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        s_temp1.clear();
        s_temp2.clear();
        tokenize(s1, s_temp1, " \t\r\n");
        for (int i = 0; i < (int) s_temp1.size(); i++) {
          s_temp2.push_back(stof(s_temp1[i]));
        }
        this->Ughb.push_back(s_temp2);
      } else {
        break;
      }

    }
  } else {
    cerr << "Ughb error!" << endl;
    throw std::invalid_argument( "model error: Ughb error" );
  }

  getline(is1, s1);
  if (s1.find("Uhh") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        s_temp1.clear();
        s_temp2.clear();
        tokenize(s1, s_temp1, " \t\r\n");
        for (int i = 0; i < (int) s_temp1.size(); i++) {
          s_temp2.push_back(stof(s_temp1[i]));
        }
        this->Uhh.push_back(s_temp2);
      } else {
        break;
      }

    }
  } else {
    cerr << "Uhh error!" << endl;
    throw std::invalid_argument( "model error: Uhh error" );
  }

  getline(is1, s1);
  if (s1.find("Uhhb") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        s_temp1.clear();
        s_temp2.clear();
        tokenize(s1, s_temp1, " \t\r\n");
        for (int i = 0; i < (int) s_temp1.size(); i++) {
          s_temp2.push_back(stof(s_temp1[i]));
        }
        this->Uhhb.push_back(s_temp2);
      } else {
        break;
      }

    }
  } else {
    cerr << "Uhhb error!" << endl;
    throw std::invalid_argument( "model error: Uhhb error" );
  }

  getline(is1, s1);
  if (s1.find("bg") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        this->bg.push_back(stof(s1));
      } else {
        break;
      }

    }
  } else {
    cerr << "bg error!" << endl;
    throw std::invalid_argument( "model error: bg error" );
  }

  getline(is1, s1);
  if (s1.find("bgb") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        this->bgb.push_back(stof(s1));
      } else {
        break;
      }

    }
  } else {
    cerr << "bgb error!" << endl;
    throw std::invalid_argument( "model error: bgb error" );
  }

  getline(is1, s1);
  if (s1.find("bh") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        this->bh.push_back(stof(s1));
      } else {
        break;
      }

    }
  } else {
    cerr << "bh error!" << endl;
    throw std::invalid_argument( "model error: bh error" );
  }

  getline(is1, s1);
  if (s1.find("bhb") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        this->bhb.push_back(stof(s1));
      } else {
        break;
      }

    }
  } else {
    cerr << "bhb error!" << endl;
    throw std::invalid_argument( "model error: bhb error" );
  }

  getline(is1, s1);
  if (s1.find("Wa") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        s_temp1.clear();
        s_temp2.clear();
        tokenize(s1, s_temp1, " \t\r\n");
        for (int i = 0; i < (int) s_temp1.size(); i++) {
          s_temp2.push_back(stof(s_temp1[i]));
        }
        this->Wa.push_back(s_temp2);
      } else {
        break;
      }

    }
  } else {
    cerr << "Wa error!" << endl;
    throw std::invalid_argument( "model error: Wa error" );
  }

  getline(is1, s1);
  if (s1.find("ba") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        this->ba.push_back(stof(s1));
      } else {
        break;
      }

    }
  } else {
    cerr << "ba error!" << endl;
    throw std::invalid_argument( "model error: ba error" );
  }

  getline(is1, s1);
  if (s1.find("va") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        this->va.push_back(stof(s1));
      } else {
        break;
      }

    }
  } else {
    cerr << "va error!" << endl;
    throw std::invalid_argument( "model error: va error" );
  }

  getline(is1, s1);
  if (s1.find("Wyc") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        s_temp1.clear();
        s_temp2.clear();
        tokenize(s1, s_temp1, " \t\r\n");
        for (int i = 0; i < (int) s_temp1.size(); i++) {
          s_temp2.push_back(this->h_drop_out_rate * stof(s_temp1[i]));
        }
        this->Wyc.push_back(s_temp2);
      } else {
        break;
      }

    }
  } else {
    cerr << "Wyc error!" << endl;
    throw std::invalid_argument( "model error: Wyc error" );
  }

  getline(is1, s1);
  if (s1.find("by") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        this->by.push_back(stof(s1));
      } else {
        break;
      }

    }
  } else {
    cerr << "by error!" << endl;
    throw std::invalid_argument( "model error: by error" );
  }

  getline(is1, s1);
  if (s1.find("h0") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        this->h0.push_back(stof(s1));
      } else {
        break;
      }

    }
  } else {
    cerr << "h0 error!" << endl;
    throw std::invalid_argument( "model error: h0 error" );
  }

  getline(is1, s1);
  if (s1.find("h0b") != string::npos) {
    while (getline(is1, s1)) {
      if ((int) s1.size() > 0) {
        this->h0b.push_back(stof(s1));
      } else {
        break;
      }

    }
  } else {
    cerr << "h0b error!" << endl;
    throw std::invalid_argument( "model error: h0b error" );
  }


  this->de = (int) (this->emb[0].size());
  this->ne = (int) (this->emb.size());
  //this->nall = (int)(this->Wgx[0].size());
  //this->nx = (int)(this->Wgx.size());
  this->nh = (int) (this->Ugh.size());
  this->nc = (int) (this->Wyc[0].size());

  cerr << "done." << endl;
  cerr << "emb size : " << this->emb.size() << " * " << this->emb[0].size()
       << endl;
  cerr << "Wgx size : " << this->Wgx.size() << " * " << this->Wgx[0].size()
       << endl;
  cerr << "Wgxb size : " << this->Wgxb.size() << " * " << this->Wgxb[0].size()
       << endl;
  cerr << "Whx size : " << this->Whx.size() << " * " << this->Whx[0].size()
       << endl;
  cerr << "Whxb size : " << this->Whxb.size() << " * " << this->Whxb[0].size()
       << endl;
  cerr << "Ugh size : " << this->Ugh.size() << " * " << this->Ugh[0].size()
       << endl;
  cerr << "Ughb size : " << this->Ughb.size() << " * " << this->Ughb[0].size()
       << endl;
  cerr << "Uhh size : " << this->Uhh.size() << " * " << this->Uhh[0].size()
       << endl;
  cerr << "Uhhb size : " << this->Uhhb.size() << " * " << this->Uhhb[0].size()
       << endl;
  cerr << "bg size : " << this->bg.size() << endl;
  cerr << "bgb size : " << this->bgb.size() << endl;
  cerr << "bh size : " << this->bh.size() << endl;
  cerr << "bhb size : " << this->bhb.size() << endl;
  cerr << "Wa size : " << this->Wa.size() << " * " << this->Wa[0].size()
       << endl;
  cerr << "ba size : " << this->ba.size() << endl;
  cerr << "va size : " << this->va.size() << endl;
  cerr << "Wyc size : " << this->Wyc.size() << " * " << this->Wyc[0].size()
       << endl;
  cerr << "by size : " << this->by.size() << endl;
  cerr << "h0 size : " << this->h0.size() << endl;
  cerr << "h0b size : " << this->h0b.size() << endl;

  trans_matrix(this->Wgx, this->Wgx);
  trans_matrix(this->Wgxb, this->Wgxb);
  trans_matrix(this->Whx, this->Whx);
  trans_matrix(this->Whxb, this->Whxb);
  trans_matrix(this->Ugh, this->Ugh);
  trans_matrix(this->Ughb, this->Ughb);
  trans_matrix(this->Uhh, this->Uhh);
  trans_matrix(this->Uhhb, this->Uhhb);
  //trans_matrix(this->Wa, this->Wa);
  trans_matrix(this->Wyc, this->Wyc);
}

Seq2label_GRU_search::Seq2label_GRU_search() {
  this->activation = TANH;
  this->x_drop_out_rate = 0.8;
  this->h_drop_out_rate = 0.5;
}

int Seq2label_GRU_search::classify(vector<int> input_vec,
                                   float &prob,
                                   vector<float> &alignment) {
  float max_temp = -INFINITY;
  float sum = 0.0;
  int max_out = -1;
  int xt_size = (int) input_vec.size();
  vector<vector<float> > xt;
  vector<float> h_tm1;
  vector<float> all_t;
  vector<float> all_t2;
  vector<float> z_t;
  vector<float> r_t;
  vector<float> ch_t;
  vector<float> ch_t2;
  vector<float> ch_t3;
  vector<float> h_t;
  vector<float> h_t2;
  vector<float> y_t;
  vector<vector<float> > h_t_src_all;
  vector<vector<float> > temp_z;

  y_t.clear();
  h_t_src_all.clear();

  //projection
  for (int i = 0; i < xt_size; i++) {
    vector<float> temp_x;
    for (int k = 0; k < this->de; k++) {
      temp_x.push_back(this->emb[input_vec[i]][k]);
    }
    xt.push_back(temp_x);
  }

  vector_copy(this->h0, h_tm1);

  for (int wordi = 0; wordi < xt_size; wordi++) {
    all_t.clear();
    all_t2.clear();
    z_t.clear();
    r_t.clear();
    ch_t.clear();
    ch_t2.clear();
    ch_t3.clear();
    h_t.clear();
    h_t2.clear();

    // z_t, r_t
    matrix_vector_mul(this->Wgx, xt[wordi], all_t);
    matrix_vector_mul(this->Ugh, h_tm1, all_t2);
    vector_add(all_t2, all_t);
    vector_add(this->bg, all_t);

    vector_sigm(all_t, all_t);
    for (int i = 0; i < this->nh; i++) {
      z_t.push_back(all_t[i]);
      r_t.push_back(all_t[i + this->nh]);
    }

    // ch_t
    vector_vector_mul(r_t, h_tm1, ch_t2); // element-wise product
    matrix_vector_mul(this->Uhh, ch_t2, ch_t3);
    matrix_vector_mul(this->Whx, xt[wordi], ch_t);
    vector_add(ch_t3, ch_t);
    vector_add(this->bh, ch_t);

    if (activation == SIGM) {
      vector_sigm(ch_t, ch_t);
    } else if (activation == TANH) {
      vector_tanh(ch_t, ch_t);
    } else {
      vector_relu(ch_t, ch_t);
    }

    // h_t
    vector_vector_mul(z_t, ch_t, h_t); // element-wise product
    for (int k = 0; k < this->nh; k++) {
      h_t2.push_back((1.0F - z_t[k]) * h_tm1[k]);
    }
    vector_add(h_t2, h_t);

    // h_tm1
    vector_copy(h_t, h_tm1);

    h_t_src_all.push_back(h_t);
  }

  vector_copy(this->h0b, h_tm1);

  for (int wordi = xt_size - 1; wordi >= 0; wordi--) {
    all_t.clear();
    all_t2.clear();
    z_t.clear();
    r_t.clear();
    ch_t.clear();
    ch_t2.clear();
    ch_t3.clear();
    h_t.clear();
    h_t2.clear();

    // z_t, r_t
    matrix_vector_mul(this->Wgxb, xt[wordi], all_t);
    matrix_vector_mul(this->Ughb, h_tm1, all_t2);
    vector_add(all_t2, all_t);
    vector_add(this->bgb, all_t);

    vector_sigm(all_t, all_t);
    for (int i = 0; i < this->nh; i++) {
      z_t.push_back(all_t[i]);
      r_t.push_back(all_t[i + this->nh]);
    }

    // ch_t
    vector_vector_mul(r_t, h_tm1, ch_t2); // element-wise product
    matrix_vector_mul(this->Uhhb, ch_t2, ch_t3);
    matrix_vector_mul(this->Whxb, xt[wordi], ch_t);
    vector_add(ch_t3, ch_t);
    vector_add(this->bhb, ch_t);

    if (activation == SIGM) {
      vector_sigm(ch_t, ch_t);
    } else if (activation == TANH) {
      vector_tanh(ch_t, ch_t);
    } else {
      vector_relu(ch_t, ch_t);
    }

    // h_t
    vector_vector_mul(z_t, ch_t, h_t); // element-wise product
    for (int k = 0; k < this->nh; k++) {
      h_t2.push_back((1.0F - z_t[k]) * h_tm1[k]);
    }
    vector_add(h_t2, h_t);

    // h_tm1
    vector_copy(h_t, h_tm1);

    //h_t_src_all.push_back(h_t);
    for (int i = 0; i < this->nh; i++) {
      h_t_src_all[wordi].push_back(h_t[i]);
    }
  }

  matrix_matrix_mul(h_t_src_all, this->Wa, temp_z);
  for (int i = 0; i < xt_size; i++) {
    vector_add(this->ba, temp_z[i]);
    vector_tanh(temp_z[i], temp_z[i]);
  }

  matrix_vector_mul(temp_z, this->va, alignment);

  vector_softmax2(alignment, alignment);

  hidden_alignment(alignment, h_t_src_all, h_t);


  matrix_vector_mul(this->Wyc, h_t, y_t);
  vector_add(this->by, y_t);

  //vector_softmax(y_t, y_t);

  for (int i = 0; i < this->nc; i++) {
    sum += y_t[i];
    if (y_t[i] > max_temp) {
      max_temp = y_t[i];
      max_out = i;
    }
    //cout << "classsss : " << i << " " << y_t[i] << endl;
  }

  for (int i = 0; i < this->nc; i++) {
    sum += expf(y_t[i] - max_temp);
  }
  max_temp = 1.0F / sum;

  prob = max_temp;
  //cerr << "classsss : " << max_out << " " << max_temp << endl;

  return max_out;
}
