#include "libmaum/brain/nlp/ta-analyzer.h"

TA_analyzer::TA_analyzer() {
  b_ne_oneword = 0;
  b_morp_oneword = 0;
}

TA_analyzer::~TA_analyzer() {
}

string TA_analyzer::ReplaceString(const string &FullString,
                                  const string &TargetStr,
                                  const string &DestStr) {
  string origin = FullString;
  string tStr = TargetStr;
  string dStr = DestStr;

  size_t pos = origin.find(tStr);
  // size_t a = string::npos;
  while (pos != string::npos) {
    origin.replace(pos, tStr.size(), dStr);
    pos = origin.find(tStr, pos + dStr.size());
    // a = string::npos;
  }

  return origin;
}

vector<string> TA_analyzer::split(const string &input, const string &delim) {
  vector<string> r;

  size_t s_pos = 0;
  size_t e_pos = 0;

  while (1) {
    e_pos = input.find(delim, s_pos);
    if (e_pos == string::npos) break;

    r.push_back(input.substr(s_pos, e_pos - s_pos));

    s_pos = e_pos + delim.size();
  }

  if (s_pos <= input.size()) {
    r.push_back(input.substr(s_pos));
  }

  return r;
}

void TA_analyzer::init_freq(const string &RSC) {
  string strLine;
  string ifs_str;

  ifs_str = RSC;
  ifs_str += "/freq_rule.txt";
  ifstream ifs((char *) ifs_str.c_str());
  if (ifs.fail()) {
    cerr << "ERR: " << ifs_str << " not exists\n";
  }
  while (getline(ifs, strLine)) {
    strLine = ReplaceString(strLine, "\r", "");
    strLine = ReplaceString(strLine, "\n", "");
    if ((int) strLine.size() == 0)
      continue;
    vec_rule.push_back(strLine);
  }
  ifs.close();
}

int TA_analyzer::lemma_type_check(const string &in_type,
                                  vector<string> vec_rule) {
  int len_rule = (int) vec_rule.size();
  // Base Rule
  if (in_type[0] == 'N' || in_type[0] == 'V' || in_type[0] == 'J'
      || in_type[0] == 'R' || in_type[0] == 'F' || in_type == "CD")
    return 0;
  // Append Rule
  for (int rid = 0; rid < len_rule; rid++) {
    if (vec_rule[rid] == in_type)
      return 0;
  }
  return 1;
}

void TA_analyzer::do_count_freq(E_Doc &edoc,
                                map<string, node> &dic_morp,
                                map<string, node> &dic_ne) {
  string key;
  int sent_size = (int) edoc.sentence.size();
  for (int sid = 0; sid < sent_size; sid++) {
    E_Sentence &sent = edoc.sentence[sid];
    decltype(sent.morp.size()) msize = sent.morp.size();
    for (decltype(msize) mid = 0; mid < msize; mid++) {
      E_Morp &morp = sent.morp[mid];
      //=======================================================================
      // check type!!!
      if (lemma_type_check(morp.type, vec_rule) == 1)
        continue;
      //=======================================================================
      key = morp.lemma + "/" + morp.type;
      if (dic_morp.find(key) != dic_morp.end())
        dic_morp[key].freq += 1;
      else {
        node tmp_node;
        tmp_node.key.append(morp.lemma);
        tmp_node.pos.append(morp.type);
        tmp_node.freq = 1;
        dic_morp[key] = tmp_node;
      }
    }

    decltype(sent.NE.size()) nsize = sent.NE.size();
    for (decltype(nsize) nid = 0; nid < nsize; nid++) {
      E_NE &ne = sent.NE[nid];
      if (ne.text.size() == 1)
        continue;
      key = ne.text + "/" + ne.type;
      if (dic_ne.find(key) != dic_ne.end())
        dic_ne[key].freq += 1;
      else {
        node tmp_node;
        tmp_node.key.append(ne.text);
        tmp_node.pos.append(ne.type);
        tmp_node.freq = 1;
        dic_ne[key] = tmp_node;
      }
    }
  }
}

void TA_analyzer::init_idx(const string &RSC) {
  string strLine;
  string ifs_str;

  ifs_str = RSC;
  ifs_str += "/idx_stop.txt";
  ifstream ifs((char *) ifs_str.c_str());
  if (ifs.fail()) {
    cerr << "ERR: " << ifs_str << " not exists\n";
  }
  while (getline(ifs, strLine)) {
    strLine = ReplaceString(strLine, "\r", "");
    strLine = ReplaceString(strLine, "\n", "");
    if ((int) strLine.size() == 0)
      continue;
    stop_map.insert(pair<string, int>(strLine, 1));
//		stop_vec.push_back(strLine) ;
  }
  ifs.close();

  ifs_str.clear();
  ifs_str = RSC;
  ifs_str += "/idx_stop_type.txt";
  ifs.open((char *) ifs_str.c_str());
  if (ifs.fail()) {
    cerr << "ERR: " << ifs_str << " not exists\n";
  }
  while (getline(ifs, strLine)) {
    strLine = ReplaceString(strLine, "\r", "");
    strLine = ReplaceString(strLine, "\n", "");
    if ((int) strLine.size() == 0)
      continue;
    stop_type_map.insert(pair<string, int>(strLine, 1));
//		stop_type_vec.push_back(strLine) ;
  }
  ifs.close();

  ifs_str.clear();
  ifs_str = RSC;
  ifs_str += "/idx_ne_stop.txt";
  ifs.open((char *) ifs_str.c_str());
  if (ifs.fail()) {
    cerr << "ERR: " << ifs_str << " not exists\n";
  }
  while (getline(ifs, strLine)) {
    strLine = ReplaceString(strLine, "\r", "");
    strLine = ReplaceString(strLine, "\n", "");
    if ((int) strLine.size() == 0)
      continue;
    ne_stop_map.insert(pair<string, int>(strLine, 1));
    // ne_stop_vec.push_back(strLine);
  }
  ifs.close();

  ifs_str.clear();
  ifs_str = RSC;
  ifs_str += "/idx_ne_stop_type.txt";
  ifs.open((char *) ifs_str.c_str());
  if (ifs.fail()) {
    cerr << "ERR: " << ifs_str << " not exists\n";
  }
  while (getline(ifs, strLine)) {
    strLine = ReplaceString(strLine, "\r", "");
    strLine = ReplaceString(strLine, "\n", "");
    if ((int) strLine.size() == 0)
      continue;
    ne_stop_type_map.insert(pair<string, int>(strLine, 1));
    // ne_stop_type_vec.push_back(strLine);
  }
  ifs.close();
}

int TA_analyzer::find_stopword(const string &str_lemma,
                               map<string, int> &stop_map) {
  return stop_map.find(str_lemma) != stop_map.end() ? 1 : 0;
}

int TA_analyzer::find_stopword(const string &str_lemma,
                               vector<string> &str_stop_vec) {
  int i = 0;
  for (const auto &s : str_stop_vec) {
    if (str_lemma == s) {
      return 1;
    }
    i++;
  }
  return 0;
}

void TA_analyzer::do_idx(E_Doc &edoc, const string &str_today,
                         string &str_idx,
                         string &str_vp, const string &fileName) {
  char char_sid[100];
  int sent_size = (int) edoc.sentence.size();
  for (int sid = 0; sid < sent_size; sid++) {
    E_Sentence &sent = edoc.sentence[sid];

    vector<int> vec_np;
    int nsize = (int) sent.NE.size();
    for (int nid = 0; nid < nsize; nid++) {
      if (find_stopword(sent.NE[nid].text, ne_stop_map) == 1)
        continue;
      if (((int) sent.NE[nid].type.size() >= 2 && sent.NE[nid].type[0] == 'I'
          && sent.NE[nid].type[1] == '-')
          || find_stopword(sent.NE[nid].type, ne_stop_type_map) == 1)
        continue;
      if (sent.NE[nid].begin == sent.NE[nid].end)
        vec_np.push_back(sent.NE[nid].begin);
      sprintf(char_sid, "%d", sid);
      str_idx +=
          str_today + sent.NE[nid].text + "\t" + sent.NE[nid].type + "\t0\t1\t"
              + fileName + "\t" + char_sid + "\n";
    }
    int vec_size = (int) vec_np.size();
    int msize = (int) sent.morp.size();

    for (int mid = 0; mid < msize; mid++) {
      // remove string list
      if (find_stopword(sent.morp[mid].lemma, stop_map) == 1)
        continue;
      // ==========================================================
      // remove pos list
      if (find_stopword(sent.morp[mid].type, stop_type_map) == 1)
        continue;
      // ==========================================================

      // NP Word
      if (sent.morp[mid].type[0] == 'N' || sent.morp[mid].type == "CD"
          || sent.morp[mid].type[0] == 'F') {
        // Using one word NE check
        bool b_check = true;
        for (int vec_i = 0; vec_i < vec_size; vec_i++) {
          if (vec_np[vec_i] == mid) {
            b_check = false;
            break;
          }
        }
        if (b_check) {
          sprintf(char_sid, "%d", sid);
          str_idx +=
              str_today + sent.morp[mid].lemma + "\tAT_NOT\t0\t1\t" + fileName
                  + "\t" + char_sid + "\n";
        }
      }
        // VP Word
      else if (sent.morp[mid].type[0] == 'V' || sent.morp[mid].type[0] == 'J'
          || sent.morp[mid].type[0] == 'R') {
        string vp_type = "VERB";
        if (sent.morp[mid].type[0] == 'J' || sent.morp[mid].type[0] == 'R')
          vp_type = "ADJ";
        sprintf(char_sid, "%d", sid);
        str_vp += str_today + sent.morp[mid].lemma + "\t" + vp_type + "\t0\t1\t"
            + fileName + "\t" + char_sid + "\n";
      }
    } // for morp size
  }// for sent size
}

string TA_analyzer::do_hmd_raw(E_Doc &edoc, const string &str_today,
                               const string &fileName) {
  char tmp[1024];
  string str_hmd = "";

  // be verb
  // plural form
  int sent_size = (int) edoc.sentence.size();
  for (int sid = 0; sid < sent_size; sid++) {
    E_Sentence &sent = edoc.sentence[sid];
    str_hmd += fileName + "\t";
    sprintf(tmp, "%d\t", sid);
    str_hmd += tmp;
    str_hmd += str_today + "\t" + sent.text + "\t";

    string str_morp = "", str_omorp = "", str_pos = "";

    int msize = (int) sent.morp.size();
    for (int mid = 0; mid < msize; mid++) {
      if (mid != 0) {
        str_morp += ' ';
        str_omorp += ' ';
        str_pos += ' ';
      }

      str_morp += sent.morp[mid].lemma;
      str_omorp += sent.word[mid].str;
      str_pos += sent.morp[mid].lemma + "/" + sent.morp[mid].type;
      // if (mid != 0) str_hmd += ' ' ;
      //   str_hmd += sent.morp[mid].lemma ;
    }
    // transform(str_omorp.begin(), str_omorp.end(), str_omorp.begin(), tolower);
    str_hmd += str_morp + "\n";
    // + "\t" + str_omorp + "\t" + str_pos + "\n";
    // str_hmd += "\n";
  }
  return str_hmd;
}

void TA_analyzer::do_Text_Morp(E_Doc &edoc, string &str_w2v) {
  int sent_size = (int) edoc.sentence.size();
  for (int sid = 0; sid < sent_size; sid++) {
    E_Sentence &sent = edoc.sentence[sid];
    int msize = (int) sent.morp.size();
    for (int mid = 0; mid < msize; mid++)
      str_w2v += sent.morp[mid].lemma + "/" + sent.morp[mid].type + " ";
    str_w2v += "\n";
  }
}

// ====================================================================================================================================
// new db type
void TA_analyzer::new_do_idx(E_Doc &edoc, const string &str_today,
                             const string &str_time,
                             string &str_idx, const string &fileName) {
  char char_sid[100];
  char char_tste[100];
  int sent_size = (int) edoc.sentence.size();
  for (int sid = 0; sid < sent_size; sid++) {
    E_Sentence &sent = edoc.sentence[sid];

    sprintf(char_tste, "%d\t%d\n", sent.ts, sent.te);

    vector<int> vec_np;
    int nsize = (int) sent.NE.size();
    for (int nid = 0; nid < nsize; nid++) {
//			if( b_oneword == 1 &&
      if ((int) sent.NE[nid].text.size() <= 1
          || find_stopword(sent.NE[nid].text, ne_stop_map) == 1)
        continue;
      if (((int) sent.NE[nid].type.size() >= 2 && sent.NE[nid].type[0] == 'I'
          && sent.NE[nid].type[1] == '-')
          || find_stopword(sent.NE[nid].type, ne_stop_type_map) == 1)
        continue;
      if (sent.NE[nid].begin == sent.NE[nid].end)
        vec_np.push_back(sent.NE[nid].begin);
      sprintf(char_sid, "%d", sid);
      // 20160812
      // final  : con_id fname sid ts te keyword ne_type sent_cnt date time channel side mon
      // origin : date/keyword ne_type ? ? fname sid
      // new db : keyword ne_type fname sid date time channel con_id side ts te
      //str_idx += str_today+sent.NE[nid].text+"\t"+sent.NE[nid].type+"\t0\t1\t"+fileName+"\t"+ char_sid +"\n" ;
      str_idx +=
          sent.NE[nid].text + "\t" + sent.NE[nid].type + "\t" + fileName + "\t"
              + char_sid + "\t" + str_today + "\t"
              + str_time + "\t";
      str_idx += edoc.channel + "\t" + edoc.con_id + "\t" + edoc.side + "\t"
          + char_tste;
    }
    int vec_size = (int) vec_np.size();
    int msize = (int) sent.morp.size();

    for (int mid = 0; mid < msize; mid++) {
      // remove string list
      if ((int) sent.morp[mid].lemma.size() <= 1
          || find_stopword(sent.morp[mid].lemma, stop_map) == 1)
        continue;
      // ==========================================================
      // remove pos list
      if (find_stopword(sent.morp[mid].type, stop_type_map) == 1)
        continue;
      // ==========================================================

      // NP Word
      if (sent.morp[mid].type[0] == 'N' || sent.morp[mid].type == "CD"
          || sent.morp[mid].type[0] == 'F') {
        // Using one word NE check
        bool b_check = true;
        for (int vec_i = 0; vec_i < vec_size; vec_i++) {
          if (vec_np[vec_i] == mid) {
            b_check = false;
            break;
          }
        }
        if (b_check) {
          sprintf(char_sid, "%d", sid);
          // 20160812
          // final  : con_id fname sid ts te keyword ne_type sent_cnt date time channel side mon
          // origin : date/keyword ne_type ? ? fname sid
          // new db : keyword ne_type fname sid date time channel con_id side ts te
          //str_idx += str_today + sent.morp[mid].lemma + "\tAT_NOT\t0\t1\t" + fileName + "\t" + char_sid + "\n" ;
          str_idx +=
              sent.morp[mid].lemma + "\tAT_NOT\t" + fileName + "\t" + char_sid
                  + "\t" + str_today + "\t" + str_time
                  + "\t";
          str_idx += edoc.channel + "\t" + edoc.con_id + "\t" + edoc.side + "\t"
              + char_tste;
        }
      }
        // VP Word
      else if (sent.morp[mid].type[0] == 'V' || sent.morp[mid].type[0] == 'J'
          || sent.morp[mid].type[0] == 'R') {
        string vp_type = "VERB";
        if (sent.morp[mid].type[0] == 'J' || sent.morp[mid].type[0] == 'R')
          vp_type = "ADJ";
        sprintf(char_sid, "%d", sid);
        // 20160812
        // final  : con_id fname sid ts te keyword ne_type sent_cnt date time channel side mon
        // origin : date/keyword ne_type ? ? fname sid
        // new db : keyword ne_type fname sid date time channel con_id side ts te
//				str_vp += str_today + sent.morp[mid].lemma + "\t" + vp_type + "\t0\t1\t" + fileName + "\t" + char_sid + "\n" ;
        str_idx +=
            sent.morp[mid].lemma + "\t" + vp_type + "\t" + fileName + "\t"
                + char_sid + "\t" + str_today + "\t"
                + str_time + "\t";
        str_idx += edoc.channel + "\t" + edoc.con_id + "\t" + edoc.side + "\t"
            + char_tste;
      }
    } // for morp size
  }// for sent size
}

string TA_analyzer::new_do_hmd_raw(E_Doc &edoc,
                                   const string &str_today,
                                   const string &str_time,
                                   const string &fileName) {
  char tmp[1024];
  string str_hmd = "";

  // be verb
  // plural form
  int sent_size = (int) edoc.sentence.size();
  for (int sid = 0; sid < sent_size; sid++) {
    E_Sentence &sent = edoc.sentence[sid];
    str_hmd += fileName + "\t";
    sprintf(tmp, "%d\t", sid);
    str_hmd += tmp;
    // 20160811
    str_hmd +=
        str_today + "\t" + str_time + "\t" + edoc.channel + "\t" + edoc.con_id
            + "\t" + edoc.side + "\t";
    sprintf(tmp, "%d\t%d\t", sent.ts, sent.te);
    str_hmd += tmp;
    str_hmd += sent.text + "\t";

    string str_morp = "", str_omorp = "", str_pos = "";

    int msize = (int) sent.morp.size();
    for (int mid = 0; mid < msize; mid++) {
      if (mid != 0) {
        str_morp += ' ';
        str_omorp += ' ';
        str_pos += ' ';
      }

      str_morp += sent.morp[mid].lemma;
      str_omorp += sent.word[mid].str;
      str_pos += sent.morp[mid].lemma + "/" + sent.morp[mid].type;
//			if (mid != 0) str_hmd += ' ' ;
//			str_hmd += sent.morp[mid].lemma ;
    }
//		transform(str_omorp.begin(), str_omorp.end(), str_omorp.begin(), tolower) ;
    str_hmd += str_morp + "\t" + str_omorp + "\t" + str_pos + "\n";
//		str_hmd += "\n" ;
  }
  return str_hmd;
}

