#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

// activation function
#define SIGM    1
#define TANH    2
#define RELU    3


/**
 *
 * @author hhs, modified by leeck
 */

/////////////////////////////////////////////////////////////////////////////////////////////
class Seq2label_LSTM_encoder {
 public:
  Seq2label_LSTM_encoder();
  void load_model(const string &file_name);
  int classify(vector<int> input_vec, float &prob);

  ///////////////////////////////////////////////////////////////////////////
  vector<int> multy_classify(vector<int> input_vec, vector<float> &vec_prob);
  ///////////////////////////////////////////////////////////////////////////

  int activation;
  float x_drop_out_rate;
  float h_drop_out_rate;

 private:
  int nh;
  int nc;
  int ne;
  int de;
  int nall;
  int nx;
  int xh;
  vector<vector<float> > emb;
  vector<vector<float> > Wgx;
  vector<vector<float> > Ugh;
  vector<float> bg;
  vector<float> h0;
  vector<float> cell0;
  vector<vector<float> > Wyc;
  vector<float> by;
};

