#if !defined(TA_ENALYZER_INCLUDED_)
#define TA_ENALYZER_INCLUDED_

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>

#include "e-doc.h"

using namespace std;

struct node {
  node(const string &a_key, const string &a_pos, int a_freq)
      : key(a_key),
        pos(a_pos),
        freq(a_freq) {
  }
  node() {
  }
  string key;
  string pos;
  int freq;
};

class TA_analyzer {
 public:
  TA_analyzer();
  virtual ~TA_analyzer();

  string ReplaceString(const string &FullString,
                       const string &TargetStr,
                       const string &DestStr);
  vector<string> split(const string &input, const string &delim);

  // HMD raw
  string do_hmd_raw(E_Doc &edoc, const string &str_today, const string &fileName);
  string new_do_hmd_raw(E_Doc &edoc, string const &str_today, const string &str_time,
                        const string &fileName);

  // W2V
  void do_Text_Morp(E_Doc &edoc, string &str_w2v);

  // Count morp, ne
  void init_freq(const string &RSC);
  int lemma_type_check(const string &in_type, vector<string> vec_rule);
  void do_count_freq(E_Doc &edoc,
                     map<string, node> &dic_morp,
                     map<string, node> &dic_ne);

  // map<string, node> dic_morp, dic_ne;
  vector<string> vec_rule;

  // make Index
  void init_idx(const string &RSC);
  int find_stopword(const string &str_lemma, vector<string> &stop_vec);
  int find_stopword(const string &str_lemma, map<string, int> &stop_map);
  void do_idx(E_Doc &edoc,
              const string &str_today,
              string &str_idx,
              string &str_vp,
              const string &fileName);
  void new_do_idx(E_Doc &edoc,
                  const string &str_today, const string &str_time,
                  string &str_idx, const string &fileName);

  int b_ne_oneword, b_morp_oneword;

  vector<string> stop_vec;
  vector<string> stop_type_vec;
  vector<string> ne_stop_vec;
  vector<string> ne_stop_type_vec;
  map<string, int> stop_map;
  map<string, int> stop_type_map;
  map<string, int> ne_stop_map;
  map<string, int> ne_stop_type_map;
};

#endif