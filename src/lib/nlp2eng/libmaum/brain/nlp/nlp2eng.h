#ifndef NLP2ENG_H
#define NLP2ENG_H

#include "libmaum/brain/nlp/nlp.h"

using maum::brain::nlp::NlpAnalysisLevel;

class Eng_analyzer;
class Nlp2Eng : public Nlp {
 public:
  Nlp2Eng(const string &res_path, int nlp_feature_mask)
      : Nlp(res_path, nlp_feature_mask) {

  }

  virtual ~Nlp2Eng() {

  }

  void UpdateFeatures() override;
  void AnalyzeOne(const InputText *text, Document *document) override;
  void Uninit() override;

  void SetClassifierModel(const string &classifier_model_path) override;
  void Classify(const Document &document, ClassifiedSummary *sum) override;
  void GetPosTaggedStr(const Document *document,
                       vector<string> *pos_tagged_str) override;
  void GetNerTaggedStr(const Document *document,
                       vector<string> *ner_tagged_str) override;
  void GetPosNerTaggedStr(const Document *document,
                       vector<string> *pos_ner_tagged_str) override {};


 private:
  std::shared_ptr<Eng_analyzer> anal_;

};

#endif //NLP2ENG_H
