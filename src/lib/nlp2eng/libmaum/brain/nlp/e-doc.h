#ifndef E_DOC_H
#define E_DOC_H

#include <string>
#include <vector>

using namespace std;

struct E_Morp {
  int id;
  string lemma;
  string type;
  int position;
};

struct E_Word {
  int id;
  string str;
  string type;
  int position;
};

struct E_NE {
  int id;
  string text;
  string type;
  int begin;
  int end;
};

struct E_SA {
  string type;
  float prob;
};

struct E_Sentence {
  int id;
  string text;
  vector<E_Morp> morp;
  vector<E_Word> word;
  vector<E_NE> NE;
  E_SA SA;
  // string SA ;
  // float sa_prob ;
  // 20160809
  int ts;
  int te;
};

struct E_Doc {
  vector<E_Sentence> sentence;
  string text;
  // 20160809
  string channel;
  string con_id;
  string side;
};

#endif
