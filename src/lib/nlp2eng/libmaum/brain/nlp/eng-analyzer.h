#if !defined(ENG_ANALIZER_INCLUDED_)
#define ENG_ANALIZER_INCLUDED_

#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <set>
#include <map>
#include <libmaum/brain/nlp/e-doc.h>

using namespace std;

class POS_tagger;
class NER;
class SA_NN;

#ifndef ENG_WORD_T_DEFINED
#define ENG_WORD_T_DEFINED
// word type
typedef struct word_struct {
  // id: 1부터 시작
  int id;
  // word string
  string str;
  // lemma
  string lemma;
  // POS tag
  string pos_tag;
  // gold POS tag: for conll2008
  string gpostag;
  // split form, lemma, postag : for conll2008
  string split_form, split_lemma, split_postag;
  // NE tag
  string ne_tag;
  // Chunk tag
  string chunk_tag;
  // head (dependency parsing): root = 0, (1부터 시작)
  int head;
  // dependency label
  string dep_label;
  // SRL infomation: predicate sense, argument, arg_info vector(id, string)
  string pred;
  vector<string> arg; // for conll format
  vector<pair<int, string> > arg_info; // 현재 pred에 할당된 arg의 id, string
} word_t;
#endif


struct ESent {
  string text;
  string SA;
  float sentiment_prob;
  vector<word_t> word;

  // 20160809
  int ts;
  int te;
};

struct EDoc {
  string text;
  string sentiment;
  float sentiment_prob;
  vector<ESent> sentence;

  // 20160809
  string channel;
  string con_id;
  string side;
};

class Eng_analyzer {
 public:
  Eng_analyzer();
  virtual ~Eng_analyzer();

  // POS tagging
  void init_pos_tagger(const string &dir);
  void close_pos_tagger();
  void do_pos_tagging(const string &input,
                      EDoc &edoc,
                      int split_sent = 1,
                      int use_tokenize = 1);
  string sprint_pos_tagging(EDoc &edoc);

  // NE tagging
  void init_ner(const string &dir);
  void close_ner();
  void do_ner(EDoc &edoc);
  string sprint_ner(EDoc &edoc);

  // SA tagging
  void init_sa(const string &dir, const string &res);
#if 0
  void init_sa(const string &dir, const string &proj);
#endif
  void close_sa();
  void do_sa(EDoc &edoc);
  void do_sa_doc(EDoc &edoc);
  vector<string> do_single_sa(E_Doc &edoc, vector<float> &vec_prob);
  vector<vector<string> > do_multy_sa(E_Doc &edoc,
                                      vector<vector<float> > &vec_prob);
  // JSON Print
  string sprint_EJSON(E_Doc &edoc, int human_readable);
  string sprint_JSON(EDoc &edoc, int human_readable);
  E_Doc Make_JSON(EDoc &edoc);

  // yghwang 20171130 for add vector<stroing> Eng_analyzer::get_postagged_str(EDoc &edoc);
  vector<string> get_postagged_str(EDoc &edoc);

  // POS tagger, NER, SA
  POS_tagger *pos_tagger;
  NER *ner;
  SA_NN *sa;
};

#endif // ENG_ANALIZER_INCLUDED_
