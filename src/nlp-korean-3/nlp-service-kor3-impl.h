#ifndef NLP_SERVICE_KOR3_IMPL_H
#define NLP_SERVICE_KOR3_IMPL_H

#include "maum/brain/nlp/nlp.grpc.pb.h"
#include "maum/brain/nlp/nlp3_custom.grpc.pb.h"
#include <libmaum/brain/nlp/nlp3kor.h>
#include <string>
#include <vector>
#include <grpc++/grpc++.h>
#include <mutex>
#include <condition_variable>
#include <unordered_set>
#include <atomic>
#include <libmaum/common/config.h>
#include <libmaum/rpc/result-status.h>

using grpc::ServerContext;
using grpc::Status;
using grpc::StatusCode;
using grpc::ServerReaderWriter;
using google::protobuf::Empty;
using maum::brain::nlp::NaturalLanguageProcessingService;
using maum::brain::nlp::Nlp3CustomizeService;
using maum::brain::nlp::NlpProvider;
using maum::brain::nlp::InputText;
using maum::brain::nlp::Document;
using maum::brain::nlp::NlpFeatures;
using maum::brain::nlp::NlpFeatureSupportList;
using maum::brain::nlp::NlpAnalysisLevel;
using maum::brain::nlp::NlpDict;
using maum::brain::nlp::TaggedList;
using maum::brain::nlp::ChangedNerDict;
using maum::brain::nlp::MorpDictList;
using maum::brain::nlp::NamedEntityDictList;
using maum::brain::nlp::NamedEntityConfig;
using maum::brain::nlp::NamedEntityTagMap;
using maum::brain::nlp::FindWordRequest;
using maum::brain::nlp::FindWordResponse;
using maum::brain::nlp::ReplacementDictList;
using std::string;
using std::atomic;
using libmaum::rpc::CommonResultStatus;

class N_Doc;

class NlpKorean3Impl
    : public NaturalLanguageProcessingService::Service,
      public Nlp3CustomizeService::Service {
 public:
  NlpKorean3Impl();
  virtual ~NlpKorean3Impl();
  const char *Name() {
    return "NLP Korean #3 Service";
  }

  Status GetProvider(ServerContext *context,
                     const Empty *empty,
                     NlpProvider *provider) override;
  Status Analyze(ServerContext *context,
                 const InputText *text,
                 Document *document) override;
  Status AnalyzeMultiple(ServerContext *context,
                         ServerReaderWriter<Document,
                                            InputText> *stream) override;
  Status HasSupport(ServerContext *context,
                    const ::maum::brain::nlp::NlpFeatures *request,
                    NlpFeatureSupportList *response) override;

  Status ApplyDict(ServerContext *context,
                   const NlpDict *dict, Empty *empty) override;

  Status GetPosTaggedList(ServerContext *context,
                          const Document *document,
                          TaggedList *taggedList) override;

  Status GetNerTaggedList(ServerContext *context,
                          const Document *document,
                          TaggedList *taggedList) override;

  Status GetPosNerTaggedList(ServerContext *context,
                             const Document *document,
                             TaggedList *taggedList) override;

  Status UpdateNerDict(ServerContext *context,
                       const NerDict *nerDict,
                       Empty *empty) override;

  Status UpdatePostNerDict(ServerContext *context,
                           const NerDict *request,
                           Empty *response) override;
  Status UpdatePreNerDict(ServerContext *context,
                          const NerDict *request,
                          Empty *response) override;
  Status UpdatePostChangeNerDict(ServerContext *context,
                                 const ChangedNerDict *request,
                                 Empty *response) override;

  Status UpdateMorpDictList(ServerContext *context,
                            const MorpDictList *request,
                            Empty *response) override;
  Status UpdateNamedEntityDictList(ServerContext *context,
                                   const NamedEntityDictList *request,
                                   Empty *response) override;
  Status UpdateNamedEntityConfig(ServerContext *context,
                                 const NamedEntityConfig *request,
                                 Empty *response) override;

  Status GetNamedEntityTagMap(ServerContext *context,
                              const Empty *request,
                              NamedEntityTagMap *response) override;
  Status UpdateNamedEntityTagMap(ServerContext *context,
                                 const NamedEntityTagMap *request,
                                 Empty *response) override;

  Status FindWord(ServerContext *context,
                  const FindWordRequest *request,
                  FindWordResponse *response) override;

  Status AnalyzeWithSpace(ServerContext *context,
                          const InputText *text,
                          Document *document) override;

  Status GetReplacementDict(ServerContext *context,
                            const Empty *request,
                            ReplacementDictList *response) override;

  Status UpdateReplacementDict(ServerContext *context,
                               const ReplacementDictList *request,
                               Empty *response) override;

  void Start(bool lock = true);
  void SetNlpFeature(int feature) {
    nlp_feature_ = feature;
  }
 private:
  void AnalyzeOne(const InputText *text, Document *document);
  void LoadConfig();

  Nlp3Kor *nlp3Kor_;
  // nlp-kor3 리소스 경로
  std::string rsc_path_;
  int nlp_feature_ = 0;

  void Stop(bool lock = true);
  bool CanService() {
    std::lock_guard<std::mutex> guard(m_);
    return loaded_ && !loading_;
  }
  void LockRunning() {
    {
      std::lock_guard<std::mutex> guard(m_);
      running_++;
    }
    cv_.notify_one();
  }
  void UnlockRunning() {
    {
      std::lock_guard<std::mutex> guard(m_);
      running_--;
    }
    cv_.notify_one();
  }
  bool loaded_ = false;
  bool loading_ = false;
  int32_t running_ = 0;
  std::mutex m_;
  std::condition_variable cv_;
  class Accessor {
   public:
    Accessor(NlpKorean3Impl *ptr) : parent_(ptr) {
      parent_->LockRunning();
    }
    ~Accessor() {
      parent_->UnlockRunning();
    }
   private:
    NlpKorean3Impl *parent_ = nullptr;
  };

  void Restart();

};

#endif //NLP_SERVICE_KOR3_IMPL_H
