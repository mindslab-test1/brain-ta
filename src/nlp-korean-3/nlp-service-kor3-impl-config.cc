#include "nlp-service-kor3-impl.h"

#include <libmaum/common/config.h>
#include <fstream>

using std::ifstream;

void NlpKorean3Impl::Restart() {
  auto logger = LOGGER();
  std::unique_lock<std::mutex> lock(m_);
  logger->debug("restart waiting... ");
  cv_.wait(lock, [this] { return running_ == 0; });

  logger->debug("calling stop ... ");
  Stop(false);
  logger->debug("calling start ... ");
  Start(false);
}

/**
  @breif nlp-kor3 리소스 경로 설정하기 위한 함수
  @param None
  @return None
*/
void NlpKorean3Impl::LoadConfig() {
  auto &c = libmaum::Config::Instance();
  rsc_path_ = c.Get("brain-ta.resource.3.kor.path");
}
