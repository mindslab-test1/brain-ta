#include <thread>

#include "nlp-service-kor3-impl.h"
#include "custom-dict.h"

using std::unique_ptr;
using maum::brain::nlp::NlpFeature;
using maum::brain::nlp::NlpAnalysisLevel;

#define ERROR_INDEX_MAJOR 0000

NlpKorean3Impl::NlpKorean3Impl() : running_(0) {
  LoadConfig();
}

/**
  @breif 서버 실행시 초기화 하는 함수
  @param bool lock : 서버 가동 여부를 확인하기 위해
  @return None
*/
void NlpKorean3Impl::Start(bool lock) {
  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = true;
    loaded_ = false;
  } else {
    loading_ = true;
    loaded_ = false;
  }

  nlp3Kor_ = new Nlp3Kor(rsc_path_, nlp_feature_);
  nlp3Kor_->UpdateFeatures();
  // init ne tag map
  nlp3Kor_->LoadNeTagMap();
  // init space dict map
  nlp3Kor_->LoadSpaceDict();

  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = false;
    loaded_ = true;
  } else {
    loading_ = false;
    loaded_ = true;
  }
}

void NlpKorean3Impl::Stop(bool lock) {
  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = true;
    loaded_ = false;
  } else {
    loading_ = true;
    loaded_ = false;
  }
  nlp3Kor_->Uninit();

}

NlpKorean3Impl::~NlpKorean3Impl() {
  Stop();
}

/**
  @breif 사용자에게 입력받은 분석 레벨에 따라서 해당 모듈 초기화 및 분석하는 함수
  @param InputText *text : 클라이언트에서 보낸 message
  @param Document *document : 언어분석 결과를 클라이언트에 보낼 message
  @return None
*/
void NlpKorean3Impl::AnalyzeOne(const InputText *text, Document *document) {
  nlp3Kor_->AnalyzeOne(text, document);
}

/**
  @breif 클라이언트에서 언어분석을 위해 호출하는 함수
  @param ServerContext *context : grpc 통신을 수행하기 위해 필요한 instance
  @param InputText *text : 클라이언트에서 보낸 message
  @param Document *document : 언어분석 결과를 클라이언트에 보낼 message
  @return grpc Status Code
*/
Status NlpKorean3Impl::Analyze(ServerContext *context,
                               const InputText *text,
                               Document *document) {
  if (!CanService()) {
    return Status(StatusCode::INTERNAL, "Loading.....");
  }
  Accessor(this);
  AnalyzeOne(text, document);
  return Status::OK;
}

Status NlpKorean3Impl::AnalyzeMultiple(ServerContext *context,
                                       ServerReaderWriter<Document,
                                                          InputText> *stream) {
  if (!CanService()) {
    return Status(StatusCode::INTERNAL, "Loading.....");
  }
  Accessor(this);
  InputText text;
  while (stream->Read(&text)) {
    Document out;
    AnalyzeOne(&text, &out);
    stream->Write(out);
  }
  return Status::OK;
}

/**
  @breif 엔진 공급자에 대한 정보를 제공하는 함수
  @param ServerContext *context : grpc 통신을 수행하기 위해 필요한 instance
  @param Empty *empty : google protobuf에서 제공하는 generic empty message
  @param NlpProvider *provider : 엔진 공급 정보를 클라이언트에 보낼 message
  @return grpc Status Code
*/
Status NlpKorean3Impl::GetProvider(ServerContext *context,
                                   const Empty *empty,
                                   NlpProvider *provider) {
  provider->set_name("MindsLAB NLP #3");
  provider->set_vendor("ETRI");
  provider->set_version("1.0");
  provider->set_description("MindsLAB NLP #3 Korean from ETRI");
  provider->set_support_encoding("utf-8");

  return Status::OK;
}

Status NlpKorean3Impl::HasSupport(ServerContext *context,
                                  const ::maum::brain::nlp::NlpFeatures *request,
                                  NlpFeatureSupportList *response) {
  for (auto f : request->features()) {
    switch (f) {
      case NlpFeature::NLPF_SPACE: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(nlp3Kor_->HasFeature(NlpFeature(f)));
        break;
      }
      case NlpFeature::NLPF_WORD: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(nlp3Kor_->HasFeature(NlpFeature(f)));
        break;
      }
      case NlpFeature::NLPF_MORPHEME: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(nlp3Kor_->HasFeature(NlpFeature(f)));
        break;
      }
      case NlpFeature::NLPF_NAMED_ENTITY: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(nlp3Kor_->HasFeature(NlpFeature(f)));
        break;
      }
      case NlpFeature::NLPF_CHUNK: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(nlp3Kor_->HasFeature(NlpFeature(f)));
        break;
      }
      case NlpFeature::NLPF_PARSER: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(nlp3Kor_->HasFeature(NlpFeature(f)));
        break;
      }
      case NlpFeature::NLPF_SEMANTIC_ROLE_LABELING: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(nlp3Kor_->HasFeature(NlpFeature(f)));
        break;
      }
      case NlpFeature::NLPF_ZERO_ANAPHORA: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(nlp3Kor_->HasFeature(NlpFeature(f)));
        break;
      }
      case NlpFeature::NLPF_SETIMENT: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(nlp3Kor_->HasFeature(NlpFeature(f)));
        break;
      }
      case NlpFeature::NLPF_DNN: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(nlp3Kor_->HasFeature(NlpFeature(f)));
        break;
      }
      default: {
        break;
      }
    }
  }
  return Status::OK;
}

Status NlpKorean3Impl::GetPosTaggedList(ServerContext *context,
                                        const Document *document,
                                        TaggedList *taggedList) {
  vector<string> tagged_lists;
  nlp3Kor_->GetPosTaggedStr(document, &tagged_lists);
  for (auto &tagged_str : tagged_lists) {
    taggedList->add_result(tagged_str);
  }

  return Status::OK;
}

Status NlpKorean3Impl::GetNerTaggedList(ServerContext *context,
                                        const Document *document,
                                        TaggedList *taggedList) {
  vector<string> tagged_lists;
  nlp3Kor_->GetNerTaggedStr(document, &tagged_lists);
  for (auto &tagged_str : tagged_lists) {
    taggedList->add_result(tagged_str);
  }

  return Status::OK;
}

Status NlpKorean3Impl::GetPosNerTaggedList(ServerContext *context,
                                           const Document *document,
                                           TaggedList *taggedList) {
  vector<string> tagged_lists;
  nlp3Kor_->GetPosNerTaggedStr(document, &tagged_lists);
  for (auto &tagged_str : tagged_lists) {
    taggedList->add_result(tagged_str);
  }

  return Status::OK;
}

Status NlpKorean3Impl::UpdateNerDict(ServerContext *context,
                                     const NerDict *nerDict,
                                     Empty *empty) {
  nlp3Kor_->UpdateNerDict(nerDict);
  return Status::OK;
}

Status NlpKorean3Impl::UpdatePreNerDict(ServerContext *context,
                                        const NerDict *nerDict,
                                        Empty *empty) {
  nlp3Kor_->UpdatePreNerDict(nerDict);
  return Status::OK;
}

Status NlpKorean3Impl::UpdatePostNerDict(ServerContext *context,
                                         const NerDict *nerDict,
                                         Empty *empty) {
  nlp3Kor_->UpdatePostNerDict(nerDict);
  return Status::OK;
}

Status NlpKorean3Impl::UpdatePostChangeNerDict(ServerContext *context,
                                               const ChangedNerDict *nerDict,
                                               Empty *empty) {
  nlp3Kor_->UpdatePostChangeNerDict(nerDict);
  return Status::OK;
}

Status NlpKorean3Impl::ApplyDict(ServerContext *context,
                                 const NlpDict *dict,
                                 Empty *empty) {
  CustomDict cd(*dict, this);

  if (cd.Download() < 0) {
    return Status(StatusCode::INTERNAL, "download failed");
  }
  if (cd.Apply()) {
    return Status(StatusCode::INTERNAL, "apply failed");
  }
  cd.Callback();

  // 다른 요청에 대해서 처리할 수 없도록 변경한다.
  // 요청이 처리중이면 처리 완료할 때까지 대기한다.
  std::thread restart = std::thread(&NlpKorean3Impl::Restart, this);
  restart.detach();
  return Status::OK;
}

Status NlpKorean3Impl::UpdateMorpDictList(ServerContext *context,
                                          const MorpDictList *morpDicList,
                                          Empty *empty) {
  int state = nlp3Kor_->UpdateMorpDictList(morpDicList);
  if (state == -1) {
    Status status = ResultGrpcStatus(ExCode::REMOTE_CALL_GRPC_ERROR,
                                     ERROR_INDEX(1100),
                                     "[{}] {}",
                                     context->peer().c_str(),
                                     "Time out error");
    LOGGER()->error(CommonResultStatus::GetErrorString(status));
    return status;
  }

  return Status::OK;
}

Status NlpKorean3Impl::UpdateNamedEntityDictList(ServerContext *context,
                                                 const NamedEntityDictList *neDictList,
                                                 Empty *empty) {
  int state = nlp3Kor_->UpdateNamedEntityDictList(neDictList);
  if (state == -1) {
    Status status = ResultGrpcStatus(ExCode::REMOTE_CALL_GRPC_ERROR,
                                     ERROR_INDEX(1100),
                                     "[{}] {}",
                                     context->peer().c_str(),
                                     "Time out error");
    LOGGER()->error(CommonResultStatus::GetErrorString(status));
    return status;
  }

  return Status::OK;
}

Status NlpKorean3Impl::UpdateNamedEntityConfig(ServerContext *context,
                                               const NamedEntityConfig *neConfig,
                                               Empty *empty) {
  nlp3Kor_->UpdateNamedEntityConfig(neConfig);
  return Status::OK;
}

Status NlpKorean3Impl::GetNamedEntityTagMap(ServerContext *context,
                                            const Empty *empty,
                                            NamedEntityTagMap *neTagMap) {
  nlp3Kor_->GetNamedEntityTagMap(neTagMap);
  return Status::OK;
}

Status NlpKorean3Impl::UpdateNamedEntityTagMap(ServerContext *context,
                                               const NamedEntityTagMap *neTagMap,
                                               Empty *empty) {
  nlp3Kor_->UpdateNamedEntityTagMap(neTagMap);
  return Status::OK;
}

Status NlpKorean3Impl::FindWord(ServerContext *context,
                                const FindWordRequest *inputDict,
                                FindWordResponse *outputDict) {
  nlp3Kor_->FindWord(inputDict, outputDict);
  return Status::OK;
}

// 치환 사전 관련 GRPC 함수
Status NlpKorean3Impl::AnalyzeWithSpace(ServerContext *context,
                                        const InputText *text,
                                        Document *document) {
  if (!CanService()) {
    return Status(StatusCode::INTERNAL, "Loading.....");
  }

  Accessor(this);
  nlp3Kor_->SpaceAndAnalyzeOne(text, document);
  return Status::OK;
}

Status NlpKorean3Impl::GetReplacementDict(ServerContext *context,
                                          const Empty *empty,
                                          ReplacementDictList *reDictList) {
  nlp3Kor_->GetReplaceDict(reDictList);
  return Status::OK;
}

Status NlpKorean3Impl::UpdateReplacementDict(ServerContext *context,
                                             const ReplacementDictList *reDictList,
                                             Empty *empty) {
  bool ret = nlp3Kor_->UpdateReplaceDict(reDictList);
  if (ret)
    return Status::OK;
  else
    return Status(StatusCode::INTERNAL, "Invalid AUTH KEY...");
}
