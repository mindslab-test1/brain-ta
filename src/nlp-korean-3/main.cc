#include <grpc++/grpc++.h>
#include "nlp-service-kor3-impl.h"
#undef MAX
#include <getopt.h>
#include <cinttypes>
#include <gitversion/version.h>
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

using namespace std;

void RunServer(const char *prog, int &nlp_feature) {
  libmaum::Config &c = libmaum::Config::Instance();
  std::string server_address("0.0.0.0:");
  server_address += c.Get("brain-ta.nlp.3.kor.port");
  int grpc_timeout = atoi(c.Get("brain-ta.nlp.3.kor.timeout").c_str());
  string mode_space = c.Get("brain-ta.nlp.3.kor.space");
  string mode_morp = c.Get("brain-ta.nlp.3.kor.morp");
  string mode_ner = c.Get("brain-ta.nlp.3.kor.ner");
  string mode_wsd = c.Get("brain-ta.nlp.3.kor.wsd");
  string mode_dp = c.Get("brain-ta.nlp.3.kor.dp");
  string mode_srl = c.Get("brain-ta.nlp.3.kor.srl");
  string mode_za = c.Get("brain-ta.nlp.3.kor.za");
  string mode_all = c.Get("brain-ta.nlp.3.kor.module.all");

  transform(mode_morp.begin(), mode_morp.end(), mode_morp.begin(), ::tolower);
  transform(mode_ner.begin(), mode_ner.end(), mode_ner.begin(), ::tolower);
  transform(mode_wsd.begin(), mode_wsd.end(), mode_wsd.begin(), ::tolower);
  transform(mode_dp.begin(), mode_dp.end(), mode_dp.begin(), ::tolower);
  transform(mode_srl.begin(), mode_srl.end(), mode_srl.begin(), ::tolower);
  transform(mode_za.begin(), mode_za.end(), mode_za.begin(), ::tolower);
  transform(mode_all.begin(), mode_all.end(), mode_all.begin(), ::tolower);

  c.DumpPid();

  NlpKorean3Impl service;

  if (nlp_feature == 0) {
    if (!mode_space.compare("true")) {
      nlp_feature |= (1 << NlpFeature::NLPF_SPACE);
    }

    if (!mode_morp.compare("true")) {
      nlp_feature |= (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION);
      nlp_feature |= (1 << NlpFeature::NLPF_MORPHEME);
    }

    if (!mode_ner.compare("true")) {
      nlp_feature |= (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION);
      nlp_feature |= (1 << NlpFeature::NLPF_MORPHEME);
      nlp_feature |= (1 << NlpFeature::NLPF_NAMED_ENTITY);
    }

    if (!mode_wsd.compare("true")) {
      nlp_feature |= (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION);
      nlp_feature |= (1 << NlpFeature::NLPF_MORPHEME);
      //nlp_feature |= (1 << NlpFeature::NLPF_NAMED_ENTITY);
      nlp_feature |= (1 << NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION);
    }

    if (!mode_dp.compare("true")) {
      nlp_feature |= (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION);
      nlp_feature |= (1 << NlpFeature::NLPF_MORPHEME);
      //nlp_feature |= (1 << NlpFeature::NLPF_NAMED_ENTITY);
      //nlp_feature |= (1 << NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION);
      nlp_feature |= (1 << NlpFeature::NLPF_DEPENDENCY_PARSER);
    }

    if (!mode_srl.compare("true")) {
      nlp_feature |= (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION);
      nlp_feature |= (1 << NlpFeature::NLPF_MORPHEME);
      //nlp_feature |= (1 << NlpFeature::NLPF_NAMED_ENTITY);
      //nlp_feature |= (1 << NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION);
      //nlp_feature |= (1 << NlpFeature::NLPF_DEPENDENCY_PARSER);
      nlp_feature |= (1 << NlpFeature::NLPF_SEMANTIC_ROLE_LABELING);
    }

    if (!mode_za.compare("true")) {
      nlp_feature |= (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION);
      nlp_feature |= (1 << NlpFeature::NLPF_MORPHEME);
      //nlp_feature |= (1 << NlpFeature::NLPF_NAMED_ENTITY);
      //nlp_feature |= (1 << NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION);
      //nlp_feature |= (1 << NlpFeature::NLPF_DEPENDENCY_PARSER);
      //nlp_feature |= (1 << NlpFeature::NLPF_SEMANTIC_ROLE_LABELING);
      nlp_feature |= (1 << NlpFeature::NLPF_ZERO_ANAPHORA);
    }

    if (!mode_all.compare("true")) {
      nlp_feature |= (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION);
      nlp_feature |= (1 << NlpFeature::NLPF_MORPHEME);
      nlp_feature |= (1 << NlpFeature::NLPF_NAMED_ENTITY);
      nlp_feature |= (1 << NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION);
      nlp_feature |= (1 << NlpFeature::NLPF_DEPENDENCY_PARSER);
      nlp_feature |= (1 << NlpFeature::NLPF_SEMANTIC_ROLE_LABELING);
      nlp_feature |= (1 << NlpFeature::NLPF_ZERO_ANAPHORA);
    }
  }

  service.SetNlpFeature(nlp_feature);

  ServerBuilder builder;
  builder.AddChannelArgument(GRPC_ARG_MAX_CONNECTION_IDLE_MS, grpc_timeout);
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  builder.RegisterService(dynamic_cast<NaturalLanguageProcessingService::Service *> (&service));
  builder.RegisterService(dynamic_cast<Nlp3CustomizeService::Service *>(&service));
  unique_ptr<Server> server(builder.BuildAndStart());

  if (server) {
    LOGGER()->info("{} {} server listening on {} with {}",
                   prog,
                   version::VERSION_STRING,
                   server_address,
                   service.Name());
    service.Start();
    server->Wait();
  }
}

// gitversion을 확인하기 위한 함수
void Help() {
  cout << "Usage: ./brain-nlp-kor3d [OPTION]" << endl
       << " NLP init level " << endl
       << "  -v, --version     ; Show version info and exit." << endl
       << "  -h, --help        ; Print thie message, or per-command usage."
       << endl
       << "      --space       ; Optional loaded space." << endl
       << "      --morp        ; loaded morpheme." << endl
       << "      --ner         ; loaded named entity." << endl
       << "      --wsd         ; loaded word sense disambiguation." << endl
       << "      --dp          ; loaded dependency parser" << endl
       << "      --srl         ; loaded semantic role labeling" << endl
       << "      --za          ; loaded zero anaphora" << endl
       << "  -a, --all         ; loaded all setting" << endl;
}

// gitversion을 확인하기 위한 함수
void ProcessOption(int argc, char *argv[], int &nlp_feature) {
  bool do_exit = false;
  int c;
  while (true) {
    static const struct option long_options[] = {
        {"version", no_argument, 0, 'v'},
        {"help", no_argument, 0, 'h'},
        {NULL, optional_argument, 0, 'c'},
        {"space", no_argument, 0, 1},
        {"morp", no_argument, 0, 2},
        {"ner", no_argument, 0, 3},
        {"wsd", no_argument, 0, 4},
        {"dp", no_argument, 0, 5},
        {"srl", no_argument, 0, 6},
        {"za", no_argument, 0, 7},
        {"all", no_argument, 0, 'a'},
        {NULL, no_argument, NULL, 0}
    };

    int option_index = 0;
    char *prog = basename(argv[0]);
    c = getopt_long(argc,
                    argv,
                    "vhc:a",
                    long_options,
                    &option_index);
    if (c == -1) break;

    switch (c) {
      case 0:
        break;
      case 'v':
        cout << prog << " version " << version::VERSION_STRING << endl;
        do_exit = true;
        break;
      case 'h':
        Help();
        do_exit = true;
        break;
      case 'c':
        continue;
      case 1:
        nlp_feature |= (1 << NlpFeature::NLPF_SPACE);
        continue;
      case 2:
        nlp_feature |= (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION);
        nlp_feature |= (1 << NlpFeature::NLPF_MORPHEME);
        break;
      case 3:
        nlp_feature |= (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION);
        nlp_feature |= (1 << NlpFeature::NLPF_MORPHEME);
        nlp_feature |= (1 << NlpFeature::NLPF_NAMED_ENTITY);
        break;
      case 4:
        nlp_feature |= (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION);
        nlp_feature |= (1 << NlpFeature::NLPF_MORPHEME);
        //nlp_feature |= (1 << NlpFeature::NLPF_NAMED_ENTITY);
        nlp_feature |= (1 << NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION);
        break;
      case 5:
        nlp_feature |= (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION);
        nlp_feature |= (1 << NlpFeature::NLPF_MORPHEME);
        //nlp_feature |= (1 << NlpFeature::NLPF_NAMED_ENTITY);
        //nlp_feature |= (1 << NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION);
        nlp_feature |= (1 << NlpFeature::NLPF_DEPENDENCY_PARSER);
        break;
      case 6:
        nlp_feature |= (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION);
        nlp_feature |= (1 << NlpFeature::NLPF_MORPHEME);
        //nlp_feature |= (1 << NlpFeature::NLPF_NAMED_ENTITY);
        //nlp_feature |= (1 << NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION);
        //nlp_feature |= (1 << NlpFeature::NLPF_DEPENDENCY_PARSER);
        nlp_feature |= (1 << NlpFeature::NLPF_SEMANTIC_ROLE_LABELING);
        break;
      case 7:
        nlp_feature |= (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION);
        nlp_feature |= (1 << NlpFeature::NLPF_MORPHEME);
        //nlp_feature |= (1 << NlpFeature::NLPF_NAMED_ENTITY);
        //nlp_feature |= (1 << NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION);
        //nlp_feature |= (1 << NlpFeature::NLPF_DEPENDENCY_PARSER);
        //nlp_feature |= (1 << NlpFeature::NLPF_SEMANTIC_ROLE_LABELING);
        nlp_feature |= (1 << NlpFeature::NLPF_ZERO_ANAPHORA);
        break;
      case 'a':
        nlp_feature |= (1 << NlpFeature::NLPF_SPACE);
        nlp_feature |= (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION);
        nlp_feature |= (1 << NlpFeature::NLPF_MORPHEME);
        nlp_feature |= (1 << NlpFeature::NLPF_NAMED_ENTITY);
        nlp_feature |= (1 << NlpFeature::NLPF_WORD_SENSE_DISAMBIGUATION);
        nlp_feature |= (1 << NlpFeature::NLPF_DEPENDENCY_PARSER);
        nlp_feature |= (1 << NlpFeature::NLPF_SEMANTIC_ROLE_LABELING);
        nlp_feature |= (1 << NlpFeature::NLPF_ZERO_ANAPHORA);
        break;
      default:
        Help();
        do_exit = true;
        break;
    }
  }

  if (do_exit) exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {
  int nlp_feature = 0;
  ProcessOption(argc, argv, nlp_feature);
  libmaum::Config::Init(argc, argv, "brain-ta.conf");

  EnableCoreDump();
  RunServer(basename(argv[0]), nlp_feature);
  return 0;
}
