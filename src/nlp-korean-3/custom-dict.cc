#include "custom-dict.h"
#include <libmaum/common/config.h>
#include <curl/curl.h>

/**
 * file 데이터를 다운로드 하는 curl callback 함수.
 *
 * @param in 처리할 데이터
 * @param size 크기
 * @param nmemb 개수
 * @param out 저장할 사용자 지정 변수
 * @return 저장한 크기
 */
static uint AppendToFile(char *in, uint size, uint nmemb, void *out) {
  auto pfd = reinterpret_cast<int *>(out);
  ssize_t ret = write(*pfd, (void *)in, size * nmemb);
  return uint(ret * size);
}

const char * kMltCallbackApiKey = "7c3a778e29ac522de62c4e22aa90c9";

/**
 * 파일을 다운로드한다.
 *
 * `dict_` 메시지에 함께 전달되어 온 URL을 통해서 처리한다.
 * @return 다운로드에 성공하면 0을 반환한다.
 */
int CustomDict::Download() {
  const auto & url = dict_.download_url();
  auto logger = LOGGER();

  string full(url);
  full += kMltCallbackApiKey;
  logger->debug("[dict download url] {}", full);

  CURL *curl;
  std::string body;
  char err_buf[CURL_ERROR_SIZE];
  curl = curl_easy_init();
  if (!curl) {
    logger->debug<const char *>("curl init failed {}");
    return -1;
  }

  auto &c = libmaum::Config::Instance();
  char fname[PATH_MAX];
  snprintf(fname, sizeof fname, "%s/dict_down_XXXXXX",
           c.Get("brain-ta.resource.3.kor.path").c_str());
  int fd = mkstemp(fname);
  logger->debug("temp file name is {}", fname);

  curl_easy_setopt(curl, CURLOPT_URL, full.c_str());
  curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, err_buf);
  curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
  curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, AppendToFile);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &fd);

  int err = curl_easy_perform(curl);
  if (err) {
    logger->error("curl perform failed {}",
                  curl_easy_strerror((CURLcode) err));
    close(fd);
    return -1;
  }
  curl_easy_cleanup(curl);
  // processing by response
  download_file_.assign(fname);
  fsync(fd);
  struct stat st = {};
  fstat(fd, &st);
  logger->debug("download file, fd = {}, size = {}", fd, st.st_size);
  close(fd);
  // unlink(fname);

  if (st.st_size == 0) {
    logger->error("dict file is empty. check download url");
    return -1;
  }

  return 0;
}

#include <sstream>
#include <libmaum/common/fileutils.h>
using std::ostringstream;

/**
 * 사용자의 형태소 사전 및 복합명사 사전을 적용한다.
 *
 * @return 성공하면 0을 반환한다.
 */
int CustomDict::GenerateCustomDict() {
  auto logger = LOGGER();
  auto &c = libmaum::Config::Instance();
  string path = c.Get("brain-ta.resource.3.kor.custom.path");
  auto custom_morp = path + "/morph_dic_custom.txt";

  if (dict_.type() == NlpDictType::DIC_CUSTOM_MORPHEME) {
    rename(download_file_.c_str(), custom_morp.c_str());
  }

  return 0;
}


// 별도로 구현되어서 구현할 필요가 없음.
int CustomDict::ApplyNamedEntityDict() {
  return 0;
}

// 동의어 사전 처리에 대한 알려진 내용이 없음
int CustomDict::ApplySynonym() {
  return 0;
}


/**
 * 사용자 사전을 적용하기 위한 외부 인테페이스 함수
 *
 * `dict_` 멤버 변수에 따라서 동작을 다르게 처리한다.
 *
 * @return 적용 결과를 반환한다.
 */
//now only using DIC_CUSTOM_MORPHEME for LGCVChatbot Prj
int CustomDict::Apply() {
  switch (dict_.type()) {
    case NlpDictType::DIC_CUSTOM_MORPHEME: {
      return GenerateCustomDict();
    }
//    case NlpDictType::DIC_CUSTOM_COMPOUND_NOUN: {
//      return GenerateCustomDict();
//    }
//    case NlpDictType::DIC_NE_PRE_PROC: {
//      return ApplyNamedEntityDict();
//    }
//    case NlpDictType::DIC_NE_POST_PROC: {
//      return ApplyNamedEntityDict();
//    }
//    case NlpDictType::DIC_SYNONYM: {
//      return ApplySynonym();
//    }
    default: {
      LOGGER()->warn("unkown dict type {} {}",
                     dict_.type(), dict_.download_url());
      return -1;
    }
  }
}

static uint ReadData(char *in, uint size, uint nmemb, void *out) {
  auto body = static_cast<string *>(out);
  body->append(in, size * nmemb);
  return size * nmemb;
}

/**
 * 사용자 사전을 처리하고 난 이후에 callback url 을 처리한다.
 *
 * @return curl callback 호출이 성공하면 0, 실패 하면 -1을 반환한다.
 */
int CustomDict::Callback() {
  const auto & url = dict_.callback_url();
  auto logger = LOGGER();

  string full(url);
  full += kMltCallbackApiKey;
  logger->debug("[dict callback url] {}", full);

  CURL *curl;
  std::string resp_body;
  char err_buf[CURL_ERROR_SIZE];
  curl = curl_easy_init();
  if (!curl) {
    logger->debug<const char *>("curl init failed {}");
    return -1;
  }

  curl_slist *headers = nullptr;
  headers = curl_slist_append(headers, "Accept: application/json");
  headers = curl_slist_append(headers, "Content-Type: application/json");
  headers = curl_slist_append(headers, "charsets: utf-8");

  ostringstream oss;
  oss << R"({ "message": ")"
      << maum::brain::nlp::NlpDictType_Name(dict_.type())
      << R"( has been applied!" })";
  string body = oss.str();

  curl_easy_setopt(curl, CURLOPT_URL, full.c_str());
  curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, err_buf);
  curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
  curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
  curl_easy_setopt(curl, CURLOPT_POST, 1L);
  curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, ReadData);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &resp_body);
  curl_easy_setopt(curl, CURLOPT_POSTFIELDS, body.c_str());
  curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, body.length());

  int err = curl_easy_perform(curl);
  curl_slist_free_all(headers);
  curl_easy_cleanup(curl);

  logger->debug("response body [{}]", resp_body);

  if (err) {
    logger->debug("curl perform failed {}",
                  curl_easy_strerror((CURLcode) err));
    return -1;
  }
  return 0;
}