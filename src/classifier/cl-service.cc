#include <libmaum/common/config.h>
#include "cl-service.h"

using grpc::ClientContext;

ClassifierServiceImpl::ClassifierServiceImpl() {
  resolver_ = ClassifierResolver::GetInstance();
}

ClassifierServiceImpl::~ClassifierServiceImpl() {
}

std::unique_ptr<Classifier::Stub>
ClassifierServiceImpl::Resolve(const ClassInputText &in) {
  Model model;
  model.set_lang(in.lang());
  model.set_model(in.model());
  ServerStatus ss;
  Status st = resolver_->Find(&model, &ss);
  if (!st.ok()) {
    return std::unique_ptr<Classifier::Stub>();
  }

  std::unique_ptr<Classifier::Stub> stub =
      Classifier::NewStub(
          grpc::CreateChannel(ss.server_address(),
                              grpc::InsecureChannelCredentials()));
  bool callable = false;
  if (ss.invoked_by() == "find&fork") {
    for (int i = 0; i < 60; i++) {
      ClientContext ctx;
      Status status = stub->Ping(&ctx, model, &ss);
      if (status.ok() && ss.running()) {
        callable = true;
        break;
      }
    }
  } else {
    callable = true;
  }
  if (callable)
    return stub;
  else
    return std::unique_ptr<Classifier::Stub>();
}

Status ClassifierServiceImpl::GetClass(ServerContext *context,
                                       const ClassInputText *in,
                                       ClassifiedSummary *sum) {
  std::unique_ptr<Classifier::Stub> stub = Resolve(*in);
  if (!stub) {
    return Status(grpc::StatusCode::INTERNAL, "real service not available!");
  }
  ClientContext ctx;
  return stub->GetClass(&ctx, *in, sum);
}


Status ClassifierServiceImpl::GetClassMultiple(
    ServerContext *context,
    ServerReaderWriter<ClassifiedSummary, ClassInputText> *stream) {
  using grpc::ClientReaderWriter;

  std::unique_ptr<Classifier::Stub> stub;
  std::unique_ptr<ClientReaderWriter<ClassInputText, ClassifiedSummary>> stub_str;

  ClassInputText in;
  ClientContext ctx;

  while (stream->Read(&in)) {
    if (!stub_str) {


      stub = Resolve(in);
      stub_str = stub->GetClassMultiple(&ctx);
    }
    stub_str->Write(in);
  }
  stub_str->WritesDone();
  Status st_stub = stub_str->Finish();
  if (st_stub.ok()) {
    ClassifiedSummary sum;
    while (stub_str->Read(&sum)) {
      stream->Write(sum);
    }
  }
  return st_stub;
}
