#ifndef _TA_CL_ENG_CLASSIFIER_H
#define _TA_CL_ENG_CLASSIFIER_H

#include <memory>
#include <string>
#include "classifier-service.h"

using std::string;

using grpc::ServerReaderWriter;


class Nlp2Eng;

/**
 * 영어 DNN 분류기
 */
class EnglishClassifier : public ClassifierService {
 public:
  explicit EnglishClassifier(const string &model, LangCode lang,
                             const string &endpoint)
      : ClassifierService(model, lang, endpoint) {}
  ~EnglishClassifier() override;

  void Initialize() override;

  Status GetClass(ServerContext* context,
                  const ClassInputText* in, ClassifiedSummary* sum) override;
  Status GetClassMultiple(
      ServerContext* context,
      ServerReaderWriter<ClassifiedSummary, ClassInputText>* stream) override;

  Status GetClassByDocument(ServerContext* context,
                            const ClassInputDocument* in,
                            ClassifiedSummary* sum) override;
  Status GetClassMultipleByDocument(ServerContext* context,
                                    ServerReaderWriter<
                                        ClassifiedSummary,
                                        ClassInputDocument>* stream) override;


 private:
  // CLASSIFIER
  std::shared_ptr<Nlp2Eng> anal_;
};

#endif // _TA_CL_ENG_CLASSIFIER_H
