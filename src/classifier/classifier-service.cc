#include "classifier-service.h"
#include <libmaum/common/config.h>

/**
 * DNN 분류기 수퍼 클래스 생성자
 *
 * @param model 분류기 서비스에 사용할 모델
 * @param lang 분류기 서비스에서 사용할 언어
 * @param endpoint 분류기 서비스가 물고 있을 endpoint 정보
 */
ClassifierService::ClassifierService(const string &model,
                                     LangCode lang,
                                     const string &endpoint)
    : model_(model),
      lang_(lang),
      endpoint_(endpoint),
      state_(maum::brain::cl::SERVER_STATE_STARTING) {
}

/**
 * 소멸자
 */
ClassifierService::~ClassifierService() {
  state_ = maum::brain::cl::SERVER_STATE_NONE;
}

/**
 * 서버가 살아있는지 점검한다.
 *
 * @param context 서버 컨텍스트
 * @param model 서버 접속 기본 정보
 * @param ss 서버 접속 엔드포인트
 * @return 호출 성공 여부
 */
Status ClassifierService::Ping(ServerContext *context,
                               const Model *m,
                               ServerStatus *ss) {
  if (m->model() == model_ && m->lang() == lang_) {
    ss->set_lang(lang_);
    ss->set_model(model_);
    ss->set_running(true);
    ss->set_server_address(endpoint_);
    ss->set_invoked_by("pong");
    ss->set_state(state_);
  } else {
    LOGGER()->debug("invalid model and lang called {} {}",
                    m->model(), m->lang());
  }
  return Status::OK;
}