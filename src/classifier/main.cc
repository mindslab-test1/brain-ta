#include <csignal>

#include <libmaum/common/config.h>
#include <grpc++/grpc++.h>
#include <sys/prctl.h>
#include "cl-resolver-impl.h"
#include "classifier-service.h"
#include "cl-service.h"
#include <gitversion/version.h>
#include <getopt.h>

#include <libmaum/common/util.h>

using grpc::Server;
using grpc::ServerBuilder;

using namespace std;
using std::shared_ptr;

volatile std::sig_atomic_t g_signal_status;

void HandleSignal(int signal) {
  // CALL DESTRUCTOR
  LOGGER()->info("handling {}", signal);
  g_signal_status = signal;
  exit(0);
}

void RunServer(const char * prog) {
  libmaum::Config &c = libmaum::Config::Instance();
  auto logger = c.GetLogger();
  std::string server_address("0.0.0.0:");
  server_address += c.Get("brain-ta.cl.front.port");
  int grpc_timeout = std::stoi(c.Get("brain-ta.cl.front.timeout"));

  ClassifierResolverImpl service_resolver;
  ClassifierServiceImpl cl_proxy;

  ServerBuilder builder;
  builder.AddChannelArgument(GRPC_ARG_MAX_CONNECTION_IDLE_MS, grpc_timeout);
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());

  builder.RegisterService(&service_resolver);
  builder.RegisterService(&cl_proxy);

  unique_ptr<Server> server(builder.BuildAndStart());
  if (server) {
    logger->info("{} {} server listening on {}",
                 prog, version::VERSION_STRING,
                 server_address);
    c.DumpPid();
    server->Wait();
  }
}

//gitversion 외에도 model name, language, port 정보를 확인하기 위한 함수
void Usage(const char *prname) {
  std::cout
      << prname << " [-n name] [-l lang] [-e endpoint] [-p port] [-v version] [-h help]"
      << std::endl
      << "-n --name : sa model name" << endl
      << "-l --lang : language (kor, eng)" << endl
      << "-e --endpoint : export_ip + \":\" + port" << endl
      << "-p --port : port" << endl
      << "-v --version : version" << endl
      << "-h --help : help" << endl;

  exit(0);
}

char *g_program = nullptr;
extern void ChildHandler(int sig);

int main(int argc, char *argv[]) {
  std::signal(SIGINT, HandleSignal);
  //std::signal(SIGSEGV, HandleSignal);
  //std::signal(SIGBUS, HandleSignal);
  std::signal(SIGTERM, HandleSignal);
  char *base = basename(argv[0]);

  int mode;
  char *name, *endpoint, *port, *lang;
  while (true) {
    int optindex = 0;
    static option sds_fork_option[] = {
        {"name", required_argument, 0, 'n'},
        {"lang", required_argument, 0, 'l'},
        {"endpoint", required_argument, 0, 'e'},
        {"port", required_argument, 0, 'p'},
        {"help", no_argument, 0, 'h'},
        {"version", no_argument, 0, 'v'}
    };
    int ch = getopt_long(argc, argv, "n:l:e:p:hv", sds_fork_option, &optindex);
    if (ch == -1) {
      if (argc == 1) {
        mode = 0;
        break;
      } else if (argc != 9) {
        Usage(base);
        break;
      } else {
        mode = 1;
        break;
      }
    }

    switch (ch) {
      case 'n':
        name = optarg;
        break;
      case 'l':
        lang = optarg;
        break;
      case 'e':
        endpoint = optarg;
        break;
      case 'p':
        port = optarg;
        break;
      case 'v':
        printf("%s version %s\n", base, version::VERSION_STRING);
        exit(EXIT_SUCCESS);
      case 'h':
        Usage(base);
        break;
      default:
        Usage(base);
        break;
    }
  }

  auto &c = libmaum::Config::Init(argc, argv, "brain-ta.conf");



  g_program = strdup(c.GetProgramPath().c_str());

  if (mode == 0) {
    signal(SIGCHLD, ChildHandler);
    auto logger = LOGGER();
    logger->debug("start cld default server {}", mode);
    EnableCoreDump();
    RunServer(base);
  } else if (mode == 1) {
    string process_name("cl-");
    process_name += lang[0];
    process_name += '-';
    process_name += name;
    c.SetLoggerName(process_name);
    auto logger = LOGGER();
    logger->debug("start cld child server {}", process_name);
    int s = ::prctl(PR_SET_NAME, process_name.c_str(), NULL, NULL, NULL);
    snprintf(argv[0], strlen(argv[0]), "sub cl %s %s",
             name, lang);
    logger->debug("[prctl status] {}", s);
    EnableCoreDump();
    RunClassifierChild(name, lang, endpoint, atoi(port));
  }
}
