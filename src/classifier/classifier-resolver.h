#ifndef CLASSIFIER__RESOLVER_H
#define CLASSIFIER__RESOLVER_H

#include <atomic>
#include <grpc++/grpc++.h>
#include "maum/brain/cl/classifier.grpc.pb.h"
#include <json/json.h>

using std::string;
using std::atomic;

using grpc::Status;
using grpc::ServerContext;
using grpc::ServerReader;
using google::protobuf::Empty;
using maum::brain::cl::Model;
using maum::brain::cl::ModelList;
using maum::brain::cl::ServerStatus;
using maum::brain::cl::ServerStatusList;
using maum::brain::cl::SetModelResponse;
using maum::brain::cl::ModelCategories;

class ClassifierResolver {
  ClassifierResolver();
 public:
  static ClassifierResolver *GetInstance();
  virtual ~ClassifierResolver();
  Status Find(const Model* request, ServerStatus* response);
  Status GetModels(ModelList *response);
  Status GetServers(ServerStatusList *response);
  Status Stop(const Model* request, ServerStatus* response);
  Status Restart(const Model* request, ServerStatus* response);
  Status Ping(const Model* request, ServerStatus * response);
  Status SetModel(const Model *model,
                  ServerReader<::maum::common::FilePart>* reader,
                  SetModelResponse * resp);
  Status DeleteModel(const Model *model, ServerStatus *status);
  Status GetModelCategories(const Model *model, ModelCategories *categories);
  int ClearModel(pid_t pid);

 private:
  string sa_model_root_;
  Json::Value root_;
  std::mutex root_lock_;
  string export_ip_;
  string state_file_;

  enum ForkResult {
    FORK_SUCCESS = 0,
    FORK_NOT_FOUND = 1,
    FORK_FAILED = 2
  };

  bool IsRunning(const Model &model, Json::Value &found);
  bool Stop(const Model &m, const Json::Value &item);

  void WriteStatus();
  void LoadStatus();
  bool PingServer(Json::Value &item, bool &updated);
  ForkResult ForkServer(const Model *model, Json::Value &item);

  // singleton
  static std::mutex mutex_;
  static atomic<ClassifierResolver *> instance_;
  const long kWaitTimeoutToStarting = 7;
};

void RunClassifierChild(const string &name, const string &lang,
                        const string &endpoint, int port);

#endif // CLASSIFIER__RESOLVER_H
