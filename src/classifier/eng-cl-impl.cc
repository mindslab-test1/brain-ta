#include <grpc++/grpc++.h>
#include <libmaum/common/config.h>
#include "eng-cl-impl.h"

#include <libmaum/brain/nlp/nlp2eng.h>

#include "cl-util.h"
#include "util.h"

using namespace google::protobuf;
using std::string;
using grpc::ServerBuilder;
using grpc::Server;
using grpc::Service;
using std::unique_ptr;

const int kFeatures = (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION) |
    (1 << NlpFeature::NLPF_MORPHEME);


void EnglishClassifier::Initialize() {

  state_ = maum::brain::cl::SERVER_STATE_INITIALIZING;
  auto &c = libmaum::Config::Instance();
  auto sa_model_root = c.Get("brain-ta.cl.model.dir");
  auto eng_rsc = c.Get("brain-ta.resource.2.eng.path");

  string model_path = model_;
  model_path += '-';
  model_path += maum::common::LangCode_Name(lang_);

  string full_path = sa_model_root;
  full_path += '/';
  full_path += model_path;

  anal_ = std::make_shared<Nlp2Eng>(eng_rsc, kFeatures);
  anal_->UpdateFeatures();
  anal_->SetClassifierModel(model_path);

  state_ = maum::brain::cl::SERVER_STATE_RUNNING;
}

/**
 * 소멸자
 */
EnglishClassifier::~EnglishClassifier() {
  LOGGER()->warn("stopping name {}, path {}", model_, lang_);
  anal_->Uninit();
  anal_.reset();
}


Status EnglishClassifier::GetClass(ServerContext *context,
                                  const ClassInputText *in,
                                  ClassifiedSummary *sum) {
  if (in->model() != model_) {
    return Status(grpc::StatusCode::NOT_FOUND, "model not found!");
  }
  if (state_ != maum::brain::cl::SERVER_STATE_RUNNING) {
    return Status(grpc::StatusCode::FAILED_PRECONDITION, "not ready");
  }
  if (in->text().empty() || in->text() == " ") {
    return Status(grpc::StatusCode::INVALID_ARGUMENT, "input text is empty!!");
  }
  Document doc;
  InputText input;
  input.set_lang(in->lang());
  input.set_level(NlpAnalysisLevel::NLP_ANALYSIS_MORPHEME);
  input.set_text(in->text());

  anal_->AnalyzeOne(&input, &doc);
  anal_->Classify(doc, sum);

  return Status::OK;
}

Status EnglishClassifier::GetClassMultiple(ServerContext *context,
                                          ServerReaderWriter<ClassifiedSummary, ClassInputText> *stream) {
  if (state_ != maum::brain::cl::SERVER_STATE_RUNNING) {
    return Status(grpc::StatusCode::FAILED_PRECONDITION, "not ready");
  }

  ClassInputText in;
  while (stream->Read(&in)) {
    if (in.model() != model_) {
      return Status(grpc::StatusCode::NOT_FOUND, "model not found!");
    }
    if (in.text().empty() || in.text() == " ") {
      return Status(grpc::StatusCode::INVALID_ARGUMENT, "input text is empty!!");
    }

    Document doc;
    InputText input;
    input.set_lang(in.lang());
    input.set_level(NlpAnalysisLevel::NLP_ANALYSIS_MORPHEME);
    input.set_text(in.text());

    ClassifiedSummary out;

    anal_->AnalyzeOne(&input, &doc);
    anal_->Classify(doc, &out);

    stream->Write(out);
  }
  return Status::OK;
}

/**
 * 문서를 통해서 분류를 수행한다.
 *
 * @param context 서비스 컨텍스트
 * @param in 입력 문서
 * @param sum 분류 결과
 * @return GRPC 수행 결과
 */
Status EnglishClassifier::GetClassByDocument(ServerContext *context,
                                             const ClassInputDocument *in,
                                             ClassifiedSummary *sum) {
  if (in->model() != model_) {
    return Status(grpc::StatusCode::NOT_FOUND, "model not found!");
  }
  if (state_ != maum::brain::cl::SERVER_STATE_RUNNING) {
    return Status(grpc::StatusCode::FAILED_PRECONDITION, "not ready");
  }
  if (in->document().sentences().size() == 0) {
    return Status(grpc::StatusCode::INVALID_ARGUMENT, "input text is empty!!");
  }

  anal_->Classify(in->document(), sum);

  return Status::OK;
}


/**
 * 문서를 통해서 분류를 수행한다.
 *
 * @param context 서비스 컨텍스트
 * @param stream Document와 분류 결과를 처리하는 스트림
 * @return GRPC 수행 결과
 */
Status EnglishClassifier::GetClassMultipleByDocument(
    ServerContext *context,
    ServerReaderWriter<ClassifiedSummary, ClassInputDocument> *stream) {

  ClassInputDocument in;

  while (stream->Read(&in)) {
    if (in.model() != model_) {
      return Status(grpc::StatusCode::NOT_FOUND, "model not found!");
    }
    if (in.document().sentences().size() == 0) {
      return Status(grpc::StatusCode::INVALID_ARGUMENT, "input text is empty!!");
    }

    ClassifiedSummary out;
    anal_->Classify(in.document(), &out);

    stream->Write(out);
  }
  return Status::OK;

}
