#ifndef __CL_UTIL_H
#define __CL_UTIL_H

#include <grpc++/grpc++.h>
#include <maum/brain/cl/classifier.pb.h>

using grpc::ServerContext;
using maum::brain::cl::Model;

void GetModelFromContext(const ServerContext * ctx, Model &model);
std::string GetFilenameFromContext(const ServerContext * ctx);

/**
 * 모델 이름과 언어를 합쳐서 처리해서 결과를 반환한다.
 *
 * `model`-`lang`
 *
 * 언어모델의 디렉토리는 "model-kor", "model-eng" 형태로 생성된다.
 * 참고로 lang의 이름은 ISO-639를 따라서 사용된다.
 * 같은 모델 이름으로 한국어 영어가 동시에 서비스되는 경우가 존재할 수 있다고
 * 가정한다.
 *
 * @param model 모델 정보
 * @return
 * @param model 모델 정보
 * @return 모델 경로
 */
inline std::string GetModelPath(const Model &model) {
  std::string model_path = model.model();
  model_path += '-';
  model_path += maum::common::LangCode_Name(model.lang());
  return model_path;
}

#endif // __CL_UTIL_H
