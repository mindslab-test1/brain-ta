#include <grpc++/grpc++.h>
#include <libmaum/common/encoding.h>
#include <libmaum/common/config.h>
#include <util.h>
#include "kor-cl-impl.h"

#include <libmaum/brain/nlp/nlp2kor.h>
#include <libmaum/brain/nlp/nlp3kor.h>

using namespace google::protobuf;
using std::string;
using grpc::ServerBuilder;
using grpc::Server;
using grpc::Service;
using std::unique_ptr;

const int kFeatures = (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION) |
    (1 << NlpFeature::NLPF_MORPHEME);


/**
 * 생성자
 *
 * 대화 시스템을 초기화한다.
 *
 * @param name 모델 이름
 * @param lang 언어 이름
 * @param endpoint 응답 엔드포인트
 */
void KoreanClassifier::Initialize() {
  state_ = maum::brain::cl::SERVER_STATE_INITIALIZING;
  auto &c = libmaum::Config::Instance();
  auto sa_model_root = c.Get("brain-ta.cl.model.dir");
  auto kor_rsc = c.Get("brain-ta.resource.2.kor.path");
  auto preprocess = c.Get("brain-ta.cl.preprocessing.nlu.kor");

  string model_path = model_;
  model_path += '-';
  model_path += maum::common::LangCode_Name(lang_);

  string full_path = sa_model_root;
  full_path += '/';
  full_path += model_path;

  auto logger = LOGGER();
  logger->info("LOAD nlp2 kor {} feat {}", kor_rsc, kFeatures);
  anal_ = std::make_shared<Nlp2Kor>(kor_rsc, kFeatures);
  anal_->UpdateFeatures();
  logger->info("DONE nlp2 kor {} feat {}", kor_rsc, kFeatures);
  logger->info("LOAD dnn classifier model {}", model_path);
  anal_->SetClassifierModel(model_path);
  logger->info("DONE dnn classifier model {}", model_path);

  logger->info("IT USE preprocessor as {}", preprocess);
  if (preprocess == "nlp3") {
    auto nlp3_rsc = c.Get("brain-ta.resource.3.kor.path");
    logger->info("LOAD nlp3 kor {}", nlp3_rsc);
    auto nlu = std::make_shared<Nlp3Kor>(nlp3_rsc, kFeatures);
    nlu->UpdateFeatures();
    logger->info("DONE nlp3 kor {}", nlp3_rsc);
    nlu_ = nlu;
  } else {
    nlu_ = anal_;
  }

  state_ = maum::brain::cl::SERVER_STATE_RUNNING;
}

bool compare_anal(std::shared_ptr<Nlp2Kor> &a,
                  std::shared_ptr<Nlp> &b) {
  void * va = a.get();
  void * vb = b.get();
  return (va == vb);
}


/**
 * 소멸자
 *
 * anal_ 을 정리하고, 필요한 경우 nlu_를 정리한다.
 */
KoreanClassifier::~KoreanClassifier() {
  LOGGER()->warn("stopping name {}, path {}", model_, lang_);

  bool diff_nlu = !compare_anal(anal_, nlu_);

  LOGGER()->info("STOP diff_nlu as {}, {}",
                 diff_nlu, diff_nlu ? "NLP3": "NLP2");

  anal_->Uninit();
  if (diff_nlu)
    nlu_->Uninit();

  anal_.reset();
  if (diff_nlu) {
    nlu_.reset();
  }
}


/**
 * 텍스트를 통해서 분류를 수행한다.
 *
 * @param context 서비스 컨텍스트
 * @param in 입력 텍스트
 * @param sum 분류 결과
 * @return GRPC 수행 결과
 */
Status KoreanClassifier::GetClass(ServerContext *context,
                                  const ClassInputText *in,
                                  ClassifiedSummary *sum) {
  if (in->model() != model_) {
    return Status(grpc::StatusCode::NOT_FOUND, "model not found!");
  }
  if (state_ != maum::brain::cl::SERVER_STATE_RUNNING) {
    return Status(grpc::StatusCode::FAILED_PRECONDITION, "not ready");
  }
  if (in->text().empty() || in->text() == " ") {
    return Status(grpc::StatusCode::INVALID_ARGUMENT, "input text is empty!!");
  }

  auto logger = LOGGER();
  logger->trace("get class [{}], {}, {}",
                in->text(), in->model(), in->lang());
  Document doc;
  InputText input;
  input.set_lang(in->lang());
  input.set_level(NlpAnalysisLevel::NLP_ANALYSIS_MORPHEME);
  input.set_text(in->text());

  nlu_->AnalyzeOne(&input, &doc);
  anal_->Classify(doc, sum);

  logger->trace("get class result, {} : {}",
                sum->cl_top(), sum->probability_top());

  return Status::OK;
}

/**
 * 텍스트를 통해서 분류를 수행한다.
 *
 * @param context 서비스 컨텍스트
 * @param stream 텍스트와 분류 결과를 처리하는 스트림
 * @return GRPC 수행 결과
 */
Status KoreanClassifier::GetClassMultiple(
    ServerContext *context,
    ServerReaderWriter<ClassifiedSummary, ClassInputText> *stream) {
  if (state_ != maum::brain::cl::SERVER_STATE_RUNNING) {
    return Status(grpc::StatusCode::FAILED_PRECONDITION, "not ready");
  }

  auto logger = LOGGER();
  ClassInputText in;
  while (stream->Read(&in)) {
    if (in.model() != model_) {
      return Status(grpc::StatusCode::NOT_FOUND, "model not found!");
    }
    if (in.text().empty() || in.text() == " ") {
      return Status(grpc::StatusCode::INVALID_ARGUMENT, "input text is empty!!");
    }

    logger->trace("get class multi [{}], {}, {}",
                  in.text(), in.model(), in.lang());
    Document doc;
    InputText input;
    input.set_lang(in.lang());
    input.set_level(NlpAnalysisLevel::NLP_ANALYSIS_MORPHEME);
    input.set_text(in.text());

    ClassifiedSummary out;

    nlu_->AnalyzeOne(&input, &doc);
    anal_->Classify(doc, &out);
    logger->trace("get class multi result, {} : {}",
                  out.cl_top(), out.probability_top());

    stream->Write(out);
  }
  return Status::OK;
}

/**
 * 문서를 통해서 분류를 수행한다.
 *
 * @param context 서비스 컨텍스트
 * @param in 입력 문서
 * @param sum 분류 결과
 * @return GRPC 수행 결과
 */
Status KoreanClassifier::GetClassByDocument(ServerContext *context,
                                            const ClassInputDocument *in,
                                            ClassifiedSummary *sum) {
  if (in->model() != model_) {
    return Status(grpc::StatusCode::NOT_FOUND, "model not found!");
  }
  if (state_ != maum::brain::cl::SERVER_STATE_RUNNING) {
    return Status(grpc::StatusCode::FAILED_PRECONDITION, "not ready");
  }
  if (in->document().sentences().size() == 0) {
    return Status(grpc::StatusCode::INVALID_ARGUMENT, "input text is empty!!");
  }
  auto logger = LOGGER();

  logger->trace("get class multi by doc [{}], {}, {}",
                in->document().sentences().begin()->text(),
                in->model(), in->lang());
  anal_->Classify(in->document(), sum);

  logger->trace("get class multi by doc result, {} : {}",
                sum->cl_top(), sum->probability_top());

  return Status::OK;
}


/**
 * 문서를 통해서 분류를 수행한다.
 *
 * @param context 서비스 컨텍스트
 * @param stream Document와 분류 결과를 처리하는 스트림
 * @return GRPC 수행 결과
 */
Status KoreanClassifier::GetClassMultipleByDocument(
    ServerContext *context,
    ServerReaderWriter<ClassifiedSummary, ClassInputDocument> *stream) {

  ClassInputDocument in;

  auto logger = LOGGER();
  while (stream->Read(&in)) {
    if (in.model() != model_) {
      return Status(grpc::StatusCode::NOT_FOUND, "model not found!");
    }
    if (in.document().sentences().size() == 0) {
      return Status(grpc::StatusCode::INVALID_ARGUMENT, "input text is empty!!");
    }

    logger->trace("get class multi by doc [{}], {}, {}",
                  in.document().sentences().begin()->text(),
                  in.model(), in.lang());
    ClassifiedSummary out;
    anal_->Classify(in.document(), &out);

    logger->trace("get class multi by doc result, {} : {}",
                  out.cl_top(), out.probability_top());

    stream->Write(out);
  }
  return Status::OK;

}
