#ifndef _CL_SERVICE_H
#define _CL_SERVICE_H

#include <grpc++/grpc++.h>
#include "maum/brain/cl/classifier.grpc.pb.h"
#include "classifier-resolver.h"

using grpc::ServerContext;
using grpc::Status;
using grpc::ServerReaderWriter;

using maum::brain::cl::Classifier;
using maum::brain::cl::Model;
using maum::brain::cl::ServerStatus;
using maum::brain::cl::ClassInputText;
using maum::brain::cl::ClassifiedSummary;
using maum::common::LangCode;

class ClassifierServiceImpl final
    : public maum::brain::cl::ClassifierService::Service {
 public:
  ClassifierServiceImpl();
  virtual ~ClassifierServiceImpl();
 public:
  Status GetClass(ServerContext* context,
                  const ClassInputText* in,
                  ClassifiedSummary* sum) override;
  Status GetClassMultiple(
      ServerContext* context,
      ServerReaderWriter<ClassifiedSummary, ClassInputText>* stream) override ;

  const char *name() {
    return "Classifier Front Service";
  }
 private:
  ClassifierResolver * resolver_;
  void GetMetaData(ServerContext *ctx, Model &model, int32_t &sec);
  std::unique_ptr<Classifier::Stub> Resolve(const ClassInputText &in);
};


#endif // _CL_SERVICE_H
