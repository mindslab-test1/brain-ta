#ifndef TA_CLASSIFIER_SERVICE_H
#define TA_CLASSIFIER_SERVICE_H

#include <maum/brain/cl/classifier.grpc.pb.h>
#include <atomic>

using grpc::ServerContext;
using grpc::Status;

using maum::brain::cl::Model;
using maum::brain::cl::ServerStatus;
using maum::brain::cl::Classifier;
using maum::brain::cl::ServerState;
using maum::brain::cl::ClassInputText;
using maum::brain::cl::ClassInputDocument;
using maum::brain::cl::ClassifiedSummary;
using maum::common::LangCode;

using std::string;

/**
 * 분류기 서비스 상위 클래스
 *
 * 각 분류기 서비스의 공통 멤버와 초기화 함수 등을 호출할 수 있는 구조를 가지고 있다.
 * 또한 Ping()을 공통으로 구현하여 반복적으로 사용되지 않도록 처리한다.
 */
class ClassifierService : public Classifier::Service {
 public:
  explicit ClassifierService(const string &model, LangCode lang,
                             const string &endpoint);
  ~ClassifierService() override;

  virtual void Initialize() = 0;

  // 서버가 살아있고, 자기가 그 서버인지를 반환한다.
  Status Ping(ServerContext *context,
              const Model *m, ServerStatus *s) override;

 protected:
  // AS SERVER
  std::string model_;    /*!< 모엘 이름 */
  LangCode lang_;        /*!< 언어 */
  std::string endpoint_; /*!< 접속 endpoint */

  // STATE
  std::atomic<ServerState> state_; /*!< 서버의 상태, INIT, RUNNING */
};


#endif // TA_CLASSIFIER_SERVICE_H
