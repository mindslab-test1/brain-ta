#include <archive.h>
#include <libmaum/common/config.h>
#include <archive_entry.h>
#include "extract-tar.h"

struct ServerReaderContext {
  ServerReaderContext(ServerReader<FilePart> * a_reader)
      : reader(a_reader) {}
  ServerReader<FilePart> * reader;
  FilePart part;
};

static ssize_t GrpcStreamReaderRead(archive *ar,
                                    void * d,
                                    const void ** buffer) {
  ServerReaderContext * ctx = static_cast<ServerReaderContext *>(d);
  int ret = ctx->reader->Read(&ctx->part);
  if (!ret) {
    return 0;
  } else {
    *buffer = ctx->part.part().c_str();
    return ctx->part.part().size();
  }
}

static int GrpcStreamReaderOpen(archive * ar, void * client_data) {
  return ARCHIVE_OK;
}

static int GrpcStreamReaderClose(archive * ar, void * client_data) {
  return ARCHIVE_OK;
}

static int CopyData(archive *ar, struct archive *aw) {
  const void *buff;
  size_t size;
  int64_t offset;

  for (;;) {
    int r = archive_read_data_block(ar, &buff, &size, &offset);
    if (r == ARCHIVE_EOF)
      return (ARCHIVE_OK);
    if (r < ARCHIVE_OK)
      return (r);
    ssize_t wr = archive_write_data_block(aw, buff, size, offset);
    if (wr < ARCHIVE_OK) {
      LOGGER()->error("write data block failed {}", archive_error_string(aw));
      return int(wr);
    }
  }
}

bool ExtractTar(ServerReader<::maum::common::FilePart>* reader,
                const string &to_dir) {
  int r;

  archive *a = archive_read_new();
  archive_read_support_filter_all(a);
  archive_read_support_format_tar(a);

  archive *ext = archive_write_disk_new();

  /* Select which attributes we want to restore. */
  int flags = ARCHIVE_EXTRACT_TIME;
  flags |= ARCHIVE_EXTRACT_PERM;
  flags |= ARCHIVE_EXTRACT_ACL;
  flags |= ARCHIVE_EXTRACT_FFLAGS;

  archive_write_disk_set_options(ext, flags);
  archive_write_disk_set_standard_lookup(ext);

  ServerReaderContext ctx(reader);
  r = archive_read_open(a, &ctx,
                        GrpcStreamReaderOpen,
                        GrpcStreamReaderRead,
                        GrpcStreamReaderClose);
  archive_entry *entry = nullptr;

  bool ret = false;

  auto logger = LOGGER();
  for(int c = 0; ; c++) {
    logger->debug("before next header {}", c);
    r = archive_read_next_header(a, &entry);
    if (r == ARCHIVE_EOF) {
      logger->debug("archive eof {}", c);
      break;
    }
    if (r < ARCHIVE_OK)
      logger->error("archive error {}", archive_error_string(a));

    const char *e_path = archive_entry_pathname(entry);
    logger->debug("archive pathname {}", e_path);

    if (r < ARCHIVE_WARN) {
      logger->error("archive warn {}", e_path);
      goto error;
    }
    string new_path(to_dir);
    new_path += '/';
    new_path += e_path;

    archive_entry_set_pathname(entry, new_path.c_str());
    r = archive_write_header(ext, entry);
    if (r < ARCHIVE_OK)
      logger->error("archive write header failed {}",
                    archive_error_string(ext));
    else if (archive_entry_size(entry) > 0) {
      r = CopyData(a, ext);
      if (r < ARCHIVE_OK)
        logger->error("copy data failed, {}", archive_error_string(ext));
      if (r < ARCHIVE_WARN) {
        goto error;
      }
    }
    r = archive_write_finish_entry(ext);
    if (r < ARCHIVE_OK) {
      logger->error("archive write finish entry failed {}",
                    archive_error_string(ext));
    }
    if (r < ARCHIVE_WARN) {
      goto error;
    }
  }

  ret = true;

  error:
  archive_read_close(a);
  archive_read_free(a);
  archive_write_close(ext);
  archive_write_free(ext);
  return ret;
}
