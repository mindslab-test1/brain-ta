#include "cl-util.h"
#include <libmaum/common/config.h>
#include <libmaum/common/base64.h>

using grpc::ServerContext;
using maum::brain::cl::Model;
using maum::common::LangCode;
using std::string;

void GetModelFromContext(const ServerContext * ctx, Model &model) {
  const auto &client_map = ctx->client_metadata();
  auto item_l = client_map.find("in.lang");
  if (item_l == client_map.end()) {
    model.set_lang(LangCode::kor);
  } else {
    string lang_name(item_l->second.begin(), item_l->second.end());
    LangCode lang;
    if (!maum::common::LangCode_Parse(lang_name, &lang)) {
      LOGGER()->warn("invalid lang {}", lang_name);
      lang = LangCode::kor;
    }
    model.set_lang(lang);
  }

  auto item_m = client_map.find("in.model");
  if (item_m == client_map.end()) {
    model.set_model("default");
  } else {
    model.set_model(string(item_m->second.begin(), item_m->second.end()));
  }
}

std::string GetFilenameFromContext(const ServerContext * ctx) {
  const auto &client_map = ctx->client_metadata();
  auto item_s = client_map.find("in.filename");
  if (item_s == client_map.end()) {
    return "";
  } else {
    return Base64Decode(string(item_s->second.begin(), item_s->second.end()));
  }
}
