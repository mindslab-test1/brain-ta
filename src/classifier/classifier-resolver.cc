#include <sys/socket.h>
#include <signal.h> // kill
#include <sys/wait.h>
#include <sys/stat.h> // lstat
#include "classifier-resolver.h"
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>
#include <libmaum/common/fileutils.h>
#include <fstream>
#include <sys/resource.h>
#include <libmaum/common/encoding.h>
#include "cl-util.h"
#include "extract-tar.h"

#include "kor-cl-impl.h"
#include "eng-cl-impl.h"

using grpc::StatusCode;
using grpc::ChannelInterface;
using grpc::ServerBuilder;
using grpc::Server;

using maum::brain::cl::Classifier;
using maum::brain::cl::SetModelResult;
using maum::brain::cl::ServerState;

using namespace std;

atomic<::ClassifierResolver *> ClassifierResolver::instance_;
mutex ClassifierResolver::mutex_;

ClassifierResolver *ClassifierResolver::GetInstance() {
  ClassifierResolver *tmp = instance_.load(std::memory_order_relaxed);
  std::atomic_thread_fence(std::memory_order_acquire);
  if (tmp == nullptr) {
    std::lock_guard<std::mutex> lock(mutex_);
    tmp = instance_.load(std::memory_order_relaxed);
    if (tmp == nullptr) {
      tmp = new ClassifierResolver;
      std::atomic_thread_fence(std::memory_order_release);
      instance_.store(tmp, std::memory_order_relaxed);
    }
  }
  return tmp;
}

/**
 * 생성자
 *
 * `run.dir`에서 상태 파일을 로딩하여
 * 기존 프로세스의 존재 여부를 로딩한다.
 */
ClassifierResolver::ClassifierResolver() {
  // 기본 정보 초기화
  // 현재 서비스에 대한 정보 "JSON" 으로 덤프하기
  auto &c = libmaum::Config::Instance();
  sa_model_root_ = c.Get("brain-ta.cl.model.dir");

  char export_ip[128];
  GetPrimaryIp(export_ip, sizeof export_ip);
  export_ip_ = string(export_ip);

  string json_dir = c.Get("run.dir");
  mkpath(json_dir);
  string json_file = c.Get("brain-ta.cl.state.file");
  state_file_ = json_dir + '/' + json_file;

  LoadStatus();
}

ClassifierResolver::~ClassifierResolver() {
  LOGGER()->warn<const char *>("oops! dying");
}

/**
 * 해당 모델 파일이 존재하는지를 점검한다.
 *
 * @param model 모델
 * @param found 발견된 모델 정보 객체
 * @return 모델이 살아 있고 실행 중이면 true를 반환한다.
 */
bool ClassifierResolver::IsRunning(const Model &model, Json::Value &found) {
  auto logger = LOGGER();
  string model_path = GetModelPath(model);
  Json::Value empty;

  lock_guard<mutex> guard(root_lock_);
  found = root_.get(model_path, empty);
  guard.~lock_guard();

  if (!found.isObject() || found.empty()) {
    logger->debug("not found {}", model_path);
    return false;
  }

  logger->debug("found {}", found.toStyledString());
  bool updated;
  if (PingServer(found, updated)) {
    if (updated) {
      lock_guard<mutex> guard2(root_lock_);
      root_[model_path] = found;
      WriteStatus();
    }
    return true;
  }
  logger->debug("Remove Member, {}", model_path);

  lock_guard<mutex> guard2(root_lock_);
  root_[model_path].clear();
  root_.removeMember(model_path);
  WriteStatus();
  return false;
}

/**
 * 서버는 `fork`로 같은 프로세스를 다시 실행한다.
 *
 * 기존에 실행 중인 서버에 대한 정보를 가져온다. 없으면 새로 실행한다.
 * 실행 후에 `PingServer()`를 호출해서 그 결과를 받아 온다.
 *
 * 이미 서버가 실행 중일 경우에도 PingServer()를 호출해서 동작 여부를 확인한다.
 * 동작이 확인 후 서버가 동작하고 있지 않으면 다시 실행한다.
 * 그래도 정상적이지 않으면 서버 레코드를 삭제한다.
 *
 * @param ctx 호출하는 클라이언트 정보
 * @param query 요청하는 DM 서버 정보
 * @param location 실행중인 서버의 주소
 * @return 서비스 처리 상태, 요청하는 서버가 존재하지 않으면 NOT_FOUND를 반환한다.
 */
Status ClassifierResolver::Find(const Model *model, ServerStatus *ss) {
  auto logger = LOGGER();
  Json::Value found;
  bool running = IsRunning(*model, found);

  if (running) {
    Json::Value endpoint = found["endpoint"];
    ss->set_server_address(endpoint.asString());
    ss->set_model(model->model());
    ss->set_lang(model->lang());
    ss->set_running(true);
    ss->set_state(ServerState(found["state"].asInt()));
    ss->set_invoked_by("find");
    return Status::OK;
  }

  Status status = Status::OK;
  Json::Value item;
  ForkResult res = ForkServer(model, item);
  logger->debug("[res] {}", res);
  switch (res) {
    case FORK_SUCCESS: {
      Json::Value endpoint = item["endpoint"];
      logger->debug("[server endpoint] {}", endpoint.asString());
      ss->set_server_address(endpoint.asString());
      ss->set_model(model->model());
      ss->set_lang(model->lang());
      ss->set_running(true);
      ss->set_state(ServerState(item["state"].asInt()));
      ss->set_invoked_by("find&fork");
      status = Status::OK;
      break;
    }
    case FORK_NOT_FOUND: {
      logger->debug("cannot find {} : {}", model->model(), model->lang());
      status = Status(StatusCode::NOT_FOUND,
                      "Cannot find requested path or name");
      break;
    }
    case FORK_FAILED: {
      logger->debug("fork failed  errno: {}", errno);
      status = Status(StatusCode::INTERNAL, "Cannot fork");
      break;
    }
  }
  return status;
}

/**
 * 자식 분류기 서버가 죽었을 경우에
 * 이를 정리하도록 처리한다.
 *
 * @param sig 시그널 번호
 */
void ChildHandler(int sig) {
  int status;
  pid_t pid;
  while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
    LOGGER()->error("CHILD EXIT : {}", pid);

    auto resolver = ClassifierResolver::GetInstance();
    resolver->ClearModel(pid);
  }
}

extern char *g_program;

/**
 * 자식 프로세스를 생성한다.
 *
 * fork가 성공하면 자식 프로그램을 실행한다.
 *
 * @param model 실행할 모델
 * @param item 실행하고 난 후 생성된 item 값
 * @return ForkResult 결과물을 반환한다.
 */
ClassifierResolver::ForkResult ClassifierResolver::ForkServer(
    const Model *model, Json::Value &item) {
  auto logger = LOGGER();
  int port;
  int counter = 5;

  string model_path = GetModelPath(*model);
  string full_path = sa_model_root_;
  full_path += '/';
  full_path += model_path;

  int path_found = access(full_path.c_str(), 0);
  if (path_found != 0) {
    logger->debug("{} is not found", full_path);
    return FORK_NOT_FOUND;
  } else {
    logger->debug("{} is found", full_path);
  }

  libmaum::Config &c = libmaum::Config::Instance();
  std::int32_t min_port;
  std::int32_t max_port;
  try {
    min_port = c.GetAsInt("brain-ta.cl.random.min.port");
    max_port = c.GetAsInt("brain-ta.cl.random.max.port");
  } catch (...) {
    min_port = 0;
    max_port = 0;
  }

  if (min_port != 0 && max_port != 0) {
    logger->debug("Avaliable port range : {} ~ {}", min_port, max_port);

    do {
      usleep(500 * 1000);
      port = GetRandomTcpPort(min_port, max_port);
      logger->debug("classifier child port {}", port);

      if (CheckPort(export_ip_.c_str(), port)) {
        counter--;
        logger->debug("[{}] : [port:{} is already used]", counter, port);
        port = -1;
      } else {
        logger->debug("[{}] : [port:{} is not used]", counter, port);
        break;
      }
    } while (counter);
  } else {
    logger->debug("Make random port without port range");
    port = GetRandomTcpPort();
  }


  int pid = fork();
  if (pid == -1) {
    return FORK_FAILED;
  }

  if (pid == 0) {
    rlimit rl;
    getrlimit(RLIMIT_NOFILE, &rl);
    for (unsigned int fd = 3; fd < rl.rlim_cur; fd++)
      (void) close(fd);

    char endpoint[100];
    char portstr[10];
    char option_n[3] = "-n";
    char option_l[3] = "-l";
    char option_e[3] = "-e";
    char option_p[3] = "-p";
    char *argv[] = {g_program,
                    option_n,
                    (char *) model->model().c_str(),
                    option_l,
                    (char *) maum::common::LangCode_Name(model->lang()).c_str(),
                    option_e,
                    endpoint,
                    option_p,
                    portstr,
                    NULL};

    snprintf(endpoint, sizeof(endpoint), "%s:%d", export_ip_.c_str(), port);
    snprintf(portstr, sizeof(portstr), "%d", port);
    execv(g_program, argv);

    // NOT REACH
    return FORK_SUCCESS;
  } else {
    string endpoint = export_ip_ + ":" + std::to_string(port);
    logger->debug("insert forked object {} {} {}",
                  pid, model->model(), model->lang());
    logger->debug("[PID] {}", pid);
    logger->debug("[Path] {}", model_path);
    logger->debug("[NAME] {}", model->model());
    logger->debug("[END point] {}", endpoint);
    Json::Value::Int64 now = time(nullptr);
    item["forkedAt"] = Json::Value(now);
    item["state"] = maum::brain::cl::SERVER_STATE_STARTING;
    item["pid"] = Json::Value(pid);
    item["path"] = Json::Value(model_path.c_str());
    item["lang"] = Json::Value(maum::common::LangCode_Name(model->lang()).c_str());
    item["model"] = Json::Value(model->model().c_str());
    item["endpoint"] = endpoint.c_str();
    {
      lock_guard<mutex> guard(root_lock_);
      root_[model_path] = item;
      WriteStatus();
    }
    return FORK_SUCCESS;
  }
}

/**
 * 현재 메모리에 있는 상태를 모두 DB에 저장한다.
 *
 * DB는 JSON 파일이다.
 * 현재 메모리에 있는 모든 정보를 다 기록한다.
 *
 * 이 함수는 변경이 발생하면 매번 호출되어야 한다.
 *
 * 이 함수는 반드시 LOCK 환경에서만 호출되어야 합니다.
 */
void ClassifierResolver::WriteStatus() {
  std::ofstream ofs(state_file_, std::ios_base::trunc | std::ios_base::out);
  Json::StreamWriterBuilder builder;
  builder["commentStyle"] = "None";
  builder["indentation"] = "  ";
  std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());

  // FIXME:
  // jsoncpp의 write()에서 죽는 문제, 디버그 환경에서 거의 100% 죽음
  // writer->write(root_, &ofs);
  ofs << std::endl;

  LOGGER()->debug("json {}", root_.toStyledString());
}

/**
 * 기존의 서버 등록 상태를 로딩한다.
 *
 * maum/run/"sds-svcd.dump.json" 파일이 존재하면 파일의 내용을 로딩한다.
 * 하나의 레코드를 로딩하면서 바로바로 서버의 존재여부를 체크한다.
 *
 * ```
 * [
 *    "model-kor":
 *    {
 *      "pid": 1234,
 *      "lang": "kor",
 *      "name": "model",
 *      "path": "model-kor",
 *      "endpoint": "x.x.x.x:nnnn"
 *      "forkedAt" : 1497964026,
 *      "state" : 3
 *    }
 * ]
 * ```
 * 체크할 때에는 `PingServer()` 함수를 호출하도록 한다.
 */
void ClassifierResolver::LoadStatus() {
  std::ifstream ifs(state_file_);
  auto logger = LOGGER();
  if (ifs.fail()) {
    logger->warn("cannot load state json {}: not exist", state_file_);
    return;
  }

  Json::CharReaderBuilder builder;
  builder["collectComments"] = false;

  Json::Value temp;
  string errs;
  bool ok = Json::parseFromStream(builder, ifs, &temp, &errs);
  if (!ok) {
    logger->error("cannot parsing state json {} {}", state_file_, errs);
    return;
  }

  logger->debug("loaded status: {}", temp.toStyledString());

  int updated_count = 0;
  for (auto item: temp) {
    bool updated = false;
    auto path = item["path"].asString();
    if (!PingServer(item, updated)) {
      temp[path].clear();
      temp.removeMember(path);
      updated_count++;
    }
    if (updated) {
      temp[path] = item;
      updated_count++;
    }
  }

  lock_guard<mutex> guard(root_lock_);
  root_ = temp;
  if (updated_count) {
    WriteStatus();
  }
}

/**
 * 개별 서버가 동작하고 있는지를 점검한다.
 *
 * 먼저 서버의 PID가 동작중인지 점검한다.
 * 해당 서버로 Ping을 호출해서 그 결과를 확인하고 정상적이면 처리한다.
 * 서버의 상태 정보는 갱신될 수 있다. 서버의 동작 여부에 따라서
 * 갱신이 이뤄지면 이를 반영한다.
 *
 * 서버가 STARTING 상태일 때 직접 서버에 Ping을 호출했을 때
 * 응답이 없을 수도 있는데 시작한 시간 3초 이내에는 정상적인 것으로 판단한다.
 *
 * @param item 개별 서버의 정보
 * @param updated item 정보가 갱산되었는지를 확인한다. 갱신될 정보는 item["state"] 값이다.
 *
 * @return 서버가 동작하고 있으면 true, 서버가 종료된 상태이면 false를 반환한다.
 */
bool ClassifierResolver::PingServer(Json::Value &item, bool &updated) {
  string endpoint = item["endpoint"].asString();
  string model = item["model"].asString();
  pid_t sub_pid = item["pid"].asInt();

  auto logger = LOGGER();

  logger->debug("Check server: {} {} {}", sub_pid, model, endpoint);

  if (sub_pid == 0 || endpoint.empty() || model.empty()) {
    logger->error("invalid item object: {} {} {}", sub_pid, model, endpoint);
    return false;
  }
  int rc = kill(sub_pid, 0);
  if (rc < 0) {
    if (errno == ESRCH) {
      logger->error("kill error : {}", errno);
      return false;
    }
  }

  auto classifier_stub = Classifier::NewStub(
      grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials()));
  grpc::ClientContext ctx;
  Model query;
  query.set_model(item["model"].asString());
  maum::common::LangCode langCode;
  maum::common::LangCode_Parse(item["lang"].asString(), &langCode);
  query.set_lang(langCode);

  ServerStatus ss;
  Status status = classifier_stub->Ping(&ctx, query, &ss);

  if (status.ok()) {
    // UPDATE
    if (item["state"].asInt() != ss.state()) {
      logger->debug("update server state as {}", ss.state());
      updated = true;
      item["state"] = ss.state();
    }
    return !ss.server_address().empty();
  } else {
    time_t now = time(nullptr);
    time_t cmp = item["forkedAt"].asInt64();
    long diff = abs(now - cmp);
    if (item["state"].asInt() == maum::brain::cl::SERVER_STATE_STARTING &&
        diff < kWaitTimeoutToStarting) {
      logger->debug("server is still starting started: {}, diff: {}",
                    cmp, diff);
      return true;
    } else {
      item["state"] = maum::brain::cl::SERVER_STATE_NONE;
      updated = true;
      logger->debug("server not running, update server state as {}",
                    maum::brain::cl::SERVER_STATE_NONE);
      return false;
    }
  }
}

/**
 * 현재의 모델 목록에서 특정한 `pid`를 가진 객체를 삭제한다.
 *
 * SIGCHLD 시그널의 결과를 처리하는 함수이다.
 * @param pid 삭제할 프로세스 ID
 * @return 성공하면 0, 실패하면 -1
 */
int ClassifierResolver::ClearModel(pid_t pid) {
  lock_guard<mutex> guard(root_lock_);
  for (auto item : root_) {
    auto run_pid = item["pid"].asInt();
    if (pid == run_pid) {
      auto path = item["path"].asString();

      root_[path].clear();
      root_.removeMember(path);
      WriteStatus();
      LOGGER()->debug("pid {} removed in root_", pid);
      return 0;
    }
  }
  LOGGER()->debug("pid {} not found in root_", pid);
  return -1;
}

/**
 * 모든 모델의 목록을 구한다.
 *
 * @param list 모델의 목록
 * @return grpc Status 대부분 OK
 */
Status ClassifierResolver::GetModels(ModelList *list) {
  struct dirent dirent;
  struct dirent *result;
  DIR *dir;
  dir = opendir(sa_model_root_.c_str());
  auto logger = LOGGER();
  if (dir == NULL) {
    logger->error("cannot open directory {}", sa_model_root_);
    return Status(grpc::StatusCode::NOT_FOUND, "cannot open directory!");
  }

  while (readdir_r(dir, &dirent, &result) == 0 && result != NULL) {
    if (strncmp(dirent.d_name, ".", 1) == 0 ||
        strncmp(dirent.d_name, "..", 2) == 0)
      continue;
    if (dirent.d_type == DT_DIR) {
      logger->debug("sub directory found {}", dirent.d_name);
      char *p = strchr(dirent.d_name, '-');
      if (p) {
        string model(dirent.d_name, p);
        p++;
        string lang(p);
        logger->debug("subdirectory model {}, lang {}", model, lang);
        LangCode lc;
        if (maum::common::LangCode_Parse(lang, &lc)) {
          auto m = list->add_models();
          m->set_lang(lc);
          m->set_model(model);
          logger->debug("adding model {} {}", m->lang(), m->model());
        } else {
          logger->debug("invalid lang {}", lang);
        }
      }
    }
  }
  closedir(dir);

  return Status::OK;
}

/**
 * 모든 실행 중인 서버의 목록을 구한다.
 *
 * @param list 모델의 목록
 * @return grpc Status 대부분 OK
 */
Status ClassifierResolver::GetServers(ServerStatusList *list) {
  lock_guard<mutex> guard(root_lock_);
  int sync = 0;
  for (auto item : root_) {
    auto server = list->add_servers();
    LangCode lc;
    maum::common::LangCode_Parse(item["lang"].asString(), &lc);
    server->set_model(item["model"].asString());
    server->set_lang(lc);
    server->set_server_address(item["endpoint"].asString());
    server->set_invoked_by("get servers");
    bool updated = false;
    auto path = item["path"].asString();
    if (PingServer(item, updated)) {
      server->set_running(true);
      if (updated) {
        root_[path] = item;
        sync++;
      }
    } else {
      lock_guard<mutex> guard2(root_lock_);
      root_[path].clear();
      root_.removeMember(path);
      server->set_running(false);
      sync++;
    }
    server->set_state(ServerState(item["state"].asInt()));
  }
  if (sync)
    WriteStatus();
  return Status::OK;
}

/**
 * 지정한 모델의 서비스를 중지한다.
 *
 * @param model 중지할 모델
 * @param found 발견된 서버 상태 정보
 * @return 성공하면 true, 아니면 false
 */
bool ClassifierResolver::Stop(const Model &model, const Json::Value &found) {
  auto logger = LOGGER();
  logger->debug("found {}", found.toStyledString());
  pid_t pid = found["pid"].asInt();
  logger->debug("killing pid : {}", pid);

  bool ret = false;
  // TODO
  // 정상적으로 죽도록 처리해야 한다!!
  int rc = kill(pid, SIGTERM);
  if (rc < 0) {
    if (errno == ESRCH) {
      logger->warn("kill ESRCH error : {}", errno);
      ret = true;
    } else if (errno == EPERM) {
      logger->error("kill EPERM error : {}", errno);
      ret = false;
    }
  } else
    ret = true;
  if (ret) {
    auto model_path = GetModelPath(model);
    lock_guard<mutex> guard(root_lock_);
    root_[model_path].clear();
    root_.removeMember(model_path);
    WriteStatus();
  }
  return ret;
}

/**
 * 동작 중인 서버를 종료시킨다.
 *
 * @param context 호출 컨텍스트
 * @param m 모델
 * @param server 서버 상태
 * @return grpc status
 */
Status ClassifierResolver::Stop(const Model *m,
                                ServerStatus *server) {
  server->set_model(m->model());
  server->set_lang(m->lang());
  server->set_invoked_by("stop");

  Json::Value found;
  auto running = IsRunning(*m, found);
  if (running) {
    Stop(*m, found);
    server->set_running(false);
    server->set_state(maum::brain::cl::SERVER_STATE_NONE);
  } else {
    server->set_running(false);
    server->set_state(maum::brain::cl::SERVER_STATE_NONE);
    server->set_invoked_by("stop&not_running");
  }
  return Status::OK;
}

/**
 * 동작 중이거나 멈춰있는 모델을 다시 실행한다.
 * 모델의 내용이 바뀌었을 경우에 호출 할 수 있다.
 *
 * @param m 모델
 * @param server 서버의 상태 정보, 실행된 결과를 반환할 데이터
 * @return grpc Status
 */
Status ClassifierResolver::Restart(const Model *m,
                                   ServerStatus *server) {
  server->set_model(m->model());
  server->set_lang(m->lang());
  server->set_invoked_by("restart");
  Json::Value found;
  auto running = IsRunning(*m, found);
  if (running) {
    Stop(*m, found);
    server->set_running(false);
  } else {
    server->set_running(false);
    server->set_invoked_by("stop&not_running");
  }

  Status status = Status::OK;

  Json::Value item;
  ForkResult res = ForkServer(m, item);
  auto logger = LOGGER();
  logger->debug("[res] fork res {}", res);
  switch (res) {
    case FORK_SUCCESS: {
      Json::Value endpoint = item["endpoint"];
      logger->debug("[server endpoint] {}", endpoint.asString());
      server->set_server_address(endpoint.asString());
      server->set_running(true);
      server->set_state(ServerState(item["state"].asInt()));
      status = Status::OK;
      break;
    }
    case FORK_NOT_FOUND: {
      logger->debug("cannot find {} : {}", m->model(), m->lang());
      status = Status(StatusCode::NOT_FOUND,
                      "Cannot find requested path or name");
      break;
    }
    case FORK_FAILED: {
      logger->debug("fork failed  errno: {}", errno);
      status = Status(StatusCode::INTERNAL, "Cannot fork");
      break;
    }
  }
  return status;
}

/**
 * 서버가 실행 중인지 점검한다.
 *
 *
 * @param m 모델
 * @param server 서버 상태 정보, 반환되는 값
 * @return grpc Status
 */
Status ClassifierResolver::Ping(const Model *m, ServerStatus *server) {

  server->set_model(m->model());
  server->set_lang(m->lang());
  server->set_invoked_by("ping");

  Json::Value found;
  auto running = IsRunning(*m, found);
  server->set_running(running);
  server->set_state(ServerState(found["state"].asInt()));
  if (!running) {
    server->clear_server_address();
  }

  return Status::OK;
}

/**
 * 새로운 파일로 모델을 업데이트 한다.
 *
 * 분류기 자식 서버가 이미 실행 중이라면 중지한다.
 *
 * @param model 업데이트할 모델
 * @param reader 파일 정보
 * @param resp 결과, 응답 내용
 * @return grpc Status
 */
Status ClassifierResolver::SetModel(const Model *model,
                                    ServerReader<::maum::common::FilePart> *reader,
                                    SetModelResponse *resp) {
  Json::Value found;
  bool running = IsRunning(*model, found);
  if (running) {
    Stop(*model, found);
  }
  resp->set_lang(model->lang());
  resp->set_model(model->model());
  if (!ExtractTar(reader, sa_model_root_)) {
    resp->set_result(SetModelResult::CL_SETMODEL_FAILED);
    resp->set_error("cannot extract tar file");
    return Status::OK;
  }
  resp->set_result(SetModelResult::CL_SETMODEL_SUCCESS);
  if (running) {
    ForkResult res = ForkServer(model, found);
    if (res == FORK_SUCCESS) {
      resp->set_result(SetModelResult::CL_SETMODEL_SUCCESS_RESTART);
    } else {
      resp->set_error("fork failed!");
      resp->set_result(SetModelResult::CL_SETMODEL_SUCCESS_NOTSTARTED);
    }
  }

  return Status::OK;
}

/**
 * 각 모델에 포함된 모든 카테고리 목록을 구해온다.
 *
 * @param model 모델의 이름 및 언어
 * @param categories 카테고리 목록
 * @return grpc Status
 */
Status ClassifierResolver::GetModelCategories(const Model *model,
                                              ModelCategories *categories) {

  string model_path = GetModelPath(*model);
  string full_path = sa_model_root_;
  full_path += '/';
  full_path += model_path;

  int path_found = access(full_path.c_str(), 0);
  if (path_found != 0) {
    return Status(StatusCode::NOT_FOUND,
                  "Cannot find requested path or name");
  } else {
    string file(full_path);
    file += "/vocab_sa.txt";

    struct stat st;
    int rc = lstat(file.c_str(), &st);
    if (rc < 0) {
      LOGGER()->warn("find error rc {}, {}", rc, file);
      return Status(StatusCode::NOT_FOUND,
                    "Cannot find requested path or name");
    }
    auto upd = categories->mutable_updated_at();
    upd->set_seconds(st.st_mtim.tv_sec);
    upd->set_nanos(st.st_mtim.tv_nsec);
    categories->set_model(model->model());
    categories->set_lang(model->lang());

    ifstream ifs(file, ios_base::in);
    for (string euc_line; std::getline(ifs, euc_line);) {
      string u_buf = EuckrToUtf8(euc_line);
      categories->add_categories(u_buf);
    }
  }

  return Status::OK;
}

/**
 * 모델을 학습 데이터를 삭제한다.
 *
 * 모델에 관련된 서버가 떠 있으면 이를 중지하고 삭제한다.
 *
 * @param m 모델정보
 * @param server 서버 상태
 * @return GRPC 리턴
 */
Status ClassifierResolver::DeleteModel(const Model *m,
                                       ServerStatus *server) {
  server->set_model(m->model());
  server->set_lang(m->lang());
  server->set_invoked_by("delete");

  Json::Value found;
  bool running = IsRunning(*m, found);
  if (running) {
    Stop(*m, found);
  }
  server->set_running(false);
  server->set_state(maum::brain::cl::SERVER_STATE_NONE);
  string path = sa_model_root_;
  path += '/';
  path += GetModelPath(*m);

  RemoveFile(path.c_str(), FILEUTILS_RECUR | FILEUTILS_FORCE);
  return Status::OK;
}

/**
 * 새로운 child process의 메인 함수
 *
 * Classifier 시스템을 초기화하고 정상적으로 동작하도록 한다.
 *
 * @param name 도메인 이름
 * @param path DB 경로
 * @param endpoint 응답 엔드포인트
 * @param port 서버 포트
 */
void RunClassifierChild(const string &name, const string &lang,
                        const string &endpoint, int port) {
  libmaum::Config &c = libmaum::Config::Instance();
  auto logger = c.GetLogger();
  std::string server_address("0.0.0.0:");
  server_address += std::to_string(port);

  LangCode lang_code;
  maum::common::LangCode_Parse(lang, &lang_code);

  ClassifierService *service;
  if (lang_code == LangCode::kor)
    service = new KoreanClassifier(name, lang_code, endpoint);
  else
    service = new EnglishClassifier(name, lang_code, endpoint);

  ServerBuilder builder;
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  builder.RegisterService(service);
  unique_ptr<Server> server(builder.BuildAndStart());

  if (server) {
    logger->info("new child classifier server {} {} listening on {}",
                 name, lang, endpoint);
    std::thread init([&]() {
      logger->info("classifier {} {} initializing", name, lang);
      service->Initialize();
      logger->info("classifier {} {} running", name, lang);
    });
    init.detach();
    server->Wait();
  }
}
