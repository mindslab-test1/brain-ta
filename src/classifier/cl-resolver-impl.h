#ifndef CL_MODEL_RESOLVER_IMPL_H
#define CL_MODEL_RESOLVER_IMPL_H

#include <grpc++/grpc++.h>
#include "maum/brain/cl/classifier.grpc.pb.h"
#include "classifier-resolver.h"
#include "cl-util.h"

class ClassifierResolverImpl final :
    public maum::brain::cl::ClassifierResolver::Service {
 public:
  ClassifierResolverImpl() {
    resolver_ = ClassifierResolver::GetInstance();
  }
  virtual ~ClassifierResolverImpl() {
  }
  const char *Name() {
    return "DNN Classifier Resolver";
  }

  Status Find(ServerContext* context,
              const Model* model, ServerStatus* status) override {
    return resolver_->Find(model, status);
  }

  Status GetModels(ServerContext* context,
                   const Empty* request, ModelList *models) override {
    return resolver_->GetModels(models);
  }
  Status GetServers(ServerContext* context,
                    const Empty* empty, ServerStatusList *list) override  {
    return resolver_->GetServers(list);
  }
  Status Stop(ServerContext* context,
              const Model* model, ServerStatus* status) override {
    return resolver_->Stop(model, status);
  }
  Status Restart(ServerContext* context,
                 const Model* model, ServerStatus* status) override {
    return resolver_->Restart(model, status);
  }
  Status Ping(ServerContext* context,
              const Model* model, ServerStatus * status) override {
    return resolver_->Ping(model, status);
  }
  Status SetModel(ServerContext* context,
                  ServerReader<::maum::common::FilePart>* reader,
                  SetModelResponse* response) {
    Model model;
    GetModelFromContext(context, model);
    Status st = resolver_->SetModel(&model, reader, response);
    return st;
  }

  Status DeleteModel(ServerContext* context,
                     const Model* model, ServerStatus* status) {
    return resolver_->DeleteModel(model, status);
  }

  Status GetModelCategories(ServerContext *context,
                            const Model *model,
                            ModelCategories *categories) override {
    return resolver_->GetModelCategories(model, categories);
  }

 private:
  ClassifierResolver * resolver_;
};

#endif