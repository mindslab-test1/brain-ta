#include <grpc++/grpc++.h>
#include <maum/brain/nlp/nlp.pb.h>
#include <maum/brain/nlp/nlp.grpc.pb.h>
#include "nlp-kor-handler.h"
#include "nlp-eng-handler.h"
#include <libmaum/brain/hmd/kor-hmd.h>
#include <libmaum/brain/hmd/eng-hmd.h>
\
using grpc::ServerContext;
using grpc::Status;
using grpc::ServerReaderWriter;
using google::protobuf::Empty;
using maum::common::LangCode;
using maum::brain::hmd::HmdModel;
using maum::brain::hmd::HmdModelList;
using maum::brain::hmd::HmdResult;
using maum::brain::hmd::HmdResultDocument;
using maum::brain::hmd::ModelKey;
using maum::brain::hmd::HmdInputText;
using maum::brain::hmd::HmdInputDocument;
using maum::brain::hmd::HmdClassifier;

using namespace::std;
using maum::brain::nlp::NaturalLanguageProcessingService;

class HmdServiceImpl: public HmdClassifier::Service {
 public:
  HmdServiceImpl();
  ~HmdServiceImpl();
  const char *Name();
  bool GetClassifier(const HmdInputDocument *doc, LangCode lan);
  bool GetClassifier(const HmdInputText *txt, LangCode lan);
  void Analyze(const HmdInputText *req, Document *doc);
  Status SetModel(ServerContext *context,
                  const HmdModel *request, Empty *empty) override;
  Status SetMatrix(ServerContext *context,
                  const HmdModel *request, Empty *empty) override;
  Status GetModel(ServerContext *context,
                  const ModelKey *request, HmdModel *provider) override;
  Status GetModels(ServerContext *context,
                   const Empty *empty, HmdModelList *provider) override;
  Status GetClass(ServerContext *context,
    const HmdInputDocument *request, HmdResult *provider) override;
  Status GetClassMultiple(ServerContext *context,
    ServerReaderWriter<HmdResultDocument, HmdInputDocument> *stream) override;
  Status GetClassByText(ServerContext *context,
    const HmdInputText *request, HmdResultDocument *provider) override;
  Status GetClassMultipleByText(ServerContext *context,
    ServerReaderWriter<HmdResultDocument, HmdInputText> *stream) override;


 private:
  ModelLoader ml_;
  KorHmd k_cl_;
  EngHmd e_cl_;
  shared_ptr<NlpKoreanHandler> n_kor3_;
  shared_ptr<NlpEnglishHandler> n_eng_;
};