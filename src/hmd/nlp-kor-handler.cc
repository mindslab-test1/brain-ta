#include <maum/brain/nlp/nlp.grpc.pb.h>
#include <libmaum/brain/nlp/nlp3kor.h>
#include "nlp-kor-handler.h"
#include <thread>
#include <libmaum/common/config.h>
#include <libmaum/brain/error/ta-error.h>

using std::unique_ptr;
using grpc::StatusCode;

const int kIntentFinderFeature = (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION) |
    (1 << NlpFeature::NLPF_MORPHEME) |
    (1 << NlpFeature::NLPF_NAMED_ENTITY);

/**
 * nlp 처리를 위한 리소스 로딩
 *
 * @return is_done 리소스 로딩 성공여부
 */
bool NlpKoreanHandler::Init() {
  bool is_done = false;
  auto logger = LOGGER();
  logger->debug(">>> [Initialize NlpKoreanHandler] Start");
  auto &c = libmaum::Config::Instance();
  rsc_path_ = c.Get("brain-ta.resource.3.kor.path");
  nlp3Kor_ = new Nlp3Kor(rsc_path_, kIntentFinderFeature);
  nlp3Kor_->UpdateFeatures();

  is_started = true;
  logger->debug("<<< [Initialize NlpKoreanHandler] End");
  is_done = true;
  return is_done;
}

/**
 * nlp 리소스 내리는 함수
 *
 * @return 리소스 내리기 성공여부
 */
bool NlpKoreanHandler::Stop() {
  bool result = false;
  auto logger = LOGGER();
  logger->debug("NlpKoreanHandler Stop");
  nlp3Kor_->Uninit();
  result = true;
  return result;
}

/**
 * 언어분석함수
 *
 * @param text 입력텍스트
 * @param document 언어분석결과 Document
 * @return is_done 언어분석 성공여부
 */
bool NlpKoreanHandler::Analyze(const InputText *text,
                               Document *document) {
  bool is_done = false;
  auto logger = LOGGER();
  if (is_started == false)
    return is_done;
  nlp3Kor_->AnalyzeOne(text, document);
  is_done = true;
  return is_done;
}