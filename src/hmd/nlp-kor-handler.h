#ifndef PROJECT_NLP_KOR_HANDLER_H
#define PROJECT_NLP_KOR_HANDLER_H

#include <string>
#include <vector>
#include <grpc++/grpc++.h>
#include <mutex>
#include <condition_variable>
#include <unordered_set>

#include <maum/brain/nlp/nlp.grpc.pb.h>
#include <libmaum/brain/nlp/nlp3kor.h>

/**
 *
 */
class NlpKoreanHandler {
 public:
  NlpKoreanHandler() { is_started = false; };
  virtual ~NlpKoreanHandler() {};

  bool Init();
  bool Stop();
  bool Analyze(const InputText *text, Document *document);

 private:

  bool is_started;
  //NLP Model
  Nlp3Kor *nlp3Kor_;
  // nlp-kor3 리소스 경로
  std::string rsc_path_;

};

#endif //PROJECT_NLP_KOR_HANDLER_H