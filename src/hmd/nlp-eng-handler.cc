#include <maum/brain/nlp/nlp.grpc.pb.h>
#include <libmaum/brain/nlp/nlp2eng.h>
#include "nlp-eng-handler.h"
#include <thread>
#include <libmaum/common/config.h>
#include <libmaum/brain/error/ta-error.h>

using std::unique_ptr;
using grpc::StatusCode;

const int kIntentFinderFeature = (1 << NlpFeature::NLPF_MORPHEME) |
    (1 << NlpFeature::NLPF_NAMED_ENTITY);

/**
 * nlp 처리를 위한 리소스 로딩
 *
 * @return is_done 리소스 로딩 성공여부
 */
bool NlpEnglishHandler::Init() {
  bool is_done = false;
  auto logger = LOGGER();
  logger->debug(">>> [Initialize NlpEnglishHandler] Start");
  auto &c = libmaum::Config::Instance();
  rsc_path_ = c.Get("brain-ta.resource.2.eng.path");

  nlp2Eng_ = new Nlp2Eng(rsc_path_, kIntentFinderFeature);
  nlp2Eng_->UpdateFeatures();

  is_started = true;
  logger->debug("<<< [Initialize NlpEnglishHandler] End");
  is_done = true;
  return is_done;
}

/**
 * nlp 리소스 내리는 함수
 *
 * @return 리소스 내리기 성공여부
 */
bool NlpEnglishHandler::Stop() {
  bool result = false;
  auto logger = LOGGER();
  logger->debug("NlpEnglishHandler Stop");
  nlp2Eng_->Uninit();
  result = true;
  return result;
}

/**
 * 언어분석함수
 *
 * @param text 입력텍스트
 * @param document 언어분석결과 Document
 * @return is_done 언어분석 성공여부
 */
bool NlpEnglishHandler::Analyze(const InputText *text,
                                Document *document) {
  bool is_done = false;
  auto logger = LOGGER();
  if (is_started == false)
    return is_done;
  nlp2Eng_->AnalyzeOne(text, document);
  is_done = true;
  return is_done;
}