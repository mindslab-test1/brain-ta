#include "hmd-impl.h"
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>
#include <grpc++/grpc++.h>
#include <maum/brain/nlp/nlp.grpc.pb.h>
#include <maum/brain/nlp/nlp.pb.h>
#include <fstream>
#include <iostream>
#include <string>
#include <gitversion/version.h>

using namespace std;
using maum::brain::hmd::HmdRule;
using maum::brain::hmd::HmdModel;
using maum::brain::hmd::HmdResultDocument;
using maum::brain::hmd::HmdClassified;

using maum::common::LangCode;
using maum::brain::nlp::Document;
using maum::brain::nlp::InputText;
using maum::brain::nlp::NlpAnalysisLevel;
using maum::brain::nlp::KeywordFrequencyLevel;
using maum::brain::nlp::NaturalLanguageProcessingService;

using google::protobuf::Empty;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::Service;
using grpc::Status;
using grpc::ClientContext;

void RunServer(const char *prog) {
  auto &c = libmaum::Config::Instance();
  string server_address("0.0.0.0:");
  server_address += c.Get("brain-ta.hmd.cpp.port");
  int grpc_timeout = stoi(c.Get("brain-ta.hmd.front.timeout").c_str());

  cout << server_address << "/" << grpc_timeout << endl;

  c.DumpPid();

  HmdServiceImpl service;
  ServerBuilder builder;
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  builder.RegisterService(&service);

  unique_ptr<Server> server(builder.BuildAndStart());
  if (server) {
    LOGGER()->info("{} {} starting listening on {} with {}",
                   prog,
                   version::VERSION_STRING,
                   server_address,
                   service.Name());
    cout << "server listening..." << endl;
    server->Wait();
  }
}


int main(int argc, char *argv[]) {
  auto& c = libmaum::Config::Init(argc, argv, "brain-ta.conf");


  ModelLoader ml(c.Get("brain-ta.hmd.model.dir"));
  RunServer(basename(argv[0]));
}
