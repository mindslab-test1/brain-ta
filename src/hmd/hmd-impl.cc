#include "hmd-impl.h"
#include "libmaum/common/config.h"

using std::string;
using maum::brain::nlp::InputText;
using maum::brain::nlp::NaturalLanguageProcessingService;
using maum::common::LangCode;
using maum::brain::hmd::HmdInputText;
using maum::brain::hmd::HmdMatrixItem;
using grpc::ClientContext;
using grpc::StatusCode;
using grpc::Status;
using ::maum::brain::nlp::KeywordFrequencyLevel;
using namespace std::chrono;

HmdServiceImpl::HmdServiceImpl() {
  auto logger = LOGGER();
  const system_clock::time_point start = system_clock::now();
  auto &c = libmaum::Config::Instance();
  ml_.SetModelDir(c.Get("brain-ta.hmd.model.dir"));

  vector<HmdMatrix> matrix_vec;
  ml_.LoadAllMatrixModels(LangCode::kor, matrix_vec);
  for (const auto &v : matrix_vec) {
    k_cl_.SetMatrix(v);
  }
  matrix_vec.clear();
  ml_.LoadAllMatrixModels(LangCode::eng, matrix_vec);
  for (const auto &v : matrix_vec) {
    e_cl_.SetMatrix(v);
  }

  n_kor3_ = make_shared<NlpKoreanHandler>();
  n_kor3_->Init();
  n_eng_ = make_shared<NlpEnglishHandler>();
  n_eng_->Init();
  auto took = duration_cast<milliseconds>(system_clock::now() - start);
  logger->trace("Hmd Server load done, took [{}] ms.", took.count());
}

HmdServiceImpl::~HmdServiceImpl() {
}

const char *HmdServiceImpl::Name() {
  return "HMD Service";
}

Status HmdServiceImpl::SetModel(ServerContext *context,
                                const HmdModel *request, Empty *empty) {
  auto logger = LOGGER();
  if (request->lang() == LangCode::kor) {
    k_cl_.SetModel(*request);
    ml_.Save(*request);
  } else if (request->lang() == LangCode::eng) {
    e_cl_.SetModel(*request);
    ml_.Save(*request);
  } else {
    logger->error("Set Model failed. not lang.");
    return Status(StatusCode::INTERNAL, "not found Language");
  }
  return Status::OK;
}

Status HmdServiceImpl::SetMatrix(ServerContext *context,
                                 const HmdModel *request, Empty *empty) {
  auto logger = LOGGER();
  const system_clock::time_point start = system_clock::now();
  if (request->lang() == LangCode::kor) {
    k_cl_.HmdModelToMatrix(*request);
  } else if (request->lang() == LangCode::eng) {
    e_cl_.HmdModelToMatrix(*request);
  } else {
    logger->error("Set Model to Matrix failed. wrong lang, {}",
                  request->lang());
    return Status(StatusCode::INTERNAL, "not found Language");
  }
  auto took = duration_cast<milliseconds>(system_clock::now() - start);
  logger->trace("Set [{}] matrix model success! took {} ms",
                request->model(),
                took.count());
  return Status::OK;
}

Status HmdServiceImpl::GetModel(ServerContext *context,
                                const ModelKey *request, HmdModel *provider) {

  auto logger = LOGGER();
  string model;
  HmdModel hmdmodel;
  if (request->lang() == LangCode::kor) {
    k_cl_.GetModel(request->model(), LangCode::kor, hmdmodel);
    provider->CopyFrom(hmdmodel);
    return Status::OK;
  } else if (request->lang() == LangCode::eng) {
    e_cl_.GetModel(request->model(), LangCode::eng, hmdmodel);
    provider->CopyFrom(hmdmodel);
    return Status::OK;
  } else {
    return Status(StatusCode::INTERNAL, "not lang");
  }
}

Status HmdServiceImpl::GetModels(ServerContext *context,
                                 const Empty *empty, HmdModelList *provider) {
  HmdModelList list;
  vector<HmdModel> models;
  k_cl_.GetModels(LangCode::kor, models);
  for (auto const &c : models) {
    list.add_models()->CopyFrom(c);
  }
  models.clear();
  e_cl_.GetModels(LangCode::eng, models);
  for (auto const &c : models) {
    list.add_models()->CopyFrom(c);
  }
  provider->CopyFrom(list);
  return Status::OK;
}

Status HmdServiceImpl::GetClass(ServerContext *context,
                                const HmdInputDocument *request,
                                HmdResult *provider) {
  auto logger = LOGGER();
  HmdResult res;
  vector<HmdClassified> cls;
  if (GetClassifier(request, LangCode::kor)) {
    cls = k_cl_.ClassifyWithMatrix(request->model(), request->document());
  } else if (GetClassifier(request, LangCode::eng)) {
    cls = e_cl_.ClassifyWithMatrix(request->model(), request->document());
  } else {
    logger->error("Get Classfier failed. not classifier.");
    return Status(StatusCode::INTERNAL, "not classifier");
  }
  for (auto const &c : cls) {
    res.add_cls()->CopyFrom(c);
  }
  provider->CopyFrom(res);
  return Status::OK;
}

Status HmdServiceImpl::GetClassMultiple(ServerContext *context,
                                        ServerReaderWriter<HmdResultDocument,
                                                           HmdInputDocument> *stream) {
  auto logger = LOGGER();
  HmdInputDocument docs;
  vector<HmdClassified> cls;
  vector<HmdResultDocument> res_vec;
  while (stream->Read(&docs)) {
    HmdResultDocument res;
    const HmdInputDocument *doc = &docs;
    if (GetClassifier(doc, LangCode::kor)) {
      cls = k_cl_.ClassifyWithMatrix(doc->model(), doc->document());
    } else if (GetClassifier(doc, LangCode::eng)) {
      cls = e_cl_.ClassifyWithMatrix(doc->model(), doc->document());
    } else {
      logger->error("Get Classifier failed. not classifier.");
      return Status(StatusCode::INTERNAL, "not classifier");
    }
    for (auto const &c : cls) {
      res.add_cls()->CopyFrom(c);
    }
    stream->Write(res);
    if (res.cls_size() == 0) {
      stream->Write(HmdResultDocument());
    }
  }
  return Status::OK;
}

void HmdServiceImpl::Analyze(const HmdInputText *req, Document *document) {
  auto logger = LOGGER();
  InputText input_text;
  input_text.set_text(req->text());
  input_text.set_level(NlpAnalysisLevel::NLP_ANALYSIS_NAMED_ENTITY);
  input_text.set_keyword_frequency_level(KeywordFrequencyLevel::KEYWORD_FREQUENCY_NONE);
  input_text.set_use_tokenizer(false);
  input_text.set_split_sentence(true);
  LangCode lang = req->lang();
  // 1. nlp 처리
  if (lang == LangCode::kor) { // 한국
    logger->debug("Analyze sentences(ko_KR)");
    input_text.set_lang(::maum::common::LangCode::kor);
    logger->trace("InputText={}", input_text.Utf8DebugString());
    // NLP 분석 전에 치환사전 적용 후 언어분석을 수행하는 함수
    n_kor3_->Analyze(&input_text, document);
    logger->trace("Document={}", document->Utf8DebugString());
    logger->debug("  +text={}", document->sentences(0).text());
  } else if (lang == LangCode::eng) { // 영어
    logger->debug("Analyze sentences(en_US)");
    input_text.set_lang(::maum::common::LangCode::eng);
    logger->trace("InputText={}", input_text.Utf8DebugString());
    n_eng_->Analyze(&input_text, document);
  }
}

Status HmdServiceImpl::GetClassByText(ServerContext *context,
                                      const HmdInputText *request,
                                      HmdResultDocument *provider) {
  auto logger = LOGGER();
  Document *doc(new Document());
  vector<HmdClassified> cl_vec;
  HmdResultDocument *res(new HmdResultDocument());

  Analyze(request, doc);
  LangCode lang = request->lang();
  if (lang == LangCode::kor) {
    if (!GetClassifier(request, lang)) {
      logger->error("Get Classifier failed. not lang.");
      delete res;
      delete doc;
      return Status(StatusCode::INTERNAL, "not model");
    }
    cl_vec = k_cl_.ClassifyWithMatrix(request->model(), *doc);
    for (auto const &c: cl_vec) {
      res->add_cls()->CopyFrom(c);
    }
    res->mutable_document()->CopyFrom(*doc);
  } else if (lang == LangCode::eng) {
    if (!GetClassifier(request, LangCode::eng)) {
      delete res;
      delete doc;
      logger->error("Get Classifier failed. not model.");
      return Status(StatusCode::INTERNAL, "not model");
    }
    cl_vec = e_cl_.ClassifyWithMatrix(request->model(), *doc);
    for (auto const &c: cl_vec) {
      res->add_cls()->CopyFrom(c);
    }
    res->mutable_document()->CopyFrom(*doc);
  } else {
    delete res;
    delete doc;
    logger->error("Get Classifier failed. not lang.");
    return Status(StatusCode::INTERNAL, "not lang");
  }
  provider->CopyFrom(*res);
  delete res;
  delete doc;
  return Status::OK;
}

Status HmdServiceImpl::GetClassMultipleByText(ServerContext *context,
                                              ServerReaderWriter<
                                                  HmdResultDocument,
                                                  HmdInputText> *stream) {
  auto logger = LOGGER();
  LangCode lang;
  Document *doc(new Document());
  HmdInputText text;
  vector<HmdResultDocument> res_vec;
  vector<HmdInputText> txt_vec;
  vector<HmdClassified> cl_vec;

  while (stream->Read(&text)) {
    ClientContext c_context;
    const HmdInputText *txt = &text;
    lang = text.lang();
    Analyze(txt, doc);
    if (lang == LangCode::kor) {
      if (!GetClassifier(txt, LangCode::kor)) {
        logger->error("Get Classifier failed. not model.");
        return Status(StatusCode::INTERNAL, "not model");
      }
      HmdResultDocument *res = new HmdResultDocument();
      cl_vec = k_cl_.ClassifyWithMatrix(txt->model(), *doc);
      for (auto const &c: cl_vec) {
        res->add_cls()->CopyFrom(c);
      }
      res->mutable_document()->CopyFrom(*doc);
      res_vec.push_back(*res);
    } else if (lang == LangCode::eng) {
      if (!GetClassifier(txt, LangCode::eng)) {
        logger->error("Get Classifier failed. not model.");
        return Status(StatusCode::INTERNAL, "not model");
      }
      HmdResultDocument *res = new HmdResultDocument();
      cl_vec = e_cl_.ClassifyWithMatrix(txt->model(), *doc);
      res->mutable_document()->CopyFrom(*doc);
      for (auto const &c: cl_vec) {
        res->add_cls()->CopyFrom(c);
      }
      res->mutable_document()->CopyFrom(*doc);
      res_vec.push_back(*res);
    } else {
      logger->error("Get Classifier failed. not lang or model.");
      return Status(StatusCode::INTERNAL, "not lang or model");
    }
  }
  for (auto const &r : res_vec) {
    stream->Write(r);
  }
  return Status::OK;
}

bool HmdServiceImpl::GetClassifier(const HmdInputDocument *doc, LangCode lan) {
  auto logger = LOGGER();
  if (lan == LangCode::kor) {
    if (k_cl_.HasModel(doc->model()) != "") {
      return true;
    }
  } else if (lan == LangCode::eng) {
    if (e_cl_.HasModel(doc->model()) != "") {
      return true;
    }
  } else {
    logger->error("[G]GetClassifier(doc, lang) failed. not lang");
  }
  return false;
}

bool HmdServiceImpl::GetClassifier(const HmdInputText *txt, LangCode lan) {
  auto logger = LOGGER();
  if (lan == LangCode::kor) {
    if (k_cl_.HasMatrix(txt->model())) {
      return true;
    }
  } else if (lan == LangCode::eng) {
    if (e_cl_.HasMatrix(txt->model())) {
      return true;
    }
  } else {
    logger->error("[G]GetClassifier(txt, lang) failed. not lang");
  }
  return false;
}
