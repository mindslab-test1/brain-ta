#include "custom-dict.h"
#include <libmaum/common/config.h>
#include <curl/curl.h>

/**
 * file 데이터를 다운로드 하는 curl callback 함수.
 *
 * @param 처리할 데이터
 * @param size 크기
 * @param nmemb 개수
 * @param out 저장할 사용자 지정 변수
 * @return 저장한 크기
 */
static uint AppendToFile(char *in, uint size, uint nmemb, void *out) {
  int *pfd = reinterpret_cast<int *>(out);
  ssize_t ret = write(*pfd, (void *)in, size * nmemb);
  return ret * size;
}

const char * kMltCallbackApiKey = "7c3a778e29ac522de62c4e22aa90c9";

/**
 * 파일을 다운로드한다.
 *
 * `dict_` 메시지에 함께 전달되어 온 URL을 통해서 처리한다.
 * @return 다운로드에 성공하면 0을 반환한다.
 */
int CustomDict::Download() {
  const auto & url = dict_.download_url();
  auto logger = LOGGER();

  string full(url);
  full += kMltCallbackApiKey;
  logger->debug("[dict download url] {}", full);

  CURL *curl;
  std::string body;
  char err_buf[CURL_ERROR_SIZE];
  curl = curl_easy_init();
  if (!curl) {
    logger->debug<const char *>("curl init failed {}");
    return -1;
  }

  auto &c = libmaum::Config::Instance();
  char fname[PATH_MAX];
  snprintf(fname, sizeof fname, "%s/dict_down_XXXXXX",
           c.Get("brain-ta.resource.1.kor.custom.path").c_str());
  int fd = mkstemp(fname);
  logger->debug("temp file name is {}", fname);

  curl_easy_setopt(curl, CURLOPT_URL, full.c_str());
  curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, err_buf);
  curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
  curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, AppendToFile);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &fd);

  int err = curl_easy_perform(curl);
  if (err) {
    logger->error("curl perform failed {}",
                  curl_easy_strerror((CURLcode) err));
    close(fd);
    return -1;
  }
  curl_easy_cleanup(curl);
  // processing by response
  download_file_.assign(fname);
  fsync(fd);
  struct stat st;
  fstat(fd, &st);
  logger->debug("download file, fd = {}, size = {}", fd, st.st_size);
  close(fd);
  // unlink(fname);

  if (st.st_size == 0) {
    logger->error("dict file is empty. check download url");
    return -1;
  }

  return 0;
}

#include <sstream>
#include <libmaum/common/fileutils.h>
using std::ostringstream;

/**
 * 사용자의 형태소 사전 및 복합명사 사전을 적용한다.
 *
 * @return 성공하면 0을 반환한다.
 */
int CustomDict::GenerateCustomDict() {
  auto logger = LOGGER();
  auto &c = libmaum::Config::Instance();
  string path = c.Get("brain-ta.resource.1.kor.custom.path");
  auto custom_morp = path + "/custom.txt";
  auto custom_compound = path + "/custom_cn.txt";
  auto target = path + "/custom.dat";

  if (dict_.type() == NlpDictType::DIC_CUSTOM_MORPHEME) {
    rename(download_file_.c_str(), custom_morp.c_str());
    if (!IsFile(custom_compound.c_str(), 0)) {
      truncate(custom_compound.c_str(), 0);
    }
  } else {
    rename(download_file_.c_str(), custom_compound.c_str());
    if (!IsFile(custom_morp.c_str(), 0)) {
      truncate(custom_morp.c_str(), 0);
    }
  }

  auto exe = c.Get("brain-ta.resource.1.kor.custom.dicgen");
  if (access(exe.c_str(), X_OK) < 0) {
    logger->error("dicgen binary not found : {} {}", errno, exe);
    return -1;
  }

  ostringstream oss;
  oss << "cd " << path.c_str() << ";"
      << "./dicgen -i -s . -m -t custom.dat -b custom.txt -r custom_cn.txt ;"
      << "rm -f klam*.*;"
      << "rm -f tbl-*.txt;";
  logger->debug("cmd [{}]", oss.str());
  system(oss.str().c_str());

  auto res_base = c.Get("brain-ta.resource.1.kor.path");
  const char * dirs[] = { "ALL", "BLOG", "TWITTER" };
  for (auto d : dirs) {
    auto dest = res_base + "/SCAN/" + d + "/custom.dat";
    if (access(dest.c_str(), R_OK | W_OK) == 0) {
      struct stat st;
      lstat(dest.c_str(), &st);
      if (S_ISLNK(st.st_mode)) {
        if (unlink(dest.c_str()) < 0)
          logger->error("unlink prev dest error: {} {}", errno, dest);
        else
          logger->debug("remove old symlink {}", dest);
      }
      if (S_ISREG(st.st_mode)) {
        string backup = dest + ".org";
        rename(dest.c_str(), backup.c_str());
        logger->debug("rename origin file {} to {}", dest, backup);
      }
    }
    if (symlink(target.c_str(), dest.c_str()) < 0) {
      logger->error("cannot symlink {} from: {} to: {}",
                    errno, target, dest);
    }
  }
  return 0;
}

/**
 * Named Entity 사전을 적용한다.
 *
 * 전처리 사전과 후처리 사전을 따로 적용한다.
 *
 * @return 성공하면 0을 반환한다.
 */
int CustomDict::ApplyNamedEntityDict() {
  auto logger = LOGGER();
  auto &c = libmaum::Config::Instance();
  string path = c.Get("brain-ta.resource.1.kor.custom.path");
  auto ne_pre = path + "/at_pre_dic.txt";
  auto ne_post = path + "/at_post_dic.txt";

  const char * src;
  if (dict_.type() == NlpDictType::DIC_NE_PRE_PROC) {
    rename(download_file_.c_str(), ne_pre.c_str());
    src = ne_pre.c_str();
  } else {
    rename(download_file_.c_str(), ne_post.c_str());
    src = ne_post.c_str();
  }
  const char * name = strrchr(src, '/');

  auto res_base = c.Get("brain-ta.resource.1.kor.path");
  const char * dirs[] = { ".", "news", "blog", "tweet" };
  for (auto d : dirs) {
    auto dest = res_base + "/ner/" + d + name;
    logger->debug("dest name {}", dest);
    if (access(dest.c_str(), R_OK | W_OK) == 0) {
      struct stat st;
      lstat(dest.c_str(), &st);
      if (S_ISLNK(st.st_mode)) {
        if (unlink(dest.c_str()) < 0)
          logger->error("unlink prev dest error: {} {}", errno, dest);
        else
          logger->debug("remove old symlink {}", dest);
      }
      if (S_ISREG(st.st_mode)) {
        string backup = dest + ".org";
        rename(dest.c_str(), backup.c_str());
        logger->debug("rename origin file {} to {}", dest, backup);
      }
    }
    if (symlink(src, dest.c_str()) < 0) {
      logger->error("cannot symlink {} from: {} to: {}",
                    errno, src, dest);
    }
  }
  return 0;
}

/**
 * 동의어 사전을 적용한다.
 *
 * 한국어 NLP의 Normalization/WikiTitle_Normalization.txt을 변경한다.
 * 원래 파일은 org로 변경하고, 다운로드 받은 파일로 해당 디렉토리에
 * 심볼릭 링크를 생성한다.
 *
 * @return 성공하면 0 실패하면 -1을 반환한다.
 */
int CustomDict::ApplySynonym() {
  auto logger = LOGGER();
  auto &c = libmaum::Config::Instance();
  string path = c.Get("brain-ta.resource.1.kor.custom.path");
  auto wiki_normal = path + "/WikiTitle_Normalization.txt";

  const char *src;
  rename(download_file_.c_str(), wiki_normal.c_str());
  src = wiki_normal.c_str();

  auto res_base = c.Get("brain-ta.resource.1.kor.path");
  auto dest = res_base + "/Normalization/WikiTitle_Normalization.txt";
  if (access(dest.c_str(), R_OK | W_OK) == 0) {
    struct stat st;
    lstat(dest.c_str(), &st);
    if (S_ISLNK(st.st_mode)) {
      if (unlink(dest.c_str()) < 0)
        logger->error("unlink prev dest error: {} {}", errno, dest);
      else
        logger->debug("remove old symlink {}", dest);
    }
    if (S_ISREG(st.st_mode)) {
      string backup = dest + ".org";
      rename(dest.c_str(), backup.c_str());
      logger->warn("rename origin file {} to {}", dest, backup);
    }
  }
  if (symlink(src, dest.c_str()) < 0) {
    logger->error("cannot symlink {} from: {} to: {}",
                  errno, src, dest);
    return -1;
  }

  return 0;
}

/**
 * 사용자 사전을 적용하기 위한 외부 인테페이스 함수
 *
 * `dict_` 멤버 변수에 따라서 동작을 다르게 처리한다.
 *
 * @return 적용 결과를 반환한다.
 */
int CustomDict::Apply() {
  switch (dict_.type()) {
    case NlpDictType::DIC_CUSTOM_MORPHEME: {
      return GenerateCustomDict();
    }
    case NlpDictType::DIC_CUSTOM_COMPOUND_NOUN: {
      return GenerateCustomDict();
    }
    case NlpDictType::DIC_NE_PRE_PROC: {
      return ApplyNamedEntityDict();
    }
    case NlpDictType::DIC_NE_POST_PROC: {
      return ApplyNamedEntityDict();
    }
    case NlpDictType::DIC_SYNONYM: {
      return ApplySynonym();
    }
    default: {
      LOGGER()->warn("unkown dict type {} {}",
                     dict_.type(), dict_.download_url());
      return -1;
    }
  }
}

static uint ReadData(char *in, uint size, uint nmemb, void *out) {
  string *body = static_cast<string *>(out);
  body->append(in, size * nmemb);
  return size * nmemb;
}

/**
 * 사용자 사전을 처리하고 난 이후에 callback url 을 처리한다.
 *
 * @return curl callback 호출이 성공하면 0, 실패 하면 -1을 반환한다.
 */
int CustomDict::Callback() {
  const auto & url = dict_.callback_url();
  auto logger = LOGGER();

  string full(url);
  full += kMltCallbackApiKey;
  logger->debug("[dict callback url] {}", full);

  CURL *curl;
  std::string resp_body;
  char err_buf[CURL_ERROR_SIZE];
  curl = curl_easy_init();
  if (!curl) {
    logger->debug<const char *>("curl init failed {}");
    return -1;
  }

  curl_slist *headers = NULL;
  headers = curl_slist_append(headers, "Accept: application/json");
  headers = curl_slist_append(headers, "Content-Type: application/json");
  headers = curl_slist_append(headers, "charsets: utf-8");

  ostringstream oss;
  oss << "{"
      << "\"message\": \""
      << maum::brain::nlp::NlpDictType_Name(dict_.type())
      << " has been applied!"
      << "\"}";
  string body = oss.str();

  curl_easy_setopt(curl, CURLOPT_URL, full.c_str());
  curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, err_buf);
  curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
  curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
  curl_easy_setopt(curl, CURLOPT_POST, 1L);
  curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, ReadData);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &resp_body);
  curl_easy_setopt(curl, CURLOPT_POSTFIELDS, body.c_str());
  curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, body.length());

  int err = curl_easy_perform(curl);
  curl_slist_free_all(headers);
  curl_easy_cleanup(curl);

  logger->debug("response body [{}]", resp_body);

  if (err) {
    logger->debug("curl perform failed {}",
                  curl_easy_strerror((CURLcode) err));
    return -1;
  }
  return 0;
}