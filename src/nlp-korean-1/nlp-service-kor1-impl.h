#ifndef BRAIN_TA_NLP_SERVICE_ETRI_IMPL_H
#define BRAIN_TA_NLP_SERVICE_ETRI_IMPL_H

#include "maum/brain/nlp/nlp.grpc.pb.h"
#include <string>
#include <vector>
#include <grpc++/grpc++.h>
#include <mutex>
#include <condition_variable>
#include <unordered_set>
#include "libmaum/brain/nlp/nlp1kor.h"

class LMInterface;

using grpc::ServerContext;
using grpc::Status;
using google::protobuf::Empty;
using maum::brain::nlp::NaturalLanguageProcessingService;
using maum::brain::nlp::NlpProvider;
using maum::brain::nlp::InputText;
using maum::brain::nlp::Document;
using maum::brain::nlp::NamedEntityTagList;
using maum::brain::nlp::NlpAnalysisLevel;
using maum::brain::nlp::KeywordFrequencyLevel;
using maum::brain::nlp::NlpFeatures;
using maum::brain::nlp::NlpFeatureSupportList;
using maum::brain::nlp::NlpDict;
using grpc::ServerReaderWriter;

class N_Doc;
using std::string;
using std::vector;

/**
 * 대화엔진을 등록하고 대화를 추척할 수 있는 기반을 마련한다.
 */
class NlpKorean1Impl : public NaturalLanguageProcessingService::Service {
 public:
  NlpKorean1Impl();
  virtual ~NlpKorean1Impl();
  const char *Name() {
    return "NLP Korean #1 Service";
  }

  Status GetProvider(ServerContext *context,
                     const Empty *empty,
                     NlpProvider *provider) override;
  Status Analyze(ServerContext *context,
                 const InputText *text,
                 Document *document) override;
  Status AnalyzeMultiple(ServerContext *context,
                         ServerReaderWriter<Document,
                                            InputText> *stream) override;
  Status HasSupport(ServerContext *context,
                    const ::maum::brain::nlp::NlpFeatures *request,
                    NlpFeatureSupportList *response) override;
  Status ApplyDict(ServerContext *context,
                   const NlpDict *dict, Empty *empty) override;
  Status GetNamedEntityTagList(ServerContext *context,
                               const Empty *empty,
                               NamedEntityTagList *namedEntityTagList) override;
  void Start(bool lock = true);
 private:
  void AnalyzeOne(const InputText *text, Document *document);
  void LoadConfig();

  void LoadStringVector();

  // nlp1kor
  Nlp1Kor *nlp1Kor_;
  // nlp-kor1 리소스 경로
  std::string rsc_path_;
  // one-word set
  std::unordered_set<std::string> one_set_;
  // stop-word set
  std::unordered_set<std::string> stop_set_;
  // ne-tag set
  std::map<std::string, std::string> ne_tag_;
  // synonym-tag set
  std::map<std::string, std::string> syn_set_;

  void Stop(bool lock = true);
  bool CanService() {
    std::lock_guard<std::mutex> guard(m_);
    return loaded_ && !loading_;
  }
  void LockRunning() {
    {
      std::lock_guard<std::mutex> guard(m_);
      running_++;
    }
    cv_.notify_one();
  }
  void UnlockRunning() {
    {
      std::lock_guard<std::mutex> guard(m_);
      running_--;
    }
    cv_.notify_one();
  }

  bool loaded_ = false;
  bool loading_ = false;
  int32_t running_ = 0;
  std::mutex m_;
  std::condition_variable cv_;

  class Accessor {
   public:
    Accessor(NlpKorean1Impl *ptr)
        : parent_(ptr) {
      parent_->LockRunning();
    }
    ~Accessor() {
      parent_->UnlockRunning();
    }
   private:
    NlpKorean1Impl *parent_ = nullptr;
  };

  void Restart();
};

// 문자열 치환을 위해
string ReplaceString(const std::string &full_string,
                     const std::string &target_string,
                     const std::string &destination_string);
// 문자열 split을 위해
vector<string> SplitString(const std::string &input,
                           const std::string &delimiter);

#endif // BRAIN_TA_NLP_SERVICE_ETRI_IMPL_H
