#include "nlp-service-kor1-impl.h"
#include <fstream>
#include <iostream>
#include <libmaum/common/encoding.h>
#include <thread>
#include <stdio.h>
#include <sstream>
#include "lm-interface.h"
#include "custom-dict.h"
#include <cstdlib>
#include <google/protobuf/util/json_util.h>
#include <string>

using std::unique_ptr;
using maum::brain::nlp::NlpFeature;
using maum::brain::nlp::NlpAnalysisLevel;
using maum::brain::nlp::KeywordFrequencyLevel;
using grpc::StatusCode;

struct node {
  int32_t seq;
  std::string keyword;
  int32_t frequency;
  std::string word_type;
  std::string word_type_nm;
};

const int feature = (1 << NlpFeature::NLPF_SPACE) |
    (1 << NlpFeature::NLPF_MORPHEME) |
    (1 << NlpFeature::NLPF_NAMED_ENTITY) |
    (1 << NlpFeature::NLPF_CHUNK);

NlpKorean1Impl::NlpKorean1Impl()
    : running_(0) {
  LoadConfig();
  LoadStringVector();
}

/**
  @breif 서버 실행시 초기화 하는 함수
  @param bool lock : 서버 가동 여부를 확인하기 위해
  @return None
*/
void NlpKorean1Impl::Start(bool lock) {
  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = true;
    loaded_ = false;
  } else {
    loading_ = true;
    loaded_ = false;
  }
  // 0x1d
  // 0001 1101
  // 각 자리가 level 의 on / off를 의미합니다.
  // on = 1, off = 0
  nlp1Kor_ = new Nlp1Kor(rsc_path_, feature);
  nlp1Kor_->UpdateFeatures();

  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = false;
    loaded_ = true;
  } else {
    loading_ = false;
    loaded_ = true;
  }
}

void NlpKorean1Impl::Stop(bool lock) {
  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = true;
    loaded_ = false;
  } else {
    loading_ = true;
    loaded_ = false;
  }

  nlp1Kor_->Uninit();
}

NlpKorean1Impl::~NlpKorean1Impl() {
  Stop();
}

/**
  @breif 문자열을 치환하기 위한 함수
  @param const std::string &full_string : 일반 문자열
  @param const std::string &target_string : 치환 대상 문자열
  @param const std::string &destination_string : 변경 문자열
  @return string : 치환된 문자열
*/
string ReplaceString(const std::string &full_string,
                     const std::string &target_string,
                     const std::string &destination_string) {
  size_t pos = full_string.find(target_string);
  if (pos != string::npos) {
    string origin = full_string;
    while (pos != string::npos) {
      origin.replace(pos, target_string.size(), destination_string);
      pos = origin.find(target_string, pos + destination_string.size());
    }
    return origin;
  }
  return full_string;
}

/**
  @breif 문자열을 delimiter를 기준으로 자르기 위한 함수
  @param const std::string &input : 일반 문자열
  @param const std::string &delimiter_string : 구분 문자열
  @return vector<string> result : 분리된 문자열을 담고 있는 vector
*/
vector<string> SplitString(const std::string &input,
                           const std::string &delimiter) {
  vector<string> result;
  size_t s_pos = 0;
  size_t e_pos = 0;
  for (;;) {
    e_pos = input.find(delimiter, s_pos);
    if (e_pos == string::npos) {
      break;
    } else {
      result.push_back(input.substr(s_pos, e_pos - s_pos));
      s_pos = e_pos + delimiter.size();
    }
  }
  if (s_pos <= input.size()) {
    result.push_back(input.substr(s_pos));
  }
  return result;
}

/**
  @breif 사용자에게 입력받은 분석 레벨에 따라서 해당 모듈 초기화 및 분석하는 함수
  @param InputText *text : 클라이언트에서 보낸 message 
  @param Document *document : 언어분석 결과를 클라이언트에 보낼 message
  @return None
*/
void NlpKorean1Impl::AnalyzeOne(const InputText *text, Document *document) {
  nlp1Kor_->AnalyzeOne(text, document);
}

/**
  @breif 클라이언트에서 언어분석을 위해 호출하는 함수
  @param ServerContext *context : grpc 통신을 수행하기 위해 필요한 instance
  @param InputText *text : 클라이언트에서 보낸 message
  @param Document *document : 언어분석 결과를 클라이언트에 보낼 message
  @return grpc Status Code
*/
Status NlpKorean1Impl::Analyze(ServerContext *context,
                               const InputText *text,
                               Document *document) {
  if (!CanService()) {
    return Status(StatusCode::INTERNAL, "loading..");
  }
  if (text->text().length() == 1) {
    auto &temp_text = text->text();
    char qa = temp_text.at(0);
    maum::brain::nlp::Sentence *sentence = document->add_sentences();
    sentence->set_seq(1);
    maum::brain::nlp::Morpheme *morpheme = sentence->add_morps();
    morpheme->set_seq(1);
    morpheme->set_lemma(temp_text);
    if (isdigit(int(qa))) {
      morpheme->set_type("nn");
    } else if (isalpha(int(qa))) {
      morpheme->set_type("nc");
    } else {
      morpheme->set_type("uk");
    }
  } else {
    Accessor(this);
    AnalyzeOne(text, document);
  }
  return Status::OK;
}

Status NlpKorean1Impl::AnalyzeMultiple(
    ServerContext *context,
    ServerReaderWriter<Document, InputText> *stream) {
  if (!CanService()) {
    return Status(StatusCode::INTERNAL, "loading..");
  }
  Accessor(this);
  InputText text;
  while (stream->Read(&text)) {
    Document out;
    AnalyzeOne(&text, &out);
    stream->Write(out);
  }
  return Status::OK;
}

/**
  @breif 엔진 공급자에 대한 정보를 제공하는 함수
  @param ServerContext *context : grpc 통신을 수행하기 위해 필요한 instance
  @param Empty *empty : google protobuf에서 제공하는 generic empty message
  @param NlpProvider *provider : 엔진 공급 정보를 클라이언트에 보낼 message
  @return grpc Status Code
*/
Status NlpKorean1Impl::GetProvider(ServerContext *context,
                                   const Empty *empty,
                                   NlpProvider *provider) {
  provider->set_name("MinddLAB NLP #1");
  provider->set_vendor("ETRI");
  provider->set_version("1.0");
  provider->set_description("MindsLAB NLP #1 Korean from ETRI");
  provider->set_support_encoding("EUC-KR");

  return Status::OK;
}

Status NlpKorean1Impl::HasSupport(ServerContext *context,
                                  const ::maum::brain::nlp::NlpFeatures *request,
                                  NlpFeatureSupportList *response) {
  for (auto f : request->features()) {
    switch (f) {
      case NlpFeature::NLPF_MORPHEME: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(true);
        break;
      }
      case NlpFeature::NLPF_WORD: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(true);
        break;
      }
      case NlpFeature::NLPF_NAMED_ENTITY: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(true);
        break;
      }
      case NlpFeature::NLPF_PARSER: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(true);
        break;
      }
      case NlpFeature::NLPF_SETIMENT: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(true);
        break;
      }
      case NlpFeature::NLPF_DNN: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(false);
        break;
      }
      default: {
        break;
      }
    }
  }
  return Status::OK;
}

Status NlpKorean1Impl::ApplyDict(ServerContext *context,
                                 const NlpDict *dict,
                                 Empty *empty) {
  CustomDict cd(*dict, this);

  if (cd.Download() < 0) {
    return Status(StatusCode::INTERNAL, "download failed");
  }
  if (cd.Apply()) {
    return Status(StatusCode::INTERNAL, "apply failed");
  }
  cd.Callback();

  // 다른 요청에 대해서 처리할 수 없도록 변경한다.
  // 요청이 처리중이면 처리 완료할 때까지 대기한다.
  std::thread restart = std::thread(&NlpKorean1Impl::Restart, this);
  restart.detach();
  return Status::OK;
}

/**
  @brief NE Tag set에 대한 정보를 제공하기 위한 함수
  @param ServerContext *context : grpc 통신을 수행하기 위해 필요한 instance
  @param Empty *empty : google protobuf에서 제공하는 generic empty message
  @param NamedEntityTagList *namedEntityTagList : namedEntityTag 정보를 담기 위한 message
  @return Status Code 
*/
Status NlpKorean1Impl::GetNamedEntityTagList(ServerContext *context,
                                             const Empty *empty,
                                             NamedEntityTagList *namedEntityTagList) {
  string ifs_str;
  string str_line;
  string ne_str;

  ifs_str = rsc_path_ + "/ne_tag.bin";
  ifstream ifs_ne_tag(ifs_str.c_str());

  if (ifs_ne_tag.fail()) {
    ifs_ne_tag.close();
    ne_str = rsc_path_ + "/ne_tag_set.txt";
    ifstream ifs_ne(ne_str.c_str());
    if (ifs_ne.fail()) {
      return Status(StatusCode::INTERNAL, "file open failed");
    } else {
      while (getline(ifs_ne, str_line)) {
        std::vector<string> ne_tag_set = SplitString(str_line, "\t");
        maum::brain::nlp::NamedEntityTag
            *ne_tags = namedEntityTagList->add_tags();

        ne_tag_set[0] = ReplaceString(ne_tag_set[0], "\r\n", "");
        ne_tag_set[0] = ReplaceString(ne_tag_set[0], "\n", "");
        ne_tag_set[0] = ReplaceString(ne_tag_set[0], "\r", "");

        ne_tag_set[1] = ReplaceString(ne_tag_set[1], "\r\n", "");
        ne_tag_set[1] = ReplaceString(ne_tag_set[1], "\n", "");
        ne_tag_set[1] = ReplaceString(ne_tag_set[1], "\r", "");

        ne_tag_set[2] = ReplaceString(ne_tag_set[2], "\r\n", "");
        ne_tag_set[2] = ReplaceString(ne_tag_set[2], "\n", "");
        ne_tag_set[2] = ReplaceString(ne_tag_set[2], "\r", "");

        ne_tag_set[3] = ReplaceString(ne_tag_set[3], "\r\n", "");
        ne_tag_set[3] = ReplaceString(ne_tag_set[3], "\n", "");
        ne_tag_set[3] = ReplaceString(ne_tag_set[3], "\r", "");

        ne_tags->set_name(ne_tag_set[0]);
        ne_tags->set_full_name(ne_tag_set[1]);
        ne_tags->set_index(atoi(ne_tag_set[2].c_str()));
        ne_tags->set_description(ne_tag_set[3]);
      }
      fstream output(rsc_path_ + "/ne_tag.bin", ios::out | ios::binary);
      namedEntityTagList->SerializeToOstream(&output);
    }
  } else {
    maum::brain::nlp::NamedEntityTagList temp_named_entity_tag_list;
    fstream input(ifs_str, ios::in | ios::binary);
    if (!temp_named_entity_tag_list.ParseFromIstream(&input)) {
      std::cerr << "Failed to parse NamedEntityTagList." << endl;
      return Status(StatusCode::INTERNAL, "file open failed");
    } else {
      for (int i = 0; i < temp_named_entity_tag_list.tags_size(); i++) {
        maum::brain::nlp::NamedEntityTag
            *ne_tags = namedEntityTagList->add_tags();
        const maum::brain::nlp::NamedEntityTag
            &net = temp_named_entity_tag_list.tags(i);
        ne_tags->CopyFrom(net);
      }
    }
  }
  return Status::OK;
}
