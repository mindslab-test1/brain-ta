#include "nlp-service-kor1-impl.h"

#include <libmaum/common/config.h>
#include <fstream>

using std::ifstream;

void NlpKorean1Impl::Restart() {
  auto logger = LOGGER();
  std::unique_lock<std::mutex> lock(m_);
  logger->debug("restart waiting... ");
  cv_.wait(lock, [this] { return running_ == 0; });

  logger->debug("calling stop ... ");
  Stop(false);
  logger->debug("calling start ... ");
  Start(false);
}

/**
  @breif nlp-kor1을 위한 리소스 경로 설정
  @param None
  @return None
*/
void NlpKorean1Impl::LoadConfig() {
  auto c = libmaum::Config::Instance();
  rsc_path_ = c.Get("brain-ta.resource.1.kor.path");
}

/**
  @breif one-word, stop-word, ne-tag set, synonym dictionary를 변수에 저장하기 위해 로딩하는 함수
  @param None
  @return None
*/
void NlpKorean1Impl::LoadStringVector() {
  auto logger = LOGGER();
  string ifs_str;
  string str_line;

  // one-word loading
  ifs_str = rsc_path_ + "/CowIdx/oneword.txt";
  ifstream ifs_one(ifs_str.c_str());
  if (ifs_one.fail()) {
    logger->debug("cannot loading failed {}", ifs_str);
    return;
  }
  while (getline(ifs_one, str_line)) {
    str_line = ReplaceString(str_line, "\r", "");
    str_line = ReplaceString(str_line, "\n", "");
    one_set_.insert(str_line);
  }
  ifs_one.close();
  ifs_str.clear();

  // stop-word loading
  ifs_str = rsc_path_ + "/idx_stop.txt";
  ifstream ifs_stop(ifs_str.c_str());
  if (ifs_stop.fail()) {
    logger->debug("cannot loading failed {}", ifs_str);
    return;
  }
  while (getline(ifs_stop, str_line)) {
    str_line = ReplaceString(str_line, "\r", "");
    str_line = ReplaceString(str_line, "\n", "");
    stop_set_.insert(str_line);
  }
  ifs_stop.close();
  ifs_str.clear();

  // ne-tag loading
  ifs_str = rsc_path_ + "/ne_tag.txt";
  ifstream ifs_ne(ifs_str.c_str());
  if (ifs_ne.fail()) {
    logger->debug("cannot loading failed {}", ifs_str);
    return;
  }
  while (getline(ifs_ne, str_line)) {
    std::vector<string> ne_tag = SplitString(str_line, "\t");
    // ne_tag_[ne_tag[0]] = stoi(ne_tag[1]);
    ne_tag[0] = ReplaceString(ne_tag[0], "\r\n", "");
    ne_tag[0] = ReplaceString(ne_tag[0], "\r", "");
    ne_tag[0] = ReplaceString(ne_tag[0], "\n", "");
    
    ne_tag[1] = ReplaceString(ne_tag[1], "\r\n", "");
    ne_tag[1] = ReplaceString(ne_tag[1], "\r", "");
    ne_tag[1] = ReplaceString(ne_tag[1], "\n", "");
    ne_tag_[ne_tag[0]] = ne_tag[1];
  }
  ifs_ne.close();
  ifs_str.clear();

  // synonym dictionary loading
  // dictionary file must be utf-8 encoding
  ifs_str = rsc_path_ + "/Normalization/temp.txt";
  ifstream ifs_syn(ifs_str.c_str());
  if (ifs_syn.fail()) {
      logger->debug("cannot loading failed {}", ifs_str);
      return;
  }

  while (getline(ifs_syn, str_line)) {
      std::vector<string> syn_tag = SplitString(str_line, " -> ");
      if (syn_tag.size() != 4) continue;
      syn_set_[syn_tag[0]] = syn_tag[1];
  }
  ifs_syn.close();
  ifs_str.clear();
}
