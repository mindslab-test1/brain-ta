// LMInterface.cpp: implementation of the LMInterface class.
//
//////////////////////////////////////////////////////////////////////
#ifdef WIN32
#pragma warning( disable : 4996 )
#pragma warning( disable : 4786 )
#pragma warning( disable : 4267 )
#pragma warning( disable : 4244 )
#pragma warning( disable : 4251 )
#endif

#include "lm-interface.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

LMInterface::LMInterface() {
  m_scan = NULL;
  m_NER = NULL;
  m_cl = NULL;
  m_space = NULL;
  m_space_util = NULL;

  m_CHUNK = NULL;
  m_DParse = NULL;
  m_srl = NULL;
  m_Rel = NULL;
  m_SA = NULL;

  m_Doc.d = NULL;
  m_Doc2.d = NULL;
//  기타 초기화 코드 삽입
}

LMInterface::~LMInterface() {
}

//-------------------------------------------------
//
//	형태소 분석
//
//-------------------------------------------------

///< 형태소 분석기 생성 및 사전 초기화 : rsc 디렉토리 지정
void LMInterface::init_morp(char *dir, QADOMAIN qdomain) {
  // 형태소 분석기 초기화
  m_scan = new CMorpAnalyzer();
  m_scan->initialize(dir, qdomain);
}

///< for multi-thread
void LMInterface::init_morp_global(char *dir, QADOMAIN qdomain) {
  // 형태소 분석기 초기화
  m_scan = new CMorpAnalyzer();
  m_scan->initialize_global(dir, qdomain);
}

///< for multi-thread
void LMInterface::init_morp_thread(char *dir) {
  // 형태소 분석기 초기화
  m_scan = new CMorpAnalyzer();
  m_scan->initialize_thread(dir);
}


///< 형태소 분석기 종료 및 finalize
void LMInterface::close_morp() {
  m_scan->finalize();
  delete m_scan;
}

///< for multi-thread
void LMInterface::close_morp_global() {
  m_scan->finalize_global();
  delete m_scan;
}

///< for multi-thread
void LMInterface::close_morp_thread() {
  m_scan->finalize_thread();
  delete m_scan;
}


/** 이충희 사용코드
*/
void LMInterface::get_sent(map<long, string> &sentList) {
  if (m_NER == NULL) {
    return;
  }

  m_NER->get_sent(&m_Doc, sentList);
}




//-------------------------------------------------
//
//	ATR
//
//-------------------------------------------------


/** AT인식기 초기화, LMInterface내부의 m_doc 사용
*/
void LMInterface::init_NER(char *dir, QADOMAIN qdomain) {
  /*
  if (m_scan == NULL)
  {
      printf("먼저 형태소 분석기 초기화가 필요합니다.\n");
      exit(-1);
  }
  */

  if (m_NER != NULL) {
    printf("이미 NER은 초기화 되어 있습니다.\n");
    return;
  }

  m_NER = new CNERecognizer();

  m_NER->init_NER(dir, qdomain);

  m_NER->set_Doc(&m_Doc);
}

/** AT인식기 초기화, m_doc변수 지정이 요구됨
*/
void LMInterface::init_NER_NO_DOC(char *dir, QADOMAIN qdomain) {
  /*
  if (m_scan == NULL)
  {
      printf("먼저 형태소 분석기 초기화가 필요합니다.\n");
      exit(-1);
  }
  */

  if (m_NER != NULL) {
    printf("이미 NER은 초기화 되어 있습니다.\n");
    return;
  }

  m_NER = new CNERecognizer();

  m_NER->init_NER(dir, qdomain);
}


///< AT인식기 초기화 for multi-thread
void LMInterface::init_NER_global(char *dir, QADOMAIN qdomain) {
  if (m_scan == NULL) {
    printf("먼저 형태소 분석기 초기화가 필요합니다.\n");
    exit(-1);
  }

  if (m_NER != NULL) {
    printf("이미 NER은 초기화 되어 있습니다.\n");
    return;
  }

  m_NER = new CNERecognizer();

  m_NER->init_NER_global(dir, qdomain);

  m_NER->set_Doc(&m_Doc);
}

///< AT인식기 초기화 for multi-thread
void LMInterface::init_NER_thread(char *dir) {
  if (m_NER != NULL) {
    printf("이미 NER은 초기화 되어 있습니다.\n");
    return;
  }

  m_NER = new CNERecognizer();

  m_NER->init_NER_thread(dir);

  m_NER->set_Doc(&m_Doc);
}


void LMInterface::do_preNER(string &pStr) {
  if (m_scan == NULL) {
    printf("먼저 형태소 분석기 초기화가 필요합니다.\n");
    exit(-1);
  }

  m_scan->m_doc_generate_short_sen(&m_Doc, pStr, false, false);
}


/** 음성인식기 출력을 입력받아 형태소 분석 및 AT인식 진행
*/
void LMInterface::do_preNER(ESTk_ASR_hypothesis *asr) {
  string sent = asr->hyp[0];

  if (m_scan == NULL) {
    printf("먼저 형태소 분석기 초기화가 필요합니다.\n");
    exit(-1);
  }

  m_scan->m_doc_generate_short_sen(&m_Doc, sent, false, false);
}


void LMInterface::do_preNER(string &pStr, struct Doc *p) {
  if (m_scan == NULL) {
    printf("먼저 형태소 분석기 초기화가 필요합니다.\n");
    exit(-1);
  }

  m_scan->m_doc_generate_short_sen(p, pStr, false, false);
}


void LMInterface::do_preNER_with_P(string &pStr) {
  if (m_scan == NULL) {
    printf("먼저 형태소 분석기 초기화가 필요합니다.\n");
    exit(-1);
  }

  m_scan->m_doc_generate_short_sen(&m_Doc, pStr, true, false);
}


void LMInterface::do_preNER_with_P(string &pStr, struct Doc *p) {
  if (m_scan == NULL) {
    printf("먼저 형태소 분석기 초기화가 필요합니다.\n");
    exit(-1);
  }

  m_scan->m_doc_generate_short_sen(p, pStr, true, false);
}


void LMInterface::do_ATR() {
  if (m_NER == NULL) {
    printf("ATR이 초기화 되지 않았습니다.\n");
    return;
  }

  m_NER->do_ATR(&m_Doc);
}


void LMInterface::do_ATR(struct Doc *p) {
  if (m_NER == NULL) {
    printf("ATR이 초기화 되지 않았습니다.\n");
    return;
  }

  m_NER->set_Doc(p);
  m_NER->do_ATR(p);
}


/** AT인식기의 대상 m_doc구조 지정, 다중 doc 구조를 대상으로 작업할 때 사용됨
*/
void LMInterface::set_Doc_LM(struct Doc *p) {
  if (m_NER == NULL) {
    printf("init_NER_NO_DOC() 호출 필요 !!\n");
    exit(-1);
  }
  m_NER->set_Doc(p);

  // 여기에 추가할 것.
}

/** AT인식 결과 출력
*/
void LMInterface::print_ATR() {
  if (m_NER == NULL) {
    printf("NER이 초기화 되지 않았습니다.\n");
    return;
  }

  m_NER->doc_test_AT(&m_Doc);
}

/** 지정된 doc구조체를 대상으로 AT인식 결과 출력
*/
void LMInterface::print_ATR(struct Doc *p) {
  if (m_NER == NULL) {
    printf("NER이 초기화 되지 않았습니다.\n");
    return;
  }

  m_NER->doc_test_AT(p);
}

///< leeck이 만듦
void LMInterface::print_ATR2(FILE *out) {
  if (m_NER == NULL) {
    printf("NER이 초기화 되지 않았습니다.\n");
    return;
  }

  m_NER->print_AT(&m_Doc, out);
}

///< leeck이 만듦
string LMInterface::sprint_ATR(bool print_feat) {
  if (m_NER == NULL) {
    printf("NER이 초기화 되지 않았습니다.\n");
    return "";
  }

  return m_NER->sprint_AT(&m_Doc, print_feat);
}


/** 개체명 인식기 종료
*/
void LMInterface::close_NER() {
  if (m_NER == NULL) return;

  m_NER->close_NER();
  delete m_NER;

  m_NER = NULL;
}

///< 개체명 인식기 종료 for multi thread
void LMInterface::close_NER_global() {
  if (m_NER == NULL) return;

  m_NER->close_NER_global();
  delete m_NER;

  m_NER = NULL;
}

///< 개체명 인식기 종료 for multi thread
void LMInterface::close_NER_thread() {
  if (m_NER == NULL) return;

  m_NER->close_NER_thread();
  delete m_NER;

  m_NER = NULL;
}


//-------------------------------------------------
//
//	문서분류기
//
//-------------------------------------------------

// 초기화
void LMInterface::init_cl(char *dir, QADOMAIN qdomain, bool use_bin_model) {
  if (m_cl != NULL) {
    printf("이미 문서분류기가 초기화 되어 있습니다.\n");
    return;
  }
  m_cl = new Classifier();
  m_cl->init(dir, qdomain, use_bin_model);
}

// for multi-thread
void LMInterface::init_cl_global(char *dir,
                                 QADOMAIN qdomain,
                                 bool use_bin_model) {
  if (m_cl != NULL) {
    printf("이미 문서분류기가 초기화 되어 있습니다.\n");
    return;
  }
  m_cl = new Classifier();
  m_cl->init_global(dir, qdomain, use_bin_model);
}

// for multi-thread
void LMInterface::init_cl_thread(char *dir, QADOMAIN qdomain) {
  if (m_cl != NULL) {
    printf("이미 문서분류기가 초기화 되어 있습니다.\n");
    return;
  }
  m_cl = new Classifier();
  m_cl->init_thread(dir, qdomain);
}

// 분류기 실행(bigram)
void LMInterface::do_cl(string text, string cat) {
  if (m_cl == NULL) {
    printf("문서분류기가 초기화 되지 않았습니다.\n");
    return;
  }
  string label = m_cl->classify(text, cat);
  m_Doc.category.clear();
  m_Doc.category_score.clear();
  m_Doc.category.push_back(label);
  m_Doc.category_score.push_back(m_cl->get_score());
}

// 분류기 실행(NP-based bigram)
void LMInterface::do_cl(Doc *doc, string cat) {
  if (m_cl == NULL) {
    printf("문서분류기가 초기화 되지 않았습니다.\n");
    return;
  }
  string label = m_cl->classify(doc, cat);
  doc->category.clear();
  doc->category_score.clear();
  doc->category.push_back(label);
  doc->category_score.push_back(m_cl->get_score());
}

// N-best 분류기 실행(bigram)
void LMInterface::do_cl_nbest(string text, string cat, int nbest) {
  if (m_cl == NULL) {
    printf("문서분류기가 초기화 되지 않았습니다.\n");
    return;
  }
  vector<string> cat_vec = m_cl->classify_nbest(text, cat, nbest);
  m_Doc.category.clear();
  m_Doc.category_score.clear();
  m_Doc.category = cat_vec;
  for (size_t i = 0; i < cat_vec.size(); i++) {
    m_Doc.category_score.push_back(m_cl->get_nth_score(i));
  }
}

// N-best 분류기 실행(NP-based bigram)
void LMInterface::do_cl_nbest(Doc *doc, string cat, int nbest) {
  if (m_cl == NULL) {
    printf("문서분류기가 초기화 되지 않았습니다.\n");
    return;
  }
  vector<string> cat_vec = m_cl->classify_nbest(doc, cat, nbest);
  doc->category.clear();
  doc->category_score.clear();
  doc->category = cat_vec;
  for (size_t i = 0; i < cat_vec.size(); i++) {
    doc->category_score.push_back(m_cl->get_nth_score(i));
  }
}

// 부모 클래스 리턴 (최상위일 경우 "" 리턴)
string LMInterface::get_parent(string category) {
  if (m_cl == NULL) {
    printf("문서분류기가 초기화 되지 않았습니다.\n");
    return "";
  }
  return m_cl->get_parent(category);
}

// 분류기 종료
void LMInterface::close_cl() {
  if (m_cl == NULL) return;
  m_cl->close();
  delete m_cl;
  m_cl = NULL;
}

// for multi-thread
void LMInterface::close_cl_global() {
  if (m_cl == NULL) return;
  m_cl->close_global();
  delete m_cl;
  m_cl = NULL;
}

// for multi-thread
void LMInterface::close_cl_thread() {
  if (m_cl == NULL) return;
  m_cl->close_thread();
  delete m_cl;
  m_cl = NULL;
}

//-------------------------------------------------
//
//	띄어쓰기
//
//-------------------------------------------------

// 초기화
void LMInterface::init_space(const char *dir, QADOMAIN qdomain) {
  if (m_space != NULL) {
    printf("이미 띄어쓰기가 초기화 되어 있습니다.\n");
    return;
  }
  m_space = new Space();
  string new_dir = dir;
  new_dir += "/space";
  m_space->init(new_dir, qdomain);

  new_dir += "/clots.txt";
  m_space_util = new SpaceUtil(new_dir);
}

// 띄어쓰기 실행
string LMInterface::do_space(string text) {
  if (m_space == NULL) {
    printf("띄어쓰기가 초기화 되지 않았습니다.\n");
    return text;
  }

  vector<string> clots;
  if (m_space->qdomain == TWEET_DOMAIN)
    text = m_space_util->replaceClots(text, clots);

  string result;
  double prob;
  vector<string> lines;
  split(text, lines, "\n");
  for (size_t i = 0; i < lines.size(); i++) {
    if (lines[i].length() > 0) {
      //result += m_space->do_space(lines[i], prob);
      // score - weight*loss (weight == 1)
      result += m_space->do_space_with_edit_dist(lines[i], prob, 1.0);
      result += "\n";
    }
  }

  if (m_space->qdomain == TWEET_DOMAIN)
    result = m_space_util->replaceClotsBack(result, clots);

  return result;
}

// 띄어쓰기 종료
void LMInterface::close_space() {
  if (m_space != NULL) {
    m_space->close();
    delete m_space;
    m_space = NULL;
  }

  if (m_space_util != NULL) {
    delete m_space_util;
    m_space_util = NULL;
  }
}

//-------------------------------------------------
//
//	청킹
//
//-------------------------------------------------

void LMInterface::init_Chunk(char *rsc_dir, QADOMAIN qdomain) {
  m_CHUNK = new CChunkAnalyzer();
  m_CHUNK->init_Chunk(&m_Doc, rsc_dir);

  // DB생성
  //m_CHUNK->make_db();
  //m_CHUNK->make_db2();
}

void LMInterface::init_Chunk_global(char *rsc_dir, QADOMAIN qdomain) {
  m_CHUNK = new CChunkAnalyzer();
  m_CHUNK->init_Chunk_global(&m_Doc, rsc_dir);

  // DB생성
  //m_CHUNK->make_db();
  //m_CHUNK->make_db2();
}

void LMInterface::init_Chunk_thread(char *rsc_dir) {
  m_CHUNK = new CChunkAnalyzer();
  m_CHUNK->init_Chunk_thread(&m_Doc, rsc_dir);

  // DB생성
  //m_CHUNK->make_db();
  //m_CHUNK->make_db2();
}


void LMInterface::init_Chunk_NO_DOC(char *rsc_dir, QADOMAIN qdomain) {
  m_CHUNK = new CChunkAnalyzer();
  m_CHUNK->init_Chunk(rsc_dir);
}

void LMInterface::init_Chunk_NO_DOC_global(char *rsc_dir,
                                           QADOMAIN qdomain) {
  m_CHUNK = new CChunkAnalyzer();
  m_CHUNK->init_Chunk_global(rsc_dir);
}

void LMInterface::init_Chunk_NO_DOC_thread(char *rsc_dir) {
  m_CHUNK = new CChunkAnalyzer();
  m_CHUNK->init_Chunk_thread(rsc_dir);
}


void LMInterface::do_Chunk() {
  if (m_CHUNK == NULL) {
    printf("CHUNK의 초기화가 필요합니다.\n");
    exit(-1);
  }

  m_CHUNK->do_Chunk();
}


void LMInterface::do_Chunk(struct Doc *p) {
  if (m_CHUNK == NULL) {
    printf("CHUNK의 초기화가 필요합니다.\n");
    exit(-1);
  }

  m_CHUNK->set_Doc(p);
  m_CHUNK->do_Chunk();
}


void LMInterface::print_Chunk() {
  if (m_CHUNK == NULL) {
    printf("CHUNK의 초기화 되지 않았습니다.\n");
    return;
  }

  m_CHUNK->print_Chunk();
}


void LMInterface::print_EChunk() {
  if (m_CHUNK == NULL) {
    printf("CHUNK의 초기화 되지 않았습니다.\n");
    return;
  }

  m_CHUNK->print_EChunk();
}


void LMInterface::print_Chunk(struct Doc *p) {
  if (m_CHUNK == NULL) {
    printf("CHUNK의 초기화 되지 않았습니다.\n");
    return;
  }

  m_CHUNK->set_Doc(p);
  m_CHUNK->print_Chunk();
}


void LMInterface::print_EChunk(struct Doc *p) {
  if (m_CHUNK == NULL) {
    printf("CHUNK의 초기화 되지 않았습니다.\n");
    return;
  }

  m_CHUNK->set_Doc(p);
  m_CHUNK->print_EChunk();
}


void LMInterface::close_Chunk() {
  if (m_CHUNK == NULL) {
    printf("CHUNK의 초기화가 필요합니다.\n");
    exit(-1);
  }

  m_CHUNK->close_Chunk();
  delete m_CHUNK;
}

void LMInterface::close_Chunk_global() {
  if (m_CHUNK == NULL) {
    printf("CHUNK의 초기화가 필요합니다.\n");
    exit(-1);
  }

  m_CHUNK->close_Chunk_global();
  delete m_CHUNK;
}

void LMInterface::close_Chunk_thread() {
  if (m_CHUNK == NULL) {
    printf("CHUNK의 초기화가 필요합니다.\n");
    exit(-1);
  }

  m_CHUNK->close_Chunk_thread();
  delete m_CHUNK;
}



//-------------------------------------------------
//
//	DParse
//
//-------------------------------------------------


void LMInterface::init_DParse(char *rsc_dir, QADOMAIN qdomain) {
  if (m_DParse != NULL) {
    printf("DepenParse가 이미 초기화되었습니다.\n");
    return;
  }

  m_DParse = new TParser(3000001);
  m_DParse->korean = 1;
  m_DParse->labeled = 1;
  string dir = rsc_dir;
  m_DParse->init(dir + "/parser");
}

void LMInterface::do_DParse(N_Doc &ndoc) {
  if (m_DParse == NULL) {
    printf("파서가 초기화 되지 않았음\n");
    return;
  }

  double prob;
  int beam = 1;
  for (size_t i = 0; i < ndoc.sentence.size(); i++) {

    if (m_DParse->has_qoute(ndoc.sentence[i])) {
      vector<N_Sentence> vecSen = m_DParse->split_qoute(ndoc.sentence[i]);
      //cerr << "HAS QOUTE:" << vecSen.size() << endl;
      for (vector<N_Sentence>::size_type k = 0; k < vecSen.size() - 1; k++) {
        vector<word_t> input = m_DParse->convert_nsent4korean(vecSen[k]);
        vector<word_t> result = m_DParse->do_parse(input, prob, beam);
        m_DParse->post_tagging(result);
        //m_DParse->post_tagging_sbjvp(result);
        for (size_t j = 0; j < result.size(); j++) {
          N_Dependency dep;
          dep.id = result[j].id - 1;
          assert(dep.id == (int) j);
          dep.text = vecSen[k].word[j].text;
          dep.head = result[j].head - 1;
          dep.label = result[j].dep_label;
          vecSen[k].dependency.push_back(dep);
        }
      }

      N_Sentence sen = m_DParse->merge_qoute(vecSen);
      if (sen.dependency.size() == sen.word.size())
        ndoc.sentence[i] = sen;
      else {
        vector<word_t> input = m_DParse->convert_nsent4korean(ndoc.sentence[i]);
        vector<word_t> result = m_DParse->do_parse(input, prob, beam);
        m_DParse->post_tagging(result);
        //m_DParse->post_tagging_sbjvp(result);
        //cerr << m_DParse->print_parse_tree(result);
        for (size_t j = 0; j < result.size(); j++) {
          N_Dependency dep;
          dep.id = result[j].id - 1;
          assert(dep.id == (int) j);
          dep.text = ndoc.sentence[i].word[j].text;
          dep.head = result[j].head - 1;
          dep.label = result[j].dep_label;
          ndoc.sentence[i].dependency.push_back(dep);
        }
      }
    }
    else {
      vector<word_t> input = m_DParse->convert_nsent4korean(ndoc.sentence[i]);
      vector<word_t> result = m_DParse->do_parse(input, prob, beam);
      m_DParse->post_tagging(result);
      //m_DParse->post_tagging_sbjvp(result);
      //cerr << m_DParse->print_parse_tree(result);
      for (size_t j = 0; j < result.size(); j++) {
        N_Dependency dep;
        dep.id = result[j].id - 1;
        assert(dep.id == (int) j);
        dep.text = ndoc.sentence[i].word[j].text;
        dep.head = result[j].head - 1;
        dep.label = result[j].dep_label;
        ndoc.sentence[i].dependency.push_back(dep);
      }
    }

    //개체명 인식결과 보고 날짜 후처리 (무조건 용언에 걸리도록)
    vector<N_NE> vecDTNE;
    for (size_t j = 0; j < ndoc.sentence[i].NE.size(); j++) {
      N_NE ne = ndoc.sentence[i].NE[j];
      string netype = ne.type;
      //if(netype == "DT_DURATION" || netype == "DT_DAY"  || netype == "DT_MONTH"  || netype == "DT_YEAR"  || netype == "DT_OTHERS" ||
      //	 netype == "TI_DURATION" || netype == "TI_HOUR"  || netype == "TI_MINUTE"  || netype == "TI_SECOND"  || netype == "TI_OTHERS")
      if (netype == "DT_DAY" || netype == "DT_MONTH" || netype == "DT_YEAR"
          || netype == "DT_OTHERS" ||
          netype == "TI_HOUR" || netype == "TI_MINUTE" || netype == "TI_SECOND"
          || netype == "TI_OTHERS") {
        vecDTNE.push_back(ndoc.sentence[i].NE[j]);

        int s_eojeol = getWordID(ne.begin, ndoc.sentence[i].word);
        int e_eojeol = getWordID(ne.end, ndoc.sentence[i].word);
        if (s_eojeol != e_eojeol) {
          for (int k = s_eojeol; k < e_eojeol; k++) {
            ndoc.sentence[i].dependency[k].head = e_eojeol;
            ndoc.sentence[i].dependency[k].label = "NP";
          }
        }

        N_Dependency &dp = ndoc.sentence[i].dependency[e_eojeol];
        int head = dp.head;
        if (head != -1 && dp.label.at(0) != 'V' && dp.label != "NP_MOD") {
          while (head != -1
              && ndoc.sentence[i].dependency[head].label.at(0) != 'V') {
            head = ndoc.sentence[i].dependency[head].head;
          }

          //마지막 NP에 걸린 경우
          if (head == -1)
            dp.head = ndoc.sentence[i].dependency.size() - 1;
          else
            dp.head = head;

          dp.label = "NP_AJT";
        }
      }
    }

    // mod
    for (size_t j = 0; j < ndoc.sentence[i].dependency.size(); j++) {
      int head_id = ndoc.sentence[i].dependency[j].head;
      if (head_id >= 0)
        ndoc.sentence[i].dependency[head_id].mod.emplace_back(j);
    }

  }
}

void LMInterface::close_DParse() {
  m_DParse->close();
  delete m_DParse;
}

void LMInterface::print_DParse(N_Doc &ndoc) {
  for (size_t i = 0; i < ndoc.sentence.size(); i++) {
    for (size_t j = 0; j < ndoc.sentence[i].dependency.size(); j++) {
      int head = ndoc.sentence[i].dependency[j].head;
      string label = ndoc.sentence[i].dependency[j].label;
      string word = ndoc.sentence[i].word[j].text;
      string head_word = "ROOT";
      if (head >= 0) head_word = ndoc.sentence[i].word[head].text;

      std::cout
          << j << "\t" << word << "\t" << head << "\t" << head_word << "\t"
          << label << std::endl;
    }
  }
}

void LMInterface::print_DParse(FILE *out, N_Doc &ndoc) {
  for (size_t i = 0; i < ndoc.sentence.size(); i++) {
    fprintf(out, "# %s\n", ndoc.sentence[i].text.c_str());
    for (size_t j = 0; j < ndoc.sentence[i].dependency.size(); j++) {
      int head = ndoc.sentence[i].dependency[j].head;
      string label = ndoc.sentence[i].dependency[j].label;
      string word = ndoc.sentence[i].word[j].text;
      string head_word = "ROOT";
      if (head >= 0) head_word = ndoc.sentence[i].word[head].text;
      fprintf(out,
              "%d\t%s\t%d\t%s\t%s\n",
              (int) j,
              word.c_str(),
              head,
              head_word.c_str(),
              label.c_str());
    }
  }
}



//-------------------------------------------------
//
//	Semantic Rel Labeling
//
//-------------------------------------------------

void LMInterface::init_srl(char *rsc_dir, QADOMAIN qdomain) {
  if (m_srl != NULL) {
    printf("SRL이 이미 초기화되었습니다.\n");
    return;
  }

  m_srl = new SRL();
  m_srl->korean = 1;
  string dir = rsc_dir;
  m_srl->load_dic(dir + "/srl/cluster.txt");
  m_srl->load_model(dir + "/srl/srl_pred.bin", dir + "/srl/srl_arg.bin", 1);
}

void LMInterface::do_srl(N_Doc &ndoc) {
  // srl needs parser
  if (m_srl == NULL || m_DParse == NULL) {
    printf("SRL 혹은 Parser가 초기화 되지 않았음\n");
    return;
  }

  double prob;
  for (size_t i = 0; i < ndoc.sentence.size(); i++) {
    // 너무 많은 어절 skip
    if (ndoc.sentence[i].word.size() > 100) continue;
    // POS, parsing info --> word_t
    vector<word_t> input = m_DParse->convert_nsent4korean(ndoc.sentence[i]);
    vector<word_t> result = m_srl->do_srl(input, prob);
    for (size_t j = 0; j < result.size(); j++) {
      if (result[j].pred == "_") continue;
      N_SRL srl;
      vector<string> token;
      tokenize(result[j].pred, token, ".");
      if (token.size() != 2) {
        cerr << "SRL:pred_error: " << result[j].pred << endl;
        continue;
      }
      // pred
      srl.verb = token[0];
      srl.sense = atoi(token[1].c_str());
      srl.word_id = j;
      srl.weight = prob;
      // arg
      for (size_t k = 0; k < result[j].arg_info.size(); k++) {
        N_SRL_Arg arg;
        // i = id - 1
        arg.word_id = result[j].arg_info[k].first - 1;
        arg.type = result[j].arg_info[k].second;
        arg.text = ndoc.sentence[i].word[arg.word_id].text;
        srl.argument.push_back(arg);
        // test
        //cerr << "pred=" << result[j].pred << " " << "arg=" << arg.text << ":" << arg.type << endl;
      }
      ndoc.sentence[i].SRL.push_back(srl);
    }
  }
}

// SRL 학습데이터 출력을 위해
void LMInterface::do_srl(N_Doc &ndoc, vector<vector<word_t> > &result_doc) {
  // srl needs parser
  if (m_srl == NULL || m_DParse == NULL) {
    printf("SRL 혹은 Parser가 초기화 되지 않았음\n");
    return;
  }

  result_doc.clear();
  double prob;
  for (size_t i = 0; i < ndoc.sentence.size(); i++) {
    // 너무 많은 어절 skip
    if (ndoc.sentence[i].word.size() > 100) continue;
    // POS, parsing info --> word_t
    vector<word_t> input = m_DParse->convert_nsent4korean(ndoc.sentence[i]);
    vector<word_t> result = m_srl->do_srl(input, prob);
    result_doc.push_back(result);
    for (size_t j = 0; j < result.size(); j++) {
      if (result[j].pred == "_") continue;
      N_SRL srl;
      vector<string> token;
      tokenize(result[j].pred, token, ".");
      if (token.size() != 2) {
        cerr << "SRL:pred_error: " << result[j].pred << endl;
        continue;
      }
      // pred
      srl.verb = token[0];
      srl.sense = atoi(token[1].c_str());
      srl.word_id = j;
      srl.weight = prob;
      // arg
      for (size_t k = 0; k < result[j].arg_info.size(); k++) {
        N_SRL_Arg arg;
        // i = id - 1
        arg.word_id = result[j].arg_info[k].first - 1;
        arg.type = result[j].arg_info[k].second;
        arg.text = ndoc.sentence[i].word[arg.word_id].text;
        srl.argument.push_back(arg);
        // test
        //cerr << "pred=" << result[j].pred << " " << "arg=" << arg.text << ":" << arg.type << endl;
      }
      ndoc.sentence[i].SRL.push_back(srl);
    }
  }
}

// 학습데이터 포맷
string LMInterface::sprint_srl(N_Doc &ndoc,
                               vector<vector<word_t> > &result_doc) {
  string result;
  char temp[1000];
  for (int sent_i = 0; sent_i < (int) result_doc.size(); sent_i++) {
    vector<word_t> &input = result_doc[sent_i];
    vector<N_Word> &word = ndoc.sentence[sent_i].word;
    vector<N_Morp> &morp = ndoc.sentence[sent_i].morp;
    // text
    if (sent_i > 0) result += "\n";
    result += "; " + ndoc.sentence[sent_i].text + "\n";
    // srl 정보
    for (int i = 0; i < (int) input.size(); i++) {
      if (input[i].pred != "_") {
        sprintf(temp, ";; %d:%s", input[i].id, input[i].pred.c_str());
        result += temp;
        // arg_info
        for (int j = 0; j < (int) input[i].arg_info.size(); j++) {
          int id = input[i].arg_info[j].first;
          string arg = input[i].arg_info[j].second;
          sprintf(temp,
                  "\t%d:%s:%s",
                  id,
                  word[id - 1].text.c_str(),
                  arg.c_str());
          result += temp;
        }
        result += "\n";
      }
    }
    // conll 포맷
    for (int i = 0; i < (int) input.size(); i++) {
      string pos_tag;
      for (int j = word[i].begin; j <= word[i].end; j++) {
        if (pos_tag != "") pos_tag += '+';
        pos_tag += morp[j].lemma + '/' + morp[j].type;
      }
      sprintf(temp,
              "%d\t%s\t%s\t%d\t%s\t%s",
              input[i].id,
              word[i].text.c_str(),
              pos_tag.c_str(),
              input[i].head,
              input[i].dep_label.c_str(),
              input[i].pred.c_str());
      result += temp;
      // arg
      for (int j = 0; j < (int) input[i].arg.size(); j++) {
        result += "\t" + input[i].arg[j];
      }
      result += "\n";
    }
  }
  return result;
}

void LMInterface::close_srl() {
  m_srl->close();
  delete m_srl;
}



//-------------------------------------------------
//
//	Relation Extraction
//
//-------------------------------------------------

///< 초기화
void LMInterface::init_Rel(char *dir, QADOMAIN qdomain) {
  // 초기화
  m_Rel = new Relation();
  m_Rel->init_Rel(dir, qdomain);
}

///< for multi-thread
void LMInterface::init_Rel_global(char *dir, QADOMAIN qdomain) {
  // 초기화
  m_Rel = new Relation();
  m_Rel->init_Rel_global(dir, qdomain);
}

///< for multi-thread
void LMInterface::init_Rel_thread(char *dir) {
  // 초기화
  m_Rel = new Relation();
  m_Rel->init_Rel_thread(dir);
}


///<  종료 및 finalize
void LMInterface::close_Rel() {
  m_Rel->close_Rel();
  delete m_Rel;
  m_Rel = NULL;
}

///<  for multi-thread
void LMInterface::close_Rel_global() {
  m_Rel->close_Rel_global();
  delete m_Rel;
  m_Rel = NULL;
}

///<  for multi-thread
void LMInterface::close_Rel_thread() {
  m_Rel->close_Rel_thread();
  delete m_Rel;
  m_Rel = NULL;
}


///< Relation 분석
void LMInterface::do_Rel(N_Doc &ndoc) {
  if (m_Rel == NULL) {
    return;
  }

  m_Rel->do_Rel(ndoc);
}



//-------------------------------------------------
//
//	Sentiment Analysis
//
//-------------------------------------------------

void LMInterface::init_SA(char *rsc_dir, QADOMAIN qdomain) {
  if (m_SA != NULL) delete m_SA;
  m_SA = new SentimentAnalyzer;
  m_SA->initialize(string(rsc_dir) + "/SA", qdomain);
}

void LMInterface::do_SA(N_Doc &ndoc, string ndocJSONStr) {
  m_SA->analyze(ndoc, ndocJSONStr);
}

void LMInterface::do_SA(N_Doc &ndoc) {
  for (size_t i = 0; i < ndoc.sentence.size(); i++)
    m_SA->analyze(ndoc.sentence[i]);
}

void LMInterface::close_SA() {
  m_SA->finalize();
  delete m_SA;
  m_SA = NULL;
}



//-------------------------------------------------
//
//	통합
//
//-------------------------------------------------

///< Doc 구조체로부터 NDoc 생성
void LMInterface::make_ndoc(Doc *doc, N_Doc &ndoc) {
  list<Sent *>::iterator lvSentItr;
  list<Word *>::iterator lvWordItr;
  list<Morpheme *>::iterator lvMorpItr;
  list<Ne *>::iterator lvNeItr;
  list<Co *>::iterator lvCoItr;
  list<Chunk *>::iterator lvCkItr;
  list<LF *>::iterator lflist;
  list<Arg *>::iterator ap;

  Morpheme *lvMorp;
  Sent *psent;

  // chunking - phrase type
  map<int, string> p_type;
  p_type[C_NP] = "NP";
  p_type[C_VP] = "VP";
  p_type[C_ADP] = "ADP";
  p_type[C_IP] = "IP";
  p_type[C_AP] = "AP";
  p_type[C_GNP] = "GNP";
  p_type[C_PNP] = "PNP";
  p_type[C_NPC] = "NPC";
  p_type[C_VPC] = "VPC";
  p_type[C_NVP] = "NVP";
  p_type[C_NONE] = "NONE";

  // ndoc clear
  ndoc.sentence.clear();
  ndoc.category = "";
  ndoc.category_weight = 0;

  // category
  if (!(doc->category.empty())) {
    ndoc.category = doc->category[0];
    ndoc.category_weight = doc->category_score[0];
  }

  int sent_id = 0, morp_id, word_id, chunk_id, ne_id;

  // sentence
  for (lvSentItr = doc->slist.begin(); lvSentItr != doc->slist.end();
       lvSentItr++) {
    psent = *lvSentItr;
    // 실제 morp, chunk, ne id 등에 반영 됨 (Doc 구조체 정보가 바뀜)
    morp_id = 0, word_id = 0, chunk_id = 0, ne_id = 0;

    N_Sentence nsent;
    // id
    nsent.id = sent_id;
    psent->sen_id = sent_id++;
    // txt
    nsent.text = psent->str;

    // morp
    for (lvWordItr = psent->wlist.begin(); lvWordItr != psent->wlist.end();
         lvWordItr++) {
      // 형태소 단위로 출력
      for (lvMorpItr = (*lvWordItr)->mlist.begin();
           lvMorpItr != (*lvWordItr)->mlist.end(); lvMorpItr++) {
        lvMorp = (*lvMorpItr);

        N_Morp nmorp;
        // id
        nmorp.id = morp_id;
        lvMorp->n_ne = morp_id++;
        // str
        nmorp.lemma = lvMorp->str;
        // type
        nmorp.type = (*ExTagMap)[lvMorp->pos].data();
        // position
        nmorp.position = lvMorp->doc_position;

        nsent.morp.push_back(nmorp);
      }
    }

    // word
    for (lvWordItr = psent->wlist.begin(); lvWordItr != psent->wlist.end();
         lvWordItr++) {
      N_Word nword;
      // id
      nword.id = word_id++;
      // str
      nword.text = (*lvWordItr)->str;
      // type
      nword.type = "";
      // begin
      int id = 0;
      if ((*lvWordItr)->mlist.begin() != (*lvWordItr)->mlist.end()) {
        lvMorpItr = (*lvWordItr)->mlist.begin();
        id = (*lvMorpItr)->n_ne;
      }
      nword.begin = id;
      // end
      nword.end = id + (int)(*lvWordItr)->mlist.size() - 1;

      nsent.word.push_back(nword);
    }

    // NER
    for (lvNeItr = psent->nelist.begin(); lvNeItr != psent->nelist.end();
         lvNeItr++) {
      N_NE nne;
      // id
      nne.id = ne_id;
      (*lvNeItr)->ne_id = ne_id++;
      // text
      nne.text = (*lvNeItr)->str;
      // type
      nne.type = (*ExAtMap)[(*lvNeItr)->at_type];
      // begin
      nne.begin = (*lvNeItr)->start->n_ne;
      // end
      nne.end = (*lvNeItr)->end->n_ne;
      // prob
      nne.weight = (*lvNeItr)->m_value;
      // common_noun
      nne.common_noun = (*lvNeItr)->common_noun;

      nsent.NE.push_back(nne);
    }

    // chunking
    for (lvCkItr = psent->e_cklist.begin(); lvCkItr != psent->e_cklist.end();
         lvCkItr++) {
      N_Chunk nchunk;
      // id
      nchunk.id = chunk_id;
      (*lvCkItr)->id = chunk_id++;
      // text
      for (lvMorp = (*lvCkItr)->start; lvMorp != NULL; lvMorp = lvMorp->next) {
        nchunk.text += lvMorp->str;
        if (lvMorp == (*lvCkItr)->end) {
          break;
        } else if (lvMorp->m_word != lvMorp->next->m_word) {
          nchunk.text += ' ';
        }
      }
      // type
      nchunk.type = p_type[(*lvCkItr)->p_type];
      // begin
      nchunk.begin = (*lvCkItr)->start->n_ne;
      // end
      nchunk.end = (*lvCkItr)->end->n_ne;

      nsent.chunk.push_back(nchunk);
    }

    // 어절 후처리: 종결 부호 (. ! ?) 등을 이전 어절에 붙인다. (지금은 종결부호가 따로 어절)
    vector<N_Word> new_word;
    for (int i = 0; i < (int) nsent.word.size(); i++) {
      N_Word &w = nsent.word[i];
      N_Word new_w = w;
      if (i + 1 < (int) nsent.word.size()) {
        N_Word &next_w = nsent.word[i + 1];
        // 종결 부호 이전 어절에 붙이기
        if ((next_w.text == "." || next_w.text == "!" || next_w.text == "?")) {
          new_w.text += next_w.text;
          new_w.end = next_w.end;
          i++;
        }
        new_w.id = (int)new_word.size();
        new_word.push_back(new_w);
      } else {
        new_w.id = (int)new_word.size();
        new_word.push_back(new_w);
      }
    }
    nsent.word = new_word;

    ndoc.sentence.push_back(nsent);
  }
}


/** Doc 구조 메모리 해제 
*/
void LMInterface::clear_Doc() {
  clear_Doc(&m_Doc);
}


/** Doc 구조 메모리 해제
*/
void LMInterface::clear_Doc(struct Doc *p) {
  list<Sent *>::iterator lvSentItr;
  list<Word *>::iterator lvWordItr;
  list<Morpheme *>::iterator lvMorpItr;
  list<Ne *>::iterator lvNeItr;

  // 문서 str
  if (p->d != NULL) {
    //free(p->d);
    delete[] p->d;
    p->d = NULL;
  }

  // 문서 분류기
  p->category.clear();
  p->category_score.clear();

  // chunk // pmryu
  list<Chunk *>::iterator lvCkItr;    // 추가 11.17 by B#
  list<CandChunk *>::iterator lvCCkItr;
  list<DepenChunk *>::iterator lvDCkItr;
  list<EtmChunk *>::iterator lvECkItr;

  list<LF *>::iterator lvLFItr;
  // end of chunk

  //Morpheme* m_ptr = NULL;
  //Word* w_ptr = NULL;

  // NE
  for (lvNeItr = p->nelist.begin();
       lvNeItr != p->nelist.end(); lvNeItr++) {
    delete[] (*lvNeItr)->str;
    delete (Ne *) (*lvNeItr);
  }

  for (lvSentItr = p->slist.begin();
       lvSentItr != p->slist.end(); lvSentItr++) {
    // chunk
    for (lvCkItr = (*lvSentItr)->cklist.begin();            // 추가 11.17 by B#
         lvCkItr != (*lvSentItr)->cklist.end(); lvCkItr++) {
      if ((*lvCkItr) == NULL) continue;
      if ((*lvCkItr)->headConcept != NULL) {
        //					 printf("headconcept[%s]\n", (*lvCkItr)->headConcept);
        free((*lvCkItr)->headConcept);
      }
      if ((*lvCkItr)->headLex != NULL) {
        //					 printf("headLex[%s]\n", (*lvCkItr)->headLex);
        free((*lvCkItr)->headLex);
      }
      if ((*lvCkItr)->headJosaEomi != NULL) {
        //					 printf("headjosaeomi[%s]\n", (*lvCkItr)->headJosaEomi);
        free((*lvCkItr)->headJosaEomi);
      }

      // 2004.12.02 에 추가된 etmHeadConcept과 etmHeadLex를 delete
      if ((*lvCkItr)->etmHeadConcept != NULL) {
        //					 printf("headconcept[%s]\n", (*lvCkItr)->headConcept);
        free((*lvCkItr)->etmHeadConcept);
      }
      if ((*lvCkItr)->etmHeadLex != NULL) {
        //					 printf("headLex[%s]\n", (*lvCkItr)->headLex);
        free((*lvCkItr)->etmHeadLex);
      }

      for (lvCCkItr = (*lvCkItr)->CandDChunkList.begin();
           lvCCkItr != (*lvCkItr)->CandDChunkList.end(); lvCCkItr++) {
        if ((*lvCCkItr)->cf == NULL) {
          delete (CandChunk *) (*lvCCkItr);
          continue;
        }
        /*
        for( cfep = (*lvCCkItr)->cf->cf.begin();
                cfep != (*lvCCkItr)->cf->cf.end(); cfep++)
        {
            for(cfargp = (*cfep)->arg.begin();
                    cfargp != (*cfep)->arg.end(); cfargp++)
            {
                delete (CFArg*)(*cfargp);
            }

            (*cfep)->arg.clear();
            delete (CFEntity*)(*cfep);
        }

        (*lvCCkItr)->cf->cf.clear();
        */
        //delete (CaseFrame*)((*lvCCkItr)->cf);
        delete (CandChunk *) (*lvCCkItr);
      }

      for (lvCCkItr = (*lvCkItr)->CandPDChunkList.begin();
           lvCCkItr != (*lvCkItr)->CandPDChunkList.end(); lvCCkItr++) {
        if ((*lvCCkItr)->cf == NULL) {
          delete (CandChunk *) (*lvCCkItr);
          continue;
        }

        /*
        for( cfep = (*lvCCkItr)->cf->cf.begin();
                cfep != (*lvCCkItr)->cf->cf.end(); cfep++)
        {
            for(cfargp = (*cfep)->arg.begin();
                    cfargp != (*cfep)->arg.end(); cfargp++)
            {
                delete (CFArg*)(*cfargp);
            }

            (*cfep)->arg.clear();
            delete (CFEntity*)(*cfep);
        }

        (*lvCCkItr)->cf->cf.clear();
        */

        // delete (CaseFrame*)((*lvCCkItr)->cf);

        delete (CandChunk *) (*lvCCkItr);
      }

      for (lvCCkItr = (*lvCkItr)->CandPDEtmChunkList.begin();
           lvCCkItr != (*lvCkItr)->CandPDEtmChunkList.end(); lvCCkItr++) {
        if ((*lvCCkItr)->cf == NULL) {
          delete (CandChunk *) (*lvCCkItr);
          continue;
        }

        /*
        for( cfep = (*lvCCkItr)->cf->cf.begin();
                cfep != (*lvCCkItr)->cf->cf.end(); cfep++)
        {
            for(cfargp = (*cfep)->arg.begin();
                    cfargp != (*cfep)->arg.end(); cfargp++)
            {
                delete (CFArg*)(*cfargp);
            }

            (*cfep)->arg.clear();
            delete (CFEntity*)(*cfep);
        }

        (*lvCCkItr)->cf->cf.clear();
        */
        //delete (CaseFrame*)((*lvCCkItr)->cf);

        delete (CandChunk *) (*lvCCkItr);
      }
      for (lvCCkItr = (*lvCkItr)->CandDEtmChunkList.begin();
           lvCCkItr != (*lvCkItr)->CandDEtmChunkList.end(); lvCCkItr++) {
        if ((*lvCCkItr)->cf == NULL) {
          delete (CandChunk *) (*lvCCkItr);
          continue;
        }

        /*
        for( cfep = (*lvCCkItr)->cf->cf.begin();
                cfep != (*lvCCkItr)->cf->cf.end(); cfep++)
        {
            for(cfargp = (*cfep)->arg.begin();
                    cfargp != (*cfep)->arg.end(); cfargp++)
            {
                delete (CFArg*)(*cfargp);
            }

            (*cfep)->arg.clear();
            delete (CFEntity*)(*cfep);
        }

        (*lvCCkItr)->cf->cf.clear();
        */

        //delete (CaseFrame*)((*lvCCkItr)->cf);

        delete (CandChunk *) (*lvCCkItr);
      }
      for (lvDCkItr = (*lvCkItr)->DChunkList.begin();
           lvDCkItr != (*lvCkItr)->DChunkList.end(); lvDCkItr++) {
        delete (DepenChunk *) (*lvDCkItr);
      }

      (*lvCkItr)->DChunkList.clear();

      for (lvDCkItr = (*lvCkItr)->PDChunkList.begin();
           lvDCkItr != (*lvCkItr)->PDChunkList.end(); lvDCkItr++) {
        delete (DepenChunk *) (*lvDCkItr);
      }
      (*lvCkItr)->PDChunkList.clear();

/*            EtmPDChunkList에서 해제해야 하는 headConcept, HeadLex, lvECKltr의 경우
              루틴을 타지 않기 때문에 다른 곳에서 이미 해제하는 것 같음.
			  해제한 것을 다시 해제하여 죽기 때문에 코멘트 처리 함
			  by mefong 2008.09.26
			 for( lvECkItr = (*lvCkItr)->EtmPDChunkList.begin();
					 lvECkItr != (*lvCkItr)->EtmPDChunkList.end(); lvECkItr++)
			 {
				 if( (*lvECkItr) == NULL) 
				 {
					 delete (EtmChunk*)(*lvECkItr);

					 continue;
				 }

				 if( (*lvECkItr)->case_rel < 0 || (*lvECkItr)->case_rel > SUB_CLA) continue;

				 // 잘못된 값이 있을 경우 체크하여 넘어감  by mefong 2008.09.26
				 if( (*lvECkItr)->chk->p_type < C_NP || (*lvECkItr)->chk->p_type > C_NONE) continue;

				 if( (*lvECkItr)->headConcept  != NULL) free((*lvECkItr)->headConcept);
				 if( (*lvECkItr)->headLex      != NULL) free((*lvECkItr)->headLex);

				 delete (EtmChunk*)(*lvECkItr);

			 }
			 */
      (*lvCkItr)->EtmPDChunkList.clear();

      if ((*lvCkItr) != NULL) delete (Chunk *) (*lvCkItr);
    }

    (*lvSentItr)->cklist.clear(); // 추가 11.17 by B#
    (*lvSentItr)->e_cklist.clear(); // 추가 11.17 by B#
    // end of chunk

    for (lvWordItr = (*lvSentItr)->wlist.begin();
         lvWordItr != (*lvSentItr)->wlist.end(); lvWordItr++) {
      //w_ptr = (*lvWordItr);

      for (lvMorpItr = (*lvWordItr)->mlist.begin();
           lvMorpItr != (*lvWordItr)->mlist.end(); lvMorpItr++) {
        //m_ptr = (*lvMorpItr);
        delete[] (*lvMorpItr)->str;
        delete (Morpheme *) (*lvMorpItr);
      }

      (*lvWordItr)->mlist.clear();
      delete[] (*lvWordItr)->str;
      delete (Word *) (*lvWordItr);
    }

    (*lvSentItr)->wlist.clear();
    (*lvSentItr)->nelist.clear(); // 추가 11.17 by B#

    // relation
    (*lvSentItr)->relation.clear();
    //(*lvSentItr)->parse_rel.clear();
    // end of relation

    delete[] (*lvSentItr)->str; // 추가 11.17 by B#

    // chunk
    //if((*lvSentItr)->lf != NULL) DelLFTree((*lvSentItr)->lf);
    if (m_CHUNK != NULL)
      m_CHUNK->chk_gb_clear();
    // end of chunk

    delete (Sent *) (*lvSentItr);
  }

  p->slist.clear();
  p->nelist.clear();
}

int LMInterface::getWordID(int morpid, vector<N_Word> &vecWord) {
  for (size_t i = 0; i < vecWord.size(); i++) {
    if (morpid >= vecWord[i].begin && morpid <= vecWord[i].end) {
      return vecWord[i].id;
    }
  }

  return -1;
}
