#ifndef _CUSTOM_DICT_H
#define _CUSTOM_DICT_H

#include <maum/brain/nlp/nlp.pb.h>

using maum::brain::nlp::NlpDictType;
using maum::brain::nlp::NlpDict;
using std::string;

class NlpKorean1Impl;

/**
 * 사용자가 정의하는 사전을 반영하는 기능을 수행한다.
 *
 * 사전은 TUTOR UI로부터 다운로드 받은 다음에
 * 필요한 경우 적절한 변환 절차를 거치고
 * 원래 있던 파일을 바꿔치기한다.
 *
 * 단순히 심볼릭 링크를 걸어주면 해결되므로 원래 있는 리소스 파일은 `.org`를 붙여서
 * 유지하고 새로운 파일은 다운로드 받은 파일로 심복릭 링크를 건다.
 *
 * 새로운 사전을 적용한 이후에는 NLP 서비스를 재시작하도록 한다.
 */
class CustomDict {
 public:
  /**
   * 생성자
   *
   * @param dict 적용할 protobuf message
   * @param impl 적용한 이후에 재시작하기 위한 NLP 서비스 포인터
   */
  CustomDict(NlpDict dict, NlpKorean1Impl *impl):
      dict_(dict),
      impl_(impl) {
  }
  /**
   * 소멸자
   */
  virtual ~CustomDict() {
  }

  int Apply();
  int Download();
  int Callback();

 private:
  NlpDict dict_;  /**< protobuf message object to handle apply method. */
  NlpKorean1Impl *impl_; /**< grpc service impl object */
  string download_file_; /**< downloaded file */

  int GenerateCustomDict();
  int ApplyNamedEntityDict();
  int ApplySynonym();
};

#endif // _CUSTOM_DICT_H