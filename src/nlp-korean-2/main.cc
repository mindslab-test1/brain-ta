#include <grpc++/grpc++.h>
#include "nlp-service-kor2-impl.h"
#include <getopt.h>
#include <cinttypes>
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>
#include <gitversion/version.h>

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

using namespace std;

void RunServer(const char *prog) {
  libmaum::Config &c = libmaum::Config::Instance();
  std::string server_address("0.0.0.0:");
  server_address += c.Get("brain-ta.nlp.2.kor.port");
  int grpc_timeout = atoi(c.Get("brain-ta.nlp.2.kor.timeout").c_str());
  c.DumpPid();

  NlpKorean2Impl service;

  ServerBuilder builder;
  builder.AddChannelArgument(GRPC_ARG_MAX_CONNECTION_IDLE_MS, grpc_timeout);
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());

  builder.RegisterService(&service);

  unique_ptr<Server> server(builder.BuildAndStart());

  if (server) {
    LOGGER()->info("{} {} server listening on {} with {}",
                   prog, version::VERSION_STRING,
                   server_address,
                   service.Name());
    server->Wait();
  }
}

// gitversion을 확인하기 위한 함수
void Help(const char *prog) {
  cout << prog << " [--version] [--help]" << endl;
}

// gitversion을 확인하기 위한 함수
void ProcessOption(int argc, char *argv[]) {
  bool do_exit = false;
  int c;
  char * prog = basename(argv[0]);
  while (1) {
    static const struct option long_options[] = {
      {"version", no_argument, 0, 'v'},
      {"help", no_argument, 0, 'h'},
      {NULL, no_argument, NULL, 0}
    };

    int option_index = 0;

    c = getopt_long(argc, argv, "vh", long_options, &option_index);

    if (c == -1) break;

    switch (c) {
      case 0:
        break;
      case 'v':
        cout << prog << " version " << version::VERSION_STRING << endl;
        do_exit = true;
        break;
      case 'h':
        Help(prog);
        do_exit = true;
        break;
      default:
        Help(prog);
        do_exit = true;
        break;
    }
  }
  if (do_exit) exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {
  ProcessOption(argc, argv);
  libmaum::Config::Init(argc, argv, "brain-ta.conf");



  EnableCoreDump();
  RunServer(basename(argv[0]));
  return 0;
}
