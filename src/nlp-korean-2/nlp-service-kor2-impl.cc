#include "nlp-service-kor2-impl.h"
#include <libmaum/common/config.h>

using std::unique_ptr;
using maum::brain::nlp::NlpFeature;
using maum::brain::nlp::NlpAnalysisLevel;

const int feature = (1 << NlpFeature::NLPF_MORPHEME) |
    (1 << NlpFeature::NLPF_NAMED_ENTITY) |
    (1 << NlpFeature::NLPF_PARSER);

/**
  @breif 서버 실행시 초기화 하는 함수
  @param None
  @return None
*/
NlpKorean2Impl::NlpKorean2Impl() {
  LoadConfig();

  // 0x4c
  // 0100 1100
  // 각 자리가 level 의 on / off를 의미합니다.
  // on = 1, off = 0
  nlp2Kor_ = new Nlp2Kor(rsc_path_, feature);
  nlp2Kor_->UpdateFeatures();
}

/**
  @breif nlp-eng2 실행에 필요한 리소스 경로 설정
  @param None
  @return None
*/
void NlpKorean2Impl::LoadConfig() {
  auto &c = libmaum::Config::Instance();
  rsc_path_ = c.Get("brain-ta.resource.2.kor.path");
}

NlpKorean2Impl::~NlpKorean2Impl() {
  nlp2Kor_->Uninit();
}

/**
  @breif 사용자에게 입력받은 분석 레벨에 따라서 해당 모듈 초기화 및 분석하는 함수
  @param InputText *text : 클라이언트에서 보낸 message
  @param Document *document : 언어분석 결과를 클라이언트에 보낼 message
  @return None
*/
void NlpKorean2Impl::AnalyzeOne(const InputText *text, Document *document) {
  nlp2Kor_->AnalyzeOne(text, document);
}

/**
  @breif 클라이언트에서 언어분석을 위해 호출하는 함수
  @param ServerContext *context : grpc 통신을 수행하기 위해 필요한 instance
  @param InputText *text : 클라이언트에서 보낸 message
  @param Document *document : 언어분석 결과를 클라이언트에 보낼 message
  @return grpc Status Code
*/
Status NlpKorean2Impl::Analyze(ServerContext *context,
                               const InputText *text,
                               Document *document) {
  AnalyzeOne(text, document);
  return Status::OK;
}

Status NlpKorean2Impl::AnalyzeMultiple(
    ServerContext *context,
    ServerReaderWriter<Document, InputText> *stream) {
  InputText text;
  while (stream->Read(&text)) {
    Document out;
    AnalyzeOne(&text, &out);
    stream->Write(out);
  }
  return Status::OK;
}

/**
  @breif 엔진 공급자에 대한 정보를 제공하는 함수
  @param ServerContext *context : grpc 통신을 수행하기 위해 필요한 instance
  @param Empty *empty : google protobuf에서 제공하는 generic empty message
  @param NlpProvider *provider : 엔진 공급 정보를 클라이언트에 보낼 message
  @return grpc Status Code
*/
Status NlpKorean2Impl::GetProvider(ServerContext *context,
                                   const Empty *empty,
                                   NlpProvider *provider) {
  provider->set_name("MindsLAB NLP #2");
  provider->set_vendor("Kangwon National University");
  provider->set_version("1.0");
  provider->set_description("MindsLAB NLP #1 Korean from KNU");
  provider->set_support_encoding("EUC-KR");
  return Status::OK;
}

Status NlpKorean2Impl::HasSupport(ServerContext *context,
                                  const ::maum::brain::nlp::NlpFeatures *request,
                                  NlpFeatureSupportList *response) {
  for (auto f : request->features()) {
    switch (f) {
      case NlpFeature::NLPF_MORPHEME: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(true);
        break;
      }
      case NlpFeature::NLPF_WORD: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(true);
        break;
      }
      case NlpFeature::NLPF_NAMED_ENTITY: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(true);
        break;
      }
      case NlpFeature::NLPF_PARSER: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(true);
        break;
      }
      case NlpFeature::NLPF_SETIMENT: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(false);
        break;
      }
      case NlpFeature::NLPF_DNN: {
        auto supp = response->add_supports();
        supp->set_feature(NlpFeature(f));
        supp->set_support(true);
        break;
      }
      default: {
        break;
      }
    }
  }
  return Status::OK;
}

Status NlpKorean2Impl::GetPosTaggedList(ServerContext *context,
                                        const Document *document,
                                        TaggedList *taggedList) {

  vector<string> tagged_lists;
  nlp2Kor_->GetPosTaggedStr(document, &tagged_lists);
  for (auto &tagged_str : tagged_lists) {
    taggedList->add_result(tagged_str);
  }

  return Status::OK;
}

Status NlpKorean2Impl::GetNerTaggedList(ServerContext *context,
                                        const Document *document,
                                        TaggedList *taggedList) {

  vector<string> tagged_lists;
  nlp2Kor_->GetNerTaggedStr(document, &tagged_lists);
  for (auto &tagged_str : tagged_lists) {
    taggedList->add_result(tagged_str);
  }

  return Status::OK;
}
